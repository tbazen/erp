import { DocumentRequest, DocumentResponse } from './documentModel'
import { IUserInformation } from './userInformationModel'

export interface MessageRequest {
  ThreadId: string
  TextMessage: string
  Document: DocumentRequest | null
  MessageBy: string
}

export interface MessageResponse {
  id: string
  textMessage: string
  document: DocumentResponse | null
  date: Date | string
  messageBy: IUserInformation
}
