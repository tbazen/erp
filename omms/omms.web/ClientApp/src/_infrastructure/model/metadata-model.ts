export interface MetaData
{
    pendingItems: number
    rejectedItems: number
    newIndividualTasks: number
    newTeamTasks: number
}