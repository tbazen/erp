import { TaskStatus } from './taskStatusModel'
import { TaskModel } from './taskFromModel'

export interface NotificationModel {
  task: TaskModel
  daysBeforeStart: number | null
  daysBeforeEnd: number | null
  notificationMessage: string
  notificationEmail: string
  onStatus: TaskStatus | null
}
