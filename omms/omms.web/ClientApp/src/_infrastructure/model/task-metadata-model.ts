export interface TaskMetaDataModel
{
    periodicTask: MDModel;
    incidentTask: MDModel;
    scheduledTask: MDModel;
    unscheduledTask: MDModel;
}

export interface MDModel
{
    waitingForCancellation: number;
    waitingForCompletion: number;
    waitingForTermination: number;

    completed: number;
    cancelled: number;
    terminated: number;
}