
import { IUserInformation } from './userInformationModel';
import { TaskType, MaintenanceType, PriorityType, Assignee, IncidentInformation } from './taskFromModel';
import { IncidentResponse } from './task-response';
import { DocumentResponse, DocumentRequest } from './documentModel';

export interface TaskRequest
{
    id: string | null
    wfid: string | null
    assetId: number | null
    
    name: string
    description: string
    createdBy: string | null
    startDateExpected: Date | string | null
    endDateExpected: Date | string | null

    startDateActual: Date | string | null
    endDateActual: Date | string | null

    taskType: TaskType
    //for periodic tasks only 
    intervalDays: number | null
    //for incident tasks only
    incidentInfo: IncidentInformation

    maintenanceType: MaintenanceType
    priority: PriorityType
    taskAssignee: Assignee
    warrantyDocument: DocumentRequest
    dependUpon: string[]    
    note: string
}

export interface TaskResponse
{
    id: string | null
    wfid: string | null
    assetId: number | null
    
    name: string
    description: string
    createdBy: IUserInformation

    startDateExpected: Date | string | null
    endDateExpected: Date | string | null

    startDateActual: Date | string | null
    endDateActual: Date | string | null

    taskType: TaskType
    //for periodic tasks only 
    intervalDays: number | null
    //for incident tasks only
    incidentInfo: IncidentResponse
    maintenanceType: MaintenanceType
    priority: PriorityType
    taskAssignee: Assignee
    warrantyDocument: DocumentResponse
    dependUpon: string[]
    state: string
    note: string
}


export const enum TaskStatus {
    ONGOING = 3,
    CANCELLED = 11,
    TERMINATED = 12,
    COMPLETED = 13
}