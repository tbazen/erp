import { DocumentRequest, DocumentResponse } from './documentModel';
import { IUserInformation } from './userInformationModel';


export interface NoteRequest
{
    id: number | null
    taskId: string
    header: string
    note: string
    date: Date | string | null
    noteBy: string | null
    
    images: DocumentRequest[]
}

export interface NoteResponse
{
    id: number
    taskId: string
    header: string
    note: string
    date: Date | string
    noteBy: IUserInformation

    images: DocumentResponse[];
}