

export interface TaskReportViewModel
{
    reports: TaskReportModel[];

    periodicTasks: TRMetadata;
    incidentTasks: TRMetadata;
    scheduledTasks: TRMetadata;
    unscheduledTasks: TRMetadata;
}

export interface TRMetadata
{
    ongoing: number;
    completed: number;
    terminated: number;
    cancelled: number;
}

export interface TaskReportModel
{
    id: string
    name: string
    type: string
    status: string
    assigneeType: string
    assigneeName: string
    startDate: Date | string | null
    endDate: Date | string | null
    delayDays: number | null
}