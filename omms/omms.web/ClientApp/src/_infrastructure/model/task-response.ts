import { DocumentResponse } from './documentModel';
import { TaskStatusModel } from './taskStatusModel';
import { Equipment, Budget, Assignee, PriorityType, MaintenanceType, TaskType } from './taskFromModel';
export interface TaskResponse
{
    id: string | null
    wfid: string | null
    name: string
    description: string

    createdBy: string

    startDateExpected: Date | string | null
    endDateExpected: Date | string | null

    startDateActual: Date | string | null
    endDateActual: Date | string | null

    taskType: TaskType
    intervalDays: number | null;
    incidentInfo: IncidentResponse;
    maintenanceType: MaintenanceType;
    priority: PriorityType;
    taskAssignee: Assignee;

    budget: Budget;
    equipment: Equipment[];
    warrantyDocument: DocumentResponse;
    taskStatus: TaskStatusModel;
    dependUpon: string[];
}


export interface IncidentResponse
{
    geoLocation: string;
    incident: string;
    description: string;
    reportedBy: string;
    images: DocumentResponse[];
}
