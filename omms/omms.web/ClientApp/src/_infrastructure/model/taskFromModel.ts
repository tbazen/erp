﻿import { TaskStatusModel } from './taskStatusModel'
import { DocumentRequest } from './documentModel'
export enum AssigneeType {
  None = 0,
  Employee = 1,
  Team = 2
}
export enum MaintenanceType {
  Preventive = 1,
  Corrective = 2
}
export enum TaskType {
  Scheduled = 1,
  Unscheduled = 2,
  Periodic = 3,
  Incident = 4
}

export enum PriorityType {
  A = 1,
  B = 2,
  C = 3,
  D = 4,
  E = 5
}

export interface TaskModel {
  id: string | null
  wfid: string | null
  name: string
  description: string

  createdBy: string

  startDateExpected: Date | string | null
  endDateExpected: Date | string | null

  startDateActual: Date | string | null
  endDateActual: Date | string | null

  taskType: TaskType
  //for periodic tasks only
  intervalDays: number | null
  //for incident tasks only
  incidentInfo: IncidentInformation | null

  maintenanceType: MaintenanceType
  priority: PriorityType
  taskAssignee: Assignee | null

  budget: Budget
  equipment: Equipment[]
  warrantyDocument: DocumentRequest
  taskStatus: TaskStatusModel | null
  dependUpon: string[]
}

export interface Budget {
  EstimatedCost: number
  ActualCost: number
}

export interface Equipment {
  Id: number
  Quantity: number
  AllocatedDate: Date | string
  CostId: number
}
export interface Cost {
  Expense: number
}

export interface Assignee {
  assigneeType: AssigneeType
  assigneeModel: AssigneeModel
}

export interface AssigneeModel {
  taskId: string
  assigneeId: string
  dateAssigned: Date | string | null
}

export interface AssigneeViewModel{
  assignee: Assignee;
  note: string;
  wfid: string;
}


export interface IncidentInformation {
  GeoLocation: string
  Incident: string
  Description: string
  ReportedBy: string
  Images: DocumentRequest[]
}
