export enum TaskStatus {
  Pending = 1,
  Deligated = 2,
  RequestApproval = 3,
  Approved = 4,
  Rejected = 5
}

export interface TaskStatusModel {
  //for updating status [ chainging the actual end date ]
  Id: number | null
  TaskId: string
  EmployeeId: string
  Status: TaskStatus
  StartDate: Date | string
  ExpectedEndDate: Date | string
  ActualEndDate: Date | string | null
  Description: string
  Progress: number
}
