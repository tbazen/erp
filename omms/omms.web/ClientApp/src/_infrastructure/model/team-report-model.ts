export interface TeamReportModel
{
    id: string
    name: string
    dateCreated: Date | string
    noOfMembers: number
    onGoingTasks: number
    completedTasks: number
    cancelledTasks: number
    terminatedTasks: number
    totalTasks: number
}