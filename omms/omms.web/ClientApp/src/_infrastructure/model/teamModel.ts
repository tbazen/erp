import { IUserInformation } from './userInformationModel';
export interface TeamModel {
  id: string | null
  wfid: string | null
  name: string
  description: string
  members: IUserInformation[]
  tasks: string[]
  note: string;
}