import { TaskModel } from './taskFromModel'
import { TeamModel } from './teamModel'

export enum ThreadState {
  Open = 1,
  Lock = 2
}

export interface ThreadModel {
  id: string
  name: string
  description: string
  state: ThreadState
  team: TeamModel
  task: TaskModel
}
