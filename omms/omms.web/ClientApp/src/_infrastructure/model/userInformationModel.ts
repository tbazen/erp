export interface IUserInformation {
  id: string | null
  userName: string
  role: string
}
