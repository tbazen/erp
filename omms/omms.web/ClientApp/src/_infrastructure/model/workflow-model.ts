export enum WorkflowTypes {
  Team = 1,
  Asset = 2,
  Task = 3
}

export interface WorkFlowModel {
  id: string
  currentState: number
  description: string
  type: WorkflowTypes
  workItem: WorkItemModel
}
export interface WorkItemModel {
  id: string | null
  wFId: string
  fromState: number
  toState: number
  trigger: number
  data: string
  seqNo: number
}

export interface WFRequestModel{
    wfid: string
    note: string
}