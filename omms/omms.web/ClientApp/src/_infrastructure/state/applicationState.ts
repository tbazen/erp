import { TeamState } from './teamState'

export interface ApplicationState {
  team: TeamState
}
