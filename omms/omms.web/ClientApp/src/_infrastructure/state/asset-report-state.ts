import { AssetReportModel } from '../model/asset-report-model'

export interface AssetReportState{
    report : AssetReportModel[]
    filteredReport : AssetReportModel[]
    loading : boolean
    error : boolean
    errorMessage : string | undefined
    filtering : boolean
}