import { AssetReading } from '../model/assetModel'
export interface AssetReadingState {
  readings: AssetReading[]
}
