import { AssetResponse } from '../model/assetModel'
export interface AssetState {
  assets: AssetResponse[]
  loading: boolean
  error: boolean
  errorMessage : string | undefined
  filtering:boolean
}
