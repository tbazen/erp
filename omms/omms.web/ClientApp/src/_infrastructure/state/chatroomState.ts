import { MessageResponse } from '../model/messageModel'
export interface ChatroomState {
  threadId: string
  memberId: string
  messages: MessageResponse[]
}
