import { EmployeeReportModel } from "../model/employee-report-model";


export interface EmployeeReportState{
    report : EmployeeReportModel[]
    filteredReport : EmployeeReportModel[]
    loading : boolean
    error : boolean
    errorMessage : string | undefined
    filtering : boolean
}


