import { IUserInformation } from '../model/userInformationModel';
export interface EmployeeState {
    employees: IUserInformation[]
    filtering : boolean
    loading : boolean
    error:boolean
    errorMessage : string | undefined
}