import { TeamModel } from '../model/teamModel';
import { AssetResponse } from '../model/assetModel';
import { TaskResponse } from '../model/task-response';
import { IUserInformation } from '../model/userInformationModel';


export interface FilterState{
    filteredTeam:TeamModel[],
    filteredAsset: AssetResponse[],
    filteredTask:TaskResponse[],
    filteredEmployee: IUserInformation[]
}