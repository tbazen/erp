import { NoteResponse } from '../model/task-note-model';
export interface NoteState {
    notes: NoteResponse[]
    loading: boolean
    error : boolean
    errorMessage : string | undefined
}