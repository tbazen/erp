import { NotificationModel } from '../model/notificationModel'

export interface NotificationState {
  notifications: NotificationModel[]
}
