import { TaskReportModel, TaskReportViewModel } from '../model/task-report-model';

export interface TaskReportState{
    report : TaskReportViewModel
    filteredReport : TaskReportModel[]
    loading : boolean
    error : boolean
    errorMessage : string | undefined
    filtering : boolean
}


