import { TeamReportModel } from '../model/team-report-model';


export interface TeamReportState{
    report : TeamReportModel[]
    filteredReport : TeamReportModel[]
    loading : boolean
    error : boolean
    errorMessage : string | undefined
    filtering : boolean
}


