import { TeamModel } from '../model/teamModel'

export interface TeamState {
  teams: TeamModel[]
  loading: boolean
  error: boolean
  errorMessage : string | undefined
  filtering : boolean
}
