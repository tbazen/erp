import { WorkflowTypes } from '../model/workflow-model';
import { WFNoteActions } from '../../_setup/actions/wfnote-modal-actions';
export interface WFModalState{
    open : boolean
    note : string
    submit : boolean
    modalAction : WFNoteActions
    wfid : string | undefined
    WFType : WorkflowTypes | undefined
}