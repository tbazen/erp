import { WorkFlowModel } from '../model/workflow-model'
export interface WorkflowState {
  workflowItems: WorkFlowModel[]
}
