import { ActionCreator, Action } from 'redux';
import { ThunkAction } from "redux-thunk";
import { AssetReportModel } from '../../_infrastructure/model/asset-report-model';
import { AssetReportActions } from "./bactionTypes";
import AssetService from '../services/asset.service';

let asset_service = new AssetService()

export enum AssetReportFilter{
    PIPELINE= 1,
    VALVE= 2,
    BOREHOLE= 3,
    ENDPOINTS= 4,
    RESERVIOR= 5,
    HYDRANTS= 6,
    REDUCE= 7,
    HOUSE_CONNECTIONS= 8,
    WATER_METER = 9,

    WithOngoingTask = 10,
    WithCompletedTask = 11,
    WithCancelledTasks = 12,
    WithTerminatedTasks = 13
}


function loadAssetReport(){
    return {type:AssetReportActions.LOAD_ASSET_REPORT}
}

function getAssetReportSuccess(report:AssetReportModel[]){
    return {type : AssetReportActions.LOAD_ASSET_REPORT_SUCCESS, report}
}

function getAssetReportError(errorMessage:string){
    return {type:AssetReportActions.LOAD_ASSET_REPORT_ERROR,errorMessage}
}

export function filterAssetReport(filterParams:AssetReportFilter[]){
    return {type:AssetReportActions.FILTER_ASSET_REPORT , filterParams}
}

export function cancelAssetFilter(){
    return {type:AssetReportActions.CANCE_FILTER_ASSET_REPORT}
}


export const getAssetReports: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = () => {
    return async (dispatch: any): Promise<Action> => {
      dispatch(loadAssetReport())  
      return await asset_service
        .getAssetReport()
        .then((res: any) => {
          return dispatch(getAssetReportSuccess(res.data))
        })
        //@ts-ignore
        .catch((error: any) => dispatch(getAssetReportError('Failed to load asset report')))
    }
}