import { ActionCreator } from 'redux'
import { Action } from 'history'
import axios from 'axios'
import { ThunkAction } from 'redux-thunk'
import * as ActionTypes from './bactionTypes'
import { AssetModel, AssetRequest, AssetResponse, AssetReadingSchema, AssetType } from '../../_infrastructure/model/assetModel';
import AssetService from '../services/asset.service'
import { loading, success, error } from './loading-modal-actions'
import { decreasePendingItems, increasePendingItem, decreaseRejectedItems } from './metadata-actions';
import UIContainer from './../../modules/shared/_helper/ui-helper/containers-util';
import { AssetSearhParams } from '../../_infrastructure/model/asset-search-params';
import { WFRequestModel } from '../../_infrastructure/model/workflow-model';


let asset_service = new AssetService()

export function loadAssetsSucess(assets:AssetResponse[]){
  return {type:ActionTypes.AssetActions.LOAD_ASSETS_SUCCESS,assets}
}

export function loadAssetSuccess(asset:AssetResponse[]){
  return {type:ActionTypes.AssetActions.LOAD_ASSET_SUCCESS,asset}
}

export function addNewAssetSuccess(asset:AssetModel){
  return {type:ActionTypes.AssetActions.ADD_NEW_ASSET_SUCCESS,asset}
}

export function updateAssetSuccess(asset:AssetModel){
  return {type:ActionTypes.AssetActions.UPDATE_ASSET_SUCCESS,asset}
}

export function deleteAssetSuccess(assetId:number){
  return {type:ActionTypes.AssetActions.DELETE_ASSET_SUCCESS,assetId}
}

function loadingAssets(){
  return {type:ActionTypes.AssetActions.LOADING}
}

function errorAssets(){
  return {type:ActionTypes.AssetActions.ERROR}
}

export function filteringAssets(){
  return {type:ActionTypes.AssetActions.FILTERING}
}
export function cancelAssetFiltering(){
  return {type:ActionTypes.AssetActions.CANCEL_FILTERING}
}

export function removeAssetByWfid(wfid:string){
  return {type:ActionTypes.AssetActions.REMOVE_ASSET_BY_WFID,wfid}
}

export const loadAssets: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = () => {
  return async (dispatch:any):Promise<Action> => {
    dispatch(loadingAssets())
    return asset_service.loadAssets()
    .then(response=>{
       return dispatch(loadAssetsSucess(response.data))        
    })
    .catch(error=>
      {
        if(!axios.isCancel(error)){
          dispatch(errorAssets())
        } 
      })
  }
}


export const createAsset : ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (asset:AssetRequest , container : UIContainer = UIContainer.REGISTERED_CONTAINER) => {
  return async (dispatch:any):Promise<Action> =>{
    dispatch(loading('Creating asset registration requiest, please wait...'))
    return asset_service.requestRegstration(asset)
    //@ts-ignore        
    .then(response=>{
          dispatch(success('Asset registration request sent sussefully!,you can find the asset on the pendign section.'))
          if(container != UIContainer.PENDING_CONTAINER){
            dispatch(increasePendingItem(1))
            
            container == UIContainer.REJECTED_CONTAINER &&
            asset.wfid != null &&
            dispatch(removeAssetByWfid(asset.wfid)) &&
            dispatch(decreaseRejectedItems(1))
            
          }
    })
    .catch(err=>dispatch(error(err.message)))
  }
}


export const deleteAsset: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (assetId:number) => {
  return async (dispatch:any):Promise<Action> => {
    return asset_service.deleteAsset(assetId)
    .then(response=>{
      console.log(response)
      if(response.status == 200)
       return dispatch(deleteAssetSuccess(assetId))        
    })
    .catch(error=>console.log(error))
  }
}

export function getAssetFormSchema(){
  return asset_service.getAssetFormSchema()
  .then(res=>{
    return res.data
  })
  .catch(error => console.log(error))
}

export const loadPendingAssets: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = () => {
  return async (dispatch:any):Promise<Action> => {
    dispatch(loadingAssets())
    return asset_service.getPendingAssets()
    .then(response=>{
       return dispatch(loadAssetsSucess(response.data))        
    })
    //@ts-ignore
    .catch(error=>{
      if(!axios.isCancel(error)){
        dispatch(errorAssets())
      }
    })
  }
}


export function getAsset(assetId : number): Promise<AssetResponse>{
  return new Promise<AssetResponse>((resolve,reject)=>{
    asset_service.getAsset(assetId)
    .then(res=>{
      resolve(res.data)
    })
    .catch(error => reject(error))
  })
}

export const approveAssetRegistration : ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (wfid:string) => {
  return async (dispatch:any):Promise<Action> => {
    dispatch(loading('Approving asset registration, please wait...'))
    return asset_service.approveAssetRegistration(wfid)
    //@ts-ignore
    .then(response=>{
      dispatch(success('Asset approved!'))
      dispatch(decreasePendingItems(1))
      dispatch(removeAssetByWfid(wfid))
    })
    .catch(err=>dispatch(error(err.message)))
  }
}

export const rejectAssetRegistration : ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (request:WFRequestModel) => {
  return async (dispatch:any):Promise<Action> => {
    dispatch(loading('Rejecting asset registration, please wait...'))
    return asset_service.rejectAssetRegistration(request)
    //@ts-ignore
    .then(response=>{
       dispatch(success('Asset registration rejected'))
       dispatch(decreasePendingItems(1))       
       dispatch(removeAssetByWfid(request.wfid))
    })
    .catch(err=>dispatch(error(err.message)))
  }
}


export const loadRejectedAssets: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = () => {
  return async (dispatch:any):Promise<Action> => {
    dispatch(loadingAssets())
    return asset_service.getRejectedAssets()
    .then(response=>{
       return dispatch(loadAssetsSucess(response.data))        
    })
    //@ts-ignore
    .catch(error=> {
      if(!axios.isCancel(error)){
        dispatch(errorAssets())
      }
    })
  }
}

export const getAssetByFid: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (fid:string) => {
  return async (dispatch:any):Promise<Action> => {
    return asset_service.getAssetByFid(fid)
    .then(response=>{
       let asset : AssetResponse = response.data
       let assets: AssetResponse[] = [asset]
       return dispatch(loadAssetSuccess(assets))
    })
    .catch(error=>console.log(error))
  }
}


export function getAssetReadingFormSchema(assetType:number){
  return asset_service.getAssetReadingFormSchema(assetType)
  .then(res=>{
    return res.data
  })
  .catch(error => console.log(error))
}



export function getAssetGisInfoByPUKID(searhParams:AssetSearhParams): Promise<AssetType>{
  return new Promise<AssetType>((resolve,reject)=>{
    asset_service.getAssetInfoByPUKID(searhParams)
    .then(res=>{
      resolve(res.data)
    })
    .catch(error => reject(error))
  })
}
