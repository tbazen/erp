import { ActionCreator } from 'redux'
import { Action } from 'history'
import { ThunkAction } from 'redux-thunk'
import * as ActionTypes from './bactionTypes'
import { AssetReading } from '../../_infrastructure/model/assetModel'
import AssetService from '../services/asset.service'
import { loading, success, error } from './loading-modal-actions';

let asset_service = new AssetService()

function loadAssetsReadingsSucess(readings: AssetReading[]) {
  return {
    type: ActionTypes.AssetReadingActions.LOAD_ALL_ASSET_READINGS_SUSSESS,
    readings
  }
}

function addAssetReadingSuccess(reading: AssetReading) {
  return {
    type: ActionTypes.AssetReadingActions.LOAD_ALL_ASSET_READINGS_SUSSESS,
    reading
  }
}

export const loadAssetReadings: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (assetId: number) => {
  return async (dispatch: any): Promise<Action> => {
    return asset_service.loadAssetReading(assetId)
      .then(response => {
        return dispatch(loadAssetsReadingsSucess(response.data))
      })
      .catch(error => console.log(error))
  }
}


export const addAssetReading: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (reading: AssetReading) => {
  return async (dispatch: any): Promise<Action> => {
    dispatch(loading('Adding asset reading,please wait...'))
   //@ts-ignore
    return asset_service
      .addAssetReading(reading)
      //@ts-ignore
      .then(response => {
        dispatch(success('Asset Reading added!'))
        reading.id = response.data
        return dispatch(addAssetReadingSuccess(response.data))
      })
      .catch(err => {
        dispatch(error(err.message))
      })
  }
}
