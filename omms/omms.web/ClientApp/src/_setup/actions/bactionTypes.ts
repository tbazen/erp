export const enum EmployeeActions {
  LOAD_EMPLOYEE_THREADS_SUCCESS = 'LOAD EMPLOYEE THREADS SUCCESS',
  LOAD_MESSAGES_SUCCESS = 'LOAD MESSAGES SUCCESS',
  SEND_MESSAGE_SUCCESS = 'SEND MESSAGE SUCCESS',
  SETUP_CHATROOM = 'SETUP CHATROOM'
}

export const enum AssetActions {
  LOAD_ASSETS_SUCCESS = 'LOAD ASSETS SUCCES',
  LOAD_ASSET_SUCCESS = 'LOAD ASSET SUCCES',
  ADD_NEW_ASSET_SUCCESS = 'ADD NEW ASSET SUCCESS',
  UPDATE_ASSET_SUCCESS = 'UPDATE ASSET SUCCESS',
  DELETE_ASSET_SUCCESS = 'DELETE ASSET SUCCESS',
  LOADING = 'ASSET LOADING',
  ERROR = 'ASSET ERROR',
  FILTERING = 'ASSET FILTERIGN',
  CANCEL_FILTERING ='ASSET CANCEL FILTERING',
  REMOVE_ASSET_BY_WFID = 'REMOVE ASSET BY WFID'
}

export const enum AssetReadingActions {
  ADD_ASSET_READING_SUCCESS = 'ADD ASSET READING SUCCESS',
  LOAD_ALL_ASSET_READINGS_SUSSESS = 'LOAD ALL ASSET READING SUCCESS'
}

export const enum NotificationActions {
  LOAD_NEW_NOTIFICATIONS_SUCCESS = 'LOAD NEW NOTIFICATIONS SUCCESS',
  LOAD_ALL_NOTIFICATIONS_SUCCESS = 'LOAD ALL NOTIFICATIONS SUCCESS',
  SET_NOTIFICATION_SUCCESS = 'SET NOTIFICATION SUCCESS'
}

export const enum WorkflowActions {
  LOAD_PENDING_ITEMS_SUCCESS = 'LOAD PENDING ITEMS SUCCESS',
  NEW_REGISTRATION_SUCCESS = 'NEW REGISTRATION SUCCESS',
  UPDATE_REGISTRATION_SUCCESS = 'UPDATE REGISTRATION SUCCESS'
}


export const enum LoadingModalActions{
  LOADING ='LOADING',
  ERROR = 'ERROR',
  SUCCESS = 'SUCCESS',
  CLOSE = 'CLOSE'
}

export const enum WFNoteModalActions{
  OPEN = 'OPEN WF NOTE MODAL',
  CANCEL = 'CANCEL NOTE',
  SUBMIT = 'SUBMIT NOTE'
}

export const enum FilterActions{
  FILTER_TEAM='FILTER TEAM',
  FILTER_ASSET = 'FILTER ASSET',
  FILTER_TASK = 'FILTER TASK',
  FILTER_EMPLOYEE = 'FILTER EMPLOYEE',
  FILTER_TASK_BY_STATE = 'FILTER TASK BY STATE'
}

export const enum DrawerActions{
  CHANGE_ACTIVE_INDEX = 'CHANGE ACTIVE INDEX'
}

export const enum MetaDataActions {
  LOAD_METADATA_SUCCESS ='LOAD METADATA SUCCESS',
  DECREASE_PENDING_ITEMS = 'REDUCE PENDING ITEMS',
  DECREASE_REJECTED_ITEMS = 'REDUCE REJECTED ITEMS',
  INCREASE_PENDING_ITEMS = 'INCREASE PENDING ITEMS',
  INCREASE_REJECTED_ITEMS = 'INCREASE REJECTED ITEMS',

  MARK_INV_TASKS_AS_SEEN = 'MARK INDIVIDUAL TAKS AS SEEN',
  MARK_TEAM_TASKS_AS_SEEN = 'MARK TEAM TASKS AS SEEN'
}


export const enum EmployeeReportActions{
  LOAD_EMPLOYEE_REPORT = 'LOAD EMPLOYEE REPORT',
  LOAD_EMPLOYEE_REPORT_SUCCESS = 'LOAD EMPLOYEE REPORT SUCCESS',
  LOAD_EMPLOYEE_REPORT_ERROR = 'LOAD EMPLOYEE REPORT ERROR',

  FILTER_EMPLOYEE_REPORT = 'FILTER EMPLOYEE REPORT',
  CANCE_FILTER_EMPLOYEE_REPORT = 'CANCEL FILTER EMPLOYEE REPORT'
}


export const enum TeamReportActions{
  LOAD_TEAM_REPORT = 'LOAD TEAM REPORT',
  LOAD_TEAM_REPORT_SUCCESS = 'LOAD TEAM REPORT SUCCESS',
  LOAD_TEAM_REPORT_ERROR = 'LOAD TEAM REPORT ERROR',

  FILTER_TEAM_REPORT = 'FILTER TEAM REPORT',
  CANCE_FILTER_TEAM_REPORT = 'CANCEL FILTER TEAM REPORT'
}

export const enum AssetReportActions{
  LOAD_ASSET_REPORT = 'LOAD ASSET REPORT',
  LOAD_ASSET_REPORT_SUCCESS = 'LOAD ASSET REPORT SUCCESS',
  LOAD_ASSET_REPORT_ERROR = 'LOAD ASSET REPORT ERROR',

  FILTER_ASSET_REPORT = 'FILTER ASSET REPORT',
  CANCE_FILTER_ASSET_REPORT = 'CANCEL FILTER ASSET REPORT'
}


export const enum TaskReportActions{
  LOAD_TASK_REPORT = 'LOAD TASK REPORT',
  LOAD_TASK_REPORT_SUCCESS = 'LOAD TASK REPORT SUCCESS',
  LOAD_TASK_REPORT_ERROR = 'LOAD TASK REPORT ERROR',

  FILTER_TASK_REPORT = 'FILTER TASK REPORT',
  CANCE_FILTER_TASK_REPORT = 'CANCEL FILTER TASK REPORT'
}