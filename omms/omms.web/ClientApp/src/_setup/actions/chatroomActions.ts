import { ActionCreator } from 'redux'
import { ThunkAction } from 'redux-thunk'
import { Action } from 'history'
import * as ActionTypes from './bactionTypes'
import {
  MessageResponse,
  MessageRequest
} from '../../_infrastructure/model/messageModel'
import ChatroomService from '../services/chatroom.service'

let chatroom_service = new ChatroomService()

function loadMessagesSuccess(messages: MessageResponse[]) {
  return { type: ActionTypes.EmployeeActions.LOAD_MESSAGES_SUCCESS, messages }
}

function sendMessageSuccess(message: MessageResponse) {
  return { type: ActionTypes.EmployeeActions.SEND_MESSAGE_SUCCESS, message }
}

export function setupChatroom(threadId: string, memberId: string) {
  return {
    type: ActionTypes.EmployeeActions.SETUP_CHATROOM,
    threadId,
    memberId
  }
}

export const loadMessages: ActionCreator<
  ThunkAction<Promise<Action>, any, void, any>
> = (threadId: string) => {
  return async (dispatch: any): Promise<Action> => {
    return chatroom_service
      .loadMessages(threadId)
      .then(response => {
        return dispatch(loadMessagesSuccess(response.data))
      })
      .catch(error => console.log(error))
  }
}

export const sendMessage: ActionCreator<
  ThunkAction<Promise<Action>, any, void, any>
> = (message: MessageRequest) => {
  return async (dispatch: any): Promise<Action> => {
    return chatroom_service
      .sendMessage(message)
      .then(response => {
        return dispatch(sendMessageSuccess(response.data))
      })
      .catch(error => console.log(error))
  }
}
