import {DrawerActions} from './bactionTypes'

export function changeActiveIndex(index: number) {
    return { type: DrawerActions.CHANGE_ACTIVE_INDEX, index }
}