import EmployeeService from "../services/employee.service";
import { ActionCreator, Action } from 'redux';
import { EmployeeReportActions } from "./bactionTypes";
import { EmployeeReportModel } from '../../_infrastructure/model/employee-report-model';
import { ThunkAction } from "redux-thunk";

let employee_service = new EmployeeService()

export enum EmployeeFilter{
    UnAssignedEmployees = 0,
    AssignedEmplyees = 1,

    WithOngoingTask = 2,
    WithCompletedTask = 3,
    WithCancelledTasks = 4,
    WithTerminatedTasks = 5
}

function loadEmployeeReport(){
    return {type:EmployeeReportActions.LOAD_EMPLOYEE_REPORT}
}

function getEmployeeReportSuccess(report:EmployeeReportModel[]){
    return {type : EmployeeReportActions.LOAD_EMPLOYEE_REPORT_SUCCESS, report}
}

function getEmployeeReportError(errorMessage:string){
    return {type:EmployeeReportActions.LOAD_EMPLOYEE_REPORT_ERROR,errorMessage}
}

export function filterEmployeeReport(filterParams : EmployeeFilter[]){
    return {type:EmployeeReportActions.FILTER_EMPLOYEE_REPORT , filterParams}
}

export function cancelEmployeeFilter(){
    return {type:EmployeeReportActions.CANCE_FILTER_EMPLOYEE_REPORT}
}


export const getEmployeeReports: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = () => {
    return async (dispatch: any): Promise<Action> => {
      dispatch(loadEmployeeReport())  
      return await employee_service
        .getEmployeeReport()
        .then((res: any) => {
          return dispatch(getEmployeeReportSuccess(res.data))
        })
        //@ts-ignore
        .catch((error: any) => dispatch(getEmployeeReportError('Failed to load employee report')))
    }
}