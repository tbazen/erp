import { EmployeeActions } from './actionTypes'
import { ActionCreator } from 'redux'
import { Action } from 'history'
import { ThunkAction } from 'redux-thunk'
import { IUserInformation } from '../../_infrastructure/model/userInformationModel'
import * as ActionTypes from './actionTypes'
import EmployeeService from '../services/employee.service'

let employee_service = new EmployeeService()

export function loadAllEmployeesSuccess(employees: IUserInformation[]) {
  return { type: EmployeeActions.LOAD_ALL_EMPLOYEES_SUCCESS, employees }
}

export function loadTeamMembersSuccess(teamMembers: IUserInformation[]) {
  return {
    type: ActionTypes.EmployeeActions.LOAD_TEAM_MEMBERS_SUCCESS,
    teamMembers
  }
}

function loadingEmployee(){
  return {type:ActionTypes.EmployeeActions.LOADING_EMPLOYEE}
}

function employeeError(msg:string){
  return {type:ActionTypes.EmployeeActions.ERROR_LOADING_EMPLOYEE,msg}
}

export function filteringEmployees(){
  return {type:ActionTypes.EmployeeActions.FILTERING}
}
export function cancelEmployeeFiltering(){
  return {type:ActionTypes.EmployeeActions.CANCEL_FILTERING}
}


export const loadAllEmployees: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = () => {
  return async (dispatch: any): Promise<Action> => {
    dispatch(loadingEmployee())
    return employee_service
      .loadAllEmployees()
      .then(response => {
        return dispatch(loadAllEmployeesSuccess(response.data))
      })
      .catch(error => employeeError(error.message))
  }
}

export const loadTeamMembers: ActionCreator<
  ThunkAction<Promise<Action>, any, void, any>
> = (teamId: string) => {
  return async (dispatch: any): Promise<Action> => {
    return employee_service
      .loadTeamMembers(teamId)
      .then(response => {
        return dispatch(loadTeamMembersSuccess(response.data))
      })
      .catch(error => console.log(error))
  }
}