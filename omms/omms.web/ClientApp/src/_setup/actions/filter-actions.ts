
import { AssetResponse } from '../../_infrastructure/model/assetModel'
import { TeamModel } from '../../_infrastructure/model/teamModel'
import {FilterActions} from './bactionTypes'
import { IUserInformation } from '../../_infrastructure/model/userInformationModel';
import { TaskResponse } from '../../_infrastructure/model/task-model';


export function filterTeam(name:string,teams:TeamModel[]){
    return {type:FilterActions.FILTER_TEAM,name,teams}
}

export function filterAsset(name : string,assets:AssetResponse[]){
    return {type:FilterActions.FILTER_ASSET,name,assets}
}

export function filterTask(name:string,tasks:TaskResponse[]){
    return {type:FilterActions.FILTER_TASK,name,tasks}
}

export function filterTaskByState(state:string,tasks:TaskResponse[]){
    return {type:FilterActions.FILTER_TASK_BY_STATE,state,tasks}
}

export function filterEmployee(name : string , employees: IUserInformation[]){
    return {type:FilterActions.FILTER_EMPLOYEE,name,employees}
}