import { GET_USER_INFO } from './actionTypes'
import { IloginViewModel } from '../../modules/shared/login/login'
import { ActionCreator } from 'redux'
import { ThunkAction } from 'redux-thunk'
import { Action } from 'history'
import { IUserInformation } from '../../_infrastructure/model/userInformationModel'
import AuthService from '../services/auth.service'

let auth = new AuthService()
export function loginAction(request: IloginViewModel,preceedLogin: () => void, issue: () => void) {
  auth
    .Login(request)
    .then(res => {
      if (res.status == 200) preceedLogin()
    })
    //@ts-ignore
    .catch(error => {
      issue()
    })
}

export function checkUser(preceedLogin: () => void, promptLogin: () => void) {
  auth
    .CheckUser()
    .then(res => {
      if (res.status == 200) preceedLogin()
    })
    //@ts-ignore
    .catch(error => {
      promptLogin()
    })
}

export const getUserInfo: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = () => {
  return async (dispatch: any): Promise<Action> => {
    return await auth
      .getProfile()
      .then(res => {
        let employee: IUserInformation = res.data
        return dispatch({ type: GET_USER_INFO, info: employee })
      })
      .catch(error => console.log(' ERROR :  ' + error))
  }
}

export function getEmployeeInfo(): Promise<IUserInformation> {
  return new Promise((resolve, reject) => {
    auth
      .getProfile()
      .then(res => {
        let { Id, UserName, Role } = res.data.user
        let employee: IUserInformation = {
          id: Id,
          userName: UserName,
          role: Role
        }
        resolve(employee)
      })
      .catch(error => reject(' ERROR :  ' + error))
  })
}

export function logout(): Promise<boolean> {
  return new Promise((resolve, reject) => {
    auth
      .Logout()
      .then(res => {
        resolve(res.data)
      })
      .catch(error => reject(error))
  })
}
