
import { UtilService } from './../services/util.setvice';
import {MetaDataActions} from './bactionTypes'
import { MetaData } from './../../_infrastructure/model/metadata-model';
import { ActionCreator } from 'redux';
import { Action } from 'history';
import { ThunkAction } from 'redux-thunk';

let util_service = new UtilService


function loadMetaDataSuccess(metadata: MetaData) {
    return { type: MetaDataActions.LOAD_METADATA_SUCCESS , metadata }
}

export function decreasePendingItems(size:number){
    return {type:MetaDataActions.DECREASE_PENDING_ITEMS,size}
}
export function increasePendingItem(size:number){
    return {type:MetaDataActions.INCREASE_PENDING_ITEMS,size}
}

export function decreaseRejectedItems(size:number){
    return {type:MetaDataActions.DECREASE_REJECTED_ITEMS,size}
}

export function increaseRejectedItems(size:number){
    return {type:MetaDataActions.INCREASE_REJECTED_ITEMS,size}
}

export function markInvTasksAsSeenSuccess(){
    return {type:MetaDataActions.MARK_INV_TASKS_AS_SEEN}
}

export function markTeamTasksAsSeenSuccess(){
    return {type:MetaDataActions.MARK_TEAM_TASKS_AS_SEEN}
}

export const loadMetaData: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = () => {
    return async (dispatch: any): Promise<Action> => {
      return util_service.getMetaData()
        .then(response => {
          return dispatch(loadMetaDataSuccess(response.data))
        })
        .catch(error =>console.log(error))
    }
}

