import { ActionCreator } from 'redux'
import { ThunkAction } from 'redux-thunk'
import { Action } from 'history'
import { NoteRequest, NoteResponse } from '../../_infrastructure/model/task-note-model'
import { NoteActions } from './actionTypes'
import TaskService from '../services/task.service'
import { loading, success, error } from './loading-modal-actions';
import UIContainer from './../../modules/shared/_helper/ui-helper/containers-util';

function createNoteSuccess(note: NoteResponse) {
  return {type: NoteActions.CREATE_NOTE, note}
}

function loadNotesSuccess(notes: NoteResponse[]) {
  return {type: NoteActions.LOAD_NOTES_SUCCESS, notes}
}

function loadNoteSuccess(notes: NoteResponse[]) {
  return {type: NoteActions.LOAD_NOTE_SUCCESS, notes}
}

function errorNote(){
  return {type:NoteActions.LOAD_NOTES_ERROR}
}

function errorNoteCreation(){
  return {type:NoteActions.CREATE_NOTE_FAIL}
}

function loadingNotes(){
  return {type:NoteActions.LOADING}
}

let task_service = new TaskService()

export const createNote: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (note: NoteRequest, container : UIContainer = UIContainer.NOTE_LIST_CONTAINER) => {
  return async (dispatch: any): Promise<Action> => {
    container == UIContainer.NOTE_LIST_CONTAINER &&
    dispatch(loading('Adding new note, please wait...'))
    return await task_service
      .addNote(note)
      .then((res:any) => {
        container == UIContainer.NOTE_LIST_CONTAINER &&
        dispatch(success('Note added!')) &&
        dispatch(createNoteSuccess(res.data))
      })
      .catch(() => dispatch(errorNoteCreation()))
  }
}

export const loadTaskNotes: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (taskId: string) => {
  return async (dispatch: any): Promise<Action> => {
    dispatch(loadingNotes())
    return await task_service
      .getTaskNotes(taskId)
      .then((res:any) => {
        return dispatch(loadNotesSuccess(res.data))
      })
      //@ts-ignore
      .catch((error:any) => dispatch(errorNote()))
  }
}

export const latestTaskNotes: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (taskId: string) => {
  return async (dispatch: any): Promise<Action> => {
    return await task_service
      .getLatestTaskNote(taskId)
      .then((res:any) => {
        return dispatch(loadNoteSuccess(res.data))
      })
      .catch((error:any) => console.log(' ERROR :  ' + error))
  }
}