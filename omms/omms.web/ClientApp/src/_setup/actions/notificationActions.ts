import { ActionCreator } from 'redux'
import { Action } from 'history'
import { ThunkAction } from 'redux-thunk'
import * as ActionTypes from './bactionTypes'
import { NotificationModel } from '../../_infrastructure/model/notificationModel'
import NotificationService from '../services/notification.service'

let notification_service = new NotificationService()

function loadAllNotificationsSucess(notifications: NotificationModel[]) {
  return {
    type: ActionTypes.NotificationActions.LOAD_ALL_NOTIFICATIONS_SUCCESS,
    notifications
  }
}

function loadNewNotificationsSucess(notifications: NotificationModel[]) {
  return {
    type: ActionTypes.NotificationActions.LOAD_NEW_NOTIFICATIONS_SUCCESS,
    notifications
  }
}

function setNotificationSuccess(notification: NotificationModel) {
  return {
    type: ActionTypes.NotificationActions.SET_NOTIFICATION_SUCCESS,
    notification
  }
}

export const loadAllNotifications: ActionCreator<
  ThunkAction<Promise<Action>, any, void, any>
> = (empId: string) => {
  return async (dispatch: any): Promise<Action> => {
    return notification_service
      .loadAllNotifications(empId)
      .then(response => {
        return dispatch(loadAllNotificationsSucess(response.data))
      })
      .catch(error => console.log(error))
  }
}

export const loadNewNotifications: ActionCreator<
  ThunkAction<Promise<Action>, any, void, any>
> = (empId: string) => {
  return async (dispatch: any): Promise<Action> => {
    return notification_service
      .loadNewNotifications(empId)
      .then(response => {
        return dispatch(loadNewNotificationsSucess(response.data))
      })
      .catch(error => console.log('NOTIFICATION REQUEST IS FAILING ' + error))
  }
}

export const setNotifications: ActionCreator<
  ThunkAction<Promise<Action>, any, void, any>
> = (notification: NotificationModel) => {
  return async (dispatch: any): Promise<Action> => {
    return notification_service
      .setNotification(notification)
      .then(response => {
        console.log('NOTFICATION RESPONSE : ')
        console.log(response.data)
        return dispatch(setNotificationSuccess(response.data))
      })
      .catch(error => console.log(error))
  }
}
