import EmployeeService from "../services/employee.service";
import { ActionCreator, Action } from 'redux';
import { ThunkAction } from "redux-thunk";
import TaskService from '../services/task.service';
import { TaskReportActions } from "./bactionTypes";
import { TaskReportModel } from '../../_infrastructure/model/task-report-model';

let task_service = new TaskService() 


export enum TaskReportFilter{
    EMPLOYEE = 1,
    TEAM =2 ,
    
    //state filter
    ONGOING_TASKS = 3,
    COMPLETED_TASTKS = 4,
    CANCELLED_TASKS = 5,
    TERMINATED_TASKS = 6,

    //task type
    UNSCHEDULED_TASKS = 7,
    SCHEDULED_TASKS =8,
    PERIODIC_TASKS = 9,
    INCIDENT_TASKS =10
}

function loadTaskReport(){
    return {type:TaskReportActions.LOAD_TASK_REPORT}
}

function getAssetReportSuccess(report:TaskReportModel[]){
    return {type : TaskReportActions.LOAD_TASK_REPORT_SUCCESS, report}
}

function getTaskReportError(errorMessage:string){
    return {type:TaskReportActions.LOAD_TASK_REPORT_ERROR,errorMessage}
}

export function filterTaskReport(filterParams : TaskReportFilter[]){
    return {type:TaskReportActions.FILTER_TASK_REPORT , filterParams}
}

export function cancelTaskFilter(){
    return {type:TaskReportActions.CANCE_FILTER_TASK_REPORT}
}

export const getTaskReports: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = () => {
    return async (dispatch: any): Promise<Action> => {
      dispatch(loadTaskReport())  
      return await task_service
        .getTaskReport()
        .then((res: any) => {
          return dispatch(getAssetReportSuccess(res.data))
        })
        //@ts-ignore
        .catch((error: any) => dispatch(getTaskReportError('Failed to load task report')))
    }
}