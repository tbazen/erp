import {
  APPROVE_TASK_SUCCESS,
  CREATE_TASK_SUCCESS,
  EDIT_TASK_SUCCESS,
  TaskActions
} from './actionTypes'
import { ActionCreator } from 'redux'
import { Action } from 'history'
import { ThunkAction } from 'redux-thunk'
import { AssigneeViewModel, TaskModel } from '../../_infrastructure/model/taskFromModel'
import Role from '../../modules/shared/_helper/role-util/roles'
import AuthService from '../services/auth.service'
import TaskService from '../services/task.service'
import { loading, success, error } from './loading-modal-actions';
import { TaskRequest, TaskResponse } from '../../_infrastructure/model/task-model'
import WorkflowService from './../services/workflow.service';
import { NoteResponse } from '../../_infrastructure/model/task-note-model';
import UIContainer from './../../modules/shared/_helper/ui-helper/containers-util';
import { increasePendingItem, decreaseRejectedItems, decreasePendingItems, markInvTasksAsSeenSuccess, markTeamTasksAsSeenSuccess } from './metadata-actions';
import { IUserInformation } from '../../_infrastructure/model/userInformationModel'
import TaskNode from '../../modules/shared/task/dependency/task-node';
import axios from 'axios';
import { UtilService } from '../services/util.setvice';
import { WFRequestModel } from '../../_infrastructure/model/workflow-model';

export function getTasksSuccess(tasks: TaskModel[]) {
  return { type: TaskActions.LOAD_TASKS_SUCCESS, tasks }
}

export function getAssetTasksSuccess(tasks: TaskModel[]) {
  return { type: TaskActions.LOAD_ASSET_TASKS, tasks }
}

function getPendingTasksSuccess(tasks: TaskModel[]) {
  return { type: TaskActions.LOAD_PENDING_TASKS_SUCCESS, tasks }
}

export function deleteTaskSuccess(taskId: string) {
  return { type: TaskActions.DELETE_TASK_SUCCESS, taskId }
}

export function getTasksLoading() {
  return { type: TaskActions.LOAD_TASKS, loading: true }
}

export function getTasksFailure(error: string) {
  return { type: TaskActions.LOAD_TASKS_FAILURE, error }
}


function loadingTasks(){
  return {type:TaskActions.LOADING}
}

function errorTasks(){
  return {type:TaskActions.ERROR}
}

export function filteringTasks(){
  return {type:TaskActions.FILTERING}
}

export function cancelTaskFiltering(){
  return {type:TaskActions.CANCEL_FILTERING}
}

export function removeTaskByWfid(wfid:string){
  return {type:TaskActions.REMOVE_TASK_BY_WFID,wfid}
}

let auth_service = new AuthService()
let task_service = new TaskService()
let util_service = new UtilService()

export const viewAllTasks: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = () => {
  return async (dispatch: any): Promise<Action> => {
    dispatch(loadingTasks())
    let userInfo: any = null
    await auth_service.getProfile().then((res: any) => {
      userInfo = res.data
    })

    let userRole = userInfo != null ? userInfo.role : null
    let userId = userInfo != null ? '{' + userInfo.id + '}' : null

    // if (userRole == Role.ADMIN || userRole == Role.TECHNICAL_OFFICER || userRole == Role.TECHNICAL_SUPERVISOR || userRole == Role.EMPLOYEE) {
      return await task_service
        .getAllTasks()
        .then(res => {
          return dispatch(getTasksSuccess(res.data.tasks))
        })
        .catch(error => {
          console.log(error)
        })
    // } else {
    //   return await task_service
    //     .getEmployeeTasks(userId as string)
    //     .then(res => {
    //       return dispatch(getTasksSuccess(res.data.tasks))
    //     })
    //     .catch(error => console.log(' ERROR :  ' + error))
    // }
  }
}

export const viewAssetTasks: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = () => {
  return async (dispatch: any): Promise<Action> => {
    dispatch(loadingTasks())
    return await task_service
      .getAllTasks()
      .then(res => {
        return dispatch(getAssetTasksSuccess(res.data.tasks))
      })
      .catch(error => console.log(' ERROR :  ' + error))
  }
}

export const deleteTask: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (id: string) => {
  return async (dispatch: any): Promise<Action> => {
    return await task_service
      .deleteTask(id)
      //@ts-ignore
      .then(res => {
        return dispatch(deleteTaskSuccess(id))
      })
      .catch(error => console.log(' ERROR :  ' + error))
  }
}

export const createTask: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (data: TaskModel,container : UIContainer = UIContainer.REGISTARED_TASK_CONTAINER) => {
   return async (dispatch: any): Promise<Action> => {
    dispatch(loading('Creating task registration request,please wait...'))
    return await task_service
      .requestTaskRegistration(data)
      //@ts-ignore
      .then(res => {
        if(container != UIContainer.PENDING_CONTAINER ){
          dispatch(increasePendingItem(1))
          container == UIContainer.REJECTED_CONTAINER &&
          data.wfid != null &&
          dispatch(removeTaskByWfid(data.wfid)) &&
          dispatch(decreaseRejectedItems(1))
        }
        dispatch(success('Task registration request sent successfully!'))
        // return dispatch({ type: CREATE_TASK_SUCCESS, payload: res })
      })
      .catch(err => dispatch(error(err.message)))
  }
}

function clearTasksSuccess(){
  return {type:TaskActions.CLEAR_TASK_LIST_SUCCESS}
}

export function clearTasks(){
  return (dispatch : any)=>{
    dispatch(clearTasksSuccess())
  }
}


export const updateTask: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (data: any) => {
  return async (dispatch: any): Promise<Action> => {
    return await task_service
      .updateTask(data)
      .then(res => {
        return dispatch({ type: CREATE_TASK_SUCCESS, payload: res })
      })
      .catch(error => console.log(' ERROR :  ' + error))
  }
}

export const getEmployeePendingTasks: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (employeeId: string = '') => {
  return async (dispatch: any): Promise<Action> => {
    dispatch(loadingTasks())
    return task_service
      .getEmployeePendingTasks(employeeId)
      .then(res => {
        return dispatch(getPendingTasksSuccess(res.data))
      })
      .catch(error => {
        console.log(error)
      })
  }
}


export const getAssetTasks: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (assetId: number) => {
  return async (dispatch: any): Promise<Action> => {
    dispatch(loadingTasks())
    return await task_service.getAssetTasks(assetId)
        .then(res => {
          return dispatch(getTasksSuccess(res.data))
        })
        .catch(error => {
          if(!axios.isCancel(error)){
            dispatch(errorTasks())
          }
        })
  }
}


export const getPendingTasks: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = () => {
  return async (dispatch: any): Promise<Action> => {
    dispatch(loadingTasks())
    return await task_service
        .getPendingTasks()
        .then(res => {
          return dispatch(getTasksSuccess(res.data))
          }
        )
        .catch(error => {
          if(!axios.isCancel(error)){
            dispatch(errorTasks())
          }
        })
    }
}

export const getRejectedTasks: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = () => {
  return async (dispatch: any): Promise<Action> => {
    dispatch(loadingTasks())  
    return await task_service
        .getRejectedTasks()
        .then(res => {
          return dispatch(getTasksSuccess(res.data))
          }
        )
        .catch(error =>{
          if(!axios.isCancel(error)){
            dispatch(errorTasks())
          }
        })
    }
}

export const rejectTaskRegistration: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (req:WFRequestModel) => {
  return async (dispatch: any): Promise<Action> => {
    dispatch(loading('Rejecting task registration,please wait ...')) 
    return await task_service.rejectTaskRegistration(req)
    //@ts-ignore
    .then(res => {
            dispatch(removeTaskByWfid(req.wfid))
            dispatch(decreasePendingItems(1))
            return dispatch(success('Task registration request rejected'))
          }
        )
        .catch(error => dispatch(error(error.message)))
    }
}

export const approveTask: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (data: string) => {
  return async (dispatch: any): Promise<Action> => {
    dispatch(loading('Approving task registration,please wait ...'))
    return await task_service
      .approveTaskRegistration(data)
      //@ts-ignore
      .then(res => {
          dispatch(removeTaskByWfid(data))
          dispatch(decreasePendingItems(1))
         return dispatch(success('Team registration request approved'))
      })
      .catch(error => dispatch(error(error.message)))
  }
}


export const getIndevidualTasks: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = () => {
  return async (dispatch: any): Promise<Action> => {
    dispatch(loadingTasks())  
    return await task_service
    .getIndevidualTasks()
        .then(res => {
          return dispatch(getTasksSuccess(res.data))
          }
        )
        .catch(error =>{
            if(!axios.isCancel(error)){
              dispatch(errorTasks())
            }
          })
    }
}

export const getTeamTasks: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = () => {
  return async (dispatch: any): Promise<Action> => {
    dispatch(loadingTasks())  
    return await task_service.getTeamTasks()
        .then(res => {
          return dispatch(getTasksSuccess(res.data))
          }
        )
        .catch(error =>{
          if(!axios.isCancel(error)){
            dispatch(errorTasks())
          }
        })
    }
}

export const getTasksByTeamId: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (teamId:string) => {
  return async (dispatch: any): Promise<Action> => {
    dispatch(loadingTasks())  
    return await task_service.getTasksByteamId(teamId)
        .then(res => {
          return dispatch(getTasksSuccess(res.data))
          }
        )
        //@ts-ignore
        .catch(error =>dispatch(errorTasks()))
    }
}

export const completeTask: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (req:WFRequestModel) => {
  return async (dispatch: any): Promise<Action> => {
    dispatch(loadingTasks())
    return await task_service.requestCompletion(req)
         //@ts-ignore
         .then(res => {
          dispatch(removeTaskByWfid(req.wfid))
          }
        )
        //@ts-ignore
        .catch(error =>dispatch(errorTasks()))
    }
}

export const terminateTask: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (req:WFRequestModel) => {
  return async (dispatch: any): Promise<Action> => {
    dispatch(loadingTasks())
    return await task_service.requestTermination(req)
        //@ts-ignore
        .then(res => {
          dispatch(removeTaskByWfid(req.wfid))
          }
        )
        //@ts-ignore
        .catch(error =>dispatch(errorTasks()))
    }
}

export const cancelTask: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (req:WFRequestModel) => {
  return async (dispatch: any): Promise<Action> => {
    dispatch(loading('Sending cancellation request, please wait...'))
    return await task_service.requestCancellation(req)
    //@ts-ignore    
    .then(res => {
           dispatch(removeTaskByWfid(req.wfid))
           dispatch(success('Task cancellation request sent ....'))
          }
        )
        //@ts-ignore
        .catch(err =>dispatch(error(err.message)))
    }
}

export const approveTaskState: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (wfid: string) => {
  return async (dispatch: any): Promise<Action> => {
    dispatch(loading('Approving task registration,please wait ...'))
    return await task_service
      .approveState(wfid)
      //@ts-ignore
      .then(res => {

        dispatch(removeTaskByWfid(wfid))
        dispatch(decreasePendingItems(1))
        return dispatch(success('Task registration approved!'))
      })
      .catch(err => {
        if (err.response) {
          dispatch(error(err.response.data.message))
        } else if (err.request) {
          dispatch(error(err.request))
        } else {
          dispatch(error(err.message))
        }
      })
  }
}

export const rejectTaskState: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (req: WFRequestModel) => {
  return async (dispatch: any): Promise<Action> => {
    dispatch(loading('Approving task registration,please wait ...'))
    return await task_service
      .rejectState(req)
      //@ts-ignore
      .then(res => {
        dispatch(removeTaskByWfid(req.wfid))
        dispatch(decreasePendingItems(1))
        return dispatch(success('Team registration request approved'))
      })
      .catch(err => dispatch(error(err.message)))
  }
}

export const getTaskByStatus: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (state:number) => {
  return async (dispatch: any): Promise<Action> => {
    dispatch(loadingTasks())  
    return await task_service
        .getTaskByState(state)
        .then(res => {
          return dispatch(getTasksSuccess(res.data))
          }
        )
        //@ts-ignore
        .catch(error =>{
          if(!axios.isCancel(error)){
            dispatch(errorTasks())
          }
        })
    }
}

export function getTasksForDependency(state:number): Promise<TaskResponse[]> {
  return new Promise((resolve, reject) => {
    task_service.getTaskByState(state)
      .then(res => {
        resolve(res.data)
      })
      .catch(error => reject(' ERROR :  ' + error))
  })
}



export function getLatestTaskNote(taskId:string): Promise<NoteResponse> {
  return new Promise((resolve, reject) => {
      task_service.getLatestTaskNote(taskId)
      .then(res => {
        let note:NoteResponse = res.data
        resolve(note)
      })
      .catch(error => reject(' ERROR :  ' + error))
  })
}


export function getTaskDepndecy(taskId:string): Promise<TaskNode> {
  return new Promise((resolve, reject) => {
      task_service.getTaskDepndecy(taskId)
      .then(res => {
        let node:TaskNode = res.data
        resolve(node)
      })
      .catch(error => reject(' ERROR :  ' + error))
  })
}

export function getTaskById(taskId:string): Promise<TaskResponse> {
  return new Promise((resolve, reject) => {
      task_service
        .getTaskById(taskId)
      .then(res => {
        resolve(res.data)
      })
      .catch(error => reject(' ERROR :  ' + error))
  })
}

export const markIndividualTasksAsSeen: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = () => {
  return async (dispatch: any): Promise<Action> => {
    dispatch(loadingTasks())
    return util_service.markIndividualTaskAsSeen ()
      .then(response => {
        dispatch(markInvTasksAsSeenSuccess())
        return dispatch(getTasksSuccess(response.data))
      })
      .catch(error =>console.log(error))
  }
}

export const markTeamTasksAsSeen: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = () => {
  return async (dispatch: any): Promise<Action> => {
    dispatch(loadingTasks())
    return util_service.markTeamTaskAsSeen()
      .then(response => {
        dispatch(markTeamTasksAsSeenSuccess())      
        return dispatch(getTasksSuccess(response.data))
   
      })
      .catch(error =>console.log(error))
  }
}

export const updateTaskAssignee: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (assignee: AssigneeViewModel) => {
  return async (dispatch: any): Promise<Action> => {
    dispatch(loading("Sending Assignee update request, please wait..."))
    return task_service.updateTaskAssignee(assignee).
    //@ts-ignore
      then(response => {
        dispatch(removeTaskByWfid(assignee.wfid))
        dispatch(increasePendingItem(1))
        return dispatch(success('Assignee Update Request Sent'))
      })
      .catch(err => {
        dispatch(error(err.message))
      })
  }
}