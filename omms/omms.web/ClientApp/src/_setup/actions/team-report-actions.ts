
import { ActionCreator, Action } from 'redux'
import { ThunkAction } from "redux-thunk";
import TeamService from '../services/team.service';
import { TeamReportModel } from '../../_infrastructure/model/team-report-model';
import { TeamReportActions } from './bactionTypes';

let team_service = new TeamService()

export enum TeamReportFilter{
    UnAssignedTeams = 0,
    AssignedTeams = 1,

    WithOngoingTask = 2,
    WithCompletedTask = 3,
    WithCancelledTasks = 4,
    WithTerminatedTasks = 5
}

function getTeamReport(){
    return  {type:TeamReportActions.LOAD_TEAM_REPORT}
}

function getTeamReportSuccess(report:TeamReportModel[]){
    return {type : TeamReportActions.LOAD_TEAM_REPORT_SUCCESS, report}
}

function getTeamReportError(errorMessage:string){
    return {type:TeamReportActions.LOAD_TEAM_REPORT_ERROR,errorMessage}
}

export function filterTeamReport(filterParams: TeamReportFilter[]){
    return {type:TeamReportActions.FILTER_TEAM_REPORT,filterParams}
}

export function cancelTeamFilter(){
    return {type:TeamReportActions.CANCE_FILTER_TEAM_REPORT}
}


export const getTeamReports: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = () => {
    return async (dispatch: any): Promise<Action> => {
        dispatch(getTeamReport())
        return await team_service
        .getTeamReport()
        .then((res: any) => {
          return dispatch(getTeamReportSuccess(res.data))
        })
        //@ts-ignore
        .catch((error: any) => dispatch(getTeamReportError('Failed to load team report')))
    }
}