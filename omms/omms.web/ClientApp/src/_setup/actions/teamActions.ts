import axios from 'axios'

import { ActionCreator } from 'redux'
import { Action } from 'history'
import { ThunkAction } from 'redux-thunk'
import * as ActionTypes from './actionTypes'
import { TeamModel } from '../../_infrastructure/model/teamModel'
import TeamService from '../services/team.service'
import { loading, success ,error} from './loading-modal-actions';
import { increasePendingItem, decreasePendingItems, decreaseRejectedItems } from './metadata-actions';
import UIContainer from '../../modules/shared/_helper/ui-helper/containers-util';
import { WFRequestModel } from '../../_infrastructure/model/workflow-model';
import { resolve } from 'path';
import { baseUrl } from '../services/url.config';
import config from '../services/header.config';

let team_service = new TeamService()

function loadAllTeamsSucess(teamList: TeamModel[]) {
  return { type: ActionTypes.TeamActions.LOAD_ALL_TEAMS_SUCCESS, teamList }
}

function addNewTeamSuccess(team: TeamModel) {
  return { type: ActionTypes.TeamActions.ADD_NEW_TEAM_SUCCESS, team }
}

function updateTeamSuccess(team: TeamModel) {
  return { type: ActionTypes.TeamActions.UPDATE_TEAM_SUCCESS, team }
}

function deleteTeamSuccess(teamId: string) {
  return { type: ActionTypes.TeamActions.DELETE_TEAM_SUCCESS, teamId }
}

function loadingTeams(){
  return {type:ActionTypes.TeamActions.LOADING}
}

function errorTeams(){
  return {type:ActionTypes.TeamActions.ERROR}
}

export function filteringTeams(){
  return {type:ActionTypes.TeamActions.FILTERING}
}
export function cancelTeamFiltering(){
  return {type:ActionTypes.TeamActions.CANCEL_FILTERING}
}

export function removeTeamByWfid(wfid:string){
  return {type:ActionTypes.TeamActions.REMOVE_TEAM_BY_WFID,wfid}
}


export const loadAllTeams: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = () => {
  return async (dispatch: any): Promise<Action> => {
    dispatch(loadingTeams())
    return team_service
      .loadAllTeams()
      .then(response => {
        return dispatch(loadAllTeamsSucess(response.data))
      })
      .catch(error => {
        if(!axios.isCancel(error)){
          dispatch(errorTeams())
        }
      })
  }
}

export const createTeam: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (team: TeamModel,container : UIContainer = UIContainer.REGISTERED_CONTAINER) => {
  return async (dispatch: any): Promise<Action> => {
    dispatch(loading('sendign team modification request...'))
    return team_service.requestTeamRegistration(team)
    //@ts-ignore
      .then(response => {
         if(container != UIContainer.PENDING_CONTAINER){
           dispatch(increasePendingItem(1))
           
           container == UIContainer.REGISTERED_CONTAINER &&
           team.wfid != null &&
           dispatch(removeTeamByWfid(team.wfid))

           container == UIContainer.REJECTED_CONTAINER &&
           team.wfid != null &&
           dispatch(decreaseRejectedItems(1)) &&
           dispatch(removeTeamByWfid(team.wfid))
         } 
          dispatch(success('operation finished successfully!'))
      })
      .catch(err => dispatch(error(err.message)))
  }
}

export const deleteTeam: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (teamId: string) => {
  return async (dispatch: any): Promise<Action> => {
    return team_service
      .deleteTeam(teamId)
      .then(response => {
        if (response.status == 200) return dispatch(deleteTeamSuccess(teamId))
      })
      .catch(error => console.log(error))
  }
}

export function loadAllEmployeeTeamsSucess(teamList: TeamModel[]) {
  return {
    type: ActionTypes.TeamActions.LOAD_ALL_EMPLOYEE_TEAMS_SUCCESS,
    teamList
  }
}


export const loadPendingTeams :ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = () => {
return async (dispatch: any): Promise<Action> => {
  dispatch(loadingTeams())
  return team_service.getPendingTeams()
    .then(response => {
       return dispatch(loadAllTeamsSucess(response.data))
    })
    .catch(error => {
        if(!axios.isCancel(error)){
          dispatch(errorTeams())
        }
      })
  }
}


export const approveTeamRegistration: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (wfid: string) => {
  return async (dispatch: any): Promise<Action> => {
    dispatch(loading('Approving team registration,please wait ...'))
    return team_service.approveTeamRegistration(wfid)
      .then(response => {
        if (response.status == 200) {
          dispatch(success('Team approved,seccessfully!'))
          dispatch(decreasePendingItems(1))
          dispatch(removeTeamByWfid(wfid))
          return dispatch(deleteTeamSuccess(wfid))
        }
      })
      .catch(err => dispatch(error(err.message)))
  }
}


export const rejectTeamRegistration: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (request: WFRequestModel) => {
  return async (dispatch: any): Promise<Action> => {
    dispatch(loading('Rejecting team registration,please wait ...'))
    return team_service.rejectTeamregistration(request)
      .then(response => {
        if (response.status == 200){
          dispatch(success('Team registration request rejected'))
          dispatch(decreasePendingItems(1))
          dispatch(removeTeamByWfid(request.wfid))
        }
      })
      .catch(err => dispatch(error(err.message)))
  }
}

export const loadRejectedTeams :ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = () => {
  return async (dispatch: any): Promise<Action> => {
    dispatch(loadingTeams())
    return team_service.getRejectedTeams()
      .then(response => {
        return dispatch(loadAllTeamsSucess(response.data))
      })
      .catch(error => {
          if(!axios.isCancel(error)){
            dispatch(errorTeams())
          }
        })
    }
}

export const loadAllEmployeeTeams: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (employeeId: string) => {
  return async (dispatch: any): Promise<Action> => {
    return team_service
      .loadAllEmployeeTeams(employeeId)
      .then(response => {
        return dispatch(loadAllEmployeeTeamsSucess(response.data))
      })
      .catch(error => console.log(error))
  }
}

export const getTeamById :ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (teamID:string) => {
  return async (dispatch: any): Promise<Action> => {
    dispatch(loadingTeams())
    return team_service.getTeambyId(teamID)
      .then(response => {
         let team : TeamModel = response.data
         if(team.id == undefined)
           return dispatch(loadAllTeamsSucess([]))
         return dispatch(loadAllTeamsSucess([team]))
      })
      .catch(error => 
        {
          if(!axios.isCancel(error)){
            dispatch(errorTeams())
          }
        })
    }
}


export const getAllTeams = () => new Promise<TeamModel[]>((resolve,reject)=>{
  axios.get(`${baseUrl}team/teamlist/`,config)
  .then(res=>{
    resolve(res.data)
  }).catch(error => reject(error))
}) 