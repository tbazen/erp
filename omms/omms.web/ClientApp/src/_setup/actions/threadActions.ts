import { ActionCreator } from 'redux'
import { ThunkAction } from 'redux-thunk'
import { Action } from 'history'
import { ThreadModel } from '../../_infrastructure/model/threadModel'
import * as ActionTypes from './bactionTypes'
import ChatroomService from './../services/chatroom.service'

let chatroom_service = new ChatroomService()
export function LoadEmployeeThreadSuccess(threads: ThreadModel[]) {
  return {
    type: ActionTypes.EmployeeActions.LOAD_EMPLOYEE_THREADS_SUCCESS,
    threads
  }
}

export const loadEmployeeThreads: ActionCreator<
  ThunkAction<Promise<Action>, any, void, any>
> = (employeeId: string) => {
  return async (dispatch: any): Promise<Action> => {
    return chatroom_service
      .loadEmployeeThreads(employeeId)
      .then(response => {
        return dispatch(LoadEmployeeThreadSuccess(response.data))
      })
      .catch(error => console.log(error))
  }
}
