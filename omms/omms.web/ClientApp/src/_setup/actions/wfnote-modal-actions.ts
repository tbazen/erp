import {WFNoteModalActions} from './bactionTypes'
import { WorkflowTypes } from '../../_infrastructure/model/workflow-model';


export enum WFNoteActions{
    REJECTION = 'Rejection',
    TASK_CANCELLATION = 'Cancelling Task',
    TASK_TERMINATION = 'Terminating Task',
    TASK_COMPLETION = 'Completing Task',
    REJECT_TASK_STATE = 'Rejecting Task State'
}


export function openNoteModal(wfid: string , WFType : WorkflowTypes , modalAction :WFNoteActions = WFNoteActions.REJECTION){
    return {type : WFNoteModalActions.OPEN,wfid,WFType ,modalAction}
}

export function submitNoteFromModal(note:string){
    return {type:WFNoteModalActions.SUBMIT,note}
}

export function cancelNoteModal(){
    return {type:WFNoteModalActions.CANCEL}
}

