import {WorkflowActions} from './bactionTypes'
import { WorkFlowModel, WorkflowTypes } from '../../_infrastructure/model/workflow-model';
import { ActionCreator } from 'redux';
import { ThunkAction } from 'redux-thunk';
import { Action } from 'history';
import { authHeader } from '../../modules/shared/_helper/auth-header';
import axios from 'axios';
import { TeamModel } from '../../_infrastructure/model/teamModel';
import { AssetModel } from '../../_infrastructure/model/assetModel';
import { TaskModel } from '../../_infrastructure/model/taskFromModel';
import WorkflowService from '../services/workflow.service';
import { loading, success, error } from './loading-modal-actions';
import { decreasePendingItems, decreaseRejectedItems } from './metadata-actions';
import UIContainer from './../../modules/shared/_helper/ui-helper/containers-util';
import { removeTeamByWfid } from './teamActions';
import { removeAssetByWfid } from './assetActions';
import { removeTaskByWfid } from './taskActions';
import { baseUrl } from '../services/url.config';

let workflow_service = new WorkflowService()

function loadPendingItemsSucess(items:WorkFlowModel[]){
    return {type:WorkflowActions.LOAD_PENDING_ITEMS_SUCCESS,items}
}

function newRegistrationSuccess(item:WorkFlowModel){
    return {type:WorkflowActions.NEW_REGISTRATION_SUCCESS,item}
}
function updateRegistrationSuccess(item:WorkFlowModel){
    return {type:WorkflowActions.UPDATE_REGISTRATION_SUCCESS,item}
}

export const loadPendingItems: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (wfType:WorkflowTypes) => {
    return async (dispatch:any):Promise<Action> => {
      return axios.get(`${baseUrl}employee/get-pending-items/${wfType as number}`,
      {headers:authHeader(), baseURL: '/'})
      .then(response=>{
          return dispatch(loadPendingItemsSucess(response.data))        
      })
      .catch(error=>console.log(error))
    }
}

export const requestRegistration: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (
    data:TeamModel | AssetModel | TaskModel,
    type : WorkflowTypes,
    wfid:string | null
    ) => {
    let url = getRegistrationUrl(type)   
    console.log(url+wfid) 
    console.log(data)
    return async (dispatch:any):Promise<Action> => {
      return axios.post(`${url}${wfid}`,
      data,
      {
          headers:authHeader(), 
          baseURL: '/'
      })
      .then(response=>{
          console.log('Response : ')
          console.log(response)
          if(wfid==null)
            return dispatch(newRegistrationSuccess(response.data))
          return dispatch(updateRegistrationSuccess(response.data))        
      })
      .catch(error=>console.log(error))
    }
}


export const cancelRegistrationRequest : ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (wfid:string,container:UIContainer = UIContainer.REGISTERED_CONTAINER) => {
    return async (dispatch:any):Promise<Action> => {
      dispatch(loading('Canceling registration request,please wait...'))
      return workflow_service.cancelRegistrationRequest(wfid)
     //@ts-ignore
      .then(response=>{
          dispatch(success('Request calcelled successfully!'))
          
          container == UIContainer.PENDING_CONTAINER &&
          dispatch(decreasePendingItems(1))

          container == UIContainer.REJECTED_CONTAINER &&
          dispatch(decreaseRejectedItems(1))

          //to remove component from the UI
          dispatch(removeTeamByWfid(wfid))
          dispatch(removeAssetByWfid(wfid))
          dispatch(removeTaskByWfid(wfid))
      })
      .catch(err=>dispatch(error(err.message)))
    }
}


function getRegistrationUrl(type:WorkflowTypes){
    switch(type)
    {
        case WorkflowTypes.Team:
            return `${baseUrl}officer/create-team/`
        case WorkflowTypes.Asset:
            return `${baseUrl}officer/create-asset/`
        case WorkflowTypes.Task:
            return `${baseUrl}officer/create-task/`
            
    }
}