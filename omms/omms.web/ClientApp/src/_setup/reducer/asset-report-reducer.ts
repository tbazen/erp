import { Reducer } from 'redux'
import { AssetReportState } from '../../_infrastructure/state/asset-report-state';
import { AssetReportActions } from '../actions/bactionTypes';
import { AssetReportModel } from '../../_infrastructure/model/asset-report-model';
import { AssetReportFilter } from '../actions/asset-report-actions';
import { AssetTypes } from 'src/_infrastructure/model/asset-search-params';

let intialState: AssetReportState = {
  report: [],
  filteredReport:[],
  loading: false,
  error: false,
  errorMessage : undefined,
  filtering :false,
}

let errorState : AssetReportState = {
  report:[],
  filteredReport:[],
  loading : false,
  error: true,
  errorMessage:'Failed to load asset report!',
  filtering:false
}

let loadingState : AssetReportState ={
  report:[],
  filteredReport:[],
  loading:true,
  error:false,
  errorMessage : undefined,
  filtering:false
}


export const assetReportReducer: Reducer<AssetReportState> = (state = intialState,action ): AssetReportState => {
  switch (action.type) {
      case AssetReportActions.LOAD_ASSET_REPORT:
         return Object.assign({},state,{...loadingState})
      

      case AssetReportActions.LOAD_ASSET_REPORT_ERROR:
         return Object.assign({},state,{...errorState})
      

      case AssetReportActions.LOAD_ASSET_REPORT_SUCCESS:
         return Object.assign(
             {},
             state,
             {
                loading:false,
                error:false,
                errorMessage : undefined,
                filtering:false,
                report:action.report
             })
      

      
      case AssetReportActions.FILTER_ASSET_REPORT:
             let filterParams : AssetReportFilter[] = action.filterParams 
             let newList : AssetReportModel[] = []
             state.report.forEach(rep => {
                if(matchesWithFilterParams(rep,filterParams))
                   newList.push(rep)
             })
             return Object.assign(
                {},
                state,
                {
                   filteredReport:newList,
                   filtering: true
                }
             )
      
             
      case AssetReportActions.CANCE_FILTER_ASSET_REPORT:
         return Object.assign(
           {},
           state,
           {
            filteredReport:[],
            filtering:false
           })         
      

      default :
         return state
  }
}


const matchesWithFilterParams = (report : AssetReportModel , filterParams : AssetReportFilter[]) : boolean =>{
   let match = false;
   filterParams.forEach(filterAttribute =>{
      if( (filterAttribute == AssetReportFilter.BOREHOLE) && (report.assetType == AssetTypes.BOREHOLE) )
       match = true;
      if( (filterAttribute == AssetReportFilter.ENDPOINTS) && (report.assetType == AssetTypes.ENDPOINTS) )
        match = true;
      if( (filterAttribute == AssetReportFilter.HOUSE_CONNECTIONS) && (report.assetType == AssetTypes.HOUSE_CONNECTIONS) )
        match = true;
      if( (filterAttribute == AssetReportFilter.HYDRANTS) && (report.assetType == AssetTypes.HYDRANTS) )
        match = true;
      if( (filterAttribute == AssetReportFilter.PIPELINE) && (report.assetType == AssetTypes.PIPELINE) )
        match = true;
      if( (filterAttribute == AssetReportFilter.REDUCE) && (report.assetType == AssetTypes.REDUCE) )
        match = true;  
      if( (filterAttribute == AssetReportFilter.RESERVIOR) && (report.assetType == AssetTypes.RESERVIOR) )
        match = true;
      if( (filterAttribute == AssetReportFilter.VALVE) && (report.assetType == AssetTypes.VALVE) )
        match = true;
      if( (filterAttribute == AssetReportFilter.WATER_METER) && (report.assetType == AssetTypes.WATER_METER) )
        match = true;
      if( (filterAttribute ==AssetReportFilter.WithOngoingTask) && (report.onGoingTasks >0 ))
        match = true;
      if( (filterAttribute == AssetReportFilter.WithCompletedTask ) && (report.completedTasks >0 ))    
        match = true;
      if( (filterAttribute ==  AssetReportFilter.WithCancelledTasks) &&  (report.cancelledTasks > 0 ) )    
          match = true
      if( (filterAttribute == AssetReportFilter.WithTerminatedTasks)  && (report.terminatedTasks > 0 ))
          match = true 
   });
   return match;
}