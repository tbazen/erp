import { Reducer } from 'redux'
import * as ActionTypes from './../actions/bactionTypes'
import { AssetReadingState } from '../../_infrastructure/state/assetReadingState'

let intialState: AssetReadingState = {
  readings: []
}

const assetReadingReducer: Reducer<AssetReadingState> = (
  state = intialState,
  action
): AssetReadingState => {
  let { AssetReadingActions } = ActionTypes
  switch (action.type) {
    case AssetReadingActions.LOAD_ALL_ASSET_READINGS_SUSSESS:
      return Object.assign({}, state, { readings: action.readings })

    case AssetReadingActions.ADD_ASSET_READING_SUCCESS:
      return Object.assign({}, state, {
        readings: [...state.readings, ...action.reading]
      })

    default:
      return state
  }
}

export default assetReadingReducer
