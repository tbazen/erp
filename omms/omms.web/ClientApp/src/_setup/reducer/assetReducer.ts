import { Reducer } from 'redux'
import * as ActionTypes from './../actions/bactionTypes'
import { AssetState } from '../../_infrastructure/state/assetState'

let intialState: AssetState = {
  assets: [],
  loading: false,
  error: false,
  errorMessage : undefined,
  filtering:false
}

let errorState : AssetState = {
  assets:[],
  loading : false,
  error: true,
  errorMessage:'Something went wrong',
  filtering:false
}

let loadingState : AssetState ={
  assets:[],
  loading:true,
  error:false,
  errorMessage : undefined,
  filtering:false
}

const assetReducer: Reducer<AssetState> = (state = intialState,action): AssetState => {
  let { AssetActions } = ActionTypes
  switch (action.type) {
    
    case AssetActions.LOADING:
      return loadingState;
    case AssetActions.ERROR:
      return errorState;

    
    case AssetActions.FILTERING:
      return Object.assign({},state,{filtering:true})  
    
    case AssetActions.CANCEL_FILTERING:
    return Object.assign({},state,{filtering:false})  
    
    case AssetActions.REMOVE_ASSET_BY_WFID :
      return Object.assign({}, state, {
        assets: state.assets.filter(t => t.wfid != action.wfid)
      })

    case AssetActions.LOAD_ASSETS_SUCCESS:
      return Object.assign(
        {}, 
        state, 
        { 
          assets: action.assets,
          loading: false,
          error: false,
          errorMessage : undefined,
          filtering:false 
        })

    case AssetActions.LOAD_ASSET_SUCCESS:
      return Object.assign(
        {}, 
        state, 
        { 
          assets: action.asset,
          loading: false,
          error: false,
          errorMessage : undefined,
          filtering : false
        })

    case AssetActions.ADD_NEW_ASSET_SUCCESS:
      return Object.assign({}, state, {
        assets: [...state.assets, ...action.asset]
      })

    case AssetActions.DELETE_ASSET_SUCCESS:
      return Object.assign({}, state, {
        assets: state.assets.filter(t => t.id != action.assetId)
      })

    case AssetActions.UPDATE_ASSET_SUCCESS:
      let index = state.assets.findIndex(t => t.id == action.asset.id)
      state.assets[index] = action.asset
      return Object.assign({}, state, { assets: [...state.assets] })

    default:
      return state
  }
}

export default assetReducer
