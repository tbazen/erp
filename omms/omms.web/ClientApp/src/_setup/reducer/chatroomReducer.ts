import { Reducer } from 'redux'
import * as ActionTypes from './../actions/bactionTypes'
import { ChatroomState } from '../../_infrastructure/state/chatroomState'

let intialState: ChatroomState = {
  threadId: '',
  memberId: '',
  messages: []
}

const chatroomReducer: Reducer<ChatroomState> = (
  state = intialState,
  action
): ChatroomState => {
  switch (action.type) {
    case ActionTypes.EmployeeActions.SETUP_CHATROOM:
      return Object.assign({}, state, {
        threadId: action.threadId,
        memberId: action.memberId
      })

    case ActionTypes.EmployeeActions.LOAD_MESSAGES_SUCCESS:
      return Object.assign({}, state, { messages: action.messages })

    case ActionTypes.EmployeeActions.SEND_MESSAGE_SUCCESS:
      return Object.assign({}, state, {
        messages: [...state.messages, action.message]
      })

    default:
      return state
  }
}

export default chatroomReducer
