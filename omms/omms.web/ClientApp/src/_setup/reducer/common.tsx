import { VIEW_TASK } from '../actions/actionTypes'

const initState = {}

export default function commonReducer(state = initState, action: any) {
  switch (action.type) {
    case VIEW_TASK:
      return {
        ...state,
        tasks: action.payload
      }
    default:
      return state
  }
}
