import { Reducer } from 'redux'
import { DrawerState } from '../../_infrastructure/state/drawer-state';
import {DrawerActions} from './../actions/bactionTypes'

let intialState: DrawerState = {
    index: 0
}


const drawerReducer: Reducer<DrawerState> = (state = intialState,action): DrawerState => {
  switch (action.type) {
    case DrawerActions.CHANGE_ACTIVE_INDEX :
      return Object.assign({},state,{index:action.index})  
    default:
      return state
  }
}

export default drawerReducer
