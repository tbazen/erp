import { Reducer } from 'redux'
import { EmployeeReportActions } from '../actions/bactionTypes';
import { EmployeeReportState } from '../../_infrastructure/state/employee-report-state';
import { EmployeeFilter } from '../actions/employee-report-actions';
import { EmployeeReportModel } from '../../_infrastructure/model/employee-report-model';
import { terminateTask } from '../actions/taskActions';

let intialState: EmployeeReportState = {
  report: [],
  filteredReport:[],
  loading: false,
  error: false,
  errorMessage : undefined,
  filtering :false,
}

let errorState : EmployeeReportState = {
  report:[],
  filteredReport:[],
  loading : false,
  error: true,
  errorMessage:'Failed to load employee report!',
  filtering:false
}

let loadingState : EmployeeReportState ={
  report:[],
  filteredReport:[],
  loading:true,
  error:false,
  errorMessage : undefined,
  filtering:false
}


export const employeeReportReducer: Reducer<EmployeeReportState> = (state = intialState,action ): EmployeeReportState => {
  switch (action.type) {
      case EmployeeReportActions.LOAD_EMPLOYEE_REPORT:
         return Object.assign({},state,{...loadingState})
      

      case EmployeeReportActions.LOAD_EMPLOYEE_REPORT_ERROR:
         return Object.assign({},state,{...errorState})
      

      case EmployeeReportActions.LOAD_EMPLOYEE_REPORT_SUCCESS:
         return Object.assign(
             {},
             state,
             {
                loading:false,
                error:false,
                errorMessage : undefined,
                filtering:false,
                report:action.report
             })
      
      case EmployeeReportActions.FILTER_EMPLOYEE_REPORT:
         let filterParams : EmployeeFilter[] = action.filterParams 
         let newList : EmployeeReportModel[] = []
         state.report.forEach(rep => {
            if(matchesWithFilterParams(rep,filterParams))
               newList.push(rep)
            
         })
         return Object.assign(
            {},
            state,
            {
               filteredReport:newList,
               filtering: true
            }
         )       

      case EmployeeReportActions.CANCE_FILTER_EMPLOYEE_REPORT:
         return Object.assign(
            {},
            state,
            {
               filteredReport:[],
               filtering:false
            })
      default :
         return state
  }
}

const matchesWithFilterParams = (report : EmployeeReportModel , filterParams : EmployeeFilter[]) : boolean =>{
   let match = false;
   filterParams.forEach(filterAttribute =>{
      if( (filterAttribute == EmployeeFilter.AssignedEmplyees) && (report.totalTasks > 0) )
       match = true;
      if( (filterAttribute == EmployeeFilter.UnAssignedEmployees) && (report.totalTasks == 0) )
        match = true;
      if( (filterAttribute ==EmployeeFilter.WithOngoingTask) && (report.onGoingTasks >0 ))
        match = true;
      if( (filterAttribute == EmployeeFilter.WithCompletedTask ) && (report.completedTasks >0 ))    
        match = true;
      if( (filterAttribute ==  EmployeeFilter.WithCancelledTasks) &&  (report.cancelledTasks > 0 ) )    
          match = true
      if( (filterAttribute == EmployeeFilter.WithTerminatedTasks)  && (report.terminatedTasks > 0 ))
          match = true 
   });
   return match;
}