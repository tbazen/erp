import { Reducer } from 'redux'
import { EmployeeActions } from './../actions/actionTypes'
import * as ActionTypes from './../actions/actionTypes'
import { EmployeeState } from '../../_infrastructure/state/employee-state';



let initialState: EmployeeState = {
  employees: [],
  filtering : false,
  loading:false,
  error:false,
  errorMessage:undefined
}

let loadingState: EmployeeState = {
  employees: [],
  filtering : false,
  loading:true,
  error:false,
  errorMessage:undefined
}
let errorState: EmployeeState = {
  employees: [],
  filtering : false,
  loading:false,
  error:true,
  errorMessage:'some thing went wrong'
}

export const employeeReducer: Reducer<EmployeeState> = (state: EmployeeState = initialState,action): EmployeeState => {
  switch (action.type) {
    
    case EmployeeActions.FILTERING:
      return Object.assign({},state,{filtering:true})
    
    case EmployeeActions.LOADING_EMPLOYEE:
      return loadingState
    case EmployeeActions.ERROR_LOADING_EMPLOYEE:
      return errorState
      
    case EmployeeActions.CANCEL_FILTERING:
      return Object.assign({},state,{filtering:false}) 
    
    case EmployeeActions.LOAD_ALL_EMPLOYEES_SUCCESS:
      return Object.assign({}, state, { employees: action.employees,filtering:false,loading:false,error:false,errorMessage:undefined })

    case ActionTypes.EmployeeActions.LOAD_TEAM_MEMBERS_SUCCESS:
      return Object.assign({}, state, { employees: action.teamMembers })
    default:
      return state
  }
}
