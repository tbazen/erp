import { Reducer } from 'redux'
import  {FilterActions} from './../actions/bactionTypes'
import { FilterState } from '../../_infrastructure/state/filter-state';
import { TaskResponse } from '../../_infrastructure/model/task-model';
import { AssetResponse } from '../../_infrastructure/model/assetModel';
import { TeamModel } from '../../_infrastructure/model/teamModel';
import { IUserInformation } from '../../_infrastructure/model/userInformationModel';



let intialState: FilterState = {
    filteredTeam:[],
    filteredAsset: [],
    filteredTask:[],
    filteredEmployee: []
}


const filterReducer: Reducer<FilterState> = (state = intialState,action): FilterState => {
    let {name} = action   
    let filtered : TeamModel[] | TaskResponse[] | AssetResponse[] | IUserInformation[]

    switch (action.type) {
        
        case FilterActions.FILTER_TEAM:
            let teams:TeamModel[] = action.teams
            filtered = teams.filter(tm=>
                tm.name.length > name.length
                ? 
                tm.name.substr(0,name.length).toLowerCase() == name.toLowerCase()
                :
                tm.name.toLowerCase() == name.substr(0,tm.name.length).toLowerCase()
                )        
        return Object.assign({}, state, { filteredTeam: filtered})
        



        case FilterActions.FILTER_ASSET:
            let assets : AssetResponse[] = action.assets 
            
            filtered = assets.filter(
                a=>
                a.name.length > name.length
                ? 
                a.name.substr(0,name.length).toLowerCase() == name.toLowerCase()
                :
                a.name.toLowerCase() == name.substr(0,a.name.length).toLowerCase()
                )
        
        return Object.assign({},state,{filteredAsset:filtered});
        
        case FilterActions.FILTER_EMPLOYEE:
            let employees : IUserInformation[] = action.employees 
            filtered = employees.filter(
                e=>
                e.userName.length > name.length
                ? 
                e.userName.substr(0,name.length).toLowerCase() == name.toLowerCase()
                :
                e.userName.toLowerCase() == name.substr(0,e.userName.length).toLowerCase()
                )
        
        return Object.assign({},state,{filteredEmployee:filtered});
        
        

        case FilterActions.FILTER_TASK:
            let tasks : TaskResponse[] = action.tasks
            filtered = tasks.filter(
                tsk=>
                tsk.name.length > name.length
                ? 
                tsk.name.substr(0,name.length).toLowerCase() == name.toLowerCase()
                :
                tsk.name.toLowerCase() == name.substr(0,tsk.name.length).toLowerCase()            
            )
        return Object.assign({},state,{filteredTask:filtered})


        case FilterActions.FILTER_TASK_BY_STATE:
            let taskList : TaskResponse[] = action.tasks
            let stt: string = action.state
            filtered = taskList.filter(tsk=> tsk.state!=null && tsk.state.toLowerCase().includes(stt.toLowerCase()))
        return Object.assign({},state,{filteredTask:filtered})

        default:
        return state
    }
}

export default filterReducer