import { combineReducers } from 'redux'
import { employeeReducer } from './employeeReducer'
import commonReducer from './common'
import { taskReducer } from './taskReducer'
import teamReducer from './teamReducer'
import loginReducer from './login'
import threadReducer from './threadReducer'
import chatroomReducer from './chatroomReducer'
import assetReducer from './assetReducer'
import assetReadingReducer from './assetReadingReduces'
import notificationReducer from './notificationReducer'
import workflowReducer from './workflow-reducer'
import { loadingModalReducer } from './loading-modal-reducer'
import { notesReducer } from './notesReducer'
import filterReducer from './filter-reducer'
import drawerReducer from './drawer-reducer'
import metadataReducer from './metadata-reducer'
import WFNoteModalReducer from './wfnote-modal-reducer'
import { employeeReportReducer } from './employee-report-reducer'
import { teamReportReducer } from './team-report-reducer'
import { assetReportReducer } from './asset-report-reducer'
import { taskReportReducer } from './task-report-reducer'

const rootReducer = combineReducers({
  asset: assetReducer,
  assetReading: assetReadingReducer,
  common: commonReducer,
  chatroom: chatroomReducer,
  employee: employeeReducer,
  loadingModal:loadingModalReducer,
  login: loginReducer,
  note: notesReducer,
  notification: notificationReducer,
  tasks: taskReducer,
  team: teamReducer,
  thread: threadReducer,
  workflow: workflowReducer,
  filter:filterReducer,
  drawer:drawerReducer,
  metadata : metadataReducer,
  noteModal : WFNoteModalReducer,

  //Report reducers
  employeeReport : employeeReportReducer,
  teamReport : teamReportReducer,
  assetReport : assetReportReducer,
  taskReport : taskReportReducer
})

export default rootReducer
