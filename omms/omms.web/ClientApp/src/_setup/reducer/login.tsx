import { combineReducers, Reducer } from 'redux'
import { GET_USER_INFO } from '../actions/actionTypes'

let initialState = {}

export const userInfo: Reducer<any> = (
  state: any = initialState,
  action
): any => {
  switch (action.type) {
    case GET_USER_INFO:
      return action.info
    default:
      return state
  }
}

const loginReducer = combineReducers({
  userInfo: userInfo
})

export default loginReducer
