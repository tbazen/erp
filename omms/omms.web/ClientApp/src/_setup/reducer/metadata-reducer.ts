import { MetadataState } from '../../_infrastructure/state/metadata-state';
import {MetaDataActions} from '../actions/bactionTypes'
import { Reducer } from 'redux';
import { MetaData } from '../../_infrastructure/model/metadata-model';

// let intialState: MetadataState = {
//     metadata:{
//         pendingItems:0,
//         rejectedItems:0
//     }
// }
let intialState: MetaData = {
    pendingItems:0,
    rejectedItems:0,
    newIndividualTasks:0,
    newTeamTasks:0
}
const metadataReducer: Reducer<MetaData> = (state = intialState,action ): MetaData => {
  // let {metadata} = state 
  switch (action.type) { 
    case MetaDataActions.LOAD_METADATA_SUCCESS:
      return action.metadata

     case MetaDataActions.DECREASE_PENDING_ITEMS:
       return Object.assign({},state,{pendingItems:state.pendingItems-action.size})  
      
    case MetaDataActions.INCREASE_PENDING_ITEMS:
      return Object.assign({},state,{pendingItems:state.pendingItems+action.size});
      
    case MetaDataActions.DECREASE_REJECTED_ITEMS:
       return Object.assign({},state,{rejectedItems:state.rejectedItems-action.size})  

    case MetaDataActions.INCREASE_REJECTED_ITEMS:
       return Object.assign({},state,{rejectedItems:state.rejectedItems+action.size})

    case MetaDataActions.MARK_INV_TASKS_AS_SEEN:
       return Object.assign({},state,{newIndividualTasks:0})

    case MetaDataActions.MARK_TEAM_TASKS_AS_SEEN:
       return Object.assign({},state,{newTeamTasks:0})
        
    default: 
      return state;
    }
}

export default  metadataReducer