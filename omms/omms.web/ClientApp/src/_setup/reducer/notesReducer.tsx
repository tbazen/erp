import { Reducer } from 'redux'
import {
  NoteActions
} from '../actions/actionTypes'
import { NoteState } from '../../_infrastructure/state/note-state';



let initialState: NoteState = { 
  notes: [] ,
  loading:false,
  error:false,
  errorMessage:undefined
}

let errorState:NoteState={
  notes: [] ,
  loading:false,
  error:true,
  errorMessage:'Something went wrong'
}

let loadingState : NoteState = {
  notes: [] ,
  loading:true,
  error:false,
  errorMessage:undefined
}

export const notesReducer: Reducer<NoteState> = (state=initialState,action): NoteState => {
  switch (action.type) {

    case NoteActions.LOADING:
      return Object.assign({},state,{...loadingState})
    
    case NoteActions.LOAD_NOTES_ERROR:
      return Object.assign({},state,{...errorState})  

    case NoteActions.LOAD_NOTES_SUCCESS:
      return Object.assign({},
        state, 
        {
          notes: action.notes,
          loading:false,
          error:false,
          errorMessage:undefined
        })

    case NoteActions.LOAD_NOTE_SUCCESS:
      return Object.assign({},
        state, 
        {
          notes: action.notes,
          loading:false,
          error:false,
          errorMessage:undefined
        })

    case NoteActions.CREATE_NOTE: 
      return Object.assign({}, 
        state,
        {
          notes:[...state.notes,action.note],
          loading:false,
          error:false,
          errorMessage:undefined
        })
    default:
      return state
  }
}
