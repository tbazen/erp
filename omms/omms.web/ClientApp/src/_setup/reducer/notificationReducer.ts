import { Reducer } from 'redux'
import * as ActionTypes from './../actions/bactionTypes'
import { NotificationState } from '../../_infrastructure/state/notificationState'

let intialState: NotificationState = {
  notifications: []
}

const notificationReducer: Reducer<NotificationState> = (
  state = intialState,
  action
): NotificationState => {
  let { NotificationActions } = ActionTypes
  switch (action.type) {
    case NotificationActions.LOAD_ALL_NOTIFICATIONS_SUCCESS:
      return Object.assign({}, state, { assets: action.notifications })

    case NotificationActions.SET_NOTIFICATION_SUCCESS:
      return Object.assign({}, state, {
        notifications: [...state.notifications, ...action.notifications]
      })

    case NotificationActions.LOAD_NEW_NOTIFICATIONS_SUCCESS:
      return Object.assign({}, state, { notifications: action.notifications })
    default:
      return state
  }
}

export default notificationReducer
