import { Reducer } from 'redux'
import { TaskReportState } from '../../_infrastructure/state/task-report-state';
import { TaskReportActions } from '../actions/bactionTypes';
import { TaskReportModel } from '../../_infrastructure/model/task-report-model';
import { TaskReportFilter } from '../actions/task-report-actions';

let intialState: TaskReportState = {
  report: {
     reports:[],
     periodicTasks:{
        ongoing:0,
        completed:0,
        cancelled:0,
        terminated:0
     },
     incidentTasks:{
      ongoing:0,
      completed:0,
      cancelled:0,
      terminated:0
   },
   scheduledTasks:{
      ongoing:0,
      completed:0,
      cancelled:0,
      terminated:0
   },
   unscheduledTasks:{
      ongoing:0,
      completed:0,
      cancelled:0,
      terminated:0
   }
  },
  filteredReport:[],
  loading: false,
  error: false,
  errorMessage : undefined,
  filtering :false,
}

let errorState : TaskReportState = {
   report: {
      reports:[],
      periodicTasks:{
         ongoing:0,
         completed:0,
         cancelled:0,
         terminated:0
      },
      incidentTasks:{
       ongoing:0,
       completed:0,
       cancelled:0,
       terminated:0
    },
    scheduledTasks:{
       ongoing:0,
       completed:0,
       cancelled:0,
       terminated:0
    },
    unscheduledTasks:{
       ongoing:0,
       completed:0,
       cancelled:0,
       terminated:0
    }
   },
 
  filteredReport:[],
  loading : false,
  error: true,
  errorMessage:'Failed to load task report!',
  filtering:false
}

let loadingState : TaskReportState ={
   report: {
      reports:[],
      periodicTasks:{
         ongoing:0,
         completed:0,
         cancelled:0,
         terminated:0
      },
      incidentTasks:{
       ongoing:0,
       completed:0,
       cancelled:0,
       terminated:0
    },
    scheduledTasks:{
       ongoing:0,
       completed:0,
       cancelled:0,
       terminated:0
    },
    unscheduledTasks:{
       ongoing:0,
       completed:0,
       cancelled:0,
       terminated:0
    }
  },
  filteredReport:[],
  loading:true,
  error:false,
  errorMessage : undefined,
  filtering:false
}

let cancelFilter :{
    filteredReport:[],
    filtering:false
}

export const taskReportReducer: Reducer<TaskReportState> = (state = intialState,action ): TaskReportState => {
  switch (action.type) {
      case TaskReportActions.LOAD_TASK_REPORT:
         return Object.assign({},state,{...loadingState})
      

      case TaskReportActions.LOAD_TASK_REPORT_ERROR:
         return Object.assign({},state,{...errorState})
      

      case TaskReportActions.LOAD_TASK_REPORT_SUCCESS:
         return Object.assign(
             {},
             state,
             {
                loading:false,
                error:false,
                errorMessage : undefined,
                filtering:false,
                report:action.report
             })
      

      case TaskReportActions.FILTER_TASK_REPORT:
         let filterParams : TaskReportFilter[] = action.filterParams 
         let newList : TaskReportModel[] = []
         state.report.reports.forEach(rep => {
            if(matchesWithFilterParams(rep,filterParams))
               newList.push(rep)
            
         })
         return Object.assign(
            {},
            state,
            {
               filteredReport:newList,
               filtering: true
            }
         )       


      case TaskReportActions.CANCE_FILTER_TASK_REPORT:
         return Object.assign(
            {},
            state,
            {
               filteredReport:[],
               filtering:false
            })         
      

      default :
         return state
  }
}

const matchesWithFilterParams = (report : TaskReportModel , filterParams : TaskReportFilter[]) : boolean =>{
   let match = false;
   filterParams.forEach(filterAttribute =>{
      if( (filterAttribute == TaskReportFilter.EMPLOYEE) && (report.assigneeType == 'Employee') )
       match = true;
      if( (filterAttribute == TaskReportFilter.TEAM) && (report.assigneeType == 'Team') )
        match = true;


      if( (filterAttribute ==TaskReportFilter.ONGOING_TASKS) && (report.status =='On going task' ))
        match = true;
      if( (filterAttribute == TaskReportFilter.CANCELLED_TASKS ) && (report.status =='Cancelled' ))    
        match = true;
      if( (filterAttribute ==  TaskReportFilter.COMPLETED_TASTKS) &&  (report.status =='Completed' ) )    
          match = true
      if( (filterAttribute == TaskReportFilter.TERMINATED_TASKS)  && (report.status == 'Terminated' ))
          match = true
          
      
      if( (filterAttribute ==TaskReportFilter.UNSCHEDULED_TASKS) && (report.type =='Unscheduled' ))
        match = true;
      if( (filterAttribute == TaskReportFilter.SCHEDULED_TASKS ) && (report.type =='Scheduled' ))    
        match = true;
      if( (filterAttribute ==  TaskReportFilter.PERIODIC_TASKS) &&  (report.type =='Periodic' ) )    
          match = true
      if( (filterAttribute == TaskReportFilter.INCIDENT_TASKS)  && (report.type == 'Incident' ))
          match = true
          
      
   });
   return match;
}