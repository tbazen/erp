import { Reducer } from 'redux'
import { TaskModel } from '../../_infrastructure/model/taskFromModel'
import {
  CREATE_TASK_SUCCESS,
  EDIT_TASK_SUCCESS, NoteActions,
  TaskActions
} from '../actions/actionTypes'
import { TaskResponse } from '../../_infrastructure/model/task-model'

export interface TaskState {
  tasks: TaskResponse[]
  loading: boolean
  error: boolean
  errorMessage : string | undefined
  filtering : boolean
}

let initialState: TaskState = { 
  loading: false, 
  tasks: [],
  error:false,
  errorMessage:undefined,
  filtering:false 
}

let errorState : TaskState = {
  tasks:[],
  loading : false,
  error: true,
  errorMessage:'Something went wrong',
  filtering:false
}

let loadingState : TaskState ={
  tasks:[],
  loading:true,
  error:false,
  errorMessage : undefined,
  filtering:false
}

export const taskReducer: Reducer<TaskState> = (state=initialState,action): TaskState => {
  switch (action.type) {
    case TaskActions.ERROR:
      return Object.assign({},errorState)
    case  TaskActions.LOADING:
      return Object.assign({},loadingState)
    case TaskActions.FILTERING:
      return Object.assign({},state,{filtering:true})  
    case TaskActions.CANCEL_FILTERING:
      return Object.assign({},state,{filtering:false})
    case TaskActions.CANCEL_TASK:
      return action.payload
    case TaskActions.TERMINATE_TASK:
      return action.payload
    case TaskActions.COMPLETE_TASK:
      return action.payload
    

    case TaskActions.REMOVE_TASK_BY_WFID:
      let newList = state.tasks.filter((t: TaskResponse) => t.wfid != action.wfid)
      return Object.assign({}, 
        state,
        { 
          tasks: newList,
          loading: false,
          error: false,
          errorMessage : undefined,
          filtering:false 
        })
      
    case TaskActions.LOAD_TASKS_SUCCESS:
      return Object.assign(
        {}, 
        state, 
        { 
          tasks: action.tasks,
          loading: false,
          error: undefined,
          errorMessage : undefined
        })
    case TaskActions.LOAD_ASSET_TASKS:
      return Object.assign(
        {},
        state,
        {
          tasks: action.tasks,
          loading: false,
          error: undefined,
          errorMessage : undefined
        })
    case TaskActions.DELETE_TASK_SUCCESS:
      let temp = state.tasks.filter(
        (a: TaskResponse) => a.id == action.taskId.toString()
      )
      return Object.assign(
        {}, 
        state, 
        { 
          tasks: temp,
          loading: false,
          error: undefined,
          errorMessage : undefined
        })
    case CREATE_TASK_SUCCESS:
      return action.payload
    case EDIT_TASK_SUCCESS:
      return action.payload
    case TaskActions.LOAD_PENDING_TASKS_SUCCESS:
      return Object.assign({}, state, { tasks: action.tasks })
    case TaskActions.CLEAR_TASK_LIST_SUCCESS:
       return  Object.assign({},initialState)
    default:
      return state
  }
}
