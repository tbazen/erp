import { Reducer } from 'redux'
import { TeamReportActions } from '../actions/bactionTypes';
import { TeamReportState } from '../../_infrastructure/state/team-report-state';
import { TeamReportModel } from '../../_infrastructure/model/team-report-model';
import { TeamReportFilter } from '../actions/team-report-actions';

let intialState: TeamReportState = {
  report: [],
  filteredReport:[],
  loading: false,
  error: false,
  errorMessage : undefined,
  filtering :false,
}

let errorState : TeamReportState = {
  report:[],
  filteredReport:[],
  loading : false,
  error: true,
  errorMessage:'Failed to load team report!',
  filtering:false
}

let loadingState : TeamReportState ={
  report:[],
  filteredReport:[],
  loading:true,
  error:false,
  errorMessage : undefined,
  filtering:false
}

export const teamReportReducer: Reducer<TeamReportState> = (state = intialState,action ): TeamReportState => {
  switch (action.type) {
      case TeamReportActions.LOAD_TEAM_REPORT:
         return Object.assign({},state,{...loadingState})
      

      case TeamReportActions.LOAD_TEAM_REPORT_ERROR:
         return Object.assign({},state,{...errorState})
      
      case TeamReportActions.LOAD_TEAM_REPORT_SUCCESS:
         return Object.assign(
             {},
             state,
             {
                loading:false,
                error:false,
                errorMessage : undefined,
                filtering:false,
                report:action.report
             })
      case TeamReportActions.FILTER_TEAM_REPORT:
             let filterParams : TeamReportFilter[] = action.filterParams 
             let newList : TeamReportModel[] = []
             state.report.forEach(rep => {
                if(matchesWithFilterParams(rep,filterParams))
                   newList.push(rep)
             })
             return Object.assign(
                {},
                state,
                {
                   filteredReport:newList,
                   filtering: true
                }
             )
      case TeamReportActions.CANCE_FILTER_TEAM_REPORT:
         return Object.assign(
            {},
            state,
            {
               filteredReport:[],
               filtering:false               
            })         
      default :
         return state
  }
}

const matchesWithFilterParams = (report : TeamReportModel , filterParams : TeamReportFilter[]) : boolean =>{
   let match = false;
   filterParams.forEach(filterAttribute =>{
      if( (filterAttribute == TeamReportFilter.AssignedTeams) && (report.totalTasks > 0) )
       match = true;
      if( (filterAttribute == TeamReportFilter.UnAssignedTeams) && (report.totalTasks == 0) )
        match = true;
      if( (filterAttribute ==TeamReportFilter.WithOngoingTask) && (report.onGoingTasks >0 ))
        match = true;
      if( (filterAttribute == TeamReportFilter.WithCompletedTask ) && (report.completedTasks >0 ))    
        match = true;
      if( (filterAttribute ==  TeamReportFilter.WithCancelledTasks) &&  (report.cancelledTasks > 0 ) )    
          match = true
      if( (filterAttribute == TeamReportFilter.WithTerminatedTasks)  && (report.terminatedTasks > 0 ))
          match = true 
   });
   return match;
}