import { Reducer } from 'redux'
import { TeamState } from '../../_infrastructure/state/teamState'
import {TeamActions} from './../actions/actionTypes'

let intialState: TeamState = {
  teams: [],
  loading: false,
  error: false,
  errorMessage : undefined,
  filtering :false
}

let errorState : TeamState = {
  teams:[],
  loading : false,
  error: true,
  errorMessage:'Something went wrong',
  filtering:false
}

let loadingState : TeamState ={
  teams:[],
  loading:true,
  error:false,
  errorMessage : undefined,
  filtering:false
}

const teamReducer: Reducer<TeamState> = (state = intialState,action ): TeamState => {
  switch (action.type) {
    
    case TeamActions.ERROR:
      return Object.assign({},errorState)
    
    case  TeamActions.LOADING:
      return Object.assign({},loadingState)
    
    case TeamActions.FILTERING:
      return Object.assign({},state,{filtering:true})  
    case TeamActions.CANCEL_FILTERING:
      return Object.assign({},state,{filtering:false})

    case TeamActions.LOAD_ALL_TEAMS_SUCCESS:
      return Object.assign({}, state, {
        teams: action.teamList,
        loading: false,
        error: undefined
      })
    
    case TeamActions.ADD_NEW_TEAM_SUCCESS:
      return Object.assign({}, state, {
        teams: [...state.teams, ...action.team]
      })
    

    case TeamActions.DELETE_TEAM_SUCCESS:
      return Object.assign({}, state, {
        teams: state.teams.filter(t => t.id != action.teamId)
      })
    
    case TeamActions.REMOVE_TEAM_BY_WFID:
      return Object.assign({}, state, {
        teams: state.teams.filter(t => t.wfid != action.wfid)
      })
    
    case TeamActions.UPDATE_TEAM_SUCCESS:
      let index = state.teams.findIndex(t => t.id == action.team.id)
      state.teams[index] = action.team
      return Object.assign({}, state, { teams: [...state.teams] })

    
    case TeamActions.LOAD_ALL_EMPLOYEE_TEAMS_SUCCESS:
      return Object.assign({}, state, {
        teams: action.teamList,
        loading: false,
        error: undefined
      })

    default:
      return state
  }
}

export default teamReducer
