import { Reducer } from 'redux'
import * as ActionTypes from './../actions/bactionTypes'
import { ThreadModel } from '../../_infrastructure/model/threadModel'

interface ThreadState {
  threads: ThreadModel[]
}

let initialState: ThreadState = {
  threads: []
}

const threadReducer: Reducer<ThreadState> = (
  state = initialState,
  action
): ThreadState => {
  switch (action.type) {
    case ActionTypes.EmployeeActions.LOAD_EMPLOYEE_THREADS_SUCCESS:
      return Object.assign({}, state, { threads: action.threads })
    default:
      return state
  }
}
export default threadReducer
