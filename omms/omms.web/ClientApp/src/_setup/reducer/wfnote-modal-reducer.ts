import { Reducer } from 'redux'
import * as ActionTypes from './../actions/bactionTypes'
import { WFModalState } from 'src/_infrastructure/state/workflow-note-modal-state'
import { WFNoteActions } from '../actions/wfnote-modal-actions';

let initialState: WFModalState = {
  open: false,
  note: '',
  submit:false,
  modalAction : WFNoteActions.REJECTION,
  wfid :undefined,
  WFType : undefined ,
}


const WFNoteModalReducer: Reducer<WFModalState> = (state = initialState,action): WFModalState => {
  let { WFNoteModalActions } = ActionTypes
  switch (action.type) {
    case WFNoteModalActions.OPEN:
      return Object.assign(
        {}, 
        state,
        {
          open:true,
          note:'',
          submit:false,
          wfid:action.wfid,
          WFType : action.WFType,
          modalAction : action.modalAction
        })

    case WFNoteModalActions.CANCEL:
      return Object.assign(
        {},
        state,
        {
          open:false,
          note:'',
          submit:false,
          wfid:undefined,
          WFType : undefined,
          modalAction :WFNoteActions.REJECTION
        })
    
    case WFNoteModalActions.SUBMIT:
      return Object.assign(
        {},
        state,
        {
          open:false,
          note:action.note,
          submit:true,
          wfid:undefined,
          WFType : undefined ,
          modalAction :WFNoteActions.REJECTION
       })    
    default:
      return state
  }
}

export default WFNoteModalReducer