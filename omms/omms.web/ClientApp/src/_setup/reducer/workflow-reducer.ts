import { Reducer } from 'redux'
import * as ActionTypes from './../actions/bactionTypes'
import { AssetState } from '../../_infrastructure/state/assetState'
import { WorkflowState } from '../../_infrastructure/state/workflow-state'

let intialState: WorkflowState = {
  workflowItems: []
}

const workflowReducer: Reducer<WorkflowState> = (state = intialState,action): WorkflowState => {
  let { WorkflowActions } = ActionTypes
  switch (action.type) {
    case WorkflowActions.LOAD_PENDING_ITEMS_SUCCESS:
      return Object.assign({}, state, { workflowItems: action.items })
    default:
      return state
  }
}

export default workflowReducer
