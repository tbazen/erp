import Axios,{CancelTokenSource} from 'axios';
import { baseUrl } from './url.config';
import config from './header.config';
import axiosCancel from 'axios-cancel';
import {  AssetReading, AssetRequest } from '../../_infrastructure/model/assetModel';
import AssetTokenFactory from './request-processor/asset.tokenfactory';
import { AssetSearhParams } from '../../_infrastructure/model/asset-search-params';
import { WFRequestModel } from '../../_infrastructure/model/workflow-model';


const enum  AssetRequestId{
    GET_ASSETS = 'GET ASSETS'
}

export default class AssetService {
    TokenFactory : AssetTokenFactory
    constructor() {
         
        this.TokenFactory = new AssetTokenFactory()

        this.addAssetReading = this.addAssetReading.bind(this)
        this.createAsset=this.createAsset.bind(this)
        this.deleteAsset = this.deleteAsset.bind(this)

        this.loadAssetReading = this.loadAssetReading.bind(this)
        
        
        this.getPendingAssets = this.getPendingAssets.bind(this)
        this.loadAssets = this.loadAssets.bind(this)
        this.getRejectedAssets = this.getRejectedAssets.bind(this)
        
        this.updateAsset = this.updateAsset.bind(this)
        this.rejectAssetRegistration = this.rejectAssetRegistration.bind(this)
        this.getAssetReadingFormSchema = this.getAssetReadingFormSchema.bind(this)
        
        this.getAssetInfoByPUKID = this.getAssetInfoByPUKID.bind(this)
        
        this.getAssetReport = this.getAssetReport.bind(this)
    }
    //#region Asset action API calls
    loadAssets(){
        let url = `${baseUrl}employee/get-all-asset`
        this.TokenFactory.cancelAllGetRequest()
        this.TokenFactory.renewGetRegisteredAssetReq()
        return Axios.get(url,{...config,cancelToken: this.TokenFactory.REGISTERED_ASSETS_TOKEN.token})
    }

    createAsset(data:AssetRequest,wfid: string | null){
        let url  = `${baseUrl}register-asset/${wfid}`
        return Axios.post(url,data,config)
    }

    

    deleteAsset(assetId:number){
        let url = `${baseUrl}admin/delete-asset?assetId=${assetId}`
        return Axios.delete(url,config)
    }
    //#endregion

    //#region Asset reading API calls
    loadAssetReading(assetId:number){
        let url = `${baseUrl}asset/get-asset-readings/${assetId}`
        return Axios.get(url,config)
    }

    addAssetReading(reading:AssetReading){
        let url = `${baseUrl}asset/add-asset-reading`
        return Axios.post(url,reading,config)
    }
    //#endregion

    //#region Asset form schema API  call
    getAssetFormSchema() {
        let url = `${baseUrl}asset/get-asset-form-schema`
        return Axios.get(url,config)
    }

    requestRegstration(asset : AssetRequest){
        let url= `${baseUrl}asset/register-asset`
        return Axios.post(url,asset,config)
    }

    updateAsset(newData : AssetRequest,wfid: string){
        let url = `${baseUrl}asset/register-asset/${wfid}`
        return Axios.post(url,newData,config)
    }

    getPendingAssets(){
        let url = `${baseUrl}asset/get-pending-assets`
        this.TokenFactory.cancelAllGetRequest()
        this.TokenFactory.renewGetPendingAssetReq()
        return Axios.get(url,{...config,cancelToken:this.TokenFactory.PENDING_ASSET_TOKEN.token})
    }

    getAsset(assetId:number){
        let url = `${baseUrl}asset/get-asset-byid/${assetId}`
        return Axios.get(url,config)
    }

    approveAssetRegistration(wfid:string){

        let url =`${baseUrl}asset/approve-asset/${wfid}`
        return Axios.get(url,config)
    }

    rejectAssetRegistration(request : WFRequestModel){
        let url = `${baseUrl}asset/reject-asset-request`
        return Axios.post(url,request,config)
    }

    getRejectedAssets(){
        let url = `${baseUrl}asset/get-rejected-assets`
        this.TokenFactory.cancelAllGetRequest()
        this.TokenFactory.renewGetRejectedAssetReq()
        return Axios.get(url,{...config,cancelToken:this.TokenFactory.REJECTED_ASSET_TOKEN.token})
    }

    getAssetByFid(fid:string){
        let url = `${baseUrl}asset/get-asset-by-location/${fid}`
        return Axios.get(url,config)
    }
    
    //#endregion

    getAssetReadingFormSchema(assetType:number){
        let url = `${baseUrl}asset/get-asset-reading-schema/${assetType}`
        return Axios.get(url,config)
    }

    getAssetInfoByPUKID(searchParam : AssetSearhParams){
        let url = `${baseUrl}asset/get-asset-byPUKID`
        return Axios.post(url,searchParam,config)
    }

    getAssetReport(){
        let url = `${baseUrl}asset/asset-report`
        return Axios.get(url,config)
    }
}