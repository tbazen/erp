import Axios from 'axios'
import { baseUrl } from './url.config'
import { IloginViewModel } from '../../modules/shared/login/login'
import config from './header.config'

export default class AuthService {
  constructor() {
    this.Login = this.Login.bind(this)
    this.Logout = this.Logout.bind(this)
  }

  CheckUser() {
    let url = `${baseUrl}user/checkuser`
    return Axios.get(url, config)
  }

  Logout() {
    let path = 'user/logout'
    return Axios.get(baseUrl + path, config)
  }

  Login(account: IloginViewModel) {
    var path = 'user/login'
    return Axios.post(baseUrl + path, account, config)
  }

  getUnauthorizedMsg() {
    let path = 'user/emp'
    return Axios.get(baseUrl + path, config)
  }

  getProfile() {
    let path = 'user/profile'
    return Axios.get(baseUrl + path, config)
  }
}
