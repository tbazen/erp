import Axios from 'axios'
import { baseUrl } from './url.config'
import config from './header.config'
import { MessageRequest } from '../../_infrastructure/model/messageModel'

export default class ChatroomService {
  constructor() {
    this.loadMessages = this.loadMessages.bind(this)
    this.sendMessage = this.sendMessage.bind(this)
    this.loadEmployeeThreads = this.loadEmployeeThreads.bind(this)
  }

  loadMessages(threadId: string) {
    let url = `${baseUrl}employee/get-messages/${threadId}`
    return Axios.get(url, config)
  }

  sendMessage(message: MessageRequest) {
    let url = `${baseUrl}employee/send-message`
    return Axios.post(url, message, config)
  }

  loadEmployeeThreads(employeeId: string) {
    let url = `${baseUrl}employee/employee-thread/${employeeId}`
    return Axios.get(url, config)
  }
}
