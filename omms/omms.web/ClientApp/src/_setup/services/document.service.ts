import Axios from 'axios';
import { baseUrl } from './url.config';
import config from './header.config';
import documentConfig from './document.config';


export default class DocumentService {
    constructor() {
        this.loadImage = this.loadImage.bind(this)
        this.startDownload = this.startDownload.bind(this)
        this.downloadFile = this.downloadFile.bind(this)
    }
    
    private startDownload(dataBlob : Blob ,documentId :string){
        if (window.navigator && window.navigator.msSaveOrOpenBlob) {
            window.navigator.msSaveOrOpenBlob(dataBlob, documentId);
         } else {
            let urlObject = URL.createObjectURL(dataBlob);
            let documentLink = document.getElementById(documentId)as HTMLAnchorElement ;
             documentLink.href = urlObject
             documentLink.setAttribute('download', documentLink.title);
             documentLink.click();
             URL.revokeObjectURL(urlObject);
        }
    }

    loadImage(docId:string){
        let url = `${baseUrl}employee/download/${docId}`
        return Axios.get(url,{
                                 responseType:'blob',
                                 withCredentials:true
                                })
        .then(res=>{
            let blob = res.data
            //this.startDownload(blob,docId)
            return blob
        })
        .catch(error => console.log(error))
    }

    downloadFile(docId : string){
      let url = `${baseUrl}employee/download/${docId}`
      return Axios.get(url,{
        responseType:'blob',
        withCredentials:true
      })
        .then(res=>{
          let blob = res.data
          this.startDownload(blob,docId)
        })
        .catch(error => console.log(error))
    }
}

