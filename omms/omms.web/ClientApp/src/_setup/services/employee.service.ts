import Axios from 'axios'
import { baseUrl } from './url.config'
import config from './header.config'

export default class EmployeeService {
  constructor() {
    this.loadAllEmployees = this.loadAllEmployees.bind(this)
    this.loadTeamMembers = this.loadTeamMembers.bind(this)
    this.getEmployeeReport = this.getEmployeeReport.bind(this)
  }

  loadAllEmployees() {
    let url = `${baseUrl}employee/get-all-employees`
    return Axios.get(url, config)
  }

  loadTeamMembers(teamId: string) {
    let url = `${baseUrl}employee/team-members/${teamId}`
    return Axios.get(url, config)
  }

  getEmployeeReport(){
    let url = `${baseUrl}task/employee-report`
    return Axios.get(url,config)
  }
}
