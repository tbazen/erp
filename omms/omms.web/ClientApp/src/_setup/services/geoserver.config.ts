const geoserver = {
  withCredentials: true,
  crossdomain: true,
  headers: {
    'Content-Type': '*/*1',
    Accept: '*/*',
    'Access-Control-Allow-Headers': '*',
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE,PATCH,OPTIONS'
  },
  auth: {
    username: 'admin',
    password: 'geoserver'
  }
}

export default geoserver
