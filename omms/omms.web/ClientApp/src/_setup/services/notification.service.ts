import Axios from 'axios'
import { baseUrl } from './url.config'
import config from './header.config'
import { NotificationModel } from '../../_infrastructure/model/notificationModel'

export default class NotificationService {
  constructor() {
    this.loadAllNotifications = this.loadAllNotifications.bind(this)
    this.loadNewNotifications = this.loadNewNotifications.bind(this)
    this.setNotification = this.setNotification.bind(this)
  }
  loadAllNotifications(employeeId: string) {
    let url = `${baseUrl}admin/get-notifications/${employeeId}`
    return Axios.get(url, config)
  }

  loadNewNotifications(empId: string) {
    let url = `${baseUrl}admin/get-new-notification/${empId}`
    return Axios.get(url, config)
  }

  setNotification(notification: NotificationModel) {
    let url = `${baseUrl}admin/set-notification`
    return Axios.post(url, notification, config)
  }
}
