import Axios,{CancelTokenSource} from 'axios'

export default class AssetTokenFactory{
    ASSET_SOURCE:typeof Axios.CancelToken = Axios.CancelToken

    REGISTERED_ASSETS_TOKEN:CancelTokenSource;
    REJECTED_ASSET_TOKEN:CancelTokenSource;
    PENDING_ASSET_TOKEN: CancelTokenSource;

    constructor(){
        this.REGISTERED_ASSETS_TOKEN = this.ASSET_SOURCE.source()
        this.REJECTED_ASSET_TOKEN = this.ASSET_SOURCE.source()
        this.PENDING_ASSET_TOKEN = this.ASSET_SOURCE.source()

        this.renewGetPendingAssetReq = this.renewGetPendingAssetReq.bind(this)
        this.renewGetRegisteredAssetReq = this.renewGetRegisteredAssetReq.bind(this)
        this.renewGetRejectedAssetReq = this.renewGetRejectedAssetReq.bind(this)

        this.cancelAllGetRequest = this.cancelAllGetRequest.bind(this)
        this.cancelGetPendingAssetReq = this.cancelGetPendingAssetReq.bind(this)
        this.cancelGetRegisteredAssetReq = this.cancelGetRegisteredAssetReq.bind(this)
        this.cancelGetRejectedAssetReq = this.cancelGetRejectedAssetReq.bind(this)

        this.logPromise = this.logPromise.bind(this)
    }
        
    cancelAllGetRequest(){
        this.cancelGetPendingAssetReq()
        this.cancelGetRegisteredAssetReq()
        this.cancelGetRejectedAssetReq()
    }

    cancelGetPendingAssetReq(){
        this.PENDING_ASSET_TOKEN.cancel()
    }
    renewGetPendingAssetReq(){
        this.PENDING_ASSET_TOKEN = this.ASSET_SOURCE.source()
        this.logPromise()
    }

    cancelGetRegisteredAssetReq(){
        this.REGISTERED_ASSETS_TOKEN.cancel()
    }

    renewGetRegisteredAssetReq(){
        this.REGISTERED_ASSETS_TOKEN = this.ASSET_SOURCE.source()
        this.logPromise()
    }

    cancelGetRejectedAssetReq(){
        this.REJECTED_ASSET_TOKEN.cancel()
    }
    renewGetRejectedAssetReq(){
        this.REJECTED_ASSET_TOKEN = this.ASSET_SOURCE.source()
    }

    logPromise(){
        //FIXME: Remove this log after testing 
        console.group('Token state asset tokens ')
        console.log(this.PENDING_ASSET_TOKEN)
        console.log(this.REGISTERED_ASSETS_TOKEN)
        console.log(this.REJECTED_ASSET_TOKEN)
        console.groupEnd()
    }
}
