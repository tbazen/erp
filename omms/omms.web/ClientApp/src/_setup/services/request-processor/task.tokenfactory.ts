import Axios,{CancelTokenSource} from 'axios'

export default class TaskTokenFactory{
    TASK_SOURCE:typeof Axios.CancelToken = Axios.CancelToken

    REGISTERED_TASK_TOKEN:CancelTokenSource;
    REJECTED_TASK_TOKEN:CancelTokenSource;
    PENDING_TASK_TOKEN: CancelTokenSource;

    INDIVIDUAL_TASK_TOKEN:CancelTokenSource;
    TEAM_TASK_TOKEN:CancelTokenSource;
    TASK_STATE_TOKEN:CancelTokenSource;

    ASSET_TASK_TOKEN:CancelTokenSource;
    
    constructor(){
        this.REGISTERED_TASK_TOKEN = this.TASK_SOURCE.source()
        this.REJECTED_TASK_TOKEN = this.TASK_SOURCE.source()
        this.PENDING_TASK_TOKEN = this.TASK_SOURCE.source()

        this.INDIVIDUAL_TASK_TOKEN = this.TASK_SOURCE.source()
        this.TEAM_TASK_TOKEN = this.TASK_SOURCE.source()
        this.TASK_STATE_TOKEN = this.TASK_SOURCE.source()
        this.ASSET_TASK_TOKEN = this.TASK_SOURCE.source()


        this.renewGetPendingTaskReq = this.renewGetPendingTaskReq.bind(this)
        this.renewGetRegisteredTaskReq = this.renewGetRegisteredTaskReq.bind(this)
        this.renewGetRejectedTaskReq = this.renewGetRejectedTaskReq.bind(this)

        this.renewGetIndividualTaskReq = this.renewGetIndividualTaskReq.bind(this)
        this.renewGetTeamTaskReq = this.renewGetTeamTaskReq.bind(this)
        this.renewGetTaskByStateReq = this.renewGetTaskByStateReq.bind(this)
        this.renewGetAssetTaskReq = this.renewGetAssetTaskReq.bind(this)



        this.cancelAllGetRequest = this.cancelAllGetRequest.bind(this)
        
        this.cancelGetPendingTaskReq = this.cancelGetPendingTaskReq.bind(this)
        this.cancelGetRegisteredTaskReq = this.cancelGetRegisteredTaskReq.bind(this)
        this.cancelGetRejectedTaskReq = this.cancelGetRejectedTaskReq.bind(this)
        
        this.cancelGetIndividualTaskReq = this.cancelGetIndividualTaskReq.bind(this)
        this.cancelGetTeamTaskReq  = this.cancelGetTeamTaskReq.bind(this)
        this.cancelGetTaskByStateReq = this.cancelGetTaskByStateReq.bind(this)
        this.cancelGetAssetTaskReq = this.cancelGetAssetTaskReq.bind(this)


        this.logPromise = this.logPromise.bind(this)
    }
        
    cancelAllGetRequest(){
        this.cancelGetPendingTaskReq()
        this.cancelGetRegisteredTaskReq()
        this.cancelGetRejectedTaskReq()

        this.cancelGetIndividualTaskReq()
        this.cancelGetTeamTaskReq()
        this.cancelGetTaskByStateReq()

        this.cancelGetAssetTaskReq()
    }

    cancelGetPendingTaskReq(){
        this.PENDING_TASK_TOKEN.cancel()
    }
    renewGetPendingTaskReq(){
        this.PENDING_TASK_TOKEN = this.TASK_SOURCE.source()
        this.logPromise()
    }

    cancelGetRegisteredTaskReq(){
        this.REGISTERED_TASK_TOKEN.cancel()
    }

    renewGetRegisteredTaskReq(){
        this.REGISTERED_TASK_TOKEN = this.TASK_SOURCE.source()
        this.logPromise()
    }

    cancelGetRejectedTaskReq(){
        this.REJECTED_TASK_TOKEN.cancel()
    }
    renewGetRejectedTaskReq(){
        this.REJECTED_TASK_TOKEN = this.TASK_SOURCE.source()
    }


    cancelGetIndividualTaskReq(){
        this.INDIVIDUAL_TASK_TOKEN.cancel()
    }
    renewGetIndividualTaskReq(){
        this.INDIVIDUAL_TASK_TOKEN = this.TASK_SOURCE.source()
    }

    cancelGetTeamTaskReq(){
        this.TEAM_TASK_TOKEN.cancel()
    }
    renewGetTeamTaskReq(){
        this.TEAM_TASK_TOKEN = this.TASK_SOURCE.source()
    }

    cancelGetTaskByStateReq(){
        this.TASK_STATE_TOKEN.cancel()
    }

    renewGetTaskByStateReq(){
        this.TASK_STATE_TOKEN = this.TASK_SOURCE.source()
    }
    

    cancelGetAssetTaskReq(){
        this.ASSET_TASK_TOKEN.cancel()
    }
    renewGetAssetTaskReq(){
        this.ASSET_TASK_TOKEN = this.TASK_SOURCE.source()
    }


    logPromise(){
        //FIXME: Remove this method after testing
        console.group('Token state asset tokens ')
        console.log(this.PENDING_TASK_TOKEN)
        console.log(this.REGISTERED_TASK_TOKEN)
        console.log(this.REJECTED_TASK_TOKEN)
        console.groupEnd()
    }
}
