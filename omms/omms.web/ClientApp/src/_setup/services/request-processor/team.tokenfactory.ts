import Axios,{CancelTokenSource} from 'axios'

export default class TeamTokenFactory{
    TEAM_SOUREC:typeof Axios.CancelToken = Axios.CancelToken

    REGISTERED_TEAM_TOKEN:CancelTokenSource;
    REJECTED_TEAM_TOKEN:CancelTokenSource;
    PENDING_TEAM_TOKEN: CancelTokenSource;

    GET_TEAM_BYID :CancelTokenSource;

    constructor(){
        this.REGISTERED_TEAM_TOKEN = this.TEAM_SOUREC.source()
        this.REJECTED_TEAM_TOKEN = this.TEAM_SOUREC.source()
        this.PENDING_TEAM_TOKEN = this.TEAM_SOUREC.source()
        this.GET_TEAM_BYID = this.TEAM_SOUREC.source()

        this.renewGetPendingTeamReq = this.renewGetPendingTeamReq.bind(this)
        this.renewGetRegisteredTeamReq = this.renewGetRegisteredTeamReq.bind(this)
        this.renewGetRejectedTeamReq = this.renewGetRejectedTeamReq.bind(this)
        this.renewGetTeamById = this.renewGetTeamById.bind(this)

        this.cancelAllGetRequest = this.cancelAllGetRequest.bind(this)
        this.cancelGetPendingTeamReq = this.cancelGetPendingTeamReq.bind(this)
        this.cancelGetRegisteredTeamReq = this.cancelGetRegisteredTeamReq.bind(this)
        this.cancelGetRejectedTeamReq = this.cancelGetRejectedTeamReq.bind(this)
        this.cancelGetTeamById = this.cancelGetTeamById.bind(this)

        this.logPromise = this.logPromise.bind(this)
    }
        
    cancelAllGetRequest(){
        this.cancelGetPendingTeamReq()
        this.cancelGetRegisteredTeamReq()
        this.cancelGetRejectedTeamReq()
        this.cancelGetTeamById()
    }

    cancelGetPendingTeamReq(){
        this.PENDING_TEAM_TOKEN.cancel()
    }
    renewGetPendingTeamReq(){
        this.PENDING_TEAM_TOKEN = this.TEAM_SOUREC.source()
        this.logPromise()
    }

    cancelGetRegisteredTeamReq(){
        this.REGISTERED_TEAM_TOKEN.cancel()
    }

    renewGetRegisteredTeamReq(){
        this.REGISTERED_TEAM_TOKEN = this.TEAM_SOUREC.source()
        this.logPromise()
    }

    cancelGetRejectedTeamReq(){
        this.REJECTED_TEAM_TOKEN.cancel()
    }
    renewGetRejectedTeamReq(){
        this.REJECTED_TEAM_TOKEN = this.TEAM_SOUREC.source()
    }

    renewGetTeamById(){
        this.GET_TEAM_BYID = this.TEAM_SOUREC.source()
    }
    cancelGetTeamById(){
        this.GET_TEAM_BYID.cancel()
    }
    
    logPromise(){
        //FIXME: Remove this method after testing
        console.group('Token state asset tokens ')
        console.log(this.PENDING_TEAM_TOKEN)
        console.log(this.REGISTERED_TEAM_TOKEN)
        console.log(this.REJECTED_TEAM_TOKEN)
        console.groupEnd()
    }
}
