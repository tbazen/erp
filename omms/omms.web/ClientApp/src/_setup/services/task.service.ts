import Axios from 'axios'
import { baseUrl } from './url.config'
import config from './header.config'
import { TaskModel, AssigneeViewModel } from '../../_infrastructure/model/taskFromModel';
import { NoteRequest } from '../../_infrastructure/model/task-note-model';
import TaskTokenFactory from './request-processor/task.tokenfactory';
import { WFRequestModel } from '../../_infrastructure/model/workflow-model';

export default class TaskService {
  TokenFactory:TaskTokenFactory
  constructor() {
    this.TokenFactory = new TaskTokenFactory()

    this.getAllTasks = this.getAllTasks.bind(this)
    this.getEmployeeTasks = this.getEmployeeTasks.bind(this)
    this.deleteTask = this.deleteTask.bind(this)
    this.createTask = this.createTask.bind(this)
    this.updateTask = this.updateTask.bind(this)
    this.getEmployeePendingTasks = this.getEmployeePendingTasks.bind(this)
    this.requestTaskRegistration = this.requestTaskRegistration.bind(this)

    this.getTaskNotes = this.getTaskNotes.bind(this)
    this.addNote = this.addNote.bind(this)
    this.getLatestTaskNote = this.getLatestTaskNote.bind(this)

    this.requestCancellation = this.requestCancellation.bind(this)
    this.requestCompletion = this.requestCompletion.bind(this)
    this.requestTermination = this.requestTermination.bind(this)

    this.approveState = this.approveState.bind(this)
    this.rejectState = this.rejectState.bind(this)
    this.getTaskByState = this.getTaskByState.bind(this)

    this.rejectTaskRegistration = this.rejectTaskRegistration.bind(this)
    this.getIndevidualTasks = this.getIndevidualTasks.bind(this)
    this.getTeamTasks = this.getTeamTasks.bind(this)

    this.getTaskDepndecy = this.getTaskDepndecy.bind(this)
    this.getTaskById = this.getTaskById.bind(this)

    this.getTaskMetaData = this.getTaskMetaData.bind(this)
    this.getTaskReport = this.getTaskReport.bind(this)

    this.updateTaskAssignee = this.updateTaskAssignee.bind(this)
  }

  getAllTasks() {
    let path = 'admin/getalltasks'
    this.TokenFactory.cancelAllGetRequest()
    this.TokenFactory.renewGetRegisteredTaskReq()
    return Axios.get(baseUrl + path, {...config,cancelToken:this.TokenFactory.REGISTERED_TASK_TOKEN.token})
  }

  getAssetTasks(assetId:number){
    let path = `task/asset-tasks/${assetId}`
    this.TokenFactory.cancelAllGetRequest()
    this.TokenFactory.renewGetAssetTaskReq()
    return Axios.get(baseUrl + path, {...config,cancelToken:this.TokenFactory.ASSET_TASK_TOKEN.token})
  }

  getEmployeeTasks(employeeId: string) {
    let path = `employee/employeetask/${employeeId}`
    return Axios.get(baseUrl + path)
  }

  deleteTask(taskId: string) {
    let path = `admin/deleteTask/${taskId}`
    return Axios.get(baseUrl + path)
  }

  createTask(data: TaskModel) {
    let path = `${baseUrl}officer/createtask`
    return Axios.post(path, data, config)
  }

  updateTask(newData: TaskModel) {
    let path = `${baseUrl}officer/updatetask`
    return Axios.post(path, newData, config)
  }

  getEmployeePendingTasks(employeeId: string) {
    let path = `${baseUrl}employee/get-approval-request/${employeeId}`
    return Axios.get(path, config)
  }

  //Workflow api calls
  getPendingTasks(){
    let url =`${baseUrl}task/get-pending-tasks`
    this.TokenFactory.cancelAllGetRequest()
    this.TokenFactory.renewGetPendingTaskReq()
    return Axios.get(url,{...config,cancelToken:this.TokenFactory.PENDING_TASK_TOKEN.token})
  }

  getRejectedTasks(){
    let url = `${baseUrl}task/get-rejected-tasks`
    this.TokenFactory.cancelAllGetRequest()
    this.TokenFactory.renewGetRejectedTaskReq()
    return Axios.get(url,{...config,cancelToken:this.TokenFactory.REJECTED_TASK_TOKEN.token})
  }

  approveTaskRegistration(wfid:string){
    let url = `${baseUrl}task/approve-task/${wfid}`
    return Axios.get(url,config)
  }
  rejectTaskRegistration(req:WFRequestModel){
    let url = `${baseUrl}task/reject-task-request`
    return Axios.post(url,req,config) 
  }

  getWorkItemTask(wfid: string){
    let url = `${baseUrl}task/get-workitem/${wfid}`
    return Axios.get(url,config)
  }

  requestTaskRegistration(task:TaskModel){
    let url =`${baseUrl}task/request-task-registration`
    return Axios.post(url,task,config)
  }

  getTaskNotes(taskId:string){
    let url =`${baseUrl}task/get-task-notes/${taskId}`
    return Axios.get(url,config)
  }

  getLatestTaskNote(taskId:string){
    let url =`${baseUrl}task/get-latest-note/${taskId}`
    return Axios.get(url,config)
  }
  
  addNote(note:NoteRequest){
    let url =`${baseUrl}task/add-note`
    return Axios.post(url,note,config)
  }

  requestCancellation(req:WFRequestModel){
    let url =`${baseUrl}task/rqst-cancel`
    return Axios.post(url,req,config)
  }

  requestTermination(req:WFRequestModel){
    let url =`${baseUrl}task/rqst-terminate`
    return Axios.post(url,req,config)
  }
  
  requestCompletion(req:WFRequestModel){
    let url =`${baseUrl}task/rqst-complete`
    return Axios.post(url,req,config)
  }
  
  approveState(wfid:string){
    let url =`${baseUrl}task/approve-state/${wfid}`
    return Axios.get(url,config)
  }

  rejectState(req:WFRequestModel){
    let url =`${baseUrl}task/reject-state`
    return Axios.post(url,req,config)
  }

  getTaskByState(state:number){
    let url =`${baseUrl}task/get-task-by-state/${state}`
    this.TokenFactory.cancelAllGetRequest()
    this.TokenFactory.renewGetTaskByStateReq()
    return Axios.get(url,{...config,cancelToken:this.TokenFactory.TASK_STATE_TOKEN.token})
  }

  getIndevidualTasks(){
    let url =`${baseUrl}task/get-individual-tasks`
    this.TokenFactory.cancelAllGetRequest()
    this.TokenFactory.renewGetIndividualTaskReq()
    return Axios.get(url,{...config,cancelToken:this.TokenFactory.INDIVIDUAL_TASK_TOKEN.token})
  }
  getTeamTasks(){
    let url =`${baseUrl}task/get-team-tasks`
    this.TokenFactory.cancelAllGetRequest()
    this.TokenFactory.renewGetTeamTaskReq()
    return Axios.get(url,{...config,cancelToken:this.TokenFactory.TEAM_TASK_TOKEN.token})
  }

  getTasksByteamId(teamId : string){
    let url =`${baseUrl}task/get-tasks-by-tid/${teamId}`
    return Axios.get(url,config)
  }

  getTaskDepndecy(taskId : string){
    let url =`${baseUrl}task/get-task-dependency/${taskId}`
    return Axios.get(url,config)
  }

  getTaskById(taskId:string){
    let url =`${baseUrl}task/get-task-byID/${taskId}`
    return Axios.get(url,config)
  }

  getTaskMetaData(){
    let url = `${baseUrl}task/task-metadata`
    return Axios.get(url,config)
  }

  getTaskReport(){
    let url = `${baseUrl}task/task-report`
    return Axios.get(url,config)
  }

  updateTaskAssignee(newAssignee : AssigneeViewModel){
    let url = `${baseUrl}task/update-task-assignee`
    return Axios.post(url,newAssignee,config)
  }
}
