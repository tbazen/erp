import Axios from 'axios'
import { baseUrl } from './url.config'
import config from './header.config'
import { TeamModel } from '../../_infrastructure/model/teamModel'
import TeamTokenFactory from './request-processor/team.tokenfactory';
import { WFRequestModel } from '../../_infrastructure/model/workflow-model';

export default class TeamService {
  TokenFactory: TeamTokenFactory
  constructor() {
    this.TokenFactory = new TeamTokenFactory()

    this.loadAllTeams = this.loadAllTeams.bind(this)
    this.loadAllEmployeeTeams = this.loadAllEmployeeTeams.bind(this)
    this.requestTeamRegistration = this.requestTeamRegistration.bind(this)
    this.deleteTeam = this.deleteTeam.bind(this)
    this.getPendingTeams = this.getPendingTeams.bind(this)
    this.approveTeamRegistration = this.approveTeamRegistration.bind(this)
    this.rejectTeamregistration = this.rejectTeamregistration.bind(this)
    this.getRejectedTeams = this.getRejectedTeams.bind(this)
    this.getTeambyId = this.getTeambyId.bind(this)
    this.getTeamReport = this.getTeamReport.bind(this)
  }

  loadAllTeams() {
    let url = `${baseUrl}team/teamlist/`
    this.TokenFactory.cancelAllGetRequest()
    this.TokenFactory.renewGetRegisteredTeamReq()
    return Axios.get(url, {...config ,cancelToken:this.TokenFactory.REGISTERED_TEAM_TOKEN.token})
  }
  
  requestTeamRegistration(team: TeamModel) {
    let url = `${baseUrl}team/request-team-registration`
    return Axios.post(url, team, config)
  }

  deleteTeam(teamId: string) {
    let url = `${baseUrl}admin/delete-team/${teamId}`
    return Axios.get(url, config)
  }
  loadAllEmployeeTeams(employeeId: string) {
    let url = `${baseUrl}employee/teamlist/${employeeId}`
    return Axios.get(url, config)
  }

  getPendingTeams(){
    let url = `${baseUrl}team/get-pending-teams`
    this.TokenFactory.cancelAllGetRequest()
    this.TokenFactory.renewGetPendingTeamReq()
    return Axios.get(url,{...config,cancelToken:this.TokenFactory.PENDING_TEAM_TOKEN.token})
  }

  approveTeamRegistration(wfid:string){
    let url = `${baseUrl}team/approve-team/${wfid}`
    return Axios.get(url,config)
  }

  rejectTeamregistration(request:WFRequestModel){
    let url = `${baseUrl}team/reject-team-request`
    return Axios.post(url,request,config)
  }

  getRejectedTeams(){
    let url  = `${baseUrl}team/get-rejected-teams`
    this.TokenFactory.cancelAllGetRequest()
    this.TokenFactory.renewGetRejectedTeamReq()
    return Axios.get(url,{...config,cancelToken:this.TokenFactory.REJECTED_TEAM_TOKEN.token})
  }

  getTeambyId(teamId:string){
    let url = `${baseUrl}team/get-team-byid/${teamId}`
    this.TokenFactory.cancelAllGetRequest()
    this.TokenFactory.renewGetTeamById()
    return Axios.get(url,{...config,cancelToken:this.TokenFactory.GET_TEAM_BYID.token})
  }
  
  getTeamReport(){
    let url = `${baseUrl}team/team-report`
    return Axios.get(url,config)
  }
}
