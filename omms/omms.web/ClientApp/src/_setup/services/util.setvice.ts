import { baseUrl } from './url.config';
import Axios from 'axios';
import config from './header.config';
export class UtilService{
    constructor(){
        this.getMetaData = this.getMetaData.bind(this)
    }
    
    getMetaData() {
        let url = `${baseUrl}util/get-metadata`
        return Axios.get(url, config)
    }

    markIndividualTaskAsSeen(){
        let url = `${baseUrl}util/seen-inv-tasks`
        return Axios.get(url, config)
    }

    markTeamTaskAsSeen(){
        let url = `${baseUrl}util/seen-team-tasks`
        return Axios.get(url, config)
    }
}