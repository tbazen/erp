import Axios from 'axios';
import { baseUrl } from './url.config';
import config from './header.config';

export default class WorkflowService {
    constructor() {
        this.cancelRegistrationRequest = this.cancelRegistrationRequest.bind(this)
    }

    cancelRegistrationRequest(wfid:string){
        let url = `${baseUrl}officer/cancel-registration/${wfid}`
        return Axios.get(url,config)
    }
}