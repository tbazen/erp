import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import { compose } from 'redux'
import rootReducer from '../reducer'

export function configureStore() {
  return createStore(
    rootReducer,
    compose(
      applyMiddleware(thunk),
      window['devToolsExtension']
        ? window['devToolsExtension']()
        : (f: any) => f
    )
  )
}
