import React from 'react'
import ReactDOM from 'react-dom'
import { App } from './_app/App'
import { Provider } from 'react-redux'
import store from './_setup/store/index'

import registerServiceWorker from './registerServiceWorker'

ReactDOM.render(
  <div>
    <Provider store={store}>
      <App />
    </Provider>
  </div>,
  document.getElementById('root') as HTMLElement
)
registerServiceWorker()
