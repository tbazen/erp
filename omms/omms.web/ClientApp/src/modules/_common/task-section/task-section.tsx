import React, { Component } from 'react'
import './task-section.scss'
interface ITaskSection {
  title: string
}

export class TaskSection extends Component<ITaskSection, any> {
  public render() {
    return (
      <div className={'task-section'}>
        <div className={'title'}>{this.props.title}</div>
        <div>{this.props.children}</div>
      </div>
    )
  }
}
