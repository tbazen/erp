// export const geoserverUrl = (vector: string) => {
//   // return `http://localhost:8080/geoserver/omms/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=omms:${vector}&outputFormat=application%2Fjson`
//   return `http://localhost:8080/geoserver/omms/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=omms%3A${vector}&outputFormat=application%2Fjson`
// }

export const geoserverUrl = (vector: string) => {
  //@ts-ignore
  let preFix = window.app && window.app.env.GIS_PRE
  //@ts-ignore
  let postFix = window.app && window.app.env.GIS_POST
  return preFix+vector+postFix
}