import axios from 'axios';
import { baseUrl } from '../../_setup/services/url.config';

export function downloadFile(documentId: string) {
    axios.get(`${baseUrl}employee/download/${documentId}`, {
        responseType: 'blob',
        withCredentials: true
    }).then(res => {
        let blob = res.data
        startBlobDownload(blob, documentId)
    }).catch(error => console.log(error))
}


function startBlobDownload(dataBlob: Blob, documentId: string) {
    if (window.navigator && window.navigator.msSaveOrOpenBlob) {
        window.navigator.msSaveOrOpenBlob(dataBlob, documentId);
    } else {
        let urlObject = URL.createObjectURL(dataBlob);
        let documentLink = document.getElementById(documentId) as HTMLAnchorElement;
        documentLink.href = urlObject
        documentLink.setAttribute('download', documentLink.title);
        documentLink.click();
        URL.revokeObjectURL(urlObject);
    }
}