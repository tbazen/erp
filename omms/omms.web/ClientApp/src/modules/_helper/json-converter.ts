import { TeamModel } from '../../_infrastructure/model/teamModel'
import { AssetModel } from '../../_infrastructure/model/assetModel'
import { TaskModel } from '../../_infrastructure/model/taskFromModel'

export function getString(data: object): string {
  return JSON.stringify(data)
}

export function getObject(data: string): TeamModel | AssetModel | TaskModel {
  return JSON.parse(data)
}
