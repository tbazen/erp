const printReport = (id:string) =>{
    //@ts-ignore
    let printContent = document.getElementById(id).innerHTML;
    document.body.innerHTML = printContent;
    window.print();
    window.location.reload()
}

export default printReport