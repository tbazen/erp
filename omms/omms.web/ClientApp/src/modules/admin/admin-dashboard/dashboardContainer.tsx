import React, { Component } from 'react'
import AppBar from '@material-ui/core/AppBar/AppBar'
import Tabs from '@material-ui/core/Tabs/Tabs'
import Tab from '@material-ui/core/Tab/Tab'

export default class DashboardContainer extends Component<{}> {
  render() {
    return (
      <div>
        <AppBar position="static">
          <Tabs value={0}>
            <Tab label="Dashboard" />
          </Tabs>
        </AppBar>
        admin-dashbard
        <ul>
          Tasks
          <hr />
          <li>Task Audit</li>
          <li>Report of tasks</li>
        </ul>
      </div>
    )
  }
}
