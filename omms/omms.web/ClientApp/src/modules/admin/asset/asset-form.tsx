import React, { Component } from 'react'
import Button from '@material-ui/core/Button'
import Modal from '@material-ui/core/Modal/Modal'
import Grid from '@material-ui/core/Grid'
import './../../shared/asset/asset.scss'
import UIContainer from './../../shared/_helper/ui-helper/containers-util';
import AssetFormContainer from '../../officer/asset-form-container'
import { AssetSearhParams } from '../../../_infrastructure/model/asset-search-params'
import { Paper } from '@material-ui/core';
import { AssetResponse } from '../../../_infrastructure/model/assetModel';


interface IProps {
  asset: AssetResponse
  openForm: boolean
  container : UIContainer
  handleClose: () => void,
  searchParams : AssetSearhParams
}

type AllProps = IProps 

class AssetFormDialog extends Component<AllProps> {
  constructor(props: any) {
    super(props)
  }

  render() {
    return (
      <React.Fragment>
        <Modal
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
          open={this.props.openForm}
          onClose={this.props.handleClose}
          >
          <Paper style={{width:'70%',margin:'auto',marginTop:'2vh'}}>
            {/* <Grid container justify="center">
                <Grid item md={11}>
                    <Grid container justify="flex-end"> 
                      <Grid item>
                          <Button
                              style={{
                                  borderRadius:'2px',
                                  margin:"10px",                          
                              }}
                              color={'secondary'}
                              variant={'contained'}
                              onClick={this.props.handleClose}
                              >
                              Cancel
                          </Button>
                      </Grid>
                    </Grid>
                </Grid>
            </Grid> */}
            <div style={{width:'100%',maxHeight:'90vh',overflowY:'auto'}}>
              <AssetFormContainer 
                                container ={this.props.container}      
                                asset={this.props.asset}
                                searchParams={this.props.searchParams}/>
            </div>
          </Paper>
        </Modal>
      </React.Fragment>
    )
  }
}


export default AssetFormDialog