import React, { Component } from 'react'
import { Schema, AssetType } from '../../../_infrastructure/model/assetModel'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography/Typography'
import { Select, MenuItem, TextField } from '@material-ui/core'

interface IProps {
  assetType: AssetType
  handleSchemaInput: (index: number, name: string, value: string) => void
}
type AllProps = IProps
export default class SchemaFrom extends Component<AllProps> {
  constructor(props: AllProps) {
    super(props)
    this.assetInput = this.assetInput.bind(this)
    this.handleChange = this.handleChange.bind(this)
  }
  assetInput(value: Schema, key: number) {
    let type = value.valueType.split(':')[0]
    let options :string []

    type == 'list' ?
        options = value.valueType.split(':')[1].split(',')
        :
        options=[]

    return (
      <Grid
        item
        xs={12}
        key={key}
        style={{
          fontSize: '15px',
          marginTop: '5px',
          marginBottom: '5px'
        }}
      >
        <Typography
          style={{
            width: '100%',
            fontSize: '15px',
            marginTop: '5px'
          }}
        >
          {value.attributeName} :{' '}
        </Typography>
        {
          type=='list'?
         <Select
            value={value.value}
            onChange={this.handleChange(value.attributeName, key)}
          >
            <MenuItem value="">
              <em>None</em>
            </MenuItem>
            {
              options.map((value:string,key:number)=><MenuItem key={key} value={value}>{value}</MenuItem> )
            }
          </Select>
          :
          <TextField
          variant="outlined"
          fullWidth
          value={value.value}
          type={value.valueType}
          placeholder={value.attributeName}
          onChange={this.handleChange(value.attributeName, key)}
        />
        }
        
      </Grid>
    )
  }

  handleChange = (name: string, index: number) => (event: any) => {
    this.props.handleSchemaInput(index, name, event.target.value)
  }

  render() {
    return <div>{this.props.assetType.attributes.map(this.assetInput)}</div>
  }
}
