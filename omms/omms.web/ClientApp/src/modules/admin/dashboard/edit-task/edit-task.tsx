import React, { Component } from 'react'
import FirstPage from './first-page/first-page'
import SecondPage from './second-page/second-page'
import { connect } from 'react-redux'
import { createTask, updateTask } from '../../../../_setup/actions/taskActions'
import { Paper } from '@material-ui/core'
import {
  Assignee,
  TaskModel
} from '../../../../_infrastructure/model/taskFromModel'

interface ICreateTask {
  page: number
  data: TaskModel
}

class CreateTask extends Component<any, any> {
  state: ICreateTask = {
    page: 1,
    data: {
      id: '68b4f548-e089-41ee-b894-8d28f02d4ca9',
      wfid: null,
      name: '',
      description: '',
      createdBy: '68b4f548-e089-41ee-b894-8d28f02d4ca9',
      taskType: 0,
      maintenanceType: 0,
      priority: 0,
      startDateExpected: '',
      endDateExpected: '',

      startDateActual: null,
      endDateActual: null,

      incidentInfo: null,
      intervalDays: 0,
      taskAssignee: null/*{
        AssigneeType: 0,
        AssigneeModel: {
          TaskId: '68b4f548-e089-41ee-b894-8d28f02d4ca9',
          AssigneeId: '68b4f548-e089-41ee-b894-8d28f02d4ca9',
          DateAssigned: null,
          ExpectedFinishDate: null,
          ActualFinishDate: null,
          CostId: 0
        }
      }*/,
      equipment: [],
      budget: {
        ActualCost: 0,
        EstimatedCost: 0
      },
      warrantyDocument: {
        Date: 0,
        Mimetype: '',
        Type: null,
        Note: '',
        Ref: '',
        Id: null,
        OverrideFilePath: '',
        Filename: '',
        File: ''
      },
      taskStatus: null,
      dependUpon: [] //TODO not done yet
    }
  }

  constructor(props: any) {
    super(props)
    this.nextPage = this.nextPage.bind(this)
    this.previousPage = this.previousPage.bind(this)
    this.submitHandler = this.submitHandler.bind(this)
    this.updateInputValue = this.updateInputValue.bind(this)
    this.handleUnscheduled = this.handleUnscheduled.bind(this)
    this.updateBudget = this.updateBudget.bind(this)
    this.setState({
      page: 1
    })
  }

  async nextPage() {
    await this.setState({
      page: this.state.page + 1
    })
  }

  previousPage() {
    //TODO save state of the whole thing so that it can be displayed when previous page is clicked
    this.setState({ page: this.state.page - 1 })
  }

  async submitHandler(prevData: Assignee) {
    console.log(this.state.data)
    console.log(prevData)
    // await this.props.createTask(this.state.data).then(async (res: any) => {
    //   console.log(res.payload.data.id)
    this.state.data.taskAssignee = prevData
    // this.state.data.taskAssignee.AssigneeModel.TaskId = res.payload.data.id
    // await this.props.updateTask(this.state.data)
    await this.props.createTask(this.state.data)
    // })
  }

  updateInputValue(e: any) {
    let { data } = this.state
    data[e.target.name] = e.target.value
    this.setState({
      data: data
    })
  }

  updateBudget(e: any) {
    let { data } = this.state
    data[e.target.name] = e.target.value.toString()
    this.setState({
      data: data
    })
  }

  handleUnscheduled(event: any) {
    this.state.data['taskTypeChecked'] = event.target.value
    this.setState({
      taskTypeChecked: event.target.value
    })
  }

  handleAssignee() {
    console.log('Assignee: ' + this.state.data.taskAssignee)
  }

  render() {
    //TODO consider a material mobile stepper for the two pages
    const { page } = this.state
    return (
      <Paper style={{ margin: ' 40px 7%', padding: '40px' }}>
        {page === 1 && (
          <FirstPage
            onSubmit={this.nextPage}
            taskModel={this.state.data}
            updateInputValue={this.updateInputValue}
            handleUnscheduled={this.handleUnscheduled}
          />
        )}
        {page === 2 && (
          <SecondPage
            previousPage={this.previousPage}
            onSubmit={this.submitHandler}
            updateInputValue={this.updateInputValue}
            updateBudget={this.updateBudget}
            taskModel={this.state.data}
          />
        )}
      </Paper>
    )
  }
}

const mapStateToProps = (state: any) => ({
  response: state.tasks
})

export default connect(
  mapStateToProps,
  { createTask, updateTask }
)(CreateTask)
