import React, { Component } from 'react'
import {
  Collapse,
  FormControlLabel,
  FormLabel,
  Grow,
  InputLabel,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  MenuItem,
  Radio,
  RadioGroup,
  Select,
  TextField
} from '@material-ui/core'
import './first-page.scss'
import Button from '@material-ui/core/Button/Button'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFileUpload, faPlus, faTrash } from '@fortawesome/free-solid-svg-icons'
import { Link } from 'react-router-dom'
import { getBase64 } from '../../../../_helper/file-encode'

interface IFirstState {
  incident: string
  incidentDescription: string
  reportedBy: string
  taskTypeChecked: number
}

class FirstPage extends Component<any, any> {
  state = {
    incident: '',
    incidentDescription: '',
    reportedBy: '',
    taskTypeChecked: 0,
    file: {
      Date: 0,
      Ref: '',
      Note: '',
      Mimetype: '',
      Type: null,
      Filename: '',
      File: '',
      Id: null,
      OverrideFilePath: ''
    }
  }

  constructor(props: any) {
    super(props)

    this.updateInputValue = this.updateInputValue.bind(this)
    this.fileInputChangeHandler = this.fileInputChangeHandler.bind(this)
    this.fileInputHandler = this.fileInputHandler.bind(this)
  }

  fileInputHandler() {
    let file = document.getElementById('messageFileInput')
    if (file != null) file.click()
  }

  async fileInputChangeHandler() {
    console.log('FILE CHANGED....')
    let file = (document.getElementById('messageFileInput') as HTMLInputElement)
      .files

    if (file != null) {
      let base64 = await getBase64(file[0] as Blob)
      console.log(file[0])
      this.setState({
        file: {
          Date: file[0].lastModified,
          Ref: '',
          Note: '',
          Mimetype: file[0].type,
          Type: null,
          Filename: file[0].name,
          File: base64,
          // only for viewing documents from a custom url (esp. in work items)
          Id: null,
          OverrideFilePath: 'api/documents/'
        }
      })

      this.props.taskModel.incidentInfo.Images.push(this.state.file)
    }
  }

  updateInputValue(e: any) {
    this.setState({
      [e.target.name]: e.target.value
    })
  }

  taskTypeChecked(value: number) {
    this.setState({
      taskTypeChecked: value
    })
  }

  render() {
    let {
      name,
      description,
      createdBy,
      taskType,
      maintenanceType,
      priority,
      startDateExpected,
      endDateExpected,
      incidentInfo,
      endDateActual,
      startDateActual,
      interval
    } = this.props.taskModel

    return (
      <div>
        <span
          className={'title block'}
          style={{ fontSize: '25px', textAlign: 'center' }}
        >
          Edit Task (1 of 2)
        </span>
        <div className={'first-page'}>
          {/*Name*/}
          <TextField
            name={'name'}
            value={name || ''}
            onChange={this.props.updateInputValue}
            placeholder="Task Name"
            className={'text-field'}
            fullWidth
          />

          {/*Description*/}
          <TextField
            name={'description'}
            placeholder={'Task Description'}
            multiline={true}
            rows={5}
            rowsMax={10}
            value={description || ''}
            onChange={this.props.updateInputValue}
            className={'modal-content text-field'}
            fullWidth
          />

          {/*Task Type*/}
          <FormLabel className={'block'}>Task Type</FormLabel>
          <RadioGroup
            aria-label={'Task Type'}
            className={'task-type'}
            value={taskType || ''}
            onChange={this.props.updateInputValue}
            name={'taskType'}
          >
            <FormControlLabel
              value="1"
              control={<Radio />}
              label="Unscheduled"
              labelPlacement="start"
              onChange={(event: any) => {
                this.props.handleUnscheduled(event)
                this.taskTypeChecked(1)
              }}
            />
            <FormControlLabel
              value="2"
              control={<Radio />}
              label="Scheduled"
              labelPlacement="start"
              onChange={(event: any) => {
                this.props.handleUnscheduled(event)
                this.taskTypeChecked(2)
              }}
            />
            <FormControlLabel
              value="3"
              control={<Radio />}
              label="Periodic"
              labelPlacement="start"
              onChange={(event: any) => {
                this.props.handleUnscheduled(event)
                this.taskTypeChecked(3)
              }}
            />
            <FormControlLabel
              value="4"
              control={<Radio />}
              label="Incident"
              labelPlacement="start"
              onChange={(event: any) => {
                this.props.handleUnscheduled(event)
                this.taskTypeChecked(4)
              }}
            />
          </RadioGroup>
          <Collapse in={this.state.taskTypeChecked == 2}>
            <Grow in={this.state.taskTypeChecked == 2}>
              <div>
                <TextField
                  name={'startDateExpected'}
                  type={'date'}
                  label={'Start Date Expected'}
                  InputLabelProps={{
                    shrink: true
                  }}
                  value={startDateExpected || ''}
                  onChange={this.props.updateInputValue}
                  className={'text-field'}
                />
                <TextField
                  name={'endDateExpected'}
                  type={'date'}
                  label={'End Date Expected'}
                  InputLabelProps={{
                    shrink: true
                  }}
                  value={endDateExpected || ''}
                  onChange={this.props.updateInputValue}
                  className={'text-field'}
                />
              </div>
            </Grow>
          </Collapse>
          <Collapse in={this.state.taskTypeChecked == 3}>
            <Grow in={this.state.taskTypeChecked == 3}>
              <div>
                <TextField
                  name={'startDateExpected'}
                  type={'date'}
                  label={'Start Date Expected'}
                  InputLabelProps={{
                    shrink: true
                  }}
                  value={startDateExpected || ''}
                  onChange={this.props.updateInputValue}
                  className={'text-field'}
                />
                <TextField
                  name={'endDateExpected'}
                  type={'date'}
                  label={'End Date Expected'}
                  InputLabelProps={{
                    shrink: true
                  }}
                  value={endDateExpected || ''}
                  onChange={this.props.updateInputValue}
                  className={'text-field'}
                />
                <TextField
                  name={'interval'}
                  type={'number'}
                  label={'Interval (in weeks)'}
                  value={interval || ''}
                  onChange={this.props.updateInputValue}
                  className={'text-field'}
                />
              </div>
            </Grow>
          </Collapse>
          <Collapse in={this.state.taskTypeChecked == 4}>
            <Grow in={this.state.taskTypeChecked == 4}>
              <div>
                <TextField
                  name={'incident'}
                  type={'string'}
                  label={'Incident'}
                  value={this.state.incident || ''}
                  onChange={this.updateInputValue}
                  className={'text-field'}
                />
                <TextField
                  name={'incidentDescription'}
                  type={'string'}
                  label={'Incident Description'}
                  multiline={true}
                  rows={5}
                  rowsMax={10}
                  value={this.state.incidentDescription || ''}
                  onChange={this.updateInputValue}
                  className={'text-field'}
                />
                <TextField
                  name={'reportedBy'}
                  type={'string'}
                  label={'Incident Reported By'}
                  value={this.state.reportedBy || ''}
                  onChange={this.updateInputValue}
                  className={'text-field'}
                />
                <div className={'file-input'}>
                  <input
                    type="file"
                    id="messageFileInput"
                    onChange={this.fileInputChangeHandler}
                    hidden
                  />
                  <div className="fileInput" onClick={this.fileInputHandler}>
                    <FontAwesomeIcon
                      icon={faFileUpload}
                      className="fileIcon"
                      size="2x"
                    />
                  </div>
                </div>
              </div>
            </Grow>
          </Collapse>

          {/*Maintenance Type*/}
          <FormLabel className={'block'}>Maintenance Type</FormLabel>
          <RadioGroup
            aria-label={'Maintenance Type'}
            className={'maintenance-type'}
            value={maintenanceType || ''}
            onChange={this.props.updateInputValue}
            name={'maintenanceType'}
          >
            <FormControlLabel
              value="1"
              control={<Radio />}
              label="Preventive"
              labelPlacement="start"
            />
            <FormControlLabel
              value="2"
              control={<Radio />}
              label="Corrective"
              labelPlacement="start"
            />
          </RadioGroup>

          {/*Priority*/}
          <div className={'priority-dropdown'}>
            <InputLabel>Priority</InputLabel>
            <Select
              value={priority || ''}
              onChange={this.props.updateInputValue}
              inputProps={{
                name: 'priority'
              }}
              fullWidth
            >
              <MenuItem value="">
                <em>None</em>
              </MenuItem>
              <MenuItem value={'1'}>A</MenuItem>
              <MenuItem value={'2'}>B</MenuItem>
              <MenuItem value={'3'}>C</MenuItem>
              <MenuItem value={'4'}>D</MenuItem>
              <MenuItem value={'5'}>E</MenuItem>
            </Select>
          </div>
        </div>
        <Button
          onClick={() => {
            this.props.onSubmit()
          }}
        >
          Next
        </Button>
        <Link to={'/tasks/admin'}>
          <Button>
            Cancel
          </Button>
        </Link>
      </div>
    )
  }
}

export default FirstPage
