import React, { Component } from 'react'
import './second-page.scss'
import {
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  FormControlLabel,
  FormLabel,
  Grid,
  InputLabel,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  MenuItem,
  Radio,
  RadioGroup,
  Select,
  TextField
} from '@material-ui/core'
import Button from '@material-ui/core/Button/Button'
import {
  faFileUpload,
  faPlus,
  faTrash
} from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { connect } from 'react-redux'
import { loadAllTeams } from '../../../../../_setup/actions/teamActions'
import { loadAllEmployees } from '../../../../../_setup/actions/employeeActions'
import { getBase64 } from '../../../../_helper/file-encode'
import TaskListForm from './../../../../shared/components/task-card/task-list-form'

interface ISecondState {
  equipment: string
  equipmentQuantity: string
  file: {
    Date: number
    Ref: string
    Note: string
    Mimetype: string
    Type: number | null
    Filename: string
    File: string
    Id: string | null
    OverrideFilePath: string
  }
  dependencies: object
  team: string
  employee: string
  taskAssignee: string
  dialog: {
    opened: boolean
    type: string
  }
  equipmentDialog: boolean
  fix: boolean
  openDependencyForm: boolean
}

class component extends Component<any, any> {
  state: ISecondState = {
    equipment: '',
    equipmentQuantity: '',
    file: {
      Date: 0,
      Ref: '',
      Note: '',
      Mimetype: '',
      Type: null,
      Filename: '',
      File: '',
      Id: null,
      OverrideFilePath: ''
    },
    dependencies: {},
    team: '',
    employee: '',
    taskAssignee: '',
    dialog: {
      opened: false,
      type: ''
    },
    equipmentDialog: false,
    fix: false,
    openDependencyForm: false
  }

  teamSuggestions: { name: string; value: string }[] = []

  employeeSuggestions: { name: string; value: string }[] = []

  equipmentList = [
    { name: 'Equipment 1', value: 'Equipment 1' },
    { name: 'Equipment 2', value: 'Equipment 2' },
    { name: 'Equipment 3', value: 'Equipment 3' },
    { name: 'Equipment 4', value: 'Equipment 4' },
    { name: 'Equipment 5', value: 'Equipment 5' }
  ]

  constructor(props: any) {
    super(props)

    this.handleClose = this.handleClose.bind(this)
    this.updateInputValue = this.updateInputValue.bind(this)
    this.handleDone = this.handleDone.bind(this)
    this.handleClickOpen = this.handleClickOpen.bind(this)
    this.handleEquipmentClose = this.handleEquipmentClose.bind(this)
    this.handleEquipmentOpen = this.handleEquipmentOpen.bind(this)
    this.handleEquipment = this.handleEquipment.bind(this)
    this.handleEquipmentDelete = this.handleEquipmentDelete.bind(this)
    this.fileInputHandler = this.fileInputHandler.bind(this)
    this.fileInputChangeHandler = this.fileInputChangeHandler.bind(this)
    this.handleCloseDependencyDialog = this.handleCloseDependencyDialog.bind(this)
    this.handleDependencyDialog = this.handleDependencyDialog.bind(this)
    this.openDependencyForm = this.openDependencyForm.bind(this)
  }

  handleEquipment() {
    this.handleEquipmentDeleteByName({ name: this.state.equipment })
    this.props.taskModel.equipment.push({
      name: this.state.equipment,
      quantity: this.state.equipmentQuantity
    })
    this.state.equipment = ''
    this.state.equipmentQuantity = ''
    this.handleEquipmentClose()
  }

  handleClickOpen = (event: any) => {
    if (event.target.value == 2)
      this.setState({ dialog: { opened: true, type: 'Team' } })
    else if (event.target.value == 1)
      this.setState({ dialog: { opened: true, type: 'Employee' } })
  }

  handleEquipmentOpen = () => {
    this.setState({ equipmentDialog: true })
  }

  handleCloseDependencyDialog() {
    this.setState({
      openDependencyForm: false
    })
  }

  openDependencyForm() {
    this.setState({
      openDependencyForm: true
    })
  }

  handleDependencyDialog(dependencies: string[]) {
    console.log(dependencies)
    this.props.taskModel.dependUpon = dependencies
  }

  handleDone() {
    let temp: {
      AssigneeType: number
      AssigneeModel: {
        TaskId?: string
        AssigneeId?: string
        DateAssigned?: string
        ExpectedFinishDate?: string
        ActualFinishDate?: null
        CostId?: 0
      }
    } = {
      AssigneeType: -1,
      AssigneeModel: {
        TaskId: '',
        AssigneeId: '',
        DateAssigned: '',
        ExpectedFinishDate: '',
        ActualFinishDate: null,
        CostId: 0
      }
    }

    if (this.state.taskAssignee == '1')
      temp = {
        AssigneeType: parseInt(this.state.taskAssignee),
        AssigneeModel: { AssigneeId: this.state.employee }
      }
    else if (this.state.taskAssignee == '2')
      temp = {
        AssigneeType: parseInt(this.state.taskAssignee),
        AssigneeModel: { AssigneeId: this.state.team }
      }

    temp = {
      AssigneeType: temp.AssigneeType,
      AssigneeModel: {
        TaskId: '',
        AssigneeId: temp.AssigneeModel.AssigneeId,
        DateAssigned: '',
        ExpectedFinishDate: '',
        ActualFinishDate: null,
        CostId: 0
      }
    }

    this.props.taskModel.taskAssignee = temp
    console.log(temp)
    console.log(this.props.taskModel)
    this.handleClose()
  }

  handleClose = () => {
    this.setState({ dialog: { opened: false, taskAssignee: '' } })
    setTimeout(() => {
      this.setState({ dialog: { opened: false, taskAssignee: '' } })
    }, 100)
  }

  handleEquipmentClose = () => {
    this.setState({ equipmentDialog: false, equipment: '' })
  }

  handleEquipmentDelete(value: { name: string; quantity: string }) {
    this.props.taskModel.equipment.splice(
      this.props.taskModel.equipment.indexOf({
        name: value.name,
        quantity: value.quantity
      }),
      1
    )
    this.setState({
      fix: false
    })
  }

  handleEquipmentDeleteByName(obj: { name: string }) {
    let temp: number = -1
    this.props.taskModel.equipment.map(
      (value: { name: string; quantity: string }, index: number) => {
        if (value.name === obj.name) {
          temp = index
        }
      }
    )

    if (temp != -1) this.props.taskModel.equipment.splice(temp, 1)
    this.setState({
      fix: !this.state.fix
    })
  }

  updateInputValue(e: any) {
    this.setState({
      [e.target.name]: e.target.value
    })
  }

  fileInputHandler() {
    let file = document.getElementById('messageFileInput')
    if (file != null) file.click()
  }

  async fileInputChangeHandler() {
    console.log('FILE CHANGED....')
    let file = (document.getElementById('messageFileInput') as HTMLInputElement)
      .files

    if (file != null) {
      let base64 = await getBase64(file[0] as Blob)
      console.log(file[0])
      this.setState({
        file: {
          Date: file[0].lastModified,
          Ref: '',
          Note: '',
          Mimetype: file[0].type,
          Type: null,
          Filename: file[0].name,
          File: base64,
          // only for viewing documents from a custom url (esp. in work items)
          Id: null,
          OverrideFilePath: 'api/documents/'
        }
      })

      this.props.taskModel.warrantyDocument = this.state.file
    }
  }

  async componentWillMount() {
    //TODO change the actions to admin routes instead of employee
    await this.props.loadAllTeams()
    this.props.teamList.map((value: any) => {
      this.teamSuggestions.push({ name: value.name, value: value.id })
    })

    await this.props.loadAllEmployees()
    this.props.employees.map((value: any) => {
      this.employeeSuggestions.push({ name: value.userName, value: value.id })
    })
  }

  render() {
    let {
      taskAssignee,
      equipment,
      warrantyDocument,
      taskStatus,
      dependencies,
      budget
    } = this.props.taskModel

    return (
      <div className={'second-page'}>
        <span
          className={'title block'}
          style={{ fontSize: '25px', textAlign: 'center' }}
        >
          Edit Task (2 of 2)
        </span>
        <div className="form">

          {/*Task Assignee*/}
          <FormLabel className={'block'}>Task Assignee</FormLabel>
          <RadioGroup
            className={'task-assignee'}
            value={this.state.taskAssignee}
            onChange={this.updateInputValue}
            onClick={() => {
              this.state.fix = !this.state.fix
              //@ts-ignore
              // this.state.taskAssignee = taskAssignee
            }}
            name={'taskAssignee'}
          >
            <FormControlLabel
              value="2"
              control={<Radio />}
              label="Team"
              labelPlacement="start"
              onChange={this.handleClickOpen}
            />
            <FormControlLabel
              value="1"
              control={<Radio />}
              label="Employee"
              labelPlacement="start"
              onChange={this.handleClickOpen}
            />
          </RadioGroup>

          {/*Budget*/}
          <TextField
            name={'budget'}
            type={'number'}
            placeholder={'Task Budget'}
            value={budget.EstimatedCost}
            onChange={this.props.updateBudget}
            onClick={() => {
              this.setState({ fix: !this.state.fix })
            }}
            className={'text-field'}
            fullWidth
          />

          {/*Equipment*/}
          <List style={{ padding: '0', margin: '10px 0' }}>
            <ListItem
              onClick={event => {
                event.preventDefault()
              }}
              style={{ padding: '0' }}
            >
              <ListItemText primary="Add Equipment" />
              <ListItemIcon>
                <Button onClick={this.handleEquipmentOpen}>
                  <FontAwesomeIcon icon={faPlus} size={'1x'} />
                </Button>
              </ListItemIcon>
            </ListItem>
            {this.props.taskModel.equipment.map(
              (value: { name: string; quantity: string }, key: number) => {
                return (
                  <ListItem style={{ padding: '0' }} key={key}>
                    <ListItemIcon>
                      <Button
                        onClick={() => {
                          this.handleEquipmentDelete({
                            name: value.name,
                            quantity: value.quantity
                          })
                        }}
                      >
                        <FontAwesomeIcon icon={faTrash} size={'1x'} />
                      </Button>
                    </ListItemIcon>
                    <ListItemText
                      primary={
                        'Name: ' + value.name + '   Quantity: ' + value.quantity
                      }
                    />
                  </ListItem>
                )
              }
            )}
          </List>

          {/*Warranty Document*/}
          <FormLabel className={'block'}>Warranty Document</FormLabel>
          <div className={'file-input'}>
            <input
              type="file"
              id="messageFileInput"
              onChange={this.fileInputChangeHandler}
              hidden
            />
            <div className="fileInput" onClick={this.fileInputHandler}>
              <FontAwesomeIcon
                icon={faFileUpload}
                className="fileIcon"
                size="4x"
              />
            </div>
          </div>

          {/*Task Status*/}
          <div className={'taskstatus-dropdown'} style={{ padding: '0', margin: '10px 0' }}>
            <InputLabel>Task Status</InputLabel>
            <Select
              value={taskStatus}
              onChange={this.props.updateInputValue}
              onClick={() => {
                this.setState({ fix: !this.state.fix })
              }}
              inputProps={{
                name: 'taskStatus'
              }}
              fullWidth
            >
              {/*<MenuItem value={""}>*/}
              {/*<em>None</em>*/}
              {/*</MenuItem>*/}
              <MenuItem value={'1'}>Pending</MenuItem>
              <MenuItem value={'2'}>Delegated</MenuItem>
              <MenuItem value={'3'}>Request Approval</MenuItem>
              <MenuItem value={'4'}>Approved</MenuItem>
              <MenuItem value={'5'}>Rejected</MenuItem>
            </Select>
          </div>

          {/*Task Dependency*/}
          <Button onClick={this.openDependencyForm}>Open Dependency</Button>
          <TaskListForm
            openForm={this.state.openDependencyForm}
            setDependency={this.handleDependencyDialog}
            task={this.props.taskModel}
            handleClose={this.handleCloseDependencyDialog}
          />
        </div>
        <Button
          onClick={() => {
            this.props.previousPage()
          }}
        >
          Previous Page
        </Button>
        <Button
          onClick={() => {
            this.props.onSubmit(this.props.taskModel.taskAssignee)
          }}
        >
          Submit
        </Button>


        {/*Dialog for team and employee selection*/}
        <Dialog open={this.state.dialog.opened} onClose={this.handleClose}>
          <DialogTitle>Select {this.state.dialog.type}</DialogTitle>
          <DialogContent>
            <DialogContentText>
              Search {this.state.dialog.type} below to add to assign to the task
              {this.state.dialog.type == 'Team' ? (
                <Select
                  value={this.state.team}
                  onChange={this.updateInputValue}
                  style={{
                    width: '100%'
                  }}
                  inputProps={{
                    name: 'team'
                  }}
                >
                  {this.teamSuggestions.map(
                    (value: { value: string; name: string }, key: number) => {
                      return (
                        <MenuItem value={value.value} key={key}>
                          {value.name}
                        </MenuItem>
                      )
                    }
                  )}
                </Select>
              ) : (
                <Select
                  value={this.state.employee}
                  onChange={this.updateInputValue}
                  style={{
                    width: '100%'
                  }}
                  inputProps={{
                    name: 'employee'
                  }}
                >
                  {this.employeeSuggestions.map(
                    (value: { value: string; name: string }, key: number) => {
                      return (
                        <MenuItem value={value.value} key={key}>
                          {value.name}
                        </MenuItem>
                      )
                    }
                  )}
                </Select>
              )}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              Cancel
            </Button>
            <Button onClick={this.handleDone} color="primary">
              Done
            </Button>
          </DialogActions>
        </Dialog>

        {/*Dialog for equipment*/}
        <Dialog
          open={this.state.equipmentDialog}
          onClose={this.handleEquipmentClose}
        >
          <DialogTitle>Choose Equipment</DialogTitle>
          <DialogContent>
            <DialogContentText>
              Choose equipment below to add to the task's equipment list
            </DialogContentText>
            <Select
              value={this.state.equipment}
              onChange={this.updateInputValue}
              style={{
                width: '100%'
              }}
              inputProps={{
                name: 'equipment'
              }}
            >
              {this.equipmentList.map(
                (value: { value: string; name: string }, key: number) => {
                  return (
                    <MenuItem value={value.value} key={key}>
                      {value.name}
                    </MenuItem>
                  )
                }
              )}
            </Select>
            <TextField
              value={this.state.equipmentQuantity}
              onChange={this.updateInputValue}
              name={'equipmentQuantity'}
              type={'number'}
              label={'Quantity'}
              inputProps={{
                shrink: true
              }}
              fullWidth
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleEquipmentClose} color="primary">
              Cancel
            </Button>
            <Button onClick={this.handleEquipment} color="primary">
              Done
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    )
  }
}

//TODO Configure the dialog done buttons so that they can save the data on the state or smt

const mapStateToProps = (state: any) => ({
  teamList: state.team.teams,
  employees: state.employee.employees
})

const SecondPage = connect(
  mapStateToProps,
  { loadAllTeams, loadAllEmployees }
)(component)

export default SecondPage
