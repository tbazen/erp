import React, { Component } from 'react'
import './../../shared/components/task-card/task-card.scss'
import { Button, Paper } from '@material-ui/core'
import { NavLink } from 'react-router-dom'
import { NotificationModel } from '../../../_infrastructure/model/notificationModel'

interface IProps {
  notification: NotificationModel
}

export default class NotificationCard extends Component<IProps> {
  constructor(props: any) {
    super(props)
  }

  public render() {
    let { notification } = this.props
    let data = notification.task

    return (
      <Paper className={'task-card-container'} style={{ padding: '20px' }}>
        <span className={'task-field'}>
          Notification: {notification.notificationMessage}
        </span>
        <span className={'task-field'}>Description: {data.description}</span>
        <span className={'task-field'}>Description: {data.description}</span>
        <span className={'task-field'}>
          Created By(get name from back): {data.createdBy}
        </span>
        <span className={'task-field'}>Start Date: {data.startDateActual}</span>
        <span className={'task-field'}>End Date: {data.endDateActual}</span>
        <Button className={'view-btn btn'} variant={'outlined'}>
          Open Task
        </Button>
      </Paper>
    )
  }
}
