import React, { Component } from 'react'
import Button from '@material-ui/core/Button'
import FormControl from '@material-ui/core/FormControl'
import Paper from '@material-ui/core/Paper'
import Typography from '@material-ui/core/Typography'
import TextField from '@material-ui/core/TextField/TextField'
import Modal from '@material-ui/core/Modal/Modal'
import { connect } from 'react-redux'
import Grid from '@material-ui/core/Grid'
import Checkbox from '@material-ui/core/Checkbox'
import { setNotifications } from '../../../_setup/actions/notificationActions'
import { NotificationModel } from '../../../_infrastructure/model/notificationModel'
import { TaskStatus } from '../../../_infrastructure/model/taskStatusModel'
import { TaskModel } from '../../../_infrastructure/model/taskFromModel'

export enum NotificationFromActionType {
  CREATE_NOTF = 'CREATE NOTIFICATION',
  UPDATE_NOTF = 'UPDATE NOTIFICATION'
}

interface IProps {
  task: TaskModel
  openForm: boolean
  handleClose: () => void
}

interface IState {
  notification: NotificationModel
}

class NotificationForm extends Component<any, IState> {
  constructor(props: any) {
    super(props)
    this.handleChange = this.handleChange.bind(this)
    this.submitNotification = this.submitNotification.bind(this)
    this.state = {
      notification: {
        task: this.props.task,
        daysBeforeStart: null,
        daysBeforeEnd: null,
        notificationMessage: '',
        notificationEmail: '',
        onStatus: null
      }
    }
  }

  handleChange = (name: string) => (event: any) => {
    let { notification } = this.state
    notification[name] = event.target.value
    this.setState({ notification })
  }

  submitNotification() {
    console.log('NOTIFICATION : ')
    console.log(this.state)
  }

  render() {
    let { notification } = this.state
    return (
      <React.Fragment>
        <Modal
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
          open={this.props.openForm}
          onClose={this.props.handleClose}
        >
          <Paper
            style={{
              width: '50%',
              margin: 'auto',
              marginTop: '10vh',
              padding: '15px'
            }}
          >
            <Grid container justify="center">
              <Grid item md={10}>
                <Typography component="h2" variant="h5">
                  Set Notification
                </Typography>
              </Grid>

              <Grid item md={10}>
                <FormControl margin="normal" required fullWidth>
                  <TextField
                    type="number"
                    id="outlined-name"
                    placeholder="Days before start"
                    name="name"
                    onChange={this.handleChange('daysBeforeStart')}
                    margin="normal"
                    variant="outlined"
                    value={
                      notification.daysBeforeStart == null
                        ? 0
                        : notification.daysBeforeStart
                    }
                  />
                </FormControl>
                <FormControl margin="normal" required fullWidth>
                  <TextField
                    type="number"
                    id="outlined-name"
                    placeholder="Days before end"
                    name="name"
                    onChange={this.handleChange('daysBeforeEnd')}
                    margin="normal"
                    variant="outlined"
                    value={
                      notification.daysBeforeEnd == null
                        ? 0
                        : notification.daysBeforeEnd
                    }
                  />
                </FormControl>
                <FormControl margin="normal" required fullWidth>
                  <TextField
                    id="outlined-name"
                    type="email"
                    placeholder="Email"
                    name="name"
                    onChange={this.handleChange('notificationEmail')}
                    margin="normal"
                    variant="outlined"
                    value={notification.notificationEmail}
                  />
                </FormControl>
                <FormControl margin="normal" required fullWidth>
                  <TextField
                    id="outlined-multiline-static"
                    placeholder="Notification message"
                    multiline
                    rows="4"
                    name="description"
                    onChange={this.handleChange('notificationMessage')}
                    margin="normal"
                    variant="outlined"
                    value={notification.notificationMessage}
                  />
                </FormControl>
                <FormControl margin="normal" fullWidth>
                  <Button
                    type="submit"
                    variant="contained"
                    color="primary"
                    onClick={this.submitNotification}
                  >
                    Submit
                  </Button>
                </FormControl>
              </Grid>
            </Grid>
          </Paper>
        </Modal>
      </React.Fragment>
    )
  }
}

function mapStateToProps() {
  return {}
}

export default connect(
  mapStateToProps,
  { setNotifications }
)(NotificationForm)
