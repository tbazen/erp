import React, { Component } from 'react'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Grid from '@material-ui/core/Grid'
import { connect } from 'react-redux'
import './notification.scss'
import { NotificationModel } from '../../../_infrastructure/model/notificationModel'
import NotificationCard from './notification-card'
import { loadAllNotifications } from '../../../_setup/actions/notificationActions'
import {
  getUserInfo,
  getEmployeeInfo
} from '../../../_setup/actions/loginActions'
import Tabs from '@material-ui/core/Tabs/Tabs'
import Tab from '@material-ui/core/Tab/Tab'
import NotificationForm from './notification-form'
import TaskListForm from './../../shared/components/task-card/task-list-form'

function DisplayEmptyList(): JSX.Element {
  return (
    <div
      style={{
        padding: '20px',
        fontFamily: 'roboto',
        fontSize: '25px',
        textAlign: 'center',
        marginTop: '40vh'
      }}
    >
      <p>You don't have any Notifications yet.</p>
    </div>
  )
}
interface IProps {}

interface PropsFromDispatch {
  loadAllNotifications: (employeeId: string) => void
}

interface PropsFromState {
  notifications: NotificationModel[]
}

type AllProps = IProps & PropsFromDispatch & PropsFromState

class Notifications extends Component<AllProps> {
  constructor(props: any) {
    super(props)
  }

  async componentDidMount() {
    let user = await getEmployeeInfo()
    if (user.id != null) this.props.loadAllNotifications(user.id)
  }

  render() {
    return (
      <div>
        <AppBar position="static">
          <Tabs value={0}>
            <Tab label="Notifications" />
          </Tabs>
        </AppBar>
        <div className="paper">
          {this.props.notifications.length == 0 ? (
            <DisplayEmptyList />
          ) : (
            this.props.notifications.map(
              (value: NotificationModel, key: number) => (
                <NotificationCard notification={value} key={key} />
              )
            )
          )}
        </div>
      </div>
    )
  }
}

function mapStateToProps(state: any) {
  return {
    tasks: state.tasks.tasks,
    notifications: state.notification.notifications
  }
}

export default connect(
  mapStateToProps,
  { loadAllNotifications }
)(Notifications)
