import React, { Component } from 'react'
import './team.scss'
import TeamContainer from './teamContainer'
import { IUserInformation } from 'src/_infrastructure/model/userInformationModel';
import Role from '../../shared/_helper/role-util/roles';
import { OfficerItems } from '../../shared/_helper/slideout-index/officer-items';
import { SupervisorItems } from '../../shared/_helper/slideout-index/supervisor-items';
import { changeActiveIndex } from '../../../_setup/actions/drawer-actions';
import { connect } from 'react-redux';

interface PropsFromState{
  user : IUserInformation
}
interface PropsFromDispatch{
  changeActiveIndex:(index:number)=>void
}

type AllProps = PropsFromState &
                PropsFromDispatch
class Team extends Component<AllProps> {
  componentWillMount(){
    let {user} = this.props
    
    if(user.role == Role.TECHNICAL_OFFICER)
        this.props.changeActiveIndex(OfficerItems.TEAM)
    else if(user.role == Role.TECHNICAL_SUPERVISOR)
        this.props.changeActiveIndex(SupervisorItems.TEAMS)
    
  }
  public render() {
    return (
      <div>
        <TeamContainer />
      </div>
    )
  }
}

function mapStateToProps(state:any){
  return {
    user: state.login.userInfo
  }
}

function mapDispatchToProps(dispatch:any){
  return {
    changeActiveIndex:(index:number)=>dispatch(changeActiveIndex(index))
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(Team)
