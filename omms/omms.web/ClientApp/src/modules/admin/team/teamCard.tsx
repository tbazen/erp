import React, { Component } from 'react'
import Card from '@material-ui/core/Card'
import CardActionArea from '@material-ui/core/CardActionArea'
import CardActions from '@material-ui/core/CardActions'
import CardContent from '@material-ui/core/CardContent'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'
import { connect } from 'react-redux'
import { deleteTeam } from '../../../_setup/actions/teamActions'
import TeamFormCard from './teamFormCard'

class TeamCard extends Component<any> {
  state = {
    updateFormIsOpen: false
  }
  constructor(props: any) {
    super(props)
    this.deleteHandler = this.deleteHandler.bind(this)
    this.openUpdateForm = this.openUpdateForm.bind(this)
    this.closeUpdateForm = this.closeUpdateForm.bind(this)
  }

  openUpdateForm() {
    this.setState({ updateFormIsOpen: true })
  }
  closeUpdateForm() {
    this.setState({ updateFormIsOpen: false })
  }

  deleteHandler() {
    this.props.deleteTeam(this.props.team.id)
  }

  render() {
    let { team, key } = this.props

    return (
      <React.Fragment>
        <Card
          key={key}
          style={{
            width: '47%',
            float: 'left',
            margin: 'auto',
            marginTop: '10px',
            marginLeft: '2%',
            marginBottom: '10px'
          }}
        >
          <CardActionArea>
            <CardContent>
              <Typography gutterBottom variant="h5" component="h2">
                {team.name}
              </Typography>
              <Typography component="p">{team.description}</Typography>
            </CardContent>
          </CardActionArea>
          <CardActions>
            <Button size="small" onClick={this.openUpdateForm} color="primary">
              Update
            </Button>
            <Button size="small" color="secondary" onClick={this.deleteHandler}>
              Delete
            </Button>
          </CardActions>
        </Card>
        <TeamFormCard
          openForm={this.state.updateFormIsOpen}
          team={team}
          handleClose={this.closeUpdateForm}
        />
      </React.Fragment>
    )
  }
}
function mapStateToProps() {
  return {}
}
export default connect(
  mapStateToProps,
  { deleteTeam }
)(TeamCard)
