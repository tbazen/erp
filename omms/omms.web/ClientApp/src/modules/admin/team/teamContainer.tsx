import React, { Component } from 'react';
import AppBar from '@material-ui/core/AppBar';
import Paper from '@material-ui/core/Paper';
import { withStyles, Theme } from '@material-ui/core/styles';
import TeamFormCard from './teamFormCard'
import { connect } from 'react-redux'
import { loadAllTeams } from '../../../_setup/actions/teamActions';
import Typography from '@material-ui/core/Typography/Typography'
import { TeamModel } from '../../../_infrastructure/model/teamModel';
import { loadAllEmployees } from '../../../_setup/actions/employeeActions';
import Tabs from '@material-ui/core/Tabs/Tabs';
import Tab from '@material-ui/core/Tab/Tab';

import * as G from './../../shared/team/team-contaner';
import UIContainer from '../../shared/_helper/ui-helper/containers-util';


const styles = (theme: Theme) => ({
    paper: {
        maxWidth: "100%",
        margin: 'auto',
        boxShadow: "0px",
        overflow: 'hidden',
    },
    searchBar: {
        borderBottom: '1px solid rgba(0, 0, 0, 0.12)',
    },
    searchInput: {
        fontSize: theme.typography.fontSize,
        marginLeft: "250px"
    },
    block: {
        display: 'block',
    },
    contentWrapper: {
        margin: '40px 16px',
    },
});


class TeamContainer extends Component<any>{
    state = {
        openCreateForm: false
    };
    constructor(props: any) {
        super(props)
        this.handleTeamFormClose = this.handleTeamFormClose.bind(this)
        this.handleTeamFormOpen = this.handleTeamFormOpen.bind(this)
    }


    handleTeamFormOpen = () => {
        this.setState({ openCreateForm: true });
    };

    handleTeamFormClose = () => {
        this.setState({ openCreateForm: false, singleTeam: undefined });
    };

    componentDidMount() {
        this.props.loadAllTeams();
        this.props.loadAllEmployees();
    }


    render() {
        const { classes } = this.props;
        const { teamList } = this.props;
        let team: TeamModel = {
            id: null,
            wfid:null,
            name: '',
            description: '',
            members: [],
            tasks: [],
            note:''
        }
        return (
            <div>
                <TeamFormCard
                    openForm={this.state.openCreateForm}
                    handleClose={this.handleTeamFormClose}
                    team={team}
                />
                <div>
                    {
                        <G.default container={UIContainer.REGISTERED_CONTAINER} withSearchBar={true}/>
                    }
                </div>
            </div>
        )
    }
}
//function mapStateToProps({team}:ApplicationState)
function mapStateToProps(state: any) {
    return {
        teamList: state.team.teams
    }
}
export default connect(mapStateToProps, { loadAllTeams, loadAllEmployees })(withStyles(styles)(TeamContainer));