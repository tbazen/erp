import React, { Component } from 'react'
import Button from '@material-ui/core/Button'
import CssBaseline from '@material-ui/core/CssBaseline'
import FormControl from '@material-ui/core/FormControl'
import Paper from '@material-ui/core/Paper'
import Typography from '@material-ui/core/Typography'
import withStyles from '@material-ui/core/styles/withStyles'
import { Theme } from '@material-ui/core'
import createStyles from '@material-ui/core/styles/createStyles'
import TextField from '@material-ui/core/TextField/TextField'
import Modal from '@material-ui/core/Modal/Modal'
import { connect } from 'react-redux'
import { TeamModel } from './../../../_infrastructure/model/teamModel'
import Grid from '@material-ui/core/Grid'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import List from '@material-ui/core/List'
import Checkbox from '@material-ui/core/Checkbox'
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction'
import { IUserInformation } from './../../../_infrastructure/model/userInformationModel'
import { requestRegistration } from '../../../_setup/actions/workflow-actions'



function listComponent(value: string, key: number): JSX.Element {
  return (
    <ListItem button key={key}>
      <ListItemText primary={value} />
      <ListItemSecondaryAction>
        <Checkbox color="primary" />
      </ListItemSecondaryAction>
    </ListItem>
  )
}

interface IState {
  team: TeamModel
  btnText: string
}
interface IProps{
  team : TeamModel,
  openForm:boolean,
  handleClose : ()=>void 
}
interface PropsFromState{
  employees : IUserInformation[]
}
type AllProps  = IProps &
                 PropsFromState

class TeamFormCard extends Component<AllProps, IState> {
  constructor(props: any) {
    super(props)
    this.submitTeam = this.submitTeam.bind(this)
    this.employeeListComponent = this.employeeListComponent.bind(this)
    this.selectHandler = this.selectHandler.bind(this)
    let { team } = this.props

    this.state = {
      team: team,
      btnText:'submit'
    }
  }

  selectHandler = (employee:IUserInformation) => (event: any)=> {
    if (event.target.checked) this.addTeamMember(employee)
    else this.removeTeamMember(employee)
  }

  addTeamMember(employee: IUserInformation) {
    let { team } = this.state
    team.members.push(employee)
    this.setState({ team })
  }

  removeTeamMember(employee: IUserInformation) {
    let { team } = this.state
    team.members = team.members.filter(emp => emp.id != employee.id)
    this.setState({ team })
  }

  employeeListComponent(value: IUserInformation, key: number): JSX.Element {
    let isMember = this.state.team.members.find(m => m.id == value.id) != undefined

    return (
      <ListItem button key={key}>
        <ListItemText primary={value.userName} />
        <ListItemSecondaryAction>
          <Checkbox
            color="primary"
            id={value.id == null ? '' : value.id}
            checked={isMember}
            onChange={this.selectHandler(value)}
          />
        </ListItemSecondaryAction>
      </ListItem>
    )
  }

  submitTeam(e: any) {
    e.preventDefault()
    /*
      if(this.props.action == TeamFormActionTypes.UPDATE_TEAM)
        this.props.updateTeam(this.state.team)
      else
        this.props.requestRegistration(this.state.team,WorkflowTypes.Team)
      */
    this.props.handleClose()
  }

  handleChange = (name: string) => (event: any) => {
    let { team } = this.state
    team[name] = event.target.value
    this.setState({ team })
  }

  render() {
    let { team, btnText } = this.state
    return (
      <React.Fragment>
        <Modal
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
          open={this.props.openForm}
          onClose={this.props.handleClose}
        >
          <Paper>
            <Grid container>
              <Grid item md={12}>
                <Typography component="h1" variant="h5">
                  Cereate new Team
                </Typography>
                <hr />
              </Grid>

              <Grid item md={6}>
                <Typography component="h5" variant="subtitle1">
                  Team Information
                </Typography>
              </Grid>
              <Grid item md={3}>
                <Typography component="h4" variant="h5">
                  Team members
                </Typography>
              </Grid>
              <Grid item md={3}>
                <Typography component="h4" variant="h5">
                  Task
                </Typography>
              </Grid>

              <Grid item md={6}>
                  <FormControl margin="normal" required fullWidth>
                    <TextField
                      id="outlined-name"
                      label="Name"
                      name="name"
                      value={team.name}
                      onChange={this.handleChange('name')}
                      margin="normal"
                      variant="outlined"
                    />
                  </FormControl>
                  <FormControl margin="normal" required fullWidth>
                    <TextField
                      id="outlined-multiline-static"
                      label="Description"
                      multiline
                      rows="4"
                      name="description"
                      defaultValue="Default Value"
                      onChange={this.handleChange('description')}
                      margin="normal"
                      variant="outlined"
                      value={team.description}
                    />
                  </FormControl>
                  <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    color="primary"
                    onClick={this.submitTeam}
                  >
                    {btnText}
                  </Button>
              </Grid>

              <Grid item md={3}>
                <List dense>
                  {this.props.employees.map(this.employeeListComponent)}
                </List>
              </Grid>
              <Grid item md={3}>
                <List dense>
                  {team.tasks.map(listComponent)}
                </List>
              </Grid>
            </Grid>
          </Paper>
        </Modal>
      </React.Fragment>
    )
  }
}
function mapStateToProps(state: any) {
  return {
    employees: state.employee.employees
  }
}

export default connect(
  mapStateToProps,
  { requestRegistration }
)(TeamFormCard)
