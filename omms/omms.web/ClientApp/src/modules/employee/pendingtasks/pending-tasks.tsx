import React, { Component } from 'react'
import './../../shared/pending-task/pending-tasks.scss'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Grid from '@material-ui/core/Grid'
import TextField from '@material-ui/core/TextField'
import { withStyles, Theme, createStyles } from '@material-ui/core/styles'
import SearchIcon from '@material-ui/icons/Search'
import { connect } from 'react-redux'
import TaskCard from '../../shared/components/task-card/task-card'
import { getEmployeePendingTasks } from '../../../_setup/actions/taskActions'
import { Redirect, Route, Switch } from 'react-router'
import ViewTask from '../../shared/components/view-task/view-task'
import { BrowserRouter } from 'react-router-dom'
import { getEmployeeInfo } from '../../../_setup/actions/loginActions'
import Tabs from '@material-ui/core/Tabs/Tabs'
import Tab from '@material-ui/core/Tab/Tab'
import UIContainer from './../../shared/_helper/ui-helper/containers-util';

const styles = (theme: Theme) =>
  createStyles({
    paper: {
      maxWidth: '85%',
      margin: 'auto',
      boxShadow: '0px',
      overflow: 'hidden'
    },
    searchBar: {
      borderBottom: '1px solid rgba(0, 0, 0, 0.12)'
    },
    searchInput: {
      fontSize: theme.typography.fontSize,
      marginLeft: '250px'
    },
    block: {
      display: 'block'
    },
    addUser: {
      marginRight: theme.spacing.unit
    },
    contentWrapper: {
      margin: '40px 16px'
    }
  })

function DisplayEmptyList(): JSX.Element {
  return (
    <div
      style={{
        padding: '20px',
        fontFamily: 'roboto',
        fontSize: '25px',
        textAlign: 'center',
        marginTop: '40vh'
      }}
    >
      <p>No pending tasks.</p>
    </div>
  )
}

interface IPendingState {
  data: object
  viewId: string
}

class PendingTasks extends Component<any, IPendingState> {
  state: IPendingState = {
    data: {},
    viewId: ''
  }

  constructor(props: any) {
    super(props)

    this.viewTask = this.viewTask.bind(this)
    this.taskCards = this.taskCards.bind(this)
    this.mainComponent = this.mainComponent.bind(this)
  }

  viewTask(id: string) {
    //TODO fix the refresh issue
    this.setState({
      viewId: id
    })
  }

  componentWillMount() {
    getEmployeeInfo().then(response => {
      if (response.id != null) this.props.getPendingTasks()
    })
  }

  taskCards(value: any, key: number): JSX.Element {
    return (
      <TaskCard container={UIContainer.PENDING_CONTAINER} data={value} viewClicked={this.viewTask} key={key} role={'-1'} />
    )
  }

  mainComponent(classes: any) {
    return (
      <div>
        <AppBar position="static">
          <Tabs value={0}>
            <Tab label="Pending Tasks" />
          </Tabs>
        </AppBar>

        <div className={classes.paper}>
          {this.props.tasks != undefined &&
          this.props.tasks.tasks != undefined ? (
            this.props.tasks.tasks.length == 0 ? (
              <DisplayEmptyList />
            ) : (
              <div>{this.props.tasks.tasks.map(this.taskCards)}</div>
            )
          ) : (
            <DisplayEmptyList />
          )}
        </div>
      </div>
    )
  }

  render() {
    const { classes } = this.props
    return (
      <BrowserRouter>
        <Switch>
          <Route
            exact
            path={'/pending-tasks/employee'}
            component={this.mainComponent}
          />
          <Route
            path={'/pending-tasks/view-task'}
            component={() => {
              return (
                <div>
                  {this.props.tasks.tasks.map((value: any, key: number) => {
                    if (value.id == this.state.viewId)
                      return <ViewTask data={value} key={key} />
                    else return null
                  })}
                </div>
              )
            }}
          />
        </Switch>
      </BrowserRouter>
    )
  }
}

function mapStateToProps(state: any) {
  return {
    tasks: state.tasks
  }
}
export default connect(
  mapStateToProps,
  { getPendingTasks: getEmployeePendingTasks }
)(withStyles(styles)(PendingTasks))
