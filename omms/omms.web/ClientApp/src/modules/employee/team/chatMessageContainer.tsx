import React, { Component } from 'react'
import './team.scss'
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import { faFileUpload, faDownload } from '@fortawesome/free-solid-svg-icons'
import TextField from '@material-ui/core/TextField';
import { Grid } from '@material-ui/core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { MessageRequest, MessageResponse } from '../../../_infrastructure/model/messageModel';
import Button from '@material-ui/core/Button/Button';
import { getBase64 } from '../../_helper/file-encode';
import { connect } from 'react-redux';
import { ChatroomState } from '../../../_infrastructure/state/chatroomState';
import { sendMessage } from '../../../_setup/actions/chatroomActions';
import { downloadFile } from '../../_helper/file-util';
interface IState {
    message: MessageRequest
}

interface IProps {
    chatroom: ChatroomState,
    sendMessage: (message: MessageRequest) => void
}

class ChatMessageContainer extends Component<IProps, IState>{
    state = {
        message: {
            ThreadId: '',
            TextMessage: '',
            Document: null,
            MessageBy: ''
        }
    }

    constructor(props: any) {
        super(props)
        this.message = this.message.bind(this)
        this.fileInputHandler = this.fileInputHandler.bind(this)
        this.handleMessageSubmit = this.handleMessageSubmit.bind(this)
        this.handleTextChange = this.handleTextChange.bind(this)
        this.fileInputChangeHandler = this.fileInputChangeHandler.bind(this)
        this.handleDownload = this.handleDownload.bind(this)
    }
    componentDidMount() {
        this.setState({
            message: {
                ThreadId: this.props.chatroom.threadId,
                TextMessage: '',
                Document: null,
                MessageBy: this.props.chatroom.memberId
            }
        })
    }

    fileInputHandler() {
        let file = document.getElementById('messageFileInput')
        if (file != null)
            file.click()
    }
    async fileInputChangeHandler() {
        let file = (document.getElementById('messageFileInput') as HTMLInputElement).files;

        if (file != null) {
            let base64 = await getBase64(file[0] as Blob);
            console.log(file[0])
            this.setState({
                message: {
                    ThreadId: this.props.chatroom.threadId,
                    TextMessage: this.state.message.TextMessage,
                    Document: {
                        Date: file[0].lastModified,
                        Ref: '',
                        Note: '',
                        Mimetype: file[0].type,
                        Type: null,
                        Filename: file[0].name,
                        File: base64,
                        // only for viewing documents from a custom url (esp. in work items)
                        Id: null,
                        OverrideFilePath: 'api/documents/',
                    },
                    MessageBy: this.props.chatroom.memberId
                }
            })
        }
    }

    handleDownload(event: any) {
        let fileId: string = event.target.id;
        downloadFile(fileId)
    }

    message(value: MessageResponse, key: number): JSX.Element {
        return (
            <ListItem key={key}>
                {
                    value.document == null ?
                        <Grid container className="messageComponent messageFromOther">
                            <Grid item md={12}>
                                <p className="messageContent">{value.textMessage}</p>
                                <p>{value.messageBy.userName} : {value.date}</p>
                            </Grid>
                        </Grid>
                        :
                        <Grid container className="messageComponent messageFromOther">
                            <Grid item md={2}>
                                <a id={value.document.id} title={value.document.filename}>
                                    <Button id={value.document.id} onClick={this.handleDownload}>
                                        <FontAwesomeIcon icon={faDownload} className="fileIcon" size="4x" />
                                    </Button>
                                </a>
                            </Grid>
                            <Grid item md={10}>
                                <p>{value.document.filename}</p>
                            </Grid>
                            <Grid item md={12}>
                                <p className="messageContent">{value.textMessage}</p>
                                <p>{value.messageBy.userName} : {value.date}</p>
                            </Grid>
                        </Grid>
                }
            </ListItem>
        )
    }

    handleMessageSubmit(e: any) {
        e.preventDefault()
        this.props.sendMessage(this.state.message)
    }

    handleTextChange(event: any) {
        this.setState({
            message: {
                ThreadId: this.props.chatroom.threadId,
                TextMessage: event.target.value,
                Document: this.state.message.Document,
                MessageBy: this.props.chatroom.memberId
            }
        })
    }
    render() {
        console.log(this.props.chatroom)
        return (
            <div>
                <p className="teamHeader">Messages</p>
                <div className="messageContainer">
                    <List className="messages">
                        {this.props.chatroom.messages.map(this.message)}
                    </List>
                    <div className="massageFrom">
                        <form>
                            <Grid container>
                                <Grid item md={2}>
                                    <input type="file" id="messageFileInput" onChange={this.fileInputChangeHandler} hidden></input>
                                    <div className="fileInput" onClick={this.fileInputHandler}>
                                        <FontAwesomeIcon icon={faFileUpload} className="fileIcon" size="4x" />
                                    </div>
                                </Grid>
                                <Grid item md={8}>
                                    <TextField
                                        className="messageBox"
                                        id="outlined-multiline-flexible"
                                        placeholder="Type your message here..."
                                        multiline
                                        rows="3"
                                        value={this.state.message.TextMessage}
                                        onChange={this.handleTextChange}
                                        variant="outlined"
                                    />
                                </Grid>
                                <Grid item md={2}>
                                    <Button variant="contained" color="primary" type="submit" className="sendButton" onClick={this.handleMessageSubmit}>
                                        Send
                            </Button>
                                </Grid>
                            </Grid>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state: any) {
    return {
        chatroom: state.chatroom
    }
}


export default connect(mapStateToProps, { sendMessage })(ChatMessageContainer)