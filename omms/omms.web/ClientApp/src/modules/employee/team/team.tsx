import React, { Component } from 'react'
import './team.scss'
import TeamListContainer from './teamListContainer'
import Grid from '@material-ui/core/Grid'
import ChatMessageContainer from './chatMessageContainer'
import TeamMemberContainer from './teamMemberContainer'
import Paper from '@material-ui/core/Paper'
import { connect } from 'react-redux'
import { loadEmployeeThreads } from '../../../_setup/actions/threadActions'
import { ThreadModel } from '../../../_infrastructure/model/threadModel'
import { IUserInformation } from '../../../_infrastructure/model/userInformationModel'
import { getEmployeeInfo } from './../../../_setup/actions/loginActions'
import {
  setupChatroom,
  loadMessages
} from '../../../_setup/actions/chatroomActions'

interface IProps {
  employeeInfo: IUserInformation
  threads: ThreadModel[]
  loadEmployeeThreads: (id: string) => void
  loadMessages: (threadId: string) => void
  setupChatroom: (threadId: string, memberId: string) => void
}

interface IState {
  employeeId: string
  threadId: string
}
class Team extends Component<IProps, IState> {
  constructor(props: any) {
    super(props)
    this.state = {
      employeeId: '',
      threadId: ''
    }
    this.changeActiveThread = this.changeActiveThread.bind(this)
  }

  componentWillMount() {
    let employee = getEmployeeInfo()
    employee.then(res => {
      if (res.id != null) {
        this.props.loadEmployeeThreads(res.id)
        this.setState({
          employeeId: res.id
        })
        console.log('FROM CHANG CB')
        console.log(this.state)
        this.props.setupChatroom(this.state.threadId, res.id)
      }
    })
  }

  changeActiveThread(id: string) {
    this.setState({
      threadId: id
    })
    console.log('FROM CHANG AT')
    console.log(this.state)
    this.props.setupChatroom(id, this.state.employeeId)
  }

  public render() {
    console.info('STATE : ')
    console.log(this.state)
    return (
      <Paper>
        <Grid container className="teamContainer">
          <Grid item md={3}>
            <TeamListContainer
              changeActiveThread={this.changeActiveThread}
              threads={this.props.threads}
            />
          </Grid>
          <Grid item md={6}>
            <ChatMessageContainer />
          </Grid>
          <Grid item md={3}>
            <TeamMemberContainer />
          </Grid>
        </Grid>
      </Paper>
    )
  }
}

function mapStateToProps(state: any) {
  return {
    threads: state.thread.threads
  }
}

export default connect(
  mapStateToProps,
  { loadEmployeeThreads, setupChatroom, loadMessages }
)(Team)
