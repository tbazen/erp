import React, { Component } from 'react'
import './team.scss'
import List from '@material-ui/core/List'
import { ThreadModel } from '../../../_infrastructure/model/threadModel'
import { connect } from 'react-redux'
import { loadTeamMembers } from '../../../_setup/actions/employeeActions'
import ListItem from '@material-ui/core/ListItem'
import { loadMessages } from '../../../_setup/actions/chatroomActions'

interface PropsFromDispatch {
  loadMessages: (threadId: string) => void
}

interface IProps {
  threads: ThreadModel[]
  loadTeamMembers: (teamId: string) => void
  changeActiveThread: (threadId: string) => void
}

type AllProps = IProps & PropsFromDispatch

class TeamListContainer extends Component<AllProps> {
  constructor(props: any) {
    super(props)
    this.memberHandler = this.memberHandler.bind(this)
    this.singleTeam = this.singleTeam.bind(this)
  }

  memberHandler(event: any) {
    let thread = this.props.threads.find(t => t.id == event.target.id)
    if (thread != undefined) {
      let teamId = thread.team.id
      this.props.changeActiveThread(thread.id)
      this.props.loadTeamMembers(teamId as string)
      this.props.loadMessages(thread.id)
    }
  }

  singleTeam(value: ThreadModel, key: number) {
    return (
      <div key={key}>
        <ListItem
          button
          id={value.id}
          className="threadComponent"
          onClick={this.memberHandler}
        >
          {value.name}
        </ListItem>
      </div>
    )
  }

  render() {
    return (
      <div>
        <p className="teamHeader">Threads </p>
        <List className="teamListContainer">
          {this.props.threads.map(this.singleTeam)}
        </List>
      </div>
    )
  }
}

function mapStateToProps() {
  return {}
}

export default connect(
  mapStateToProps,
  { loadTeamMembers, loadMessages }
)(TeamListContainer)
