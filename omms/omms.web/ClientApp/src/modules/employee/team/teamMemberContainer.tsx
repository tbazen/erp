import React, { Component } from 'react'
import './team.scss'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import { connect } from 'react-redux'

class TeamMemberContainer extends Component<any> {
  constructor(props: any) {
    super(props)
  }

  member(value: any, key: any) {
    console.log(value)
    return (
      <ListItem key={key}>
        <p>{value.userName}</p>
      </ListItem>
    )
  }

  render() {
    return (
      <div>
        <p className="teamHeader">Members</p>
        <List className="memberContainer">
          {this.props.members.map(this.member)}
        </List>
      </div>
    )
  }
}

function mapStateToProps(state: any) {
  return {
    members: state.employee.employees
  }
}

export default connect(mapStateToProps)(TeamMemberContainer)
