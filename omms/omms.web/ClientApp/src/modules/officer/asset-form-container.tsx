import React, { Component } from 'react'
import Button from '@material-ui/core/Button'
import FormControl from '@material-ui/core/FormControl'
import Paper from '@material-ui/core/Paper'
import Typography from '@material-ui/core/Typography'
import TextField from '@material-ui/core/TextField/TextField'
import Grid from '@material-ui/core/Grid'
import { connect } from 'react-redux'
import DeleteIcon from '@material-ui/icons/Delete'
import Radio from '@material-ui/core/Radio'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import {  createAsset } from '../../_setup/actions/assetActions'
import {  AssetResponse, AssetType, AssetRequest, AssetModel } from '../../_infrastructure/model/assetModel'
import { DocumentRequest } from '../../_infrastructure/model/documentModel'
import { getBase64 } from '../_helper/file-encode'
import IconButton from '@material-ui/core/IconButton';
import Color from '../shared/_helper/color-container/index';
import { AssetSearhParams } from '../../_infrastructure/model/asset-search-params';
import AssetGisInformation from '../shared/asset/asset-gis-information';
import UIContainer from '../shared/_helper/ui-helper/containers-util';

interface IState{
  asset:AssetRequest,
}

interface IProps{
  asset:AssetResponse
  container : UIContainer
  searchParams:AssetSearhParams
}
interface  PropsFromDispatch{
  createAsset:(asset:AssetRequest,container:UIContainer)=>void,
}

type AllProps = IProps & PropsFromDispatch

class AssetFormContainer extends Component <AllProps,IState> {
    constructor(props:any){
      super(props)
      let {asset,searchParams} = this.props
      this.state={
        asset:{
          id:asset.id,
          wfid:asset.wfid,
          name:asset.name,
          note : '',
          description:asset.description,
          geoLocation:asset.geoLocation,
          typeId:asset.assetType == undefined ? 0 : asset.assetType.id,
          pkuId:searchParams.pkuId,
          images:[],
        }
      }

      this.handleChange = this.handleChange.bind(this)
      this.submitAsset = this.submitAsset.bind(this)
      this.assetType = this.assetType.bind(this)
      this.handleFileChange = this.handleFileChange.bind(this)
      this.addFile = this.addFile.bind(this)
      this.removeFile = this.removeFile.bind(this)
      this.fileRow = this.fileRow.bind(this)
      this.updateTypeId = this.updateTypeId.bind(this)
    }

    handleChange = (name:string)=>(event:any)=>{
      let {asset} = this.state
      asset[name] = event.target.value
      this.setState({
        asset
      })
    }

    async handleFileChange(){
      let file = (document.getElementById('assetImageInput') as HTMLInputElement).files;
        if(file!=null)
        {
            let base64 = await getBase64(file[0] as Blob); 
            let doc: DocumentRequest =  {
                Date:file[0].lastModified,
                Ref: '',
                Note: '',
                Mimetype: file[0].type,
                Type:  null,
                Filename: file[0].name,
                File: base64,
                Id:  null,
                OverrideFilePath: 'api/documents/',
            }

            this.addFile(doc)
            file = null
        }
    }

    addFile(doc:DocumentRequest){
      let {asset} = this.state
      asset.images.push(doc)
      this.setState({asset})
    }

    removeFile = (index:number)=>()=>{
      let {asset} = this.state
      asset.images.splice(index,1)
      this.setState({asset})
    }

    fileRow(value:DocumentRequest , key:number){
      return (
        <Grid style={{
                  padding:"5px",
                  marginBottom:"5px",
                  border:"1px solid lightgray" 
              }}
              container 
              key={key} 
              justify="center">
           <Grid item xs={11} >
              <p style={{paddingTop:"10px"}}>{value.Filename}</p> 
           </Grid>
           <Grid item xs={1}>
              <IconButton onClick = {this.removeFile(key)} >
                 <DeleteIcon/>
              </IconButton>
           </Grid>
        </Grid>
      )
    }
    submitAsset(){
      let {asset} = this.state
      this.setState({asset})
      this.props.createAsset(this.state.asset,this.props.container) 
    }

    assetType(value:AssetType,key:number){
      let stringKey:string = `${key}`
      return <FormControlLabel key={key} value={stringKey} control={<Radio color="primary"/>} label={value.name} /> 
    }

    updateTypeId(newTypeId:number){
      let {asset} = this.state
      asset.typeId = newTypeId
      this.setState({asset})
    }
    render(){        
        let {asset} = this.state
        let {searchParams } = this.props
        return (
                <Paper style={{ width:'100%',minHeight:'70vh', padding: '40px' }}>
                    <Grid container justify="center">
                    <Grid item md={10}>
                        <Typography component="h1" variant="h5">
                            Fill asset information 
                        </Typography>
                        <hr/>
                    </Grid>
                    <Grid item md={10}>
                        <FormControl margin="normal" required fullWidth>
                        <TextField
                            id="outlined-name"
                            placeholder="Name"
                            name="name"
                            value={asset.name}
                            onChange={this.handleChange('name')}
                            margin="normal"
                            variant="outlined"
                        />
                        </FormControl>

                        <FormControl margin="normal" required fullWidth>
                        <TextField
                            id="outlined-name"
                            placeholder="Location Id"
                            name="geoLocation"
                            value={asset.geoLocation}
                            disabled
                            margin="normal"
                            variant="outlined"
                        />
                        </FormControl>
                        
                        <FormControl margin="normal" required fullWidth>
                        <TextField
                            id="outlined-multiline-static"
                            placeholder="Description"
                            multiline
                            rows="4"
                            name="description"
                            onChange={this.handleChange('description')}
                            margin="normal"
                            variant="outlined"
                            value={asset.description}
                        />
                        </FormControl>
                        <FormControl margin="normal" required fullWidth>
                        <TextField
                            id="outlined-multiline-static"
                            placeholder="Note"
                            multiline
                            rows="3"
                            name="note"
                            onChange={this.handleChange('note')}
                            margin="normal"
                            variant="outlined"
                            value={asset.note}
                        />
                        </FormControl>
                    </Grid>
                    <Grid item md={10}>
                      {this.state.asset.images.map(this.fileRow)}
                    </Grid>
                    <Grid item md={10}>
                      <input id='assetImageInput' type="file" onChange={this.handleFileChange}/>
                    </Grid>
                    <Grid item md={10} >
                      {<AssetGisInformation 
                                           searchParams = {searchParams}
                                           updateTypeId = {this.updateTypeId}
                                           />}
                    </Grid>
                    </Grid>
                    <Grid container justify="center">
                         <Grid item md={10}>
                          <Grid container justify="flex-start"> 
                           <Grid item md={6}>
                              <Button
                                style={{
                                    borderRadius:'2px',
                                    marginBottom:"10px",
                                    color:Color.PRIMARY_FOREGROUND,
                                    backgroundColor:Color.PRIMARY                              
                                }}
                                fullWidth
                                type="submit"
                                onClick={this.submitAsset}
                                >
                                Submit
                              </Button>
                          </Grid>
                          </Grid>
                         </Grid>
                    </Grid>
                </Paper>
        );
    }
}

function mapStateToProps(){
  return {
  }
}

export default connect(mapStateToProps,{createAsset})(AssetFormContainer);
