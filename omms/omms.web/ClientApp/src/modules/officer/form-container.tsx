import React, { Component } from 'react'
import AppBar from '@material-ui/core/AppBar'
import Tabs from '@material-ui/core/Tabs'
import Tab from '@material-ui/core/Tab'
import CreateTask from './../shared/create-task/create-task';
import TeamFormContainer from './team-form-container'
import AssetFormContainer from './asset-form-container';
import { AssetResponse } from '../../_infrastructure/model/assetModel';
import { IUserInformation } from 'src/_infrastructure/model/userInformationModel';
import { changeActiveIndex } from '../../_setup/actions/drawer-actions';
import { connect } from 'react-redux';
import Role from '../shared/_helper/role-util/roles';
import { OfficerItems } from '../shared/_helper/slideout-index/officer-items';
import { TeamModel } from '../../_infrastructure/model/teamModel';
import UIContainer from './../shared/_helper/ui-helper/containers-util';
import { AssetSearhParams, AssetTypes } from '../../_infrastructure/model/asset-search-params';

interface IState{
    value:number
}
interface PropsFromState{
    user : IUserInformation
}
interface PropsFromDispatch{
    changeActiveIndex:(index:number)=>void
}
  
type AllProps = PropsFromState &
                  PropsFromDispatch

                  
let sp:AssetSearhParams ={
    pkuId:229,
    assetType:AssetTypes.PIPELINE
}
class FormContainer extends Component <AllProps,IState> {
    constructor(props:any) {
        super(props)
        this.handleChange = this.handleChange.bind(this)
        this.state = {
            value:0
        }
    }
    
    componentWillMount(){
        let {user} = this.props
        if(user.role == Role.TECHNICAL_OFFICER)
            this.props.changeActiveIndex(OfficerItems.CREATE)
    }
    //@ts-ignore
    handleChange(event:any,value:number){
        this.setState({value})
    }

    render() {
        const { value } = this.state;
        let team:TeamModel = {
            id:null,
            wfid:null,
            name:'',
            description:'',
            members:[],
            tasks:[],
            note:''
        }
        return (
        <div>
            <AppBar position="sticky" color="default">
            <Tabs
                 indicatorColor="primary"
                 textColor="primary"  
                 value={value} 
                 onChange={this.handleChange}>
                 
                <Tab label="Task" />
                <Tab label="Team" />
            </Tabs>
            </AppBar>
            {value === 0 && <CreateTask/>}
            {value === 1 && <TeamFormContainer container={UIContainer.FORM_CONTAINER} team={team}/>}
        </div>
        );
    }
}

function mapStateToProps(state:any){
    return {
      user: state.login.userInfo
    }
}
  
function mapDispatchToProps(dispatch:any){
    return {
      changeActiveIndex:(index:number)=>dispatch(changeActiveIndex(index))
    }
  }
export default connect(mapStateToProps,mapDispatchToProps)(FormContainer)