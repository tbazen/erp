import React, { Component,Fragment } from 'react'
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField/TextField'
import Grid from '@material-ui/core/Grid';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import List from '@material-ui/core/List';
import Checkbox from '@material-ui/core/Checkbox';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import { IUserInformation } from '../../_infrastructure/model/userInformationModel';
import FormControl from '@material-ui/core/FormControl'
import { connect } from 'react-redux'
import {createTeam} from '../../_setup/actions/teamActions'
import { TeamModel } from '../../_infrastructure/model/teamModel'
import { loadAllEmployees, filteringEmployees, cancelEmployeeFiltering } from '../../_setup/actions/employeeActions';
import { EmployeeState } from '../../_infrastructure/state/employee-state';
import { filterEmployee } from '../../_setup/actions/filter-actions';
import SearchField from './../shared/components/search-bar/search-field';
import UIContainer from '../shared/_helper/ui-helper/containers-util';
import Color from '../shared/_helper/color-container/index';
import LoadingCircle from '../shared/components/loading/loading-circle';



interface IState{
  team:TeamModel
}


interface IProps{
  team:TeamModel
  container : UIContainer
}

interface PropsFromState{
  employee : EmployeeState,
  filteredEmployees: IUserInformation[]
}

interface PropsFromDispatch{
  loadAllEmployees:()=>void
  createTeam:(team:TeamModel,container:UIContainer)=>void
  filterEmployee:(name:string,employee:IUserInformation[])=>void,
  filteringEmployees:()=>void,
  cancelEmployeeFiltering:()=>void
}

type AllProps = IProps & 
                PropsFromState &
                PropsFromDispatch

class TeamFormContainer extends Component <AllProps,IState> {
    constructor(props:AllProps){
      super(props)
      this.submitTeam = this.submitTeam.bind(this);
      this.employeeListComponent = this.employeeListComponent.bind(this);
      this.selectHandler = this.selectHandler.bind(this)
      this.handleFiltering = this.handleFiltering.bind(this)
      let {team} = this.props
      this.state={team}
    }

    componentDidMount(){
      this.props.loadAllEmployees()
    }

    selectHandler =(employee:IUserInformation)=>(event:any)=>{
      if(event.target.checked)
         this.addTeamMember(employee)
      else
         this.removeTeamMember(employee)
    }

    addTeamMember(employee:IUserInformation){
      let {team}  = this.state
      team.members.push(employee)
      this.setState({team})
    }

    removeTeamMember(employee:IUserInformation){
      let {team} = this.state
      team.members = team.members.filter(emp=> emp.id != employee.id)
      this.setState({team})
    }

    employeeListComponent(value:IUserInformation,key:number):JSX.Element{
      let isMember = this.state.team.members.find(m => m.id == value.id) != undefined;
      
      return(
        <ListItem button key={key}>
          <ListItemText primary={value.userName} />
          <ListItemSecondaryAction>
            <Checkbox color="primary" id={value.id == null ? '': value.id} checked={isMember}  onChange= {this.selectHandler(value)}/>
          </ListItemSecondaryAction>
        </ListItem>
      )
    }
    

    submitTeam(e:any){
      e.preventDefault()
      this.props.createTeam(this.state.team,this.props.container)
    }
    
    handleChange = (name:string) => (event:any) => {
      let {team} = this.state
      team[name] = event.target.value
      this.setState({team});
    };

    handleFiltering(event:any){
      let {employee} = this.props
      let name:string  = event.target.value
      if(!employee.loading && !employee.error){
        if(name.length == 0 )
        this.props.cancelEmployeeFiltering()
        else{
            this.props.filteringEmployees()
            this.props.filterEmployee(name,employee.employees)
        }
      }
    }


    render()
      {
        let {team } = this.state;
        let {employee,filteredEmployees,container} = this.props
        return (
            <Paper style={{ margin: ' 40px 7%', padding: '40px' }}>
                <Grid container justify="flex-start" >
                <Grid item md={12}>
                    <Typography component="h1" variant="h5">
                        Add Team Information
                    </Typography>
                    <hr/>
                </Grid>
                <Grid item md={6} xs={12}>
                    <FormControl margin="normal" required fullWidth>
                    <TextField
                        id="outlined-name"
                        label="Name"
                        name="name"
                        value={team.name}
                        onChange={this.handleChange('name')}
                        margin="normal"
                        variant="outlined"
                    />
                    </FormControl>
                    <FormControl margin="normal" required fullWidth>
                    <TextField
                        id="outlined-multiline-static"
                        label="Description"
                        multiline
                        rows="4"
                        name="description"
                        defaultValue="Default Value"
                        onChange={this.handleChange('description')}
                        margin="normal"
                        variant="outlined"
                        value={team.description}
                    />
                    </FormControl>
                    

                    {
                      container != UIContainer.FORM_CONTAINER &&
                      <FormControl margin="normal" required fullWidth>
                      <TextField
                          id="outlined-name"
                          label="Note"
                          name="note"
                          value={team.note}
                          onChange={this.handleChange('note')}
                          margin="normal"
                          variant="outlined"
                      />
                      </FormControl>
                    }

                    <Button
                        type="submit"
                        fullWidth
                        style={{
                          color:Color.PRIMARY_FOREGROUND,
                          backgroundColor:Color.PRIMARY
                        }}
                        onClick={this.submitTeam}
                        >
                        submit
                        </Button>
                </Grid>
                <Grid item md={6} xs={12}>
                    <div 
                        style={{
                          padding:"10px",
                          backgroundColor:"white"
                        }}>
                      <SearchField handler={this.handleFiltering}/>
                    </div>
                    <List  dense style={{maxHeight:"60vh",overflowY:"auto"}} >
                       {
                         employee.loading &&
                         <span style={{ top:'50%',left:'50%',position:'relative'}}><LoadingCircle/></span>
                       }
                       {
                         employee.error &&
                         <p>{employee.errorMessage}</p>
                       }
                       {
                         employee.filtering &&
                         filteredEmployees.map(this.employeeListComponent)
                       }
                       {
                         !employee.filtering &&
                         employee.employees.map(this.employeeListComponent)
                       }
                    </List>
                </Grid>      
                </Grid>
            </Paper>
        );
      }
}

function mapStateToProps(state:any){
  return {
    employee : state.employee,
    filteredEmployees : state.filter.filteredEmployee
  }
}

function mapDispatchToProps(dispatch:any){
  return {
    createTeam : (team:TeamModel,container:UIContainer)=>dispatch(createTeam(team,container)),
    loadAllEmployees:()=>dispatch(loadAllEmployees()),
    filterEmployee:(name:string,employees:IUserInformation[])=>dispatch(filterEmployee(name, employees)),
    filteringEmployees: ()=>dispatch(filteringEmployees()),
    cancelEmployeeFiltering : ()=>dispatch(cancelEmployeeFiltering()) 
  }
}


//export default connect(mapStateToProps,{createTeam,loadAllEmployees})(TeamFormContainer);
export default connect(mapStateToProps,mapDispatchToProps)(TeamFormContainer);
