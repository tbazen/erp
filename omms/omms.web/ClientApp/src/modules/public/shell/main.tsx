import React, { Component,Fragment } from 'react'
import './main.scss'
import { connect } from 'react-redux'
import { getUserInfo } from '../../../_setup/actions/loginActions'
import { loadNewNotifications } from '../../../_setup/actions/notificationActions'
import Slideout from '../../shared/components/slideout/slideout'
import LoadingModal from './../../shared/components/loading/loading-modal'
import { loadMetaData } from './../../../_setup/actions/metadata-actions'
import WorkflowNoteModal from './../../shared/components/workflow-modal/workflow-note-modal'
interface IMain {
  role: number //1 Admin, 2 Employee
  admin: boolean
  token: string
}

class MainComponent extends Component<any, any> {
  state: IMain = {
    role: 0,
    admin: false,
    token: ''
  }

  constructor(props:any){
    super(props)
    this.autoReload = this.autoReload.bind(this)
  }

  async componentWillMount() {
    await this.props.getUserInfo()
    let n: number = this.props.userInfo.role
    this.setState({
      role: n
    })
  }

  componentDidMount(){
    this.props.loadMetaData()
    this.autoReload()
  }

  autoReload(){
    setInterval(()=>{
      this.props.loadMetaData()
    },60000)
  }
  
  public render() {
    let role = this.props.userInfo.role
    return role != undefined 
           ?
            <Fragment>
              <LoadingModal/>
              <WorkflowNoteModal/>
              <Slideout role={role} /> 
            </Fragment>
           : null
  }
}

const mapStateToProps = (state: any) => ({
  userInfo: state.login.userInfo
})


const Main = connect(
  mapStateToProps,
  { 
    getUserInfo, 
    loadNewNotifications,
    loadMetaData
  }
)(MainComponent)

export default Main
