export function authHeader(): { Authorization: string } {
  let tokenFromlocalStorage = localStorage.getItem('user')
  // return authorization header with jwt token
  if (tokenFromlocalStorage != null && tokenFromlocalStorage != '') {
    return { Authorization: 'Bearer ' + tokenFromlocalStorage }
  } else return { Authorization: '' }
}
