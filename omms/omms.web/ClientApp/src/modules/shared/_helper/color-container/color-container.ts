export const PRIMARY = '#242a52e7'
export const SECONDARY = '#9e2e2e'
export const HAPPY = '#1eaf1e'
export const PRIMARY_FOREGROUND = 'white'
export const ACTIVE_ITEM  = '#242a52'
export const PRIMARY_DARK = '#171b35'
export const TRANSPARENT = 'transparent'