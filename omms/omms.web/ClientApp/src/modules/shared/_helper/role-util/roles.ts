const enum Role {
  EMPLOYEE = '1',
  ADMIN = '2',
  TECHNICAL_OFFICER = '3',
  TECHNICAL_SUPERVISOR = '4'
}

export default Role
