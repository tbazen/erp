import React from 'react'
import { Route, Switch, Redirect } from 'react-router';
import TaskComponent from './../../components/task-component/task-component'
import  Team  from './../../../admin/team/team';
import { NotFound } from './../../not-found/not-found';
import PendingTasks from './../../../admin/pendingtasks/pending-tasks';
import DashboardContainer from './../../../admin/admin-dashboard/dashboardContainer';
import RegisteredAssetContainer from '../../asset/registered/registered-asset-container'
import Notifications from './../../../admin/notifications/notifications';
import MapContainer from '../../map/map-container'
import { logout } from '../../../../_setup/actions/loginActions';
import AssetDetailPage from '../../components/pending/pending-asset/asset-detail-page'
import TeamDetailPage from '../../team/team-detail-page';
import ViewTask from '../../components/view-task/view-task'

export default function AdminRoutes(): JSX.Element {
    return (
        <Switch>
            <Route exact path={'/'} component={() => {
                return <Redirect to={'/tasks'} />
            }} />
            <Route exact path={'/tasks'} component={() => {
                return <Redirect to={'/tasks/admin'} />
            }} />
            <Route path={'/tasks/admin'} component={TaskComponent} />
            <Route path={'/tasks/view-task'} component={() => {
              return <Redirect to={'/tasks/admin'} />
            }} />
            <Route path={'/map'} component={MapContainer} />
            <Route path={'/asset-detail/:assetid'} component={AssetDetailPage} />
            <Route path={'/admin-team'} component={Team} />
              <Route exact path={'/asset'} component={() => {
                  return <Redirect to={'/asset/admin'} />
              }} />
            <Route path={'/asset/admin'} component={RegisteredAssetContainer} />
            <Route exact path={'/pending-tasks'} component={() => {
                return <Redirect to={'/pending-tasks/admin'} />
            }} />
            <Route path={'/pending-tasks/admin'} component={PendingTasks} />
            <Route path={'/notifications'} component={Notifications} />
            <Route path={'/dashboard'} component={DashboardContainer} />
            <Route path={'/logout'} component={() => {
                logout().then(res => {
                    if (res == true)
                        window.location.replace('/')
                }
                ).catch(error => console.error(`Cant logout ${error}`))
                return null
            }} />
            <Route path={'/team-detail/:wfid/:teamId'} component={TeamDetailPage} />
          <Route path={'/view-task/:taskid'} component={ViewTask} />
            <Route component={NotFound} />
            <Redirect to={'/'} />
        </Switch>
    )
}

