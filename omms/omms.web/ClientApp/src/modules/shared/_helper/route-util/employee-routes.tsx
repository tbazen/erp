import React, { Component } from 'react'
import { Switch, Redirect, Route } from 'react-router'
import { NotFound } from '../../not-found/not-found'
import Team from './../../../employee/team/team'
import PendingTasks from './../../../employee/pendingtasks/pending-tasks'
import TaskComponent from './../../components/task-component/task-component'
import RegisteredAssetContainer from '../../asset/registered/registered-asset-container'
import { logout } from '../../../../_setup/actions/loginActions';
import AssetDetailPage from '../../components/pending/pending-asset/asset-detail-page';
import TeamDetailPage from '../../team/team-detail-page';
import Map from '../../map/map'
import MapContainer from '../../map/map-container'
import IndividualTaskContainer from './../../task/indevidual-task-container'
import TeamTaskContainer from './../../task/team-task-container'
import ViewTask from '../../components/view-task/view-task'
import PendingContainer from '../../components/pending/pending-container';
import RejectedContainer from '../../components/rejected/rejected-container';


export default function EmployeeRoutes(): JSX.Element {
    return (
        <Switch>
            <Route exact path={'/'} component={() => {
                return <Redirect to={'/tasks/individual'} />
            }} />
            <Route path={'/tasks/individual'} component={IndividualTaskContainer}/>
            <Route path={'/tasks/team'} component={TeamTaskContainer}/>
            <Route path={'/tasks/:type'} component={TaskComponent} />
            <Route path={'/team'} component={Team} />
            <Route path={'/view-task/:taskid'} component={ViewTask} />
            <Route path={'/map'} component={MapContainer} />
            <Route exact path={'/asset'} component={() => {
                return <Redirect to={'/asset/employee'} />
            }} />
            <Route path={'/asset/employee'} component={RegisteredAssetContainer} />
            <Route path={'/pending-tasks/employee'} component={PendingTasks} />
            <Route path={'/logout'} component={() => {
                logout().then(res => {
                    if (res == true)
                        window.location.replace('/')
                }
                ).catch(error => console.error(`Cant logout ${error}`))
                return null
            }} />

            <Route path={'/rejected'} component={RejectedContainer} />
            <Route path={'/pending'} component={PendingContainer} />

            <Route path={'/asset-detail/:assetid'} component={AssetDetailPage} />

            <Route path={'/team-detail/:wfid/:teamId'} component={TeamDetailPage} />
            
            <Route path={'/task/view-task'} component={() => {
                return <Redirect to={'/'} />
            }} />
            <Route component={NotFound} />
            <Redirect to={'/'} />
        </Switch>
    )
}

