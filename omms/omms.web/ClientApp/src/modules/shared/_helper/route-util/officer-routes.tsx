import React, { Component } from 'react'
import { Route, Switch, Redirect } from 'react-router';

import TaskComponent from './../../components/task-component/task-component'
import Team  from './../../../admin/team/team';
import { NotFound } from './../../not-found/not-found';
import RegisteredAssetContainer from '../../asset/registered/registered-asset-container'
import PendingContainer from '../../components/pending/pending-container';
import RejectedContainer from '../../components/rejected/rejected-container';
import { logout } from '../../../../_setup/actions/loginActions';
import FormContainer from '../../../officer/form-container';
import AssetDetailPage from '../../components/pending/pending-asset/asset-detail-page';
import TeamDetailPage from '../../team/team-detail-page'
import MapContainer from '../../map/map-container'
import CreateTask from '../../create-task/create-task'
import ViewTask from '../../components/view-task/view-task'
import EditTask from '../../edit-task/edit-task'
import RegisteredTaskContainer from './../../task/registered-task/registered-task-container';
import Report from '../../report/report'

export default function OfficerRoutes(): JSX.Element {
    return (
        <Switch>
            <Route exact path={'/'} component={() => {
                return <Redirect to={'/tasks'} />
            }} />
            <Route exact path={'/tasks'} component={() => {
                return <Redirect to={'/tasks/officer'} />
            }} />
            {/* <Route path={'/tasks/officer'} component={TaskComponent} /> */}
            
            {/* <Route exact path={'/'} component={TaskComponent} /> */}


            <Route exact path={'/'} component={RegisteredTaskContainer} />
            <Route path={'/tasks/officer'} component={RegisteredTaskContainer} />

            <Route path={'/map'} component={MapContainer} />
            <Route path={'/admin-team'} component={Team} />
            <Route exact path={'/asset'} component={() => {
                return <Redirect to={'/asset/admin'} />
            }} />

            <Route path={'/asset/admin'} component={RegisteredAssetContainer} />

            <Route exact path={'/pending-tasks'} component={() => {
                return <Redirect to={'/pending-tasks/admin'} />
            }} />
            <Route path={'/pending'} component={PendingContainer} />

            <Route path={'/logout'} component={() => {
                logout().then(res => {
                    if (res == true)
                        window.location.replace('/')
                }
                ).catch(error => console.error(`Cant logout ${error}`))
                return null
            }} />
            <Route path={'/registration-form'} component={FormContainer} />
            
            <Route path={'/asset-detail/:assetid'} component={AssetDetailPage} />
            <Route path={'/team-detail/:wfid/:teamId'} component={TeamDetailPage} />
            <Route path={'/rejected'} component={RejectedContainer} />
            <Route path={'/task/create-task'} component={CreateTask} />
            <Route path={'/edit-task/:taskid'} component={EditTask} />
            <Route path={'/view-task/:taskid'} component={ViewTask} />
            <Route path={'/report'} component={Report}/>
            <Route component={NotFound} />
            <Redirect to={'/'} />
        </Switch>
    )
}