import React, { Fragment } from 'react'
import AdminRoutes from './admin-routes'
import EmployeeRoutes from './employee-routes'
import OfficerRoutes from './officer-routes'
import SuperVisorRoutes from './supervisor-routes'
import Role from '../role-util/roles'

export interface IRouteSet {
  routes: JSX.Element
}

function routeSet(): IRouteSet[] {
  return [
    { routes: <Fragment /> },
    { routes: EmployeeRoutes() },
    { routes: AdminRoutes() },
    { routes: OfficerRoutes() },
    { routes: SuperVisorRoutes() }
  ]
}

export function getRouteSet(role: Role): IRouteSet {
  return routeSet()[role]
}
