import React from 'react'
import { Route, Switch, Redirect } from 'react-router';
import TaskComponent from './../../components/task-component/task-component'
import  Team  from './../../../admin/team/team';
import { NotFound } from './../../not-found/not-found';
import RegisteredAssetContainer from '../../asset/registered/registered-asset-container'
import PendingContainer from '../../components/pending/pending-container';
import MapContainer from '../../map/map-container'
import { logout } from '../../../../_setup/actions/loginActions';
import AssetDetailPage from '../../components/pending/pending-asset/asset-detail-page';
import TeamDetailPage from '../../team/team-detail-page'
import RegisteredTaskContainer from './../../task/registered-task/registered-task-container'
import ViewTask from '../../components/view-task/view-task'
import Report from '../../report/report'


export default function SuperVisorRoutes(): JSX.Element {
    return (
        <Switch>
            <Route exact path={'/'} component={() => {
                return <Redirect to={'/tasks'} />
            }} />
            <Route exact path={'/tasks'} component={() => {
                return <Redirect to={'/tasks/supervisor'} />
            }} />
            <Route path={'/tasks/supervisor'} component={RegisteredTaskContainer} />
            <Route exact path={'/'} component={TaskComponent} />
            <Route path={'/map'} component={MapContainer} />
            <Route path={'/admin-team'} component={Team} />

            <Route exact path={'/asset'} component={() => {
                return <Redirect to={'/asset/admin'} />
            }} />
            <Route path={'/asset/admin'} component={RegisteredAssetContainer} />


            <Route path={'/pending'} component={PendingContainer} />
            <Route path={'/logout'} component={() => {
                logout().then(res => {
                    if (res == true)
                        window.location.replace('/')
                }
                ).catch(error => console.error(`Cant logout ${error}`))
                return null
            }} />
            <Route path={'/asset-detail/:assetid'} component={AssetDetailPage} />
            <Route path={'/team-detail/:wfid/:teamId'} component={TeamDetailPage} />
          <Route path={'/view-task/:taskid'} component={ViewTask} />
          <Route path={'/task/view-task'} component={() => {
                return <Redirect to={'/'} />
            }} />
          <Route path={'/report'} component={Report}/>
          <Route component={NotFound} />
            <Redirect to={'/'} />
        </Switch>
    )
}

