export enum EmployeeItems{
    INDEIVIDUAL_TASK = 0,
    TEAM_TASK = 1,
    MAP = 2,
    ASSET = 3,
    PENDING = 4,
    REJECTED = 5,
    LOGOUT = 6
}