export enum SupervisorItems{
    TASK = 0,
    MAP = 1,
    ASSET = 2,
    TEAMS = 3,
    PENDING  = 4,
    REPORT = 5,
    LOGOUT = 6
}