import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { ISlideoutItem, ISlideoutContent } from './slideout-content'
import {
  faTasks,
  faToolbox,
  faSignOutAlt,
  faUsers,
  faBell,
  faThLarge,
  faMap
} from '@fortawesome/free-solid-svg-icons'

export const adminSlideoutContent: ISlideoutContent = {
  items: [
    {
      label: 'Dashboard',
      icon: <FontAwesomeIcon icon={faThLarge} size={'lg'} />,
      navigateTo: '/dashboard'
    },
    {
      label: 'Task',
      icon: <FontAwesomeIcon icon={faTasks} size={'lg'} />,
      navigateTo: '/tasks'
    },
    {
      label: 'Map',
      icon: <FontAwesomeIcon icon={faMap} size={'lg'} />,
      navigateTo: '/map'
    },
    {
      label: 'Asset',
      icon: <FontAwesomeIcon icon={faToolbox} size={'lg'} />,
      navigateTo: '/asset'
    },
    {
      label: 'Teams',
      icon: <FontAwesomeIcon icon={faUsers} size={'lg'} />,
      navigateTo: '/admin-team'
    },
    {
      label: `Notification`,
      icon: <FontAwesomeIcon icon={faBell} size={'lg'} />,
      navigateTo: '/notifications'
    },
    {
      label: 'Logout',
      icon: <FontAwesomeIcon icon={faSignOutAlt} size={'lg'} />,
      navigateTo: '/logout'
    }
  ]
}
