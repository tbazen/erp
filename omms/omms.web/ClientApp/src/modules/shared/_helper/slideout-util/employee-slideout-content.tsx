import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { ISlideoutContent } from './slideout-content'
import {
  faToolbox,
  faSignOutAlt,
  faUsers,
  faUser,
  faSpinner,
  faExclamationTriangle,
  faMap
} from '@fortawesome/free-solid-svg-icons'

export const employeeSlideOutContent: ISlideoutContent = {
  items: [
    {
      label: 'Individual Task',
      icon: <FontAwesomeIcon icon={faUser} size={'lg'} />,
      navigateTo: '/tasks/individual'
    },
    {
      label: 'Team Task',
      icon: <FontAwesomeIcon icon={faUsers} size={'1x'} />,
      navigateTo: '/tasks/team'
    },
    {
      label: 'Map',
      icon: <FontAwesomeIcon icon={faMap} size={'1x'} />,
      navigateTo: '/map'
    },
    {
      label: 'Asset',
      icon: <FontAwesomeIcon icon={faToolbox} size={'lg'} />,
      navigateTo: '/asset'
    },
    {
      label: 'Pending',
      icon: <FontAwesomeIcon icon={faSpinner} size={"lg"} />,
      navigateTo: '/pending'
    },
    {
        label: 'Rejected',
        icon: <FontAwesomeIcon icon={faExclamationTriangle} size={"1x"} />,
        navigateTo: '/rejected'
    },
    // {
    //   label: 'Chat Room',
    //   icon: <FontAwesomeIcon icon={faCommentAlt} size={'lg'} />,
    //   navigateTo: '/team'
    // },
    // {
    //   label: 'Rejected Tasks',
    //   icon: <FontAwesomeIcon icon={faExclamationTriangle} size={'lg'} />,
    //   navigateTo: '/pending-tasks'
    // },
    {
      label: 'Logout',
      icon: <FontAwesomeIcon icon={faSignOutAlt} size={'lg'} />,
      navigateTo: '/logout'
    }
  ]
}
