import { ISlideoutItem, ISlideoutContent } from './slideout-content';
import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faTasks,
  faToolbox,
  faSignOutAlt,
  faSpinner,
  faUsers,
  faExclamationTriangle,
  faPlus, faMap, faChartLine
} from '@fortawesome/free-solid-svg-icons'

export const officerSlideoutContent: ISlideoutContent =
{
    items: [
        {
            label: 'Task',
            icon: <FontAwesomeIcon icon={faTasks} size={"lg"} />,
            navigateTo: '/tasks'
        },
        {
            label: 'Map',
            icon: <FontAwesomeIcon icon={faMap} size={"lg"} />,
            navigateTo: '/map'
        },
        {
            label: 'Asset',
            icon: <FontAwesomeIcon icon={faToolbox} size={"lg"} />,
            navigateTo: '/asset'
        },
        {
            label: 'Team',
            icon: <FontAwesomeIcon icon={faUsers} size={"1x"} />,
            navigateTo: '/admin-team'
        },
        {
            label: 'Create',
            icon: <FontAwesomeIcon icon={faPlus} size={"lg"} />,
            navigateTo: '/registration-form'
        },
        {
            label: 'Pending',
            icon: <FontAwesomeIcon icon={faSpinner} size={"lg"} />,
            navigateTo: '/pending'
        },
        {
            label: 'Rejected',
            icon: <FontAwesomeIcon icon={faExclamationTriangle} size={"1x"} />,
            navigateTo: '/rejected'
        },
        {
            label: 'Report',
            icon: <FontAwesomeIcon icon={faChartLine} size={"1x"} />,
            navigateTo: '/report'
        },
        {
            label: 'Logout',
            icon: <FontAwesomeIcon icon={faSignOutAlt} size={"lg"} />,
            navigateTo: '/logout'
        }
    ]
}
