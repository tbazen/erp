import { employeeSlideOutContent } from './employee-slideout-content'
import { adminSlideoutContent } from './admin-slideout-content'
import { officerSlideoutContent } from './officer-slideout-content'
import { supervisorSlideoutContent } from './superviser-slideout-content'
import Role from '../role-util/roles'

export interface ISlideoutItem {
  label: string
  icon: JSX.Element
  navigateTo: string | object
}

export interface ISlideoutContent {
  items: ISlideoutItem[]
}

interface ISlideoutContentSet {
  contents: ISlideoutContent[]
}

function slideoutContents(): ISlideoutContentSet {
  return {
    contents: [
      { items: [] },
      employeeSlideOutContent,
      adminSlideoutContent,
      officerSlideoutContent,
      supervisorSlideoutContent
    ]
  }
}

export function getSlideoutContent(role: Role): ISlideoutContent {
  return slideoutContents().contents[role]
}
