import axios from 'axios'
import { IloginViewModel } from './../login/login'
export const userService = {
  login,
  logout
}

function login(
  loginViewModel: IloginViewModel,
  preceedLogIn: (isValid: boolean) => void
) {
  logout()
  return axios
    .post('/api/user/login', loginViewModel)
    .then(res => validateRequest(res))
    .then((data: any) => {
      localStorage.setItem('user', data.token)
      preceedLogIn(true)
    })
    .catch(error => console.log(`ERROR ${error}`))
}

export function validateRequest(res: any) {
  if (res.status == 200) return res.data
  else if (res.status == 401) logout()
  return res
}

function logout() {
  localStorage.removeItem('user')
  //location.reload(true);
}
