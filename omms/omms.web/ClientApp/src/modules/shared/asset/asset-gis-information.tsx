import React, { Component,Fragment } from 'react'
import { AssetSearhParams } from '../../../_infrastructure/model/asset-search-params';
import { Schema, AssetType } from '../../../_infrastructure/model/assetModel';
import './asset-info.scss'
import LoadingCircle from '../components/loading/loading-circle';
import { getAssetGisInfoByPUKID } from '../../../_setup/actions/assetActions';
import ErrorPage from '../components/error/error-page';
import { loading } from '../../../_setup/actions/loading-modal-actions';
import { Grid, Typography } from '@material-ui/core';

interface IProps{
    searchParams : AssetSearhParams
    updateTypeId : (typeId : number)=>void
}

interface IState{
    loading : boolean
    error:boolean
    errorMessage : string
    gisInformation : AssetType
}

type AllProps = IProps
export default class AssetGisInformation extends Component<AllProps,IState>{
    constructor(props:AllProps){
        super(props)
        this.state={
            loading:true,
            error:false,
            errorMessage:'',
            gisInformation:{
                id:0,
                name:'',
                attributes:[]
            }
        }
        this.infoRow = this.infoRow.bind(this)
    }

    componentDidMount(){
        let {searchParams} = this.props
        let gisRequest = getAssetGisInfoByPUKID(searchParams)
        let {error,loading} = this.state
            
        gisRequest.then(gisInformation=>{
            loading = false
            error = false
            this.props.updateTypeId(gisInformation.id)
            this.setState({error,loading,gisInformation})
        })
        .catch(errorMessage=>{
            error = true
            loading = false
            this.setState({loading,error,errorMessage})
        })
    }
    infoRow(value: Schema, key: number) {
        return (
            <Grid key={key} container justify='flex-start' spacing={8}>
                <Grid item xs={3}>
                  <Typography variant="h6" gutterBottom>
                    {value.attributeName}
                  </Typography>
                </Grid>
                <Grid item xs={9}>
                    <Typography variant="h6" gutterBottom>
                        {value.value}
                    </Typography>
                </Grid>
            </Grid>
        )
    }

    render() {
        let {gisInformation,loading,error} = this.state
        return (
            <Fragment>
                {
                    loading &&
                    <Grid container style={{margin:'10px'}} justify={'center'}>
                      <Grid item md={12}>
                        <Grid container justify={'center'}>
                           <LoadingCircle/>
                        </Grid>
                      </Grid>
                      <Grid item md={12}> 
                        <Grid container justify={'center'}>
                            <Typography variant="subheading" gutterBottom>
                                Fetching GIS information
                            </Typography>
                        </Grid>
                      </Grid>
                    </Grid>
                }
                {
                    !loading &&
                    error &&
                    <ErrorPage message={'Filed ti load GIS data'}/>
                }
                {
                    !loading &&
                    !error &&
                    <Fragment>
                        <Typography variant="h6" gutterBottom>
                            Asset GIS Information
                        </Typography>
                        <Grid container justify='flex-start' spacing={8}>
                            <Grid item xs={3} justify='flex-end'>
                                <Typography variant="h6" gutterBottom>
                                    Asset Type 
                                </Typography>
                            </Grid>
                            <Grid item xs={9}>
                                <Typography variant="h6" gutterBottom>
                                    {gisInformation.name}
                                </Typography>
                            </Grid>
                        </Grid>
                        {gisInformation.attributes.map(this.infoRow)}
                    </Fragment>
                }
            </Fragment>
        )
    }
}
