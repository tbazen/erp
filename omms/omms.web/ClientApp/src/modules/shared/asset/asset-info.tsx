import React, { Component, Fragment } from 'react'
import './asset-info.scss'
import { Paper } from '@material-ui/core';
import { getAsset, approveAssetRegistration, rejectAssetRegistration } from '../../../_setup/actions/assetActions';
import { AssetResponse, Schema } from '../../../_infrastructure/model/assetModel';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux'
import { IUserInformation } from '../../../_infrastructure/model/userInformationModel';
import Role from '../_helper/role-util/roles';
import DocumentService from '../../../_setup/services/document.service';
import { cancelRegistrationRequest } from '../../../_setup/actions/workflow-actions';
import AssetReadingForm from './form/asset-reading-form'
import MiniMap from '../map/mini-map/mini-map'
import AssetFormDialog from './../../admin/asset/asset-form'
import TaskFromModal from './../task/task-form-modal';
import UIContainer from '../_helper/ui-helper/containers-util';
import { AssetTypes } from 'src/_infrastructure/model/asset-search-params';
import { error } from '../../../_setup/actions/loading-modal-actions';
import ErrorPage from '../components/error/error-page';
import LoadingCircle from '../components/loading/loading-circle';

let document_service = new DocumentService()
interface IState {
    loading: boolean
    error : boolean

    asset: AssetResponse,
    assetImages: Blob[],
    openAssetReadingForm : boolean,
    openAssetForm: boolean,
    openTaskForm : boolean
}
interface IProps {
    wfid: string,
    aid: number
}


interface PropsFromState {
    employee: IUserInformation
}

type AllProps = IProps &
    PropsFromState


class AssetInformation extends Component<AllProps, IState>{
    constructor(props: AllProps) {
        super(props)
        this.state = {
            loading: true,
            error : false,
            asset: {
                id: null,
                wfid: null,
                name: '',
                note:'',
                pkuId: 0,
                description: '',
                geoLocation: '',
                images: [],
                assetType: {
                    id: 0,
                    name: '',
                    attributes: []
                }
            },
            assetImages: [],
            openAssetReadingForm:false,
            openAssetForm:false,       
            openTaskForm:false
        }
        this.infoRow = this.infoRow.bind(this)
        this.getRegisteredAssetActions = this.getRegisteredAssetActions.bind(this)
        this.loadImages = this.loadImages.bind(this)
        this.imageDisplay = this.imageDisplay.bind(this)
        this.handleAssetReadingFormClose = this.handleAssetReadingFormClose.bind(this)
        this.handleAssetReadingFormOpen = this.handleAssetReadingFormOpen.bind(this)
        this.readingDialog = this.readingDialog.bind(this)

        this.handleAssetFormOpen = this.handleAssetFormOpen.bind(this)
        this.handleAssetFormClose = this.handleAssetFormClose.bind(this)
        
        this.taskFromDialog = this.taskFromDialog.bind(this)
        this.handleTaskFormDialog = this.handleTaskFormDialog.bind(this)
    }

    handleTaskFormDialog(){
        this.setState({ openTaskForm: !this.state.openTaskForm })
    }

    handleAssetReadingFormOpen(){
        this.setState({ openAssetReadingForm: true })
        this.readingDialog()
    }

    handleAssetReadingFormClose(){
        this.setState({ openAssetReadingForm: false })
    }

    readingDialog(){
        return (
            <AssetReadingForm
                openForm={this.state.openAssetReadingForm}
                handleClose={this.handleAssetReadingFormClose}
                asset={this.state.asset}
            />
        )
    }

    taskFromDialog(){
        return(
            <TaskFromModal
                assetId={this.state.asset.id}
                handleClose = {this.handleTaskFormDialog}
                openForm = {this.state.openTaskForm}
            />
        )
    }
    handleAssetFormOpen(){
        this.setState({ openAssetForm: true })
        this.readingDialog()
    }

    handleAssetFormClose(){
        this.setState({ openAssetForm: false })
    }

    async componentDidMount() {
        let assetId = isNaN(this.props.aid) ? 0 : this.props.aid
        getAsset(assetId).
        then(asset => {
            let {loading,error} = this.state
            loading = false
            error = false
            if(asset.assetType == undefined)
                throw("Asset not found")
            this.setState({ asset ,loading,error})  
            this.loadImages();  
        })
        //@ts-ignore
        .catch(err => {
            let {loading,error} = this.state
            loading = false
            error = true
            this.setState({loading,error})  
        })
    }
    
    async loadImages() {
        for (let i = 0; i < this.state.asset.images.length; i++) {
            let image = await document_service.loadImage(this.state.asset.images[i].id)
            let { assetImages: images } = this.state
            images = [...images, image]
            this.setState({ assetImages: images })
        }
    }

    imageDisplay(blobData: Blob) {
        let urlObject = URL.createObjectURL(blobData);
        return (
            <div>
                <img src={urlObject}></img>
            </div>
        )
    }


    infoRow(value: Schema, key: number) {
        return (
            <div className={'asset-info-list'} key={key}>
                <span className={'attribute'}>
                    {value.attributeName}
                </span>
                <span className={'value'}>
                    {value.value}
                </span>
            </div>
        )
    }

    getRegisteredAssetActions() {
        let { role } = this.props.employee
        let {asset} = this.state
        if(asset.id != null) 
        {
            if (role == Role.EMPLOYEE)
                return (
                    <Grid item xs={12}>
                        {/* <Button style={{ marginLeft: "10px" }} onClick={this.handleAssetReadingFormOpen} color="primary" variant={'outlined'}>
                            Add Reading
                        </Button> */}
                      <Button style={{ marginLeft: "10px" }} onClick={this.handleTaskFormDialog} color="primary" variant={'outlined'}>
                        Add Task
                      </Button>
                    </Grid>
                );
            else if (role == Role.TECHNICAL_OFFICER || role == Role.EMPLOYEE)
                return (
                    <Grid item xs={12}>
                        <Button style={{ marginLeft: "10px" }} onClick={this.handleTaskFormDialog} color="primary" variant={'outlined'}>
                            Add Task
                        </Button>
                    </Grid>
                );
        }
        return <React.Fragment />
    }

    render() {
        let { asset, assetImages: images , loading , error } = this.state
        return (
            <Fragment>
            {
                loading &&
                <div style={{width:'20px',margin:'auto',marginTop:'40vh'}}>
                    <LoadingCircle/>
                </div>
            }
            {
                !loading &&
                error &&
                <ErrorPage message="Could not find asset" />
            }
            {
                !loading  &&
                !error &&
                <Paper className="info-container" style={{minHeight:'87vh'}} >
                {
                    this.getRegisteredAssetActions()
                }
                <div className="text-info">
                    <div className="align-info">
                        <div className="name">
                            <span className={'value'}>{asset.name}</span>
                        </div>

                        <div className="description info">
                            <span className={'label'}>Description </span>
                            <span className={'value'}>{asset.description}</span>
                        </div>

                        <div className="location info">
                            <span className={'label'}>Location</span>
                            <span className={'value'}>{asset.geoLocation}</span>
                        </div>

                        <div className="asset-type info">
                            <span className={'label'}>Asset Type</span>
                            <span className={'value'}>{asset.assetType.name}</span>
                        </div>

                        <div className="asset-info info">
                            <span className="info-title">
                                Asset Specific Information
                            </span>
                            {asset.assetType.attributes.map(this.infoRow)}
                        </div>
                    </div>

                    <Paper className="mini-map">
                        {
                            asset.geoLocation != "" ?
                                <MiniMap asset={asset}/>
                                :
                                null
                        }
                    </Paper>
                </div>

                <div className={'images'}>
                    {images.map(this.imageDisplay)}
                </div>
                {this.readingDialog()}
                {this.taskFromDialog()}
            </Paper>
            }

            </Fragment>
        )
    }
}

function mapStateToProps(state: any) {
    return {
        employee: state.login.userInfo
    }
}
export default connect(mapStateToProps, {
})(AssetInformation)