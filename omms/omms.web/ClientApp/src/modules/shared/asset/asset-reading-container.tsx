﻿import React, { Component } from 'react' 
import { AssetReading, AssetResponse } from '../../../_infrastructure/model/assetModel';
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import Paper from '@material-ui/core/Paper'
import './../../shared/asset/asset.scss'
import Typography from '@material-ui/core/Typography/Typography'
import TableFooter from '@material-ui/core/TableFooter'
import { connect } from 'react-redux'
import { loadAssetReadings } from '../../../_setup/actions/assetReadingActions'
import EmptyContent from './../components/empty/empty-list-container'

import {Schema} from './../../../_infrastructure/model/assetModel';
import { AssetReadingState } from './../../../_infrastructure/state/assetReadingState';
import { dateConverter } from '../../_helper/date-converter';
interface IState{
  page:number,
  noOfPages:number
} 
interface IProps{
}
interface PropsFromState{
  assetReading : AssetReadingState
}

type AllProps = IProps &
                PropsFromState

class AssetReadingContainer extends Component<AllProps, IState> {
  constructor(props:any) {
    super(props);
    this.state = {
      page:0,
      noOfPages:0,
    }
    
    this.nextPage = this.nextPage.bind(this)
    this.prevPage = this.prevPage.bind(this)
    this.readingDisplay = this.readingDisplay.bind(this)
    this.initializeState = this.initializeState.bind(this)
  }
 
  nextPage(){

    if(this.state.page < (this.props.assetReading.readings.length - 1))
    {
      let p = this.state.page+1
      this.setState({
        page:p,
        noOfPages:this.props.assetReading.readings.length
      })
    }
  }

  prevPage(){
    if(this.state.page>0)
    {
      let p = this.state.page-1
      this.setState({
        page:p,
        noOfPages:this.props.assetReading.readings.length
      })
    }
  }

  readingRow(row:Schema,key:number){
    return(
      <TableRow key={key}>
        <TableCell  align="left">
          {row.attributeName}
        </TableCell>
        <TableCell
                  colSpan={2} >
                  {row.value}</TableCell>
      </TableRow>
      )
  }

  initializeState(){
    this.setState({
      page: 0,
      noOfPages: this.props.assetReading.readings.length,
    })
  }
  
  readingDisplay(){
    return (
      <Paper className="reading-container">
            <div
                style={{
                  margin:"20px",
                  marginTop:"30px"
                }}
            >
              <Typography 
                          variant="h5" 
                          >
                          Asset name comes here...
              </Typography> 
              <Typography 
                          color="inherit" 
                          variant="subtitle1"
                          >
                          Date Comes here...
                </Typography>
            </div>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell align="left">Type of Measure</TableCell>
                  <TableCell colSpan={2}>Value</TableCell>
                </TableRow>
              </TableHead>
              <TableBody 
              >
              </TableBody>
              <TableFooter>
                  <TableRow>
                    <Typography
                                style={{
                                  paddingLeft:"20px",
                                  fontSize:"15px",
                                  fontFamily:"roboto"
                                }}>
                      Page : {this.state.page+1} / {this.state.noOfPages+1}
                    </Typography>
                    <TableCell colSpan={3}  align="right">
                       <button onClick={this.prevPage}>Prev</button>
                       <button onClick={this.nextPage}>Next</button>
                    </TableCell>
                  </TableRow>
                </TableFooter>
                
            </Table>
          </Paper>
        
    )
  }
  render() {
    
    return (
          this.props.assetReading.readings.length == 0 
          ?
          <EmptyContent message="No assset reading so far!"/>
          :
          <Paper className="reading-container">
          <div
              style={{
                margin:"20px",
                marginTop:"30px"
              }}
          >
            <Typography 
                        variant="h5" 
                        >
                        {this.props.assetReading.readings[0].asset.name}
            </Typography> 
            <Typography 
                        color="inherit" 
                        variant="subtitle1"
                        >
                        { dateConverter(this.props.assetReading.readings[this.state.page].dateRecorded  as string)}
              </Typography>
              <Typography 
                        color="inherit" 
                        variant="subtitle1"
                        >
                        Reading by : { this.props.assetReading.readings[this.state.page].readingBy.userName}
              </Typography>
          </div>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell align="left">Type of Measure</TableCell>
                <TableCell colSpan={2}>Value</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
                {
                 this.props.assetReading.readings[this.state.page].readings.map(this.readingRow)
                }
            </TableBody>
            <TableFooter>
                <TableRow>
                  <Typography
                              style={{
                                paddingLeft:"20px",
                                fontSize:"15px",
                                fontFamily:"roboto"
                              }}>
                    Page : {this.state.page+1} / {this.props.assetReading.readings.length}
                  </Typography>
                  <TableCell colSpan={3}  align="right">
                     <button onClick={this.prevPage}>Prev</button>
                     <button onClick={this.nextPage}>Next</button>
                  </TableCell>
                </TableRow>
              </TableFooter>
              
          </Table>
        </Paper>
   
         );
      }
}


function mapStateToProps(state:any){
  return{
    assetReading : state.assetReading
  }
}

export default connect(mapStateToProps,{loadAssetReadings})(AssetReadingContainer);
