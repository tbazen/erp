import React,{Component} from 'react'
import { AssetModel, AssetReading } from '../../../_infrastructure/model/assetModel';
import { AppBar, Button, Paper, Tab, Tabs, TextField } from '@material-ui/core'
import './asset.scss'
import { connect } from 'react-redux';
import { loadAssetReadings } from '../../../_setup/actions/assetReadingActions';



interface IViewState {
  asset?: AssetModel
}

interface IState{
  value: number
}

class AssetView extends Component<any, IState> {
  constructor(props:any) {
    super(props)
    this.state = {
      value: 0
    }

    this.handleChange = this.handleChange.bind(this)
  }

  //@ts-ignore
  handleChange(e: any, value: number) {

    this.setState({ value });
  }

  async componentWillMount() {

  }

  TabContainer(props: any) {
    return(
      <div>
        {props.children}
      </div>
    )
  }


  render() {
    return (
      <Paper className="view-container">
        <Paper>
          <AppBar position="static">
            <Tabs value={this.state.value} onChange={this.handleChange}>
              <Tab label="Asset Information" />
              <Tab label="Asset Readings" />
              <Tab label="Asset Tasks" />
            </Tabs>
          </AppBar>
          {this.state.value === 0 &&
          <this.TabContainer>
            Asset Information
          </this.TabContainer>}
          {
            this.state.value === 1 &&
            <this.TabContainer>
              Asset Readings
            </this.TabContainer>
          }
          {
            this.state.value === 2 &&
            <this.TabContainer>
              Asset Tasks
            </this.TabContainer>
          }
        </Paper>
      </Paper>
    );
  }
}


function mapStateToProps(state:any){
  return{
    readings : state.assetReading.readings
  }
}

export default connect(mapStateToProps,{loadAssetReadings})(AssetView);
