import React, { Component } from 'react'
import {
  AssetReading,
  AssetResponse
} from '../../../../_infrastructure/model/assetModel'
import { Modal } from '@material-ui/core'
import '../asset.scss'
import Paper from '@material-ui/core/Paper'
import Typography from '@material-ui/core/Typography'
import Grid from '@material-ui/core/Grid'
import Button from '@material-ui/core/Button'
import { connect } from 'react-redux'
import { addAssetReading } from '../../../../_setup/actions/assetReadingActions'
import { AssetReadingSchema } from '../../../../_infrastructure/model/assetModel';
import SchemaFrom from '../../../admin/asset/schema-form';
import { getAssetReadingFormSchema } from '../../../../_setup/actions/assetActions';


interface IState {
  readingSchema : AssetReadingSchema
}

interface IProps {
  asset: AssetResponse
  openForm: boolean
  handleClose: () => void
}

interface PropsFromDispatch {
  addAssetReading: (reading: AssetReading) => void
}

type AllProps = IProps & PropsFromDispatch

class AssetReadingForm extends Component<AllProps, IState> {
  constructor(props: any) {
    super(props)
    let {assetType} = this.props.asset
    this.state = {
      readingSchema:{
        reading:{
          id:assetType.id,
          name:assetType.name,
          attributes:[]
        }
      }
    }

    this.handleInputChange = this.handleInputChange.bind(this)
    this.submitReading = this.submitReading.bind(this)
  }


  async componentDidMount(){
    let {asset} = this.props
    let readingSchema = await getAssetReadingFormSchema(asset.assetType.id)
    this.setState({readingSchema})
  }
  
  handleInputChange(index:number, name:string, value:string){
    let {readingSchema} = this.state
    readingSchema.reading.attributes[index].attributeName = name
    readingSchema.reading.attributes[index].value = value
    this.setState({readingSchema})
  }

  submitReading() {
      let { asset } = this.props
      let assetReading: AssetReading = {
        id: null,
        asset,
        dateRecorded: null,
        readingBy:{
          id:null,
          role:'',
          userName:''
        },
        readings: this.state.readingSchema.reading.attributes
      }
      this.props.addAssetReading(assetReading)
      this.props.handleClose()
  }
  

  render() {
    
    return (
      <Modal
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
        open={this.props.openForm}
        onClose={this.props.handleClose}
      >
        <Paper className="form-card-container">
          <Grid container justify="center">
            <Grid item md={12}>
              <Typography component="h1" variant="h5">
                Add new reading
              </Typography>
              <hr />
            </Grid>
          </Grid>
          <SchemaFrom 
                      assetType={this.state.readingSchema.reading}
                      handleSchemaInput={this.handleInputChange}
                      />
          
          <Grid container justify="flex-start">
            <Grid item xs={5}>
              <Button
                type="submit"
                variant="contained"
                color="primary"
                fullWidth
                onClick={this.submitReading}
              >
                Submit
              </Button>
            </Grid>
          </Grid>
        </Paper>
      </Modal>
    )
  }
}

function mapStateToProps() {
  return {}
}

export default connect(
  mapStateToProps,
  { addAssetReading }
)(AssetReadingForm)
