import React, { Component } from 'react'
import AppBar from '@material-ui/core/AppBar/AppBar'
import Tabs from '@material-ui/core/Tabs/Tabs'
import Tab from '@material-ui/core/Tab/Tab'
import AssetMap from '../../map/map'
import { connect } from 'react-redux'
import { loadAssets } from '../../../../_setup/actions/assetActions'
import AssetContainer from './../../components/asset/asset-container'
import { changeActiveIndex } from '../../../../_setup/actions/drawer-actions';
import { IUserInformation } from '../../../../_infrastructure/model/userInformationModel';
import Role from '../../_helper/role-util/roles';
import { EmployeeItems } from '../../_helper/slideout-index/employee-items';
import { OfficerItems } from '../../_helper/slideout-index/officer-items';
import { SupervisorItems } from '../../_helper/slideout-index/supervisor-items';
import UIContainer from '../../_helper/ui-helper/containers-util';
interface ITaskState {
  modal: boolean
  create: boolean
  counter: number
  load: boolean
  viewId: string
  key: number
  value: number
  data: any //Fix the any types man, idk what you're using
}


interface PropsFromState{
  user : IUserInformation
}
interface PropsFromDispatch{
  loadAssets:()=>void,
  changeActiveIndex:(index:number)=>void
}

type AllProps = PropsFromState &
                PropsFromDispatch

class RegisteredAssetContainer extends Component<AllProps, ITaskState> {
  state: ITaskState = {
    modal: false,
    create: false,
    counter: 0,
    load: false,
    viewId: '',
    key: 0,
    value: 0,
    data: null
  }

  constructor(props: AllProps) {
    super(props)

    this.handleChange = this.handleChange.bind(this)
    this.setAssets = this.setAssets.bind(this)
    //this.mainComponent = this.mainComponent.bind(this)
  }

  componentWillMount() {
    let {user} = this.props
    
    if(user.role == Role.EMPLOYEE)
        this.props.changeActiveIndex(EmployeeItems.ASSET)
    else if(user.role == Role.TECHNICAL_OFFICER)
        this.props.changeActiveIndex(OfficerItems.ASSET)
    else if(user.role == Role.TECHNICAL_SUPERVISOR)
        this.props.changeActiveIndex(SupervisorItems.ASSET)
    
    this.setState({
      load: true
    })
    this.props.loadAssets()
  }

  //@ts-ignore
  handleChange(e: any, value: number) {
    this.setState({ value })
  }

  async setAssets(data: any) {
    await this.setState({
      data: data.assets
    })
  }
  
  public render() {
    return (
      <AssetContainer container={UIContainer.REGISTERED_CONTAINER} withSearchBar={true}/>
    )
  }
}

function mapStateToProps(state:any){
  return {
    user: state.login.userInfo
  }
}

function mapDispatchToProps(dispatch:any){
  return {
    loadAssets:()=>dispatch(loadAssets()),
    changeActiveIndex:(index:number)=>dispatch(changeActiveIndex(index))
  }
}
export default connect(mapStateToProps,mapDispatchToProps)(RegisteredAssetContainer)
