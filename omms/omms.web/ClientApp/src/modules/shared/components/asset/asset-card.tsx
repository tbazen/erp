import React ,{Component , Fragment}from 'react'
import { AssetResponse } from '../../../../_infrastructure/model/assetModel'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'
import { Link } from 'react-router-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faMapMarkedAlt, faStickyNote,faTools,faAsterisk , faBookmark } from '@fortawesome/free-solid-svg-icons'
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent'
import CardActionArea from '@material-ui/core/CardActionArea'
import CardActions from '@material-ui/core/CardActions'
import Color from '../../_helper/color-container/index'
import {connect} from 'react-redux'
import AssetInformationDialog from './asset-information-modal';
import { IUserInformation } from './../../../../_infrastructure/model/userInformationModel';
import Role from '../../_helper/role-util/roles';
import { approveAssetRegistration, rejectAssetRegistration } from '../../../../_setup/actions/assetActions';
import { cancelRegistrationRequest } from '../../../../_setup/actions/workflow-actions';
import AssetFormDialog from '../../../admin/asset/asset-form'
import UIContainer from '../../_helper/ui-helper/containers-util';
import { WorkflowTypes } from '../../../../_infrastructure/model/workflow-model';
import { openNoteModal } from '../../../../_setup/actions/wfnote-modal-actions';
import { Divider } from '@material-ui/core';

interface IProps{
    asset:AssetResponse
    container : UIContainer
}

interface PropsFromDispatch{
    approveAssetRegistration: (wfid: string) => void
    openNoteModal: (wfid:string,WFType : WorkflowTypes)=>void
    cancelRegistrationRequest:(wfid:string,container : UIContainer) => void
}
interface PropsFromState{
    user: IUserInformation
}

type AllProps = IProps &
                PropsFromDispatch &
                PropsFromState



interface IState{
    openAssetInfoDialog : boolean
    openAssetForm:boolean
}

class AssetCard extends Component <AllProps,IState>{
    
    constructor(props:AllProps){
        super(props)
        this.state = {
            openAssetInfoDialog : false,
            openAssetForm : false
        }
        this.handleAssetInfoDialog = this.handleAssetInfoDialog.bind(this)
        this.approveAsset = this.approveAsset.bind(this)
        this.rejectRegistrationHanlder = this.rejectRegistrationHanlder.bind(this)
        this.cancelRegistrationHandler = this.cancelRegistrationHandler.bind(this)
        this.handleAssetForm = this.handleAssetForm.bind(this)
        this.getPendingAssetActions = this.getPendingAssetActions.bind(this)
    }
    
    handleAssetInfoDialog() {
        this.setState({ openAssetInfoDialog: !this.state.openAssetInfoDialog })
    }

    approveAsset() {
        let {wfid} = this.props.asset

        wfid != null &&
        this.props.approveAssetRegistration(wfid)
    }
    rejectRegistrationHanlder(){
        let {wfid} = this.props.asset
        
        wfid != null &&
        this.props.openNoteModal(wfid,WorkflowTypes.Asset)
    }
    cancelRegistrationHandler() {
        let { wfid } = this.props.asset
        let {container} = this.props
        wfid != null &&   
        this.props.cancelRegistrationRequest(wfid,container)
    }

    handleAssetForm(){
        this.setState({
            openAssetForm:!this.state.openAssetForm
        })
    }


    getPendingAssetActions() {
        let { role } = this.props.user
        let {asset} = this.props
        if(asset.id == null && asset.wfid != null )
        {
            if (role == Role.TECHNICAL_SUPERVISOR)
            return (
                <CardActions>
                    <Button size="small" onClick={this.handleAssetInfoDialog} style={{borderRadius:'2px', color:Color.PRIMARY_FOREGROUND,backgroundColor:Color.PRIMARY}} variant={'outlined'}>
                        Detail
                    </Button>
                    <Button size="small" onClick={this.approveAsset} style={{ borderRadius:'2px',color:Color.PRIMARY_FOREGROUND,backgroundColor:Color.HAPPY}} variant={'outlined'}>
                        Approve
                    </Button>
                    <Button size="small" onClick={this.rejectRegistrationHanlder} style={{borderRadius:'2px',color:Color.PRIMARY_FOREGROUND,backgroundColor:Color.SECONDARY}} variant={'outlined'} color="secondary">
                        Reject
                    </Button>
                </CardActions>
            )
            if (role == Role.TECHNICAL_OFFICER)
                return (
                    <CardActions>
                        <Button size="small" onClick={this.handleAssetInfoDialog} style={{borderRadius:'2px',color:Color.PRIMARY_FOREGROUND,backgroundColor:Color.PRIMARY}}>
                            Detail
                        </Button>
                        <Button size="small" onClick={this.handleAssetForm}  style={{borderRadius:'2px',color:Color.PRIMARY_FOREGROUND,backgroundColor:Color.HAPPY}} variant={'outlined'}>
                            Edit Asset
                        </Button>
                        <Button size="small" onClick={this.cancelRegistrationHandler} style={{borderRadius:'2px',color:Color.PRIMARY_FOREGROUND,backgroundColor:Color.SECONDARY}} variant={'outlined'}>
                            Cancel Request
                        </Button>
                    </CardActions>
                )
        }  
        return <React.Fragment />
    }
    

    render(){
        let {asset,container} = this.props
        let {openAssetForm,openAssetInfoDialog} = this.state
        return (
              <Grid item lg={4} md={6} xs={12}>
                  <Card>
                     <CardActionArea>
                        <CardContent>
                            <table style={{width:'100%'}}>
                                <tr>
                                    <td>
                                        <FontAwesomeIcon size={'lg'} title="Loaction" icon={faTools}/>
                                    </td>
                                    <td>
                                    <Typography component="h2" variant="headline">
                                        {asset.name}
                                    </Typography>
                                    </td>
                                </tr>
                                <tr>
                                    <td colSpan={2}>
                                      <Divider/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <FontAwesomeIcon size={'lg'} title="Loaction" icon={faAsterisk}/>
                                    </td>
                                    <td>
                                    <Typography variant="subtitle2">
                                        Type : { asset.assetType != undefined && asset.assetType.name }
                                    </Typography>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <FontAwesomeIcon size={'lg'} title="Loaction" icon={faBookmark}/>
                                    </td>
                                    <td>
                                        <Typography variant="body1">
                                            {asset.description}
                                        </Typography>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <FontAwesomeIcon size={'lg'} title="Loaction" icon={faMapMarkedAlt}/>
                                    </td>
                                    <td>
                                        <Typography component='h6'>
                                                {asset.geoLocation}
                                        </Typography>
                                    </td>
                                </tr>
                              {
                                asset.note != '' &&
                                asset.note != null &&
                                <tr>
                                    <td>
                                        <FontAwesomeIcon size={'lg'} title="Note" icon={faStickyNote}/>
                                    </td>
                                    <td>
                                        <Typography component='h6'>
                                                {asset.note}
                                        </Typography>
                                    </td>
                                </tr>
                              }
                            </table>                                  
                         </CardContent>
                     </CardActionArea>
                     <CardActions>
                          {
                              this.getPendingAssetActions()
                          }
                          {
                              asset.id != null &&
                              <Link to={`/asset-detail/${asset.id}`}>
                                <Button
                                    style={{
                                        backgroundColor:Color.PRIMARY,
                                        color:Color.PRIMARY_FOREGROUND,
                                        borderRadius:'2px'
                                    }}
                                    fullWidth
                                    size="small"
                                >
                                    More Information
                                </Button>
                              </Link>
                          }
                      </CardActions>
                  </Card>
                  
                  <AssetInformationDialog
                                         openForm={openAssetInfoDialog}
                                         asset={asset}
                                         handleClose={this.handleAssetInfoDialog} />
                  
                  <AssetFormDialog
                                  asset = {asset}
                                  container = {container} 
                                  searchParams={{pkuId:asset.pkuId,assetType:asset.assetType != undefined ? asset.assetType.name : ''}}
                                  handleClose = {this.handleAssetForm}
                                  openForm = {openAssetForm}/>
                  
              </Grid>
          )    
    }
    
}


function mapStateToProps(state:any){
    return {
        user: state.login.userInfo
    }
}

function mapDispatchToProps(dispatch:any){
    return {
        approveAssetRegistration : (wfid:string)=>dispatch(approveAssetRegistration(wfid)),
        cancelRegistrationRequest : (wfid:string,container : UIContainer) => dispatch(cancelRegistrationRequest(wfid,container)),
        openNoteModal: (wfid:string,WFType : WorkflowTypes)=>dispatch(openNoteModal(wfid,WFType))
    }
}


export default connect(mapStateToProps,mapDispatchToProps)(AssetCard)