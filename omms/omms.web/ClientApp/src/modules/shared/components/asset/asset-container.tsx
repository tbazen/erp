import React, { Component } from 'react'
import { connect } from 'react-redux';
import { AssetState } from '../../../../_infrastructure/state/assetState';
import AssetCard  from './asset-card';
import EmptyContent from '../empty/empty-list-container';
import LoadingPage from '../loading/loading-page';
import ErrorPage from '../error/error-page';
import { AssetResponse } from '../../../../_infrastructure/model/assetModel';
import SearchBar from '../search-bar/search-bar';
import { filteringAssets, cancelAssetFiltering } from '../../../../_setup/actions/assetActions';
import { filterAsset } from '../../../../_setup/actions/filter-actions';
import Grid from '@material-ui/core/Grid';
import UIContainer from '../../_helper/ui-helper/containers-util';


interface IProps {
    withSearchBar:boolean,
    container : UIContainer
}

interface PropsFromState {
    asset: AssetState,
    filteredAssets : AssetResponse[]
}
interface PropsFromDispatch {
    filteringAssets : ()=>void,
    cancelAssetFiltering : () => void,
    filterAsset: (name:string,assets:AssetResponse[])=>void
}

type AllProps = IProps &
    PropsFromState &
    PropsFromDispatch

class AssetContainer extends Component<AllProps> {
    constructor(props: AllProps) {
        super(props)
        this.handleFiltering = this.handleFiltering.bind(this)
    }

    handleFiltering(event:any){
        let {asset} = this.props
        let name:string  = event.target.value
        if(name.length == 0 )
          this.props.cancelAssetFiltering()
        else{
            this.props.filteringAssets()
            this.props.filterAsset(name,asset.assets)
        }
    }

    render() {
        let { asset,filteredAssets,withSearchBar,container } = this.props
        return (
            <div>
                {
                    withSearchBar &&
                    <SearchBar handler = {this.handleFiltering}/>
                }
                {
                    asset.loading &&
                    <LoadingPage/>
                }
                {
                    !asset.loading &&
                    asset.error &&
                    <ErrorPage message={asset.errorMessage}/>
                }
                {
                    !asset.loading &&
                    !asset.error &&
                    asset.filtering &&
                    filteredAssets.length > 0 &&
                    <Grid style={{width:"98%",margin:'auto',marginTop:"5px"}} container spacing={8} >
                      {filteredAssets.map( (value:AssetResponse ,key: number)=> <AssetCard container={container} asset={value} key={key}/>)}
                    </Grid>
                }
                {
                    !asset.loading &&
                    !asset.error &&
                    asset.filtering &&
                    !(filteredAssets.length > 0) &&
                    <EmptyContent message="No match found!"/>
                }
                {
                    !asset.loading &&
                    !asset.error &&
                    !asset.filtering &&
                    asset.assets.length > 0 &&
                    <Grid style={{width:"98%",margin:'auto',marginTop:"5px"}} container spacing={8} >
                       {asset.assets.map((value:AssetResponse ,key: number)=> <AssetCard container={container} asset={value} key={key}/>)}
                    </Grid>
                }
                {
                    !asset.loading &&
                    !asset.error && 
                    !asset.filtering &&
                    !(asset.assets.length > 0) &&
                    <EmptyContent message="No assets on this section!"/>
                }
            </div>
        )
    }
}

function mapStateToProps(state: any) {
    return {
        asset: state.asset,
        filteredAssets : state.filter.filteredAsset
    }
}

function mapDispatchToProps(dispatch:any){
    return {
        filteringAssets : ()=>dispatch(filteringAssets()),
        cancelAssetFiltering : ()=>dispatch(cancelAssetFiltering()),
        filterAsset : (name:string,assets : AssetResponse[])=>dispatch(filterAsset(name,assets))
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(AssetContainer)