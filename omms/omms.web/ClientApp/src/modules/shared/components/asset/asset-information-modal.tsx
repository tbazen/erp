import React, { Component } from 'react'
import { Paper, Modal } from '@material-ui/core';
import { AssetResponse, Schema } from '../../../../_infrastructure/model/assetModel'
import DocumentService from '../../../../_setup/services/document.service'
import MiniMap from '../../map/mini-map/mini-map'
import Button from '@material-ui/core/Button';

let document_service = new DocumentService()
interface IState {
    assetImages: Blob[],
}

interface IProps {
    asset : AssetResponse
    handleClose: () => void
    openForm: boolean
}




type AllProps = IProps 


class AssetInformationDialog extends Component<AllProps, IState>{
    constructor(props: AllProps) {
        super(props)
        this.state = {
            assetImages: [],
        }
        this.infoRow = this.infoRow.bind(this)
        this.loadImages = this.loadImages.bind(this)
        this.imageDisplay = this.imageDisplay.bind(this)
        this.closeModal = this.closeModal.bind(this)
    }



    async componentDidMount() {
        this.loadImages();
    }

    async loadImages() {
        let {images} = this.props.asset
        for (let i = 0; i < images.length; i++) {
            let image = await document_service.loadImage(images[i].id)
            let { assetImages } = this.state
            assetImages = [...assetImages, image]
            this.setState({assetImages})
        }
    }

    imageDisplay(blobData: Blob) {
        let urlObject = URL.createObjectURL(blobData);
        return (
            <div>
                <img src={urlObject}></img>
            </div>
        )
    }


    infoRow(value: Schema, key: number) {
        return (
            <div className={'asset-info-list'} key={key}>
                <span className={'attribute'}>
                    {value.attributeName}
                </span>
                <span className={'value'}>
                    {value.value}
                </span>
            </div>
        )
    }

   
    closeModal(){
        this.props.handleClose()
    }

    render() {
        let { assetImages: images } = this.state
        let {asset} = this.props
        return (
            <Modal
                  aria-labelledby="simple-modal-title"
                  aria-describedby="simple-modal-description"
                  open={this.props.openForm}
                  onClose={this.props.handleClose}
                >
               <Paper className="info-container" >
                <div>
                    <Button style={{float:'right'}} variant={'contained'} color="secondary" onClick={this.closeModal}>Close</Button>
                </div>
               <div style={{height:'80vh',clear:'both',maxHeight:'80vh',overflowY:'auto'}}> 
                <div className="text-info">
                    <div className="align-info">
                        <div className="name">
                            <span className={'value'}>{asset.name}</span>
                        </div>
                        <div className="description info">
                            <span className={'label'}>Description </span>
                            <span className={'value'}>{asset.description}</span>
                        </div>

                        <div className="location info">
                            <span className={'label'}>Location</span>
                            <span className={'value'}>{asset.geoLocation}</span>
                        </div>

                        <div className="asset-type info">
                            <span className={'label'}>Asset Type</span>
                            <span className={'value'}>{ asset.assetType != undefined && asset.assetType.name }</span>
                        </div>

                        <div className="asset-info info">
                            <span className="info-title">
                                Asset Specific Information
                            </span>
                            {asset.assetType != undefined && asset.assetType.attributes.map(this.infoRow)}
                        </div>
                    </div>

                    <Paper className="mini-map">
                        {
                            asset.geoLocation != "" ?
                                <MiniMap asset={asset}/>
                                :
                                null
                        }
                    </Paper>
                </div>
                <div className={'images'}>
                    {images.map(this.imageDisplay)}
                </div>
               </div>
            </Paper>
            </Modal>
        )
    }
}


export default AssetInformationDialog