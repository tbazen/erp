import React from 'react'
import { Paper } from '@material-ui/core';

interface IProps{
  message : string
}

function EmptyContent(props:IProps): JSX.Element {
    return (
      <div
        style={{
          padding: '20px',
          fontFamily: 'roboto',
          fontSize: '25px',
          textAlign: 'center',
          height:'94vh'
        }}
      >
        <p 
            style={{
              
          marginTop: '40vh',
            }}
            >{props.message}</p>
      </div>
    )
  }

  export default EmptyContent