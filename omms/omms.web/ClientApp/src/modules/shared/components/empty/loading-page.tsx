import React from 'react'
import { Paper } from '@material-ui/core';

interface IProps{
  message : string
}

function LoadingPage(): JSX.Element {
    return (
      <Paper
        style={{
          padding: '20px',
          fontFamily: 'roboto',
          fontSize: '25px',
          textAlign: 'center',
          height:'94vh'
        }}
      >
        <p 
            style={{
              
          marginTop: '40vh',
            }}
            >Loading...</p>
      </Paper>
    )
  }

  export default LoadingPage