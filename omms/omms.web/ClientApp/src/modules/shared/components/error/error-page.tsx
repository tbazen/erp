import React, { Component } from 'react'
import LinearProgress from '@material-ui/core/LinearProgress';
import CircularProgress from '@material-ui/core/CircularProgress';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography/Typography';

interface IProps{
    message : string | undefined
}
interface PropsFromState{

}
interface PropsFromDispatch{

}

type AllProps = IProps &
                PropsFromState &
                PropsFromDispatch

interface IState{

}

export default class ErrorPage extends Component<AllProps,IState> {
    
    constructor(props:AllProps) {
        super(props)
    }

    render() {
        return (
        <Grid style={{width:"100%"}} container justify="center">
            <Typography variant="headline" color="secondary" component="h2" style={{marginTop:"40vh"}}>
                {this.props.message}
            </Typography>
        </Grid>
        )
    }
}
