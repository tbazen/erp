import React from 'react'
import CircularProgress from '@material-ui/core/CircularProgress';
import Color from '../../_helper/color-container/index';

let LoadingCircle= () => {
  return (
    <CircularProgress style={{
        color:Color.PRIMARY,
        animationDuration: '550ms'
      }} 
    size={30}
    thickness={5}
    variant="indeterminate"
    disableShrink />
  )
}
export default LoadingCircle