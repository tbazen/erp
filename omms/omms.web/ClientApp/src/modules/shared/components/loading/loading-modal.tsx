import React, { Component } from 'react'
import Modal from '@material-ui/core/Modal/Modal';
import Paper from '@material-ui/core/Paper';
import CircularProgress from '@material-ui/core/CircularProgress';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { connect } from 'react-redux';
import { LoadingModalState } from '../../../../_infrastructure/state/loading-modal-state';
import { closeModal } from '../../../../_setup/actions/loading-modal-actions';
import { Fab } from '@material-ui/core';
import CheckIcon from '@material-ui/icons/Check';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faExclamation } from '@fortawesome/free-solid-svg-icons'

interface IProps{
   
}
interface PropsFromDispatch{
    closeModal : ()=> void
}
interface PropsFromState{
    modalState : LoadingModalState
}
type AllProps = IProps &
                PropsFromState &
                PropsFromDispatch

class LoadingModal extends Component<AllProps> {
    constructor(props:AllProps) {
        super(props);
        this.handleClose = this.handleClose.bind(this)
    }
    
    handleClose(){
        if(!this.props.modalState.loading) 
           this.props.closeModal()
    }

    render() {
        let {modalState} = this.props
    return (
        <Modal
          open={modalState.open}
          onClose={this.handleClose}>
            <Paper
                style={{
                width:'60%',
                margin:'auto',
                marginTop:'10vh',
                maxHeight:'80vh',
                overflowY:'auto',
                padding:'20px'
                }}>
                
                    
                    {
                        !modalState.loading &&
                         modalState.open &&
                        !modalState.error &&
                        <Grid container justify="center">
                            <Grid item xs={10}>
                                <Typography variant="body1">
                                  { this.props.modalState.successMessage}
                                </Typography>
                            </Grid>
                            <Grid item xs={2}>
                                <Fab style={{backgroundColor:"green",color:"white"}}>
                                    <CheckIcon />
                                </Fab>
                            </Grid>
                        </Grid>    
                    }
                    {   
                        this.props.modalState.loading 
                        &&
                        <Grid container justify="center">
                            <Grid item xs={10}>
                                <Typography variant="body1">
                                  { this.props.modalState.loadingMessage}
                                </Typography>
                            </Grid>
                            <Grid xs={2}>
                                <CircularProgress size={55} color="primary" />
                            </Grid>
                        </Grid>    
                    }
                    {
                       !modalState.loading &&
                        modalState.open &&
                        modalState.error &&
                        <Grid container justify="center">
                            <Grid item xs={10}>
                                <Typography variant="body1">
                                  { this.props.modalState.errorMessage}
                                </Typography>
                            </Grid>
                            <Grid item xs={2}>
                                <Fab color="secondary">
                                    <FontAwesomeIcon icon={faExclamation}/>
                                </Fab>
                            </Grid>
                        </Grid>
                    }
            </Paper>
        </Modal>
    )
  }
}

function mapStateToProps(state:any){
    return {
        modalState:state.loadingModal
    }
}

function mapDispatchToProps(dispatch:any){
    return {
        closeModal :()=>dispatch(closeModal())
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(LoadingModal)
