import React, { Component } from 'react'
import './note-card.scss'
import { Paper } from '@material-ui/core'
import { NoteResponse } from '../../../../_infrastructure/model/task-note-model'

interface INoteProps {
  note: NoteResponse
  // note: any //TODO change type to NoteResponse when integrating with backend
}

class NoteCard extends Component<INoteProps, any> {
  constructor(props: INoteProps) {
    super(props)
  }

  render() {
    return (
      <Paper className={'note-card-container'}>
        <div className="header">
          Note Header
        </div>
        <div className="note-content">
          Note Content
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi aperiam aut consequuntur debitis dolore
          doloribus ducimus enim error eveniet mollitia natus nulla pariatur placeat praesentium quaerat qui, rerum
          saepe suscipit.
          Adipisci culpa ducimus eius fugit, maxime natus odio odit omnis repellat sunt. Accusamus dolor dolorum,
          incidunt itaque iure maxime, modi nam nemo quos tenetur voluptate voluptatem. Doloremque mollitia
          perspiciatis tempora!
        </div>
        <div className="images">

        </div>
        <div className="documents">

        </div>
        <div className="created-by">
          Created By
        </div>

      </Paper>
    )
  }
}

export default NoteCard