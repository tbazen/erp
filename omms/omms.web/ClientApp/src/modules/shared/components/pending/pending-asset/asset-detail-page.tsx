import React, { Component } from 'react'
import AppBar from '@material-ui/core/AppBar'
import Tabs from '@material-ui/core/Tabs'
import Tab from '@material-ui/core/Tab'
import './../../../asset/asset.scss'
import AssetInformation from '../../../asset/asset-info'
import AssetReadingContainer from '../../../asset/asset-reading-container'
import { getAssetTasks, clearTasks, filteringTasks, cancelTaskFiltering } from '../../../../../_setup/actions/taskActions';
import { connect } from 'react-redux'
import { loadAssetReadings } from './../../../../../_setup/actions/assetReadingActions'
import TaskContainer from './../pending-task-container'
import { TaskState } from '../../../../../_setup/reducer/taskReducer';
import { TaskResponse } from '../../../../../_infrastructure/model/task-model';
import { filterTask } from '../../../../../_setup/actions/filter-actions';
import Grid from '@material-ui/core/Grid';
import SearchField from '../../search-bar/search-field';
import UIContainer from '../../../_helper/ui-helper/containers-util';

interface IState {
    value: number
}

interface PropsFromState{
    task :TaskState
}
interface PropsFromRoute{
    match:any
}
interface PropsFromDispatch{
    getAssetTasks:(assetId:number) => void
    loadAssetReadings:(assetId : number) => void
    clearTasks:()=>void

    filteringTasks: ()=>void,
    cancelTaskFiltering : ()=>void,
    filterTask:(name:string,tasks : TaskResponse[])=>void
}

type AllProps = PropsFromState &
                PropsFromRoute &
                PropsFromDispatch
class AssetDetailPage extends Component<AllProps, IState> {

    constructor(props: any) {
        super(props)
        this.handleChange = this.handleChange.bind(this)
        this.handleFiltering = this.handleFiltering.bind(this)
        this.state = {
            value: 0
        }
    }

    //@ts-ignore
    handleChange(event: any, value: number) {
        this.setState({ value })
    }
    componentDidMount(){
        let str: string = this.props.match.params.assetid
        let aid: number = Number(str)

        if(isNaN(aid))
        { 
            this.props.clearTasks()
        }
        else{
            this.props.getAssetTasks(aid)
            this.props.loadAssetReadings(aid)
        }
    }

    handleFiltering(event:any){
        let {task} = this.props
        let name:string  = event.target.value
        if(name.length == 0 ){            
            this.props.cancelTaskFiltering()
        }
        else{
            this.setState({value:1})
            this.props.filteringTasks()
            this.props.filterTask(name,task.tasks)
        }        
    }

    render() {
        const { value } = this.state;
        let { wfid, assetid } = this.props.match.params
        
        return (
            <div>
                <AppBar position="sticky" color="default">
                    <Grid container spacing={8}> 
                        <Grid item lg={5} md={12} xs={12}>                   
                            <Tabs 
                                 value={value} 
                                 indicatorColor="primary"
                                 textColor="primary"
                                 onChange={this.handleChange}>
                                <Tab label="Asset Information" />
                                <Tab label="Asset Task" />
                                <Tab label="Reading" />
                            </Tabs>
                        </Grid>
                        <Grid item lg={7} md={12} xs={12}>
                            <SearchField handler={this.handleFiltering}/>
                        </Grid>
                    </Grid>
                </AppBar>
                {value === 0 && <AssetInformation aid={Number(assetid)} wfid={wfid} />}
                {value === 1 && <TaskContainer container = {UIContainer.ASSET_DETAIL_PAGE} withSearchBar={false}/>}
                {value === 2 && <AssetReadingContainer />}
            </div>
        );
    }
}

function mapStateToProps(state:any){
    return{
        task : state.tasks
    }
}

function mapDispatchToProps(dispatch:any){
    return {
        //api calls 
        getAssetTasks:(assetId:number)=>dispatch(getAssetTasks(assetId)),
        clearTasks:()=>dispatch(clearTasks()),
        loadAssetReadings:(assetId:number)=>dispatch(loadAssetReadings(assetId)),

        //Dispatchers for task filtering
        filteringTasks : ()=>dispatch(filteringTasks()),
        cancelTaskFiltering : ()=>dispatch(cancelTaskFiltering()),
        filterTask : (name:string,tasks : TaskResponse[])=>dispatch(filterTask(name,tasks)),        
    }    
}
//FIXME: Remove connector after checking the new connector
// export default connect(mapStateToProps,{getAssetTasks,clearTasks,loadAssetReadings})(AssetDetailPage)
export default connect(mapStateToProps,mapDispatchToProps)(AssetDetailPage)