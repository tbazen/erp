import React from 'react'
import { AssetResponse } from '../../../../../_infrastructure/model/assetModel';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { Link } from 'react-router-dom';

export function PendingAssetCard(value: AssetResponse, key: number) {
    return (
        <div style={{
            width: "60%",
            padding: "15px",
            margin: "auto",
            marginTop: "10px",
            border: "1px solid lightgray"
        }}
            key={key}
        >
            <Grid
                container justify="flex-start" key={key}>
                <Grid item xs={4}>
                    <Typography>
                        Name
           </Typography>
                </Grid>
                <Grid item xs={8}>
                    <Typography>
                        {value.name}
                    </Typography>
                </Grid>
                <Grid item xs={4}>
                    <Typography>
                        Description
           </Typography>
                </Grid>
                <Grid item xs={8}>
                    <Typography>
                        {value.description}
                    </Typography>
                </Grid>
                <Grid item xs={4}>
                    <Typography>
                        Location
           </Typography>
                </Grid>
                <Grid item xs={8}>
                    <Typography>
                        {value.geoLocation}
                    </Typography>
                </Grid>
                <Grid item xs={4}>
                    <Typography>
                        Asset Type
           </Typography>
                </Grid>
                <Grid item xs={8}>
                    <Typography>
                        {value.assetType == undefined ? '' : value.assetType.name}
                    </Typography>
                </Grid>


                <Grid item xs={5}>
                    <Link to={`/asset-detail/${value.wfid}/${value.id}`}>
                        <Button
                            fullWidth
                        >
                            More...
                </Button>
                    </Link>
                </Grid>
            </Grid>
        </div>
    )
}
