import React, { Component } from 'react'
import { connect } from 'react-redux';
import { AssetState } from '../../../../../_infrastructure/state/assetState';
import Paper from '@material-ui/core/Paper';
import { PendingAssetCard } from './pending-asset-card';
import EmptyContent from '../../empty/empty-list-container';


interface IProps {
}
interface PropsFromState {
    asset: AssetState
}
interface PropsFromDispatch {
}


type AllProps = IProps &
    PropsFromState &
    PropsFromDispatch


class PendingAssetContainer extends Component<AllProps> {
    constructor(props: AllProps) {
        super(props)

    }

    render() {
        let { asset } = this.props
        return (
            <Paper>
                {
                    asset.assets.length > 0 ?
                        asset.assets.map(PendingAssetCard)
                        :
                        <EmptyContent message="No Assets onthis section!"/>
                }
            </Paper>
        )
    }
}

function mapStateToProps(state: any) {
    return {
        asset: state.asset
    }
}
export default connect(mapStateToProps)(PendingAssetContainer)