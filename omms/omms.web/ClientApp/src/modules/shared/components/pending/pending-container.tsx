import React, { Component } from 'react'
import AppBar from '@material-ui/core/AppBar'
import Tabs from '@material-ui/core/Tabs'
import Tab from '@material-ui/core/Tab'
import AssetContainer from '../asset/asset-container';
import TaskContainer from './pending-task-container';
import TeamContainer from '../../team/team-contaner';
import { loadPendingAssets, filteringAssets, cancelAssetFiltering } from '../../../../_setup/actions/assetActions';
import { connect } from 'react-redux';
import { loadPendingTeams } from './../../../../_setup/actions/teamActions';
import { loadAllEmployees } from '../../../../_setup/actions/employeeActions';
import { getPendingTasks, filteringTasks, cancelTaskFiltering } from '../../../../_setup/actions/taskActions';
import Grid from '@material-ui/core/Grid';
import SearchField from '../search-bar/search-field';
import { TeamModel } from '../../../../_infrastructure/model/teamModel';
import { TaskResponse } from '../../../../_infrastructure/model/task-model';
import { AssetResponse } from '../../../../_infrastructure/model/assetModel';
import { AssetState } from '../../../../_infrastructure/state/assetState';
import { TeamState } from '../../../../_infrastructure/state/teamState';
import { TaskState } from '../../../../_setup/reducer/taskReducer';
import { filteringTeams, cancelTeamFiltering } from '../../../../_setup/actions/teamActions';
import { filterTeam, filterTask, filterAsset } from '../../../../_setup/actions/filter-actions';
import { changeActiveIndex } from '../../../../_setup/actions/drawer-actions';
import { IUserInformation } from 'src/_infrastructure/model/userInformationModel';
import Role from '../../_helper/role-util/roles';
import { OfficerItems } from '../../_helper/slideout-index/officer-items';
import { SupervisorItems } from '../../_helper/slideout-index/supervisor-items';
import Color from '../../_helper/color-container/index';
import UIContainer from './../../_helper/ui-helper/containers-util';
import { Badge } from '@material-ui/core';
import { EmployeeItems } from '../../_helper/slideout-index/employee-items';

interface IState {
    value: number
}

interface PropsFromState{
    task :TaskState
    team: TeamState
    asset: AssetState
    user : IUserInformation
}
interface PropsFromDispatch {
    changeActiveIndex:(index:number)=>void
    //api calls
    loadPendingAssets: () => void
    loadPendingTeams: () => void
    loadAllEmployees:() => void
    getPendingTasks:()=>void

    //filtering initiator
    filteringTeams : ()=>void,
    cancelTeamFiltering : ()=>void,
    filterTeam: (name:string,teams : TeamModel[])=>void

    filteringTasks: ()=>void,
    cancelTaskFiltering : ()=>void,
    filterTask:(name:string,tasks : TaskResponse[])=>void

    filteringAssets : ()=>void,
    cancelAssetFiltering : () => void,
    filterAsset: (name:string,assets:AssetResponse[])=>void

}
type AllProps = PropsFromState &
                PropsFromDispatch
class PendingContainer extends Component<AllProps, IState> {
    
    constructor(props: any) {
        super(props)
        this.handleChange = this.handleChange.bind(this)
        this.handleFiltering = this.handleFiltering.bind(this)
        this.state = {
            value: 0
        }
    }
    componentWillMount(){
        let {user} = this.props
        if(user.role == Role.TECHNICAL_OFFICER)
            this.props.changeActiveIndex(OfficerItems.PENDING)
        else if(user.role == Role.TECHNICAL_SUPERVISOR)
            this.props.changeActiveIndex(SupervisorItems.PENDING)
        else if(user.role == Role.EMPLOYEE)    
            this.props.changeActiveIndex(EmployeeItems.PENDING)    
        
    }

    componentDidMount() {
        this.props.loadPendingAssets()
        this.props.loadPendingTeams()
        this.props.loadAllEmployees()
        this.props.getPendingTasks()
    }

    handleFiltering(event:any){
        let {team,task,asset} = this.props
        let name:string  = event.target.value
        if(name.length == 0 ){
            
            this.props.cancelTeamFiltering()
            this.props.cancelTaskFiltering()
            this.props.cancelAssetFiltering()
        }
        else{
            this.props.filteringTasks()
            this.props.filterTask(name,task.tasks)

            this.props.filteringTeams()
            this.props.filterTeam(name,team.teams)

            this.props.filteringAssets()
            this.props.filterAsset(name,asset.assets)
        }        
    }

    //@ts-ignore
    handleChange(event: any, value: number) {
        this.setState({ value })
    }

    render() {
        let {asset,team,task,user} = this.props
        const { value } = this.state
        return (
            <div>
                <AppBar position="sticky"  color="default">
                    <Grid container spacing={8}> 
                        <Grid item lg={5} md={12} xs={12}>                    
                            <Tabs 
                                 value={value} 
                                 indicatorColor="primary"
                                 textColor="primary"
                                 onChange={this.handleChange}>
                                <Tab label={ <Badge color="secondary" badgeContent={task.tasks.length} style={{padding:'5px'}}>Task</Badge>} />
                                {
                                  user.role != Role.EMPLOYEE &&
                                  <Tab label={ <Badge color="secondary" badgeContent={asset.assets.length} style={{padding:'5px'}}>Asset</Badge>}/>
                                }
                                {
                                  user.role != Role.EMPLOYEE &&
                                  <Tab label={ <Badge color="secondary" badgeContent={team.teams.length} style={{padding:'5px'}}>Team</Badge>} />
                                }
                            </Tabs>
                        </Grid>
                        <Grid item lg={7} md={12} xs={12}>
                            <SearchField handler={this.handleFiltering}/>
                        </Grid>
                    </Grid>
                </AppBar>
                {value === 0 && <TaskContainer  container={UIContainer.PENDING_CONTAINER} withSearchBar={false}/>}
                {value === 1 && <AssetContainer container={UIContainer.PENDING_CONTAINER}   withSearchBar={false}/>}
                {value === 2 && <TeamContainer container={UIContainer.PENDING_CONTAINER} withSearchBar={false}/>}
            </div>
        );
    }
}


function mapStateToProps(state:any) {
    return {
        asset: state.asset,
        task : state.tasks,
        team: state.team,
        user: state.login.userInfo
    }
}

function mapDispatchToProps(dispatch:any){
    return {
        changeActiveIndex:(index:number)=>dispatch(changeActiveIndex(index)),

        loadPendingAssets:()=>dispatch(loadPendingAssets()),
        loadPendingTeams:()=>dispatch(loadPendingTeams()),
        loadAllEmployees:()=>dispatch(loadAllEmployees()),
        getPendingTasks:()=>dispatch(getPendingTasks()),

        //Dispatchers for team filtering
        filteringTeams : ()=>dispatch(filteringTeams()),
        cancelTeamFiltering : ()=>dispatch(cancelTeamFiltering()),
        filterTeam : (name:string,teams : TeamModel[])=>dispatch(filterTeam(name,teams)),
       

        //Dispatchers for task filtering
        filteringTasks : ()=>dispatch(filteringTasks()),
        cancelTaskFiltering : ()=>dispatch(cancelTaskFiltering()),
        filterTask : (name:string,tasks : TaskResponse[])=>dispatch(filterTask(name,tasks)), 
       
       //Dispatchers for asset filtering 
        filteringAssets : ()=>dispatch(filteringAssets()),
        cancelAssetFiltering : ()=>dispatch(cancelAssetFiltering()),
        filterAsset : (name:string,assets : AssetResponse[])=>dispatch(filterAsset(name,assets))
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(PendingContainer)