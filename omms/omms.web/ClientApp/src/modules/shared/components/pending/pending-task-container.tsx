import React, { Component } from 'react'
import './pending-task-container.scss'
import { connect } from 'react-redux'
import { TaskState } from '../../../../_setup/reducer/taskReducer'
import { TaskResponse } from '../../../../_infrastructure/model/task-model'
import TaskCard from '../task-card/task-card'
import LoadingPage from '../loading/loading-page';
import ErrorPage from '../error/error-page';
import EmptyContent from '../empty/empty-list-container';
import { filterTask, filterTaskByState } from '../../../../_setup/actions/filter-actions';
import { filteringTasks, cancelTaskFiltering } from '../../../../_setup/actions/taskActions';
import SearchBar from '../search-bar/search-bar'
import UIContainer from '../../_helper/ui-helper/containers-util';
import { Select, MenuItem } from '@material-ui/core';

interface IProps {
  withSearchBar : boolean
  container : UIContainer
}

interface PropsFromState{
  task :TaskState
  filteredTasks : TaskResponse[]
}

interface PropsFromDispatch{
  filteringTasks: ()=>void,
  cancelTaskFiltering : ()=>void,
  filterTask:(name:string,tasks : TaskResponse[])=>void
  filterTaskByState:(state:string,tasks:TaskResponse[])=>void
}

type AllProps = IProps &
                PropsFromState &
                PropsFromDispatch

interface IState{
  task_state : string
}
class TaskContainer extends Component<AllProps,IState> {

  constructor(props: AllProps) {
    super(props)
    this.state={
      task_state:'all'
    }
    this.cards = this.cards.bind(this)
    this.completedCards = this.completedCards.bind(this)
    this.terminatedCards = this.terminatedCards.bind(this)
    this.handleFiltering = this.handleFiltering.bind(this)
    this.handleTaskFilteringByState = this.handleTaskFilteringByState.bind(this)
  }

  cards(value: any, key: number): JSX.Element {
    return (
          <TaskCard
            data={value}
            viewClicked={null}
            key={key}
            role={'-1'}
            container = {this.props.container}
          />
        )
  }

  completedCards(value: any, key: number): JSX.Element {
    return (
          <TaskCard
            data={value}
            viewClicked={null}
            key={key}
            role={'-1'}
            container = {this.props.container}
          />
        )
  }

  terminatedCards(value: any, key: number): JSX.Element {
    return (
          <TaskCard
            data={value}
            viewClicked={null}
            key={key}
            role={'-1'}
            container = {this.props.container}
          />
        )
  }

  handleFiltering(event:any){
    let {task} = this.props
    let name:string  = event.target.value
    if(name.length == 0 )
      this.props.cancelTaskFiltering()
    else{
        this.props.filteringTasks()
        this.props.filterTask(name,task.tasks)
    }
  }

  handleTaskFilteringByState(event:any){
    let status:string = event.target.value
    let {tasks} = this.props.task
    let fltr = tasks.filter(t=>t.state==status)
    this.setState({task_state:status})
    if(status.toLowerCase() == 'all' )
      this.props.cancelTaskFiltering()
    else{
        this.props.filteringTasks()
        this.props.filterTaskByState(status,tasks)
    }
  }

  render() {
    let {task,filteredTasks,withSearchBar,container} = this.props  
    return (
      <div>
        {
          withSearchBar &&
          <SearchBar handler = {this.handleFiltering}/>
        }
        {
          task.loading &&
          <LoadingPage/>
        }
        {
          container == UIContainer.PENDING_CONTAINER && 
          <Select
                  value={this.state.task_state}
                  defaultValue={"1"}
                  style={{float:'right', display: 'block', width: '100%', padding: '5px 10px'}}
                  className={'task-types'}
                  onChange={this.handleTaskFilteringByState}
                >
                  <MenuItem value={"all"}>ALL WAITING TASKS</MenuItem>
                  <MenuItem value={'Waiting registration approval'}>REGISTRATION</MenuItem>
                  <MenuItem value={'Waiting Completion approval'}>COMPLETION</MenuItem>
                  <MenuItem value={'cancel'}>CANCELLATION</MenuItem>
                  <MenuItem value={'Waiting Termination Approval'}>TERMINATION</MenuItem>
                  <MenuItem value={'Waiting Assignee Modification Approval'}>ASSIGNEE MODIFICATION</MenuItem>
          </Select>
        }
        {
          !task.loading &&
          task.error &&
          <ErrorPage message={task.errorMessage}/>  
        }
        
            {
              !task.loading &&
              !task.error &&
              task.filtering &&
              filteredTasks.length > 0 &&
              filteredTasks.map(this.cards)
            }
            {
              !task.loading &&
              !task.error &&
              task.filtering &&
              !(filteredTasks.length > 0) &&
              <EmptyContent message ="No match found!"/>
            }
        
            {
              !task.loading &&
              !task.error &&
              !task.filtering &&
              task.tasks.length > 0 &&
              task.tasks.map((value: TaskResponse, key: number) => {
                if(value.id != null) {
                  return this.cards(value, key)
                }
                else
                  return null
              })
            }
          {
            !task.loading &&
            !task.error &&
            !task.filtering &&
            task.tasks.length > 0 &&
            task.tasks.map((value: TaskResponse, key: number) => {
              if(value.id == null) {
                return this.cards(value, key)
              }
              else
                return null
            })
          }
          {
            !task.loading &&
            !task.error &&
            !task.filtering &&
            !(task.tasks.length > 0) &&
            <EmptyContent message ="No task on this section!"/>
          }
      </div>
    )
  }
}
function mapStateToProps(state:any){
  return{
    task : state.tasks,
    filteredTasks : state.filter.filteredTask
  }
}

function mapDisptchToProps(dispatch:any){
  return {
    filteringTasks : ()=>dispatch(filteringTasks()),
    cancelTaskFiltering : ()=>dispatch(cancelTaskFiltering()),
    filterTask : (name:string,tasks : TaskResponse[])=>dispatch(filterTask(name,tasks)),
    filterTaskByState : (state:string,tasks:TaskResponse[])=>dispatch(filterTaskByState(state,tasks))
  }
}

export default connect(mapStateToProps,mapDisptchToProps)(TaskContainer)