import React, { Component } from 'react'
import CircularProgress from '@material-ui/core/CircularProgress';
import { Theme } from '@material-ui/core/es'
import { withStyles } from '@material-ui/core'

const styles = (theme:Theme) => ({
  progress: {
    margin: theme.spacing.unit * 2,
  },
})

class Circular extends Component <any, any>{
  render() {
    const { classes } = this.props;
    return (
      <div>
        <CircularProgress className={classes.progress}/>
      </div>
  )
  }
}

export default withStyles(styles)(Circular);