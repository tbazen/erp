import React, { Component } from 'react'
import classNames from 'classnames'
import { Theme, withStyles } from '@material-ui/core/styles'
import CircularProgress from '@material-ui/core/CircularProgress'
import green from '@material-ui/core/colors/green'
import Fab from '@material-ui/core/Fab'
import CheckIcon from '@material-ui/icons/Check'
import SaveIcon from '@material-ui/icons/Save'
import Paper from '@material-ui/core/Paper'
import Typography from '@material-ui/core/Typography'
import createStyles from '@material-ui/core/styles/createStyles'
import Modal from '@material-ui/core/Modal/Modal'

const styles = (theme: Theme) =>
  createStyles({
    root: {
      ...theme.mixins.gutters(),
      display: 'flex',
      alignItems: 'center',
      paddingTop: theme.spacing.unit * 2,
      width: '50%',
      margin: 'auto',
      marginTop: '10%',
      paddingBottom: theme.spacing.unit * 2
    },
    wrapper: {
      margin: theme.spacing.unit * 8,
      position: 'relative'
    },
    buttonSuccess: {
      backgroundColor: green[500],
      '&:hover': {
        backgroundColor: green[700]
      }
    },
    fabProgress: {
      color: green[500],
      position: 'absolute',
      top: -6,
      left: -6,
      zIndex: 1
    }
  })

class CircularIntegration extends Component<any> {
  constructor(props: any) {
    super(props)
    this.handleClose = this.handleClose.bind(this)
  }

  handleClose = () => {
    this.setState({ open: false })
  }
  render() {
    const { classes } = this.props
    const buttonClassname = classNames({
      [classes.buttonSuccess]: this.props.success
    })

    return (
      <Modal
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
        open={this.props.openProgressIndicator}
        onClose={this.handleClose}
      >
        <Paper className={classes.root} elevation={1}>
          <Typography
            variant="h5"
            style={{ width: '87%', marginRight: '3%' }}
            component="h3"
          >
            {this.props.message}
          </Typography>
          <div className={classes.wrapper} style={{ width: '10%' }}>
            <Fab color="primary" className={buttonClassname}>
              {this.props.success ? <CheckIcon /> : <SaveIcon />}
            </Fab>
            {this.props.loading && (
              <CircularProgress size={68} className={classes.fabProgress} />
            )}
          </div>
        </Paper>
      </Modal>
    )
  }
}

export default withStyles(styles)(CircularIntegration)
