import React, { Component,Fragment } from 'react'
import AppBar from '@material-ui/core/AppBar'
import Tabs from '@material-ui/core/Tabs'
import Tab from '@material-ui/core/Tab'
import { loadRejectedAssets, filteringAssets, cancelAssetFiltering } from '../../../../_setup/actions/assetActions';
import { connect } from 'react-redux';
import AssetContainer from '../asset/asset-container';
import TeamContainer from '../../team/team-contaner';
import { loadRejectedTeams, filteringTeams, cancelTeamFiltering } from '../../../../_setup/actions/teamActions';
import TaskContainer from './../pending/pending-task-container';
import { getRejectedTasks } from './../../../../_setup/actions/taskActions';
import SearchField from '../search-bar/search-field';
import Grid from '@material-ui/core/Grid';
import { TeamState } from '../../../../_infrastructure/state/teamState';
import { TeamModel } from '../../../../_infrastructure/model/teamModel';
import { filterTeam, filterTask, filterAsset } from '../../../../_setup/actions/filter-actions';
import { filteringTasks, cancelTaskFiltering } from '../../../../_setup/actions/taskActions';
import { TaskResponse } from './../../../../_infrastructure/model/task-model';
import { TaskState } from '../../../../_setup/reducer/taskReducer';
import { AssetResponse } from '../../../../_infrastructure/model/assetModel';
import { AssetState } from '../../../../_infrastructure/state/assetState';
import { changeActiveIndex } from '../../../../_setup/actions/drawer-actions';
import Role from '../../_helper/role-util/roles';
import UIContainer from '../../_helper/ui-helper/containers-util';
import { OfficerItems } from '../../_helper/slideout-index/officer-items';
import { Badge } from '@material-ui/core';
import { EmployeeItems } from '../../_helper/slideout-index/employee-items';
import { IUserInformation } from '../../../../_infrastructure/model/userInformationModel';

interface IState {
    value: number
}

interface PropsFromState{
    task :TaskState
    team: TeamState
    asset: AssetState

    user : IUserInformation
}

interface PropsFromDispatch {

    changeActiveIndex:(index:number)=>void

    loadRejectedAssets: () => void
    loadRejectedTeams:() => void
    getRejectedTasks:()=>void


    filteringTeams : ()=>void,
    cancelTeamFiltering : ()=>void,
    filterTeam: (name:string,teams : TeamModel[])=>void

    filteringTasks: ()=>void,
    cancelTaskFiltering : ()=>void,
    filterTask:(name:string,tasks : TaskResponse[])=>void

    filteringAssets : ()=>void,
    cancelAssetFiltering : () => void,
    filterAsset: (name:string,assets:AssetResponse[])=>void
}
type AllProps = PropsFromState &
                PropsFromDispatch

class RejectedContainer extends Component<AllProps, IState> {
    constructor(props: any) {
        super(props)
        this.handleChange = this.handleChange.bind(this)
        this.state = {
            value: 0
        }
        this.handleFiltering = this.handleFiltering.bind(this)
    }

    componentWillMount(){
        let {user} = this.props
        if(user.role == Role.TECHNICAL_OFFICER)
            this.props.changeActiveIndex(OfficerItems.REJECTED)
        else if(user.role == Role.EMPLOYEE)    
             this.props.changeActiveIndex(EmployeeItems.REJECTED)
    }


    componentDidMount() {
        this.props.loadRejectedAssets()
        this.props.loadRejectedTeams()
        this.props.getRejectedTasks()
    }
    handleChange(event: any, value: number) {
        console.log(event.undefined)
        this.setState({ value })
    }

    handleFiltering(event:any){
        let {team,task,asset} = this.props
        let name:string  = event.target.value
        if(name.length == 0 ){
            
            this.props.cancelTeamFiltering()
            this.props.cancelTaskFiltering()
            this.props.cancelAssetFiltering()
        }
        else{
            this.props.filteringTasks()
            this.props.filterTask(name,task.tasks)

            this.props.filteringTeams()
            this.props.filterTeam(name,team.teams)

            this.props.filteringAssets()
            this.props.filterAsset(name,asset.assets)
        }
    }


    render() {
        const { value } = this.state;
        let {asset,team,task,user} = this.props
        return (
            <div>
                <AppBar position="sticky" color="default">
                    <Grid container spacing={8}>
                      <Grid item lg={5} md={12} xs={12}>
                            <Tabs 
                                  value={value} 
                                  indicatorColor="primary"
                                  textColor="primary"
                                  onChange={this.handleChange}>
                                <Tab label={ <Badge color="secondary" badgeContent={task.tasks.length} style={{padding:'5px'}}>Task</Badge>} />
                                {
                                  user.role != Role.EMPLOYEE &&
                                  <Tab label={ <Badge color="secondary" badgeContent={asset.assets.length} style={{padding:'5px'}}>Asset</Badge>}/>
                                }
                                {
                                  user.role != Role.EMPLOYEE &&
                                  <Tab label={ <Badge color="secondary" badgeContent={team.teams.length} style={{padding:'5px'}}>Team</Badge>} />
                                }
                            </Tabs>
                      </Grid>
                      <Grid item lg={7} md={12} xs={12}>
                            <SearchField handler={this.handleFiltering}/>
                      </Grid>
                    </Grid>                    
                </AppBar>
                {value === 0 && <TaskContainer  container={UIContainer.REJECTED_CONTAINER} withSearchBar={false}/>}
                {value === 1 && <AssetContainer container={UIContainer.REJECTED_CONTAINER} withSearchBar={false} />}
                {value === 2 && <TeamContainer container={UIContainer.REJECTED_CONTAINER} withSearchBar={false}/>}
            </div>
        );
    }
}

function mapStateToProps(state:any) {
    return {
        user: state.login.userInfo,
        asset: state.asset,
        task : state.tasks,
        team: state.team
    }
}


function mapDispatchToProps(dispatch:any){
    return {

        //keeping the slide out interactivity
        changeActiveIndex:(index:number)=>dispatch(changeActiveIndex(index)),

        //api calls
        loadRejectedAssets:()=>dispatch(loadRejectedAssets()),
        loadRejectedTeams:()=>dispatch(loadRejectedTeams()),
        getRejectedTasks:()=>dispatch(getRejectedTasks()),

        //Dispatchers for team filtering
        filteringTeams : ()=>dispatch(filteringTeams()),
        cancelTeamFiltering : ()=>dispatch(cancelTeamFiltering()),
        filterTeam : (name:string,teams : TeamModel[])=>dispatch(filterTeam(name,teams)),
       

        //Dispatchers for task filtering
        filteringTasks : ()=>dispatch(filteringTasks()),
        cancelTaskFiltering : ()=>dispatch(cancelTaskFiltering()),
        filterTask : (name:string,tasks : TaskResponse[])=>dispatch(filterTask(name,tasks)), 
       
       //Dispatchers for asset filtering 
        filteringAssets : ()=>dispatch(filteringAssets()),
        cancelAssetFiltering : ()=>dispatch(cancelAssetFiltering()),
        filterAsset : (name:string,assets : AssetResponse[])=>dispatch(filterAsset(name,assets))
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(RejectedContainer) 