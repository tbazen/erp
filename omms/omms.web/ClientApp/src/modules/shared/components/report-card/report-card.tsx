import React,{  Component } from 'react'
import CardContent from '@material-ui/core/CardContent';
import { Typography, Divider } from '@material-ui/core';
import Card from '@material-ui/core/Card';


export interface IEntry{
  name : string
  value:string | number
  percentage :string | number
}

export interface IReportCard{
  title : string
  content : IEntry[]
  footer? : {name:string,value:string | number}
}


class ReportCard extends Component<IReportCard>{

  row =(data:IEntry)=>{
    return (
      <tr>
        <td align={'left'}>
          <Typography variant={'body1'}>
            {data.name}
          </Typography>
        </td>
        <td align={'left'}>
          <Typography variant={'body1'}>
          {data.value}
          </Typography>
        </td>
        <td>
          <Typography variant={'body1'}>
          {'('+data.percentage+'% )'}
          </Typography>
        </td>
      </tr>
    )
  }


  render(){
    const {title,content,footer} =this.props 
    return (
        <Card>
            <CardContent>
              <Typography variant={'title'} align={'center'}>
                  {title}   
              </Typography>
              <Divider/>
              <table style={{width:'80%',margin:'auto'}}>
                {content.map(this.row)}
                {
                  footer != null &&
                  footer != undefined &&
                  <tr>
                    <td align={'left'}>
                      <Typography variant={'subheading'} color={'primary'}>
                        {footer.name}
                      </Typography>
                    </td>
                    <td align={'left'}>
                      <Typography variant={'subheading'} color={'primary'}>
                        {footer.value}
                      </Typography>
                    </td>
                  </tr>
                }   
              </table>
            </CardContent>
        </Card>
    )
  }
}

export default ReportCard