import React,{Component} from 'react'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import InputBase from '@material-ui/core/InputBase'
import SearchIcon from '@material-ui/icons/Search'
import Grid from '@material-ui/core/Grid';


interface IProps{
    handler : (event:any)=>void
}

type AllProps = IProps
/*
* TODO:  primary color 3f51b5 this will be removed verry soon
*/ 
class SearchBar extends Component<AllProps> {
  render() {
    return (
        <AppBar color="default" position="sticky">
          <div style={{margin:"7px",backgroundColor:"transparent"}}>
            <Grid container justify="center">
                <Grid item xs={8}>
                    <table style={{width:"100%"}}>
                     <tr>
                         <td style={{width:'3px'}}>
                            <SearchIcon style={{color:"gray"}} />  
                         </td>
                         <td>
                            <input type="text" onChange={this.props.handler} style={{
                              width:"100%",
                              padding:"5px",
                              fontSize:"15px",
                              color:"gray",
                              backgroundColor:"transparent",
                              border:"none"
                              }} placeholder="Search…"/>
                         </td> 
                     </tr>
                    </table>
                </Grid>
            </Grid>
            </div>
        </AppBar>
    );
  }
}

export default SearchBar;
