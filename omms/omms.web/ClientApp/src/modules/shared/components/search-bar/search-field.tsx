import React,{Component} from 'react'
import SearchIcon from '@material-ui/icons/Search'
import Grid from '@material-ui/core/Grid';


interface IProps{
    handler : (event:any)=>void
}

type AllProps = IProps

class SearchField extends Component<AllProps> {
  render() {
    return (
          <div style={{margin:"7px",backgroundColor:"#fafafa"}}>
            <Grid container justify="flex-start">
                <Grid item xs={10}>
                   <table style={{width:"100%"}}>
                     <tr>
                         <td style={{width:'3px'}}>
                            <SearchIcon style={{color:"gray"}} />  
                         </td>
                         <td>
                            <input type="text" 
                            onChange={this.props.handler} 
                            style={{
                              width:"100%",
                              padding:"5px",
                              fontSize:"15px",
                              color:"gray",
                              backgroundColor:"transparent",
                              border:"none"
                              }} placeholder="Search…"/>
                         </td> 
                     </tr>
                    </table>
                </Grid>
            </Grid>
            </div>
    );
  }
}

export default SearchField;
