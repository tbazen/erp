import React, { Component,Fragment } from 'react'
import classNames from 'classnames'
import { createStyles, Theme, withStyles } from '@material-ui/core/styles'
import Drawer from '@material-ui/core/Drawer'
import List from '@material-ui/core/List'
import CssBaseline from '@material-ui/core/CssBaseline'
import Divider from '@material-ui/core/Divider'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Badge, ListItemIcon } from '@material-ui/core'
import {
  ISlideoutContent,
  getSlideoutContent
} from '../../_helper/slideout-util/slideout-content'
import { Link, BrowserRouter } from 'react-router-dom'
import { faBars } from '@fortawesome/free-solid-svg-icons'
import { IRouteSet, getRouteSet } from '../../_helper/route-util/route-provider'
import './slideout.scss'
import { changeActiveIndex } from './../../../../_setup/actions/drawer-actions'
import { connect } from 'react-redux'
import Color from './../../_helper/color-container'
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Role from '../../_helper/role-util/roles';
import { MetaData } from '../../../../_infrastructure/model/metadata-model';


const drawerWidth = 240

const styles = (theme: Theme) =>
  createStyles({
    root: {
      display: 'flex'
    },
    appBar: {
      zIndex: theme.zIndex.drawer + 1,
      transition: theme.transitions.create(['width', 'margin'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen
      })
    },
    appBarShift: {
      marginLeft: drawerWidth,
      width: `calc(100% - ${drawerWidth}px)`,
      transition: theme.transitions.create(['width', 'margin'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen
      })
    },
    menuButton: {
      marginLeft: 12,
      marginRight: 36
    },
    hide: {
      display: 'none'
    },
    drawer: {
      width: drawerWidth,
      flexShrink: 0,
      whiteSpace: 'nowrap'
    },
    drawerOpen: {
      width: drawerWidth,
      transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen
      })
    },
    drawerClose: {
      transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen
      }),
      overflowX: 'hidden',
      width: theme.spacing.unit * 7 + 1,
      [theme.breakpoints.up('sm')]: {
        width: theme.spacing.unit * 9 - 8
      }
    },
    toolbar: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'flex-end',
      padding: '0 8px',
      ...theme.mixins.toolbar
    },
    content: {
      flexGrow: 1,
      padding: 0
    }
  })

interface IState {
  open: boolean
  slideoutContent: ISlideoutContent
  routeSet: IRouteSet
}

class Slideout extends Component<any, IState> {
  constructor(props: any) {
    super(props)
    let { role } = this.props
    let slideContent = getSlideoutContent(role)
    let routeSet = getRouteSet(role)
    
    this.item= this.item.bind(this)
    this.changeActiveItem = this.changeActiveItem.bind(this)
    this.notificationBatch = this.notificationBatch.bind(this)

    this.state = {
      open: true,
      slideoutContent: slideContent,
      routeSet: routeSet
    }
  }

  handleDrawerAction = () => {
    this.setState({
      open: !this.state.open,
      slideoutContent: this.state.slideoutContent,
      routeSet: this.state.routeSet
    })
  }
  //@ts-ignore
  changeActiveItem = (index:number)=>(event:any)=>{
    this.props.changeActiveIndex(index)
  }


  notificationBatch(index:number,children:JSX.Element):JSX.Element{
    let {role} = this.props
    let meta: MetaData = this.props.metadata
    

    if(role== Role.TECHNICAL_OFFICER)
    {
      if(index == 5)
      {
        return  (<Badge color="secondary" badgeContent={meta.pendingItems} >{children}</Badge>)
      }
      else if(index == 6)
      {
        return  (<Badge color="secondary" badgeContent={meta.rejectedItems} >{children}</Badge>)
      }
    }
    else if(role == Role.TECHNICAL_SUPERVISOR)
    {
      if(index == 4)
      {
        return  (<Badge color="secondary" badgeContent={meta.pendingItems} >{children}</Badge>)
      }
    }
    else if(role == Role.EMPLOYEE){
      switch(index){
        case 0:
          return  (<Badge color="secondary" badgeContent={meta.newIndividualTasks} >{children}</Badge>)
        case 1:
          return  (<Badge color="secondary" badgeContent={meta.newTeamTasks} >{children}</Badge>)
        case 4:
          return  (<Badge color="secondary" badgeContent={meta.pendingItems} >{children}</Badge>)
        case 5:
          return  (<Badge color="secondary" badgeContent={meta.rejectedItems} >{children}</Badge>) 
      }
    }
    return children
  }

  item(item:any , key: number){
    let {index} = this.props.drawer
    let style = 
    key == index
    ? 
    {
      backgroundColor:Color.ACTIVE_ITEM
    }
    :
    {
      backgroundColor:Color.TRANSPARENT
    }
    return (
      <Link
            to={item.navigateTo}
            key={key}
            style={{
                    textDecoration: 'none',
                    color:Color.PRIMARY_FOREGROUND
                    }}
            onClick={this.changeActiveItem(key)}
            className='list-item'
            >
              <ListItem button  style={style}>
                {this.notificationBatch(key,
                  <Fragment>
                    <ListItemIcon style={{color:Color.PRIMARY_FOREGROUND}}>{item.icon}</ListItemIcon>
                    <ListItemText >
                            <span style={{color:Color.PRIMARY_FOREGROUND}}>{item.label}</span>
                    </ListItemText>
                  </Fragment>
                  )}
            </ListItem>
      </Link>
    )
  }

  render() {
    const { classes } = this.props
    return (
      <BrowserRouter>
        <div className={classes.root}>
          <CssBaseline />
          <div
            style={{ position: 'fixed' }}
            className={classNames(classes.appBar, {
              [classes.appBarShift]: this.state.open
            })}
          />
          <Drawer
            variant="permanent"
            className={classNames(classes.drawer, {
              [classes.drawerOpen]: this.state.open,
              [classes.drawerClose]: !this.state.open
            })}
            classes={{
              paper: classNames({
                [classes.drawerOpen]: this.state.open,
                [classes.drawerClose]: !this.state.open
              })
            }}
            open={this.state.open}
          >
            <span onClick={this.handleDrawerAction}>
              <ListItem button style={{backgroundColor:Color.PRIMARY_DARK}}>
                <ListItemIcon>
                  <FontAwesomeIcon style={{color:Color.PRIMARY_FOREGROUND}} icon={faBars} size={'2x'} />
                </ListItemIcon>
                <ListItemText>
                     <Typography style={{color:Color.PRIMARY_FOREGROUND}} variant="h6">OMMS</Typography>
                </ListItemText>
              </ListItem>
            </span>

            <Divider />
            <div style={{height:'100vh',backgroundColor:Color.PRIMARY}}>
              <List>
                {
                this.state.slideoutContent.items.map(this.item)
                }
              </List>
            </div>
          </Drawer>
          <main className={classes.content}>{this.state.routeSet.routes}</main>
        </div>
      </BrowserRouter>
    )
  }
}


function mapStateToProps(state:any){
  return {
    drawer : state.drawer,
    metadata : state.metadata
  }  
}

function mapDispatchToProps(dispatch:any){
  return {
    changeActiveIndex: (index:number )=>dispatch(changeActiveIndex(index))
  }
}
export default connect(mapStateToProps,mapDispatchToProps)(withStyles(styles, { withTheme: true })(Slideout))
