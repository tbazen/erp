import React, { Component } from 'react'
import './task-card.scss'
import {
  Button,
  InputLabel,
  MenuItem,
  Paper, 
  Select
} from '@material-ui/core'
import { Link } from 'react-router-dom'
import {
  approveTask, approveTaskState,
  cancelTask, completeTask,
  terminateTask
} from '../../../../_setup/actions/taskActions'
import { connect } from 'react-redux'
import Role from '../../_helper/role-util/roles'
import { TaskResponse } from '../../../../_infrastructure/model/task-model'
import { IUserInformation } from '../../../../_infrastructure/model/userInformationModel'
import { dateConverter } from '../../../_helper/date-converter'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCalendar, faEnvelopeOpenText, faExclamationCircle, faStickyNote } from '@fortawesome/free-solid-svg-icons'
import { cancelRegistrationRequest } from '../../../../_setup/actions/workflow-actions'
import UIContainer from '../../_helper/ui-helper/containers-util';
import Color from '../../_helper/color-container' 
import EditTask from '../../edit-task/edit-task'
import AssigneeFormModal from '../task/task-assignee-form-modal';
import { openNoteModal, WFNoteActions } from '../../../../_setup/actions/wfnote-modal-actions';
import { WorkflowTypes } from '../../../../_infrastructure/model/workflow-model';


interface IProps {
  role: string
  data: TaskResponse
  viewClicked: any,
  key: number,
  container:UIContainer
}

interface PropsFromDispatch {
  openNoteModal: (wfid:string,WFType : WorkflowTypes,modalAction: WFNoteActions)=>void
  approveTask: (id: string) => void
  approveTaskState:(wfid:string) =>void
  cancelRegistrationRequest: (wfid: string) => void
}

interface PropsFromState {
  userInfo: IUserInformation
}


type AllProps = IProps & PropsFromDispatch & PropsFromState//TODO fix the all props issue, don't use any #Bazen

interface ICardState {
  load: boolean
  page: string
  taskState: number,
  cardTask: string
  //open: boolean,
  data?: TaskResponse,
  openEdit: boolean,
  dialog: {
    opened: boolean,
    type: string
  },
  editNote: string,
  editAssigneeId: string,
  team: string,
  employee: string,
  assigneeType: number,
  fix: boolean,
  teamTemp: string
}

class TaskCard extends Component<AllProps, any> {


  state: ICardState = {
    load: false,
    page: '',
    taskState: 0,
    cardTask: '',
    openEdit: false,
    team: '',
    employee: '',
    dialog: {
      opened: false,
      type: ''
    },
    editNote: '',
    editAssigneeId: '',
    assigneeType: 2,
    fix: false,
    teamTemp: ''
  }

  constructor(props: any) {
    super(props)

    this.deleteHandler = this.deleteHandler.bind(this)
    this.updateInputValue = this.updateInputValue.bind(this)
    this.approveTask = this.approveTask.bind(this)
    this.rejectTask = this.rejectTask.bind(this)
    this.updateStatus = this.updateStatus.bind(this)
    this.approveState = this.approveState.bind(this)
    this.rejectState = this.rejectState.bind(this)
    this.closeEdit = this.closeEdit.bind(this)
    this.handleOpen = this.handleOpen.bind(this)
    this.handleAssigneeEdit = this.handleAssigneeEdit.bind(this)
    this.handleOpen = this.handleOpen.bind(this)
    this.handleClose = this.handleClose.bind(this)
    this.handleDone = this.handleDone.bind(this)
  }

  handleDone() {
    this.handleClose()
  }

  async componentWillMount() {
    this.setState({
      load: true
    })


    //Check for the user role and assign card task
    if(this.props.userInfo.role == Role.TECHNICAL_OFFICER) {
      this.setState({
        cardTask: 'e&c'
      })
    }
    else if(this.props.userInfo.role == Role.TECHNICAL_SUPERVISOR) {
      this.setState({
        cardTask: 'a&r'
      })
    }
    else if(this.props.userInfo.role == Role.EMPLOYEE) {
      this.setState({
        cardTask: 'm'
      })
    }
    if (this.props.role == '-1') {
      this.setState({
        page: 'pending-tasks'
      })
    } else if (this.props.role == Role.ADMIN || this.props.role == Role.TECHNICAL_SUPERVISOR || this.props.role == Role.TECHNICAL_OFFICER || this.props.role == Role.EMPLOYEE) {
      this.setState({
        page: 'task'
      })
    } else if (this.props.role == '-2') {
      this.setState({
        page: 'notifications'
      })
    } else if (this.props.role == '-3') {
      this.setState({
        page: 'notifications'
      })
    }

    this.setState({
      fix: !this.state.fix
    })
  }

  // FIXME: @ HISK Remove this comment after checking  : Unnecessary functions
  // async componentDidMount() {
  //   if(this.props.data.id != null) {
  //     await this.props.latestTaskNotes(this.props.data.id as string)
  //     this.state.latestNote = this.props.note
  //   }
  // }
  deleteHandler() {
    let data: TaskResponse = this.props.data
    if(data != null) {
      if(data.id == null)
        this.props.cancelRegistrationRequest(data.wfid as string)
    }
  }

  updateInputValue(e: any) {
    this.setState({
      [e.target.name]: e.target.value
    })
  }

  approveTask() {
    this.props.approveTask(this.props.data.wfid as string)
  }

  rejectTask() {

    //TODO: change workflow 

    let {wfid} = this.props.data
    wfid != null &&
    this.props.openNoteModal(wfid,WorkflowTypes.Task,WFNoteActions.REJECTION)
    //this.props.rejectTaskRegistration(this.props.data.wfid as string)
  }

  approveState(){
    this.props.approveTaskState(this.props.data.wfid as string)
  }

  async rejectState(){
    let {wfid} = this.props.data
    wfid != null &&
    this.props.openNoteModal(wfid,WorkflowTypes.Task,WFNoteActions.REJECT_TASK_STATE)
  }

  updateStatus(event: any) {
    let value : string = event.target.value
    // this.addFinalNote(this.props.data.id as string)
    let {wfid} = this.props.data
      if(value == WFNoteActions.TASK_TERMINATION) {
        //terminate task here
        wfid != null &&
        this.props.openNoteModal(wfid,WorkflowTypes.Task,WFNoteActions.TASK_TERMINATION)
      }
      else if(value == WFNoteActions.TASK_COMPLETION) {
        //complete task here
        wfid != null &&
        this.props.openNoteModal(wfid,WorkflowTypes.Task,WFNoteActions.TASK_COMPLETION)
      }
      else if(value == WFNoteActions.TASK_CANCELLATION) {
        //cancel task here
        wfid != null &&
        this.props.openNoteModal(wfid,WorkflowTypes.Task,WFNoteActions.TASK_CANCELLATION)
      }

  }

  handleAssigneeEdit() {
    this.setState({
      dialog: {
        opened: true
      }
    })
  }

  closeEdit() {
    this.setState({
      openEdit: false
    })
  }

  handleOpen = () => {
    this.setState({ openEdit: true });
    this.setState({ fix: !this.state.fix});
  }


  handleClose = () => {
    this.setState({ dialog: { opened: false, taskAssignee: '' } })
    setTimeout(() => {
      this.setState({ dialog: { opened: false, taskAssignee: '' } })
    }, 100)
  }


  //FIXME here's where the object is sent from the front end to the edit assignee api


  public render() {
    const {data} = this.props
    let waiting:boolean = data.id != null && data.state.split(' ')[0].toLowerCase() == 'Waiting'.toLowerCase()
    return (
      <Paper className={'task-card-container'} style={{ padding: '20px' }}>
        <div className="card">
          <div className="style-buttons" style={{display: 'inline-block', alignItems: 'center', float: 'right', width: 'fit-content', flexDirection: 'column' }}>
            {this.state.cardTask == 'm' && data.id != null && !data.state.includes('Waiting') ? (
              <div className={'display-inline'}>
                <InputLabel>Task Status</InputLabel>
                <Select
                  value={this.state.taskState}
                  defaultValue={"0"}
                  onChange={this.updateStatus
                  //   (event: any) => {
                  //   this.updateInputValue(event)
                  //   if(event.target.value != 0)
                  //     this.setState({
                  //       open: true
                  //     })
                  // }
                }
                  inputProps={{
                    name: 'taskState'
                  }}
                  fullWidth
                >
                  <MenuItem value={"0"}>
                    <em>None</em>
                  </MenuItem>
                  <MenuItem value={WFNoteActions.TASK_TERMINATION}>Terminated</MenuItem>
                  <MenuItem value={WFNoteActions.TASK_COMPLETION}>Completed</MenuItem>
                  <MenuItem value={WFNoteActions.TASK_CANCELLATION}>Cancel</MenuItem>
                </Select>
              </div>
            ) : this.state.cardTask == 'm' && data.id == null ? (
              <div className={'display-inline'}>
                <InputLabel>Task Status</InputLabel>
                <Select
                  value={this.state.taskState}
                  defaultValue={"0"}
                  onChange={(event: any) => {
                    this.updateInputValue(event)
                    if(event.target.value != 0)
                      this.setState({
                        open: true
                      })
                  }}
                  inputProps={{
                    name: 'taskState'
                  }}
                  fullWidth
                >
                  <MenuItem value={'0'}>
                    <em>None</em>
                  </MenuItem>
                  <MenuItem value={'1'}>Terminated</MenuItem>
                  <MenuItem value={'2'}>Completed</MenuItem>
                  <MenuItem value={'3'}>Cancel</MenuItem>
                </Select>
              </div>
            ) : this.state.cardTask == 'a&r' && data.id == null  ? (
              <div className={'display-inline'}>
                <Button
                  className={'delete-btn btn'}
                  color={'primary'}
                  variant={'outlined'}
                  onClick={this.approveTask}
                  size={'small'}
                  style={{
                    backgroundColor:Color.HAPPY,
                    color:Color.PRIMARY_FOREGROUND,
                    borderRadius:'2px !important;'
                  }}
                >
                  Approve Task
                </Button>
                <Button
                  className={'delete-btn btn'}
                  color={'secondary'}
                  variant={'outlined'}
                  onClick={this.rejectTask}
                  size={'small'}
                  style={{
                    backgroundColor:Color.SECONDARY,
                    color:Color.PRIMARY_FOREGROUND,
                    borderRadius:'2px'
                  }}
                >
                  Reject Task
                </Button>
              </div>
            ) : this.state.cardTask == 'e&c' && data.id == null ? (
                <div className={'display-inline'} style={{width: 'fit-content !important'}}>
                  {
                  <Button
                    className={'delete-btn btn'}
                    color={'secondary'}
                    onClick={this.deleteHandler}
                    size={'small'}
                    style={{
                      backgroundColor:Color.SECONDARY,
                      color:Color.PRIMARY_FOREGROUND,
                      borderRadius:'2px'
                    }}
                  >
                    Cancel Task
                  </Button>
                  }
                  <Button
                    className={'edit-btn btn'}
                    variant={'outlined'}
                    onClick={this.handleOpen}
                    size={'small'}
                    style={{
                      backgroundColor:Color.HAPPY,
                      color:Color.PRIMARY_FOREGROUND,
                      borderRadius:'2px'
                    }}
                  >
                    Edit Task
                  </Button>
                </div>
              ): this.state.cardTask == 'e&c' && data.id != null && data.state == 'On going task' ? (
                <div className={'display-inline'} style={{width: 'fit-content !important'}}>
                  <Button
                    className={'edit-btn btn'}
                    variant={'outlined'}
                    onClick={this.handleAssigneeEdit}
                    size={'small'}
                    style={{
                      backgroundColor:Color.HAPPY,
                      color:Color.PRIMARY_FOREGROUND,
                      borderRadius:'2px'
                    }}
                  >
                    Edit Assignee
                  </Button>
                </div>
              )
              :
              null
            }
            {
              waiting && this.props.userInfo.role == Role.TECHNICAL_SUPERVISOR &&
              <div className={'display-inline'}>
                <Button
                  className={'delete-btn btn'}
                  color={'primary'}
                  style={{
                    backgroundColor:Color.HAPPY,
                    color:Color.PRIMARY_FOREGROUND,
                    borderRadius:'2px'
                  }}
                  onClick={this.approveState}
                  size={'small'}
                >
                  Approve Status
                </Button>
                <Button
                  className={'delete-btn btn'}
                  color={'secondary'}
                  // variant={'outlined'}
                  style={{
                    backgroundColor:Color.SECONDARY,
                    color:Color.PRIMARY_FOREGROUND,
                    borderRadius:'2px'
                  }}
                  onClick={this.rejectState}
                  size={'small'}
                >
                  Reject Status
                </Button>
              </div>
            }
            <Link to={data.id == null ? `/view-task/${data.wfid}` : `/view-task/${data.id}`} className={'display-inline'} >
              <Button
                className={'view-btn btn'}
                // variant={'outlined'}
                // onClick={() => this.props.viewClicked(data.id)}
                size={'small'}
                style={{
                  backgroundColor:Color.PRIMARY,
                  color:Color.PRIMARY_FOREGROUND,
                  borderRadius:'2px'
                }}
              >
                More Details
              </Button>
            </Link>
          </div>

          <span className={'name'}>{data.name}</span>

          <div className="date-section">
            <FontAwesomeIcon icon={faCalendar} size={'2x'} className={'icon'}/>
            <div className="start-date info">
              <span className={'label'}>
                Start Date Expected
              </span>
              <br/>
              <span className={'value'}>
                {
                  dateConverter(data != null && data.startDateExpected != null ? data.startDateExpected as string : '')
                }
              </span>
            </div>
            <div className="end-date info">
              <span className={'label'}>
                End Date Expected
              </span>
              <br/>
              <span className={'value'}>
                {
                  dateConverter(data != null && data.endDateExpected != null ? data.endDateExpected as string : '')
                }
              </span>
            </div>
          </div>

          <div className="description-section">
            <FontAwesomeIcon icon={faEnvelopeOpenText} size={'2x'} className={'icon'}/>
            <div className="info">
              <span className={'label'}>Description:</span>
              <span className={'task-field'}>{data.description}</span>
            </div>
          </div>


          <div className="multi-section">
            <div className="description-section">
              <FontAwesomeIcon icon={faExclamationCircle} size={'2x'} className={'icon'}/>
              <div className="info">
                <span className={'label'}>Priority </span>
                <span className={'task-field'}>{
                  data.priority == 1 ?
                    <i style={{color: '#f33'}}>Very High</i>
                    :
                  data.priority == 2 ?
                    <i style={{color: '#f99'}}>High</i>
                    :
                  data.priority == 3 ?
                    <i style={{color: '#f9f'}}>Medium</i>
                    :
                  data.priority == 4 ?
                    <i style={{color: '#99f'}}>Low</i>
                    :
                  data.priority == 5 ?
                    <i style={{color: '#66f'}}>Very Low</i>
                    :
                  data.priority

                }&nbsp;</span>
              </div>
            </div>
            {/*
            FIXME: @Hisk remove this code after testing
             {
              this.state.latestNote != null &&
              this.state.latestNote != undefined &&
              this.props.data.state != undefined &&
              this.props.data.state != 'Completed' &&
              // data.state == 'Awaiting Completion Approval' &&
              <div className="note-view">
                <div className="description-section">
                  <FontAwesomeIcon icon={faStickyNote} size={'2x'} className={'icon'}/>
                  <div className="info">
                    <span className={'label'}>Notes: </span>
                    <span className={'task-field'}>
                      {
                        this.state.latestNote.note
                      }
                    </span>
                  </div>
                </div>
              </div>
            } */}
            {
              data.note != '' &&
              data.note != undefined &&
              data.note != null &&
              <div className="note-view">
                <div className="description-section">
                  <FontAwesomeIcon icon={faStickyNote} size={'2x'} className={'icon'}/>
                  <div className="info">
                    <span className={'label'}>Notes: </span>
                    <span className={'task-field'}>
                      {
                        data.note
                      }
                    </span>
                  </div>
                </div>
              </div>
            }
          </div>
          <div className="created-by">
            <span className={'label'}>Created By:
              {
                data.createdBy.userName
                //FIXME: @Hisk : Remove this comment after checking 
                // this.state.employeeSuggestions.map(( value: {name: string; value: string}) => {
                //   if(data.createdBy.id == value.value)
                //     return value.name
                //   else
                //     return null
                // })
              }
            </span>
          </div>
          <div className="state">
            <i className={'state'}>{data.state == null ? 'Pending Task' : data.state}</i>
          </div>
        </div>

        <EditTask
          data={this.props.data}
          open={this.state.openEdit}
          onClose={this.closeEdit}
          onOpen={this.handleOpen}
          container={this.props.container}
        />
{/* FIXME: @Hisk remove this comment its unnecessarty
        <NoteFormModal
          openForm={this.state.open}
          closeHandler={this.closeModal}
          taskId={this.props.data.id as string}
          done={this.createNote}
          container = {UIContainer.TASK_CARD}
        /> */}
        <AssigneeFormModal
          dialogOpen ={this.state.dialog.opened}
          task={data}
          handleClose = {this.handleClose}
        />

      </Paper>
    )
  }
}

const mapStateToProps = (state:any) => ({
  userInfo: state.login.userInfo,
  employees: state.employee.employees,
  teams: state,
  note: state.note.notes
})

function mapDispatchToProps(dispatch:any){
  return {
    openNoteModal: (wfid:string,WFType : WorkflowTypes,modalAction: WFNoteActions)=>dispatch(openNoteModal(wfid,WFType,modalAction)),
    //FIXME: Remove all the unnecessary comments
    // deleteTask: (id: string) => dispatch(deleteTask(id)),
    approveTask: (id: string) => dispatch(approveTask(id)),
    approveTaskState:(wfid:string) =>dispatch(approveTaskState(wfid)),
    cancelRegistrationRequest: (wfid: string) => dispatch(cancelRegistrationRequest(wfid)), 
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TaskCard)
