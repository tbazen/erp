import React, { Component } from 'react'
import Button from '@material-ui/core/Button'
import FormControl from '@material-ui/core/FormControl'
import Paper from '@material-ui/core/Paper'
import Typography from '@material-ui/core/Typography'
import Modal from '@material-ui/core/Modal/Modal'
import { connect } from 'react-redux'
import Grid from '@material-ui/core/Grid'
import Checkbox from '@material-ui/core/Checkbox'
import { getTaskByStatus, getTasksForDependency, viewAllTasks } from '../../../../_setup/actions/taskActions'
import { TaskModel } from '../../../../_infrastructure/model/taskFromModel'
import List from '@material-ui/core/List'
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import { TaskStatus } from '../../../../_infrastructure/model/task-model'

interface IState {
  dependUpon: string[]
}

interface IProps {
  openForm: boolean
  handleClose: () => void
  task: TaskModel
  setDependency: (dependency: string[]) => void
}

interface PropsFromDispatch {
  getTaskByStatus: (status: number) => void
}

interface PropsFromState {
  tasks: TaskModel[]
}

type AllProps = IProps & PropsFromDispatch & PropsFromState

class TaskListForm extends Component<any, IState> {
  constructor(props: any) {
    super(props)
    this.saveDependency = this.saveDependency.bind(this)
    this.addDependency = this.addDependency.bind(this)
    this.removeDependency = this.removeDependency.bind(this)
    this.taskListComponent = this.taskListComponent.bind(this)
    this.selectHandler = this.selectHandler.bind(this)
    this.closeForm = this.closeForm.bind(this)
    this.state = {
      dependUpon: this.props.task.dependUpon
    }
  }

  async componentDidMount() {
    getTasksForDependency(TaskStatus.ONGOING).then((res:any) => {
      console.log('res')
      console.log(res)
    })
  }

  saveDependency(event: any) {
    event.preventDefault()
    this.props.setDependency(this.state.dependUpon)
    this.closeForm()
  }

  addDependency(id: string) {
    this.setState({
      dependUpon: [...this.state.dependUpon, id]
    })
  }

  removeDependency(id: string) {
    this.setState({
      dependUpon: [...this.state.dependUpon.filter(t => t != id)]
    })
  }

  selectHandler(event: any) {
    if (event.target.checked) this.addDependency(event.target.id)
    else this.removeDependency(event.target.id)
  }

  taskListComponent(value: TaskModel, key: number): JSX.Element {
    let isMember = this.state.dependUpon.find(t => t == value.id) != undefined

    return (
      <ListItem button key={key}>
        <ListItemText primary={value.name} />
        <ListItemSecondaryAction>
          <Checkbox
            color="primary"
            id={value.id == null ? '' : value.id}
            checked={isMember}
            onChange={this.selectHandler}
          />
        </ListItemSecondaryAction>
      </ListItem>
    )
  }

  closeForm() {
    this.props.handleClose()
  }
  render() {
    return (
      <Modal
        aria-labelledby="Task Dependency"
        aria-describedby="Task Dependency Selection"
        open={this.props.openForm}
        onClose={this.closeForm}
      >
        <div>
          <Paper
            style={{
              width: '40%',
              margin: 'auto',
              marginTop: '10vh',
              overflow: 'auto',
              padding: '15px'
            }}
          >
            <Grid container justify="center" spacing={8}>
              <Grid item xs={10}>
                <Typography component="h2" variant="h5">
                  Select dependency tasks
                </Typography>
              </Grid>
              <Grid
                item
                xs={10}
                style={{
                  maxHeight: '70vh',
                  minHeight: '70vh',
                  overflow: 'auto'
                }}
              >
                <List dense>
                  {this.props.tasks.map(this.taskListComponent)}
                </List>
              </Grid>
              <Grid item xs={10}>
                <Button
                  variant="contained"
                  color="primary"
                  onClick={this.saveDependency}
                >
                  Save Dependency
                </Button>
              </Grid>
            </Grid>
          </Paper>
        </div>
      </Modal>
    )
  }
}

function mapStateToProps(state: any) {
  return {
    tasks: state.tasks.tasks
  }
}

export default connect(
  mapStateToProps,
  { getTasksForDependency }
)(TaskListForm)
