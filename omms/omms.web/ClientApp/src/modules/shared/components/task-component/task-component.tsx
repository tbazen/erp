import React, { Component } from 'react'
import './task-component.scss'
import TaskCard from '../task-card/task-card'
import { Fab, Paper, Theme, withStyles } from '@material-ui/core'
import { connect } from 'react-redux'
import { viewAllTasks } from '../../../../_setup/actions/taskActions'
import { TaskModel } from '../../../../_infrastructure/model/taskFromModel'
import ViewTask from '../../../shared/components/view-task/view-task'
import AppBar from '@material-ui/core/AppBar'
import Tabs from '@material-ui/core/Tabs'
import Tab from '@material-ui/core/Tab'
import { getUserInfo } from '../../../../_setup/actions/loginActions'
import Circular from '../progress/Circular'
import Role from '../../_helper/role-util/roles'
import UIContainer from '../../_helper/ui-helper/containers-util';

interface ITaskState {
  modal: boolean
  create: boolean
  counter: number
  load: boolean
  viewId: string
  key: number
  value: number
}

const styles = (theme: Theme) => ({
  fab: {
    margin: theme.spacing.unit
  },
  extendedIcon: {
    marginRight: theme.spacing.unit
  }
})

class TaskComponent extends Component<any, ITaskState> {
  state: ITaskState = {
    modal: false,
    create: false,
    counter: 0,
    load: false,
    viewId: '',
    key: 0,
    value: 0
  }

  constructor(props: any) {
    super(props)

    this.props.getUserInfo()

    this.viewTask = this.viewTask.bind(this)
    this.cardScheduled = this.cardScheduled.bind(this)
    this.cardUnscheduled = this.cardUnscheduled.bind(this)
    this.cardPeriodic = this.cardPeriodic.bind(this)
    this.cardIncident = this.cardIncident.bind(this)
    this.cardUnassigned = this.cardUnassigned.bind(this)
    this.cardCompleted = this.cardCompleted.bind(this)
    this.viewComponent = this.viewComponent.bind(this)
    this.handleChange = this.handleChange.bind(this)
  }

  async componentDidMount() {
    await this.props.viewAllTasks()
    await this.props.getUserInfo()
    console.log(this.props)
    this.setState({
      load: true
    })

    console.log(this.props.match.params.type)
  }

  componentWillUpdate(prevProps: any) {
    if (this.props.match.params.type !== prevProps.match.params.type) {
      this.setState({
        key: this.state.key++
      })
      this.componentDidMount()
    }
  }

  viewTask(id: string) {
    //TODO fix the refresh issue
    this.setState({
      viewId: id
    })
  }

  cardScheduled(value: any, key: number): JSX.Element {
    console.log(value.taskType)
    if (value.taskType == 0)
      return (
        <TaskCard
          data={value}
          viewClicked={this.viewTask}
          key={key}
          container={UIContainer.REGISTARED_TASK_CONTAINER}
          role={this.props.user.role}
        />
      )
    else return <div key={key} />
  }

  cardUnscheduled(value: any, key: number): JSX.Element {
    if (value.taskType == 1)
      return (
        <TaskCard
          data={value}
          container={UIContainer.REGISTARED_TASK_CONTAINER}
          viewClicked={this.viewTask}
          key={key}
          role={this.props.user.role}
        />
      )
    else return <div key={key} />
  }

  cardPeriodic(value: any, key: number): JSX.Element {
    if (value.taskType == 2)
      return (
        <TaskCard
        container={UIContainer.REGISTARED_TASK_CONTAINER}
          data={value}
          viewClicked={this.viewTask}
          key={key}
          role={this.props.user.role}
        />
      )
    else return <div key={key} />
  }

  cardIncident(value: any, key: number): JSX.Element {
    if (value.taskType == 4)
      return (
        <TaskCard
        container={UIContainer.REGISTARED_TASK_CONTAINER}
          data={value}
          viewClicked={this.viewTask}
          key={key}
          role={this.props.user.role}
        />
      )
    else return <div key={key} />
  }

  cardUnassigned(value: any, key: number): JSX.Element {
    if (value.taskAssignee == null) return <div key={key} />
    else {
      if (
        value.taskAssignee.assigneeModel == null ||
        value.taskAssignee.assigneeType == 0
      )
        return (
          <TaskCard
            data={value}
            viewClicked={this.viewTask}
            key={key}
            container={UIContainer.REGISTARED_TASK_CONTAINER}
            role={this.props.user.role}
          />
        )
      else return <div key={key} />
    }
  }

  cardCompleted(value: any, key: number): JSX.Element {
    if (value.taskStatus != undefined) {
      if (value.taskStatus.status == 4)
        return (
          <TaskCard
            data={value}
            viewClicked={this.viewTask}
            key={key}
            container={UIContainer.REGISTARED_TASK_CONTAINER}
            role={this.props.user.role}
          />
        )
      else return <div key={key} />
    } else return <div key={key} />
  }

  //@ts-ignore
  handleChange(e: any, value: number) {
    this.setState({ value })
  }

  TabContainer(props: any) {
    return <div>{props.children}</div>
  }

  viewComponent() {
    console.log(this.props.tasks)
    return (
      <div>
        {this.props.tasks.map((value: any) => {
          if (value.id == this.state.viewId) return <ViewTask data={value} />
          else return
        })}
      </div>
    )
  }

  public render() {
    return (
      <div>
        {this.state.load && this.props.tasks.tasks != undefined ? (
          <div className={'task-component-container'}>
            <Paper>
              <AppBar position="static" color={'default'}>
                <Tabs value={this.state.value} onChange={this.handleChange}>
                  <Tab label="Scheduled" />
                  <Tab label="Unscheduled" />
                  <Tab label="Periodic" />
                  <Tab label="Incident" />
                  {this.props.user.role == Role.ADMIN ||  this.props.user.role == Role.TECHNICAL_SUPERVISOR || this.props.user.role == Role.TECHNICAL_OFFICER ? <Tab label="Unassigned" /> : null}
                  {this.props.user.role == Role.ADMIN ||  this.props.user.role == Role.TECHNICAL_SUPERVISOR || this.props.user.role == Role.TECHNICAL_OFFICER? <Tab label="Completed" /> : null}
                </Tabs>
              </AppBar>
              {this.state.value === 0 && (
                <this.TabContainer>
                  {this.props.tasks.tasks.map(this.cardScheduled)}
                </this.TabContainer>
              )}
              {this.state.value === 1 && (
                <this.TabContainer>
                  {this.props.tasks.tasks.map(this.cardUnscheduled)}
                </this.TabContainer>
              )}
              {this.state.value === 2 && (
                <this.TabContainer>
                  {this.props.tasks.tasks.map(this.cardPeriodic)}
                </this.TabContainer>
              )}
              {this.state.value === 3 && (
                <this.TabContainer>
                  {this.props.tasks.tasks.map(this.cardIncident)}
                </this.TabContainer>
              )}
              {this.state.value === 4 && (
                <this.TabContainer>
                  {this.props.tasks.tasks.map(
                    (value: TaskModel, key: number) => {
                      return this.cardUnassigned(value, key)
                    }
                  )}
                </this.TabContainer>
              )}
              {this.state.value === 5 && (
                <this.TabContainer>
                  {this.props.tasks.tasks.map(
                    (value: TaskModel, key: number) => {
                      return this.cardCompleted(value, key)
                    }
                  )}
                </this.TabContainer>
              )}
            </Paper>
          </div>
        ) : (
          <div className={'task-component-container margin-fix'}>
            <div
              style={{
                width: '100px',
                height: 'fit-content',
                margin: 'auto'
              }}
            >
              <Circular />
            </div>
          </div>
        )}
      </div>
    )
  }
}


const mapStateToProps = (state: any) => ({
  tasks: state.tasks,
  user: state.login.userInfo
})

export default withStyles(styles)(
  connect(
    mapStateToProps,
    { viewAllTasks, getUserInfo }
  )(TaskComponent)
)
