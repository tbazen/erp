import React,{Component, Fragment} from 'react'
import { TextField, FormLabel, RadioGroup, FormControlLabel, Radio, Paper, Grid, FormControl } from '@material-ui/core';
import Button from '@material-ui/core/Button'
import { AssigneeViewModel } from '../../../../_infrastructure/model/taskFromModel';
import { TaskResponse} from '../../../../_infrastructure/model/task-model'
import { Typography, Modal } from '@material-ui/core/es';
import Color from '../../_helper/color-container/index';
import { connect } from 'react-redux';
import { loadAllEmployees} from '../../../../_setup/actions/employeeActions';
import { EmployeeState } from '../../../../_infrastructure/state/employee-state';
import { IUserInformation } from '../../../../_infrastructure/model/userInformationModel';
import LoadingCircle from '../loading/loading-circle';
import { TeamState } from './../../../../_infrastructure/state/teamState';
import { TeamModel } from '../../../../_infrastructure/model/teamModel';
import { updateTaskAssignee } from '../../../../_setup/actions/taskActions'

interface IProps{
    dialogOpen : boolean 
    handleClose : ()=>void
    task: TaskResponse
} 

interface PropsFromState{
  teamState: TeamState
  employeeState : EmployeeState
}

interface PropsFromDispatch {
  updateTaskAssignee: (assignee: AssigneeViewModel) => void
}

interface IState{
  newAssignee : AssigneeViewModel
}

type AllProps = IProps &
                PropsFromState &
                PropsFromDispatch

class AssigneeFormModal extends Component<AllProps,IState>{
    constructor(props:AllProps){
        super(props)
        let {task} = this.props
        this.state = {
          newAssignee:{
            note:'',
            wfid:task.wfid ==null ? '' : task.wfid,
            assignee:{
              assigneeType :1,
              assigneeModel: {
                taskId: task.id==null?'':task.id,
                dateAssigned : null,
                assigneeId:''
              }
            }
          }
        }
        this.handleModalClose = this.handleModalClose.bind(this)
        this.handleDone = this.handleDone.bind(this)
    }

    handleModalClose(){
        this.props.handleClose()
    }

    handleInputChange = (event:any)=>{
      let {newAssignee} = this.state
      newAssignee.note = event.target.value
      this.setState({newAssignee}) 
    }

    handleAssigneeChange=(event:any)=>{
      let {newAssignee} = this.state
      newAssignee.assignee.assigneeModel.assigneeId = event.target.value
      this.setState({newAssignee}) 
    }

    handleAssigneeTypeChange = (event:any)=>{
      let {newAssignee} = this.state
      newAssignee.assignee.assigneeType = event.target.value
      console.group('Assignee Type change : ')
      console.log(newAssignee)
      console.groupEnd()
      this.setState({newAssignee}) 
    }

    handleDone() {
      console.log(this.state.newAssignee)
      this.props.updateTaskAssignee(this.state.newAssignee)
      this.handleModalClose()
    }

    employeeListContaineer = () => {
      let {employeeState} = this.props
      return (
        <Fragment>
          {
            employeeState.loading &&
            <span style={{ top:'50%',left:'50%',position:'relative'}}><LoadingCircle/></span>
          }
          {
            employeeState.error &&
            <p>{'Failed to load employee team list'}</p>
          }
          {
            !employeeState.loading && 
            !employeeState.error &&
            <RadioGroup onChange={this.handleAssigneeChange}>
            {
              employeeState.employees.map((value:IUserInformation, key: number) =>
                <FormControlLabel
                   value={value.id != null ? value.id+'' :''}
                   control={<Radio/>}
                   key={key}
                   label={value.userName}
                 />
               )
             }
           </RadioGroup> 
          }
        </Fragment>
      )
    }

    teamListContainer = () => {
      let {teamState} = this.props
      return (
        <Fragment>
          {
            teamState.loading &&
            <span style={{ top:'50%',left:'50%',position:'relative'}}><LoadingCircle/></span>
          }
          {
            teamState.error &&
            <p>{'Failed to load team list'}</p>
          }
          {
            !teamState.loading && 
            !teamState.error &&
            <RadioGroup onChange={this.handleAssigneeChange} >
                {
                  teamState.teams.map((value: TeamModel, key: number) =>
                        <FormControlLabel
                          value={value.id!=null ? value.id+'' : ''}
                          control={<Radio/>}
                          key={key}
                          label={value.name}
                        />
                  )
                }
            </RadioGroup>
          }
        </Fragment>
      )
    }

    render(){
        let {dialogOpen} = this.props
        let {assigneeType} = this.state.newAssignee.assignee

        return (
        <Modal 
              open={dialogOpen} 
              onClose={this.handleModalClose} 
        >
           <Paper style={{width:'50%',maxHeight:'95vh',height:'95vh',margin:'auto',marginTop:'2vh'}}>
              <Grid container justify={'center'}>
                 <Grid item md={10}>
                  <Typography style={{marginBottom:'10px'}} component="h1" variant="h5">
                    Change Task Assignee 
                  </Typography>
                 </Grid>
                 <Grid item xs={10}>
                          <TextField
                              placeholder={'Note...'}
                              multiline={true}
                              rows={4}
                              rowsMax={5}
                              fullWidth
                              value={this.state.newAssignee.note}
                              variant={'outlined'}
                              onChange={this.handleInputChange}
                            />
                 </Grid>
                 <Grid item xs={10}>
                  <div style={{marginTop:'15px'}}>
                      <FormLabel className={'block'}>Assignee Type</FormLabel>
                      <RadioGroup
                        aria-label={'Assignee Type'}
                        className={'assignee-type'}
                        value={this.state.newAssignee.assignee.assigneeType+''}
                        onChange={this.handleAssigneeTypeChange}
                        name={'assigneeType'}
                        style={{display: 'block'}}
                      >
                        <FormControlLabel
                          value='2'
                          control={<Radio />}
                          label="Team"
                          labelPlacement="start"
                          style={{paddingRight: '25px'}}
                        />
                        <FormControlLabel
                          value="1"
                          control={<Radio />}
                          label="Employee"
                          labelPlacement="start"
                          style={{paddingRight: '25px'}}
                        />
                      </RadioGroup>
                    </div>
                 </Grid>
                 <Grid item xs ={10}>
                    <div style={{width:'100%',backgroundColor:'lightgray',height:'55vh',padding:'10px',overflow:'auto',maxHeight:'55vh'}}>
                    {
                      assigneeType == 2 ?
                      this.teamListContainer()
                      :
                      this.employeeListContaineer()
                    }
                    </div>
                 </Grid>
                 <Grid item xs={10}> 
                    <Grid container spacing={16} justify={'flex-start'}>
                       <Grid item xs={4}>
                         <Button
                                style={{
                                  borderRadius:'2px',
                                  marginBottom:"10px",
                                  marginTop:"10px",
                                  color:Color.PRIMARY_FOREGROUND,
                                  backgroundColor:Color.SECONDARY                              
                                }}
                                fullWidth 
                                onClick={this.handleModalClose}>
                          Cancel
                         </Button>
                       </Grid>
                       <Grid item xs={4}>
                         <Button 
                                 style={{
                                  borderRadius:'2px',
                                  marginTop:"10px",
                                  marginBottom:"10px",
                                  color:Color.PRIMARY_FOREGROUND,
                                  backgroundColor:Color.PRIMARY                              
                                }}
                                fullWidth
                                onClick={this.handleDone}>
                          Done
                        </Button>
                       </Grid>
                    </Grid>
                 </Grid> 
              </Grid>
            </Paper>
        </Modal>
        )
    }
}
function mapStateToProps(state:any){
  return {
    employeeState : state.employee,
    teamState : state.team,
  }
}

function mapDispatchToProps(){
  return {
    updateTaskAssignee
  }
}
export default connect(mapStateToProps,{ updateTaskAssignee })(AssigneeFormModal)