import React, { Component } from 'react'
import './view-task.scss'
import {
  Button,
  ExpansionPanel,
  ExpansionPanelDetails,
  ExpansionPanelSummary, List, ListItem,
  Paper,
} from '@material-ui/core'
import { Redirect } from 'react-router-dom'
import AppBar from '@material-ui/core/AppBar/AppBar'
import Tabs from '@material-ui/core/Tabs/Tabs'
import Tab from '@material-ui/core/Tab/Tab'
import { connect } from 'react-redux'
import { loadTaskNotes } from '../../../../_setup/actions/noteActions'
import {  TaskResponse } from '../../../../_infrastructure/model/task-model'
import NoteContainer from './../../task/note/note-container'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
  faAngleDown,
  faCalendar,
  faCog,
  faEnvelopeOpenText,
  faExclamationCircle,
  faFile,
  faTasks,
  faUser,
  faUsers
} from '@fortawesome/free-solid-svg-icons'
import {
  approveTask,
  approveTaskState, getTaskById,
  rejectTaskRegistration,
  rejectTaskState,
  viewAllTasks
} from '../../../../_setup/actions/taskActions'
import { dateConverter } from '../../../_helper/date-converter'
import DocumentService from '../../../../_setup/services/document.service'
import MiniMap from '../../map/mini-map/mini-map'
import { loadAssets } from '../../../../_setup/actions/assetActions'
import { AssetResponse } from '../../../../_infrastructure/model/assetModel'
import { getTeamById } from '../../../../_setup/actions/teamActions'
import Color from '../../_helper/color-container'
const document_service  = new DocumentService()


interface IViewProps {
  data: TaskResponse,
  loadTaskNotes: any
}

interface IState {
  open: boolean,
  value: number,
  note: string,
  header: string,
  data: any,
  imageIndex : number,
  incidentImages: any[],
  dependencyList: TaskResponse[]
}

class ViewTask extends Component<any, any> {
  employeeSuggestions: { name: string; value: string }[] = []

  state: IState = {
    open: false,
    value: 0,
    note: '',
    header: '',
    data: null,
    imageIndex : 0,
    incidentImages: [],
    dependencyList: []
  }

  constructor(props: any) {
    super(props)

    this.handleChange = this.handleChange.bind(this)
    this.handleOpen = this.handleOpen.bind(this)
    this.handleClose = this.handleClose.bind(this)
    this.updateInputValue = this.updateInputValue.bind(this)
    this.doneHandler = this.doneHandler.bind(this)
    this.handleWarDownload = this.handleWarDownload.bind(this)
    this.imageDisplay = this.imageDisplay.bind(this)
    this.prevImage = this.prevImage.bind(this)
    this.nextImage = this.nextImage.bind(this)
    this.approveState = this.approveState.bind(this)
    this.rejectState = this.rejectState.bind(this)
    this.getTeamInfo = this.getTeamInfo.bind(this)
    this.getDependency = this.getDependency.bind(this)
    this.fetchDepndencyTasks = this.fetchDepndencyTasks.bind(this)
  }

  //TODO Configure routes here
  TabContainer(props: any) {
    return <div>{props.children}</div>
  }

  //@ts-ignore
  handleChange(e: any, value: number) {
    this.setState({ value })
  }

  handleOpen = () => {
    this.setState({ open: true })
  }

  handleClose = () => {
    this.setState({ open: false })
  }

  updateInputValue(e: any) {
    this.setState({
      [e.target.name]: e.target.value
    })
  }

  doneHandler() {
    this.handleClose()
  }

  async componentWillMount() {
    // this.props.loadTaskNotes(this.props.data.id)

    if(this.props.data == null) {
      this.props.viewAllTasks()
    }

    this.props.tasks.map((task: any) => {
      if(this.props.match.params.taskid == task.id || this.props.match.params.taskid == task.wfid) {
        this.setState({
          data: task
        })
        if(task.id != null)
          this.props.loadTaskNotes(task.id)
      }
    })

    this.props.employees.map((value: any) => {
      this.employeeSuggestions.push({ name: value.userName, value: value.id })
    })

    await this.props.loadAssets()

  }

  async componentDidMount() {
    let images = null

    if(this.state.data != null)
    //@ts-ignore
      images = this.state.data.incidentInfo.images

    for (let i = 0; i < images.length; i++) {
      let image = await document_service.loadImage(images[i].id)
      let incidentImages = this.state.incidentImages

      //@ts-ignore
      incidentImages = [...incidentImages, image]
      this.setState({ incidentImages })
    }

    //@ts-ignore
    await this.props.getTeamById(this.state.data != null && this.state.data.taskAssignee.assigneeModel.assigneeId)

    await this.fetchDepndencyTasks()
  }


  fetchDepndencyTasks(){
    this.state.data.dependUpon.forEach((dtsk:string) => {
      getTaskById(dtsk)
        .then(tsk=>{
          let {dependencyList} = this.state
          this.setState({dependencyList : [...dependencyList,tsk]})
        })
    })
  }

  getTeamInfo() {
    return (
      <div style={{width: '100% !important', maxWidth: '100% !important'}}>
        {
          this.props.team.map((res: any) => {
            return(
              <div>
                <ExpansionPanel>
                  <ExpansionPanelSummary expandIcon={<FontAwesomeIcon icon={faAngleDown} size={'sm'}/>}>
                    <span style={{fontSize: '20px', fontWeight: 'lighter'}}>{res.name}</span>
                  </ExpansionPanelSummary>
                  <ExpansionPanelDetails>
                    <List>
                      {res.members.map((value:any) => {
                        return (<ListItem>{value.userName}</ListItem>)
                      })}
                    </List>
                  </ExpansionPanelDetails>
                </ExpansionPanel>
              </div>
            )
          })
        }
      </div>
    )
  }

  getDependency(value : TaskResponse, key:number) {
    return(
      <ListItem key={key}>
        {value.name}
      </ListItem>
    )
  }


  prevImage() {
    let {imageIndex} = this.state
    if(imageIndex > 0)
      this.setState({imageIndex:imageIndex-1})
  }

  nextImage() {
    let {imageIndex} = this.state

    let images = this.state.incidentImages
    if(imageIndex < images.length-1)
      this.setState({imageIndex:imageIndex+1})
  }

  imageDisplay(blobData: Blob) {
    console.log(blobData)
    let urlObject = URL.createObjectURL(blobData);
    return (
      <div className="container">
        <img style={{width:"100%"}} src={urlObject}></img>
        <button onClick={this.prevImage} className="left-btn">Prev</button>
        <button onClick={this.nextImage} className="right-btn">Next</button>
      </div>
    )
  }

  //@ts-ignore
  handleWarDownload=(docId:string)=>(event:any)=> {
    document_service.downloadFile(docId)
  }

  approveState(){
    // @ts-ignore
    this.props.approveTaskState(this.state.data.wfid as string)
  }
  rejectState(){
    // @ts-ignore
    this.props.rejectTaskState(this.state.data.wfid as string)
  }

  approveTask() {
    // @ts-ignore
    this.props.approveTask(this.state.data.wfid as string)
  }

  rejectTask() {
    // @ts-ignore
    this.props.rejectTaskRegistration(this.state.data.wfid as string)
  }

  render() {
    const data: TaskResponse = this.state.data as unknown as TaskResponse

    return (
      data != null ?
      <div>
        <div className={'task-component-container'}>
          <div>
            <AppBar position="sticky" color={'default'}>
              <Tabs 
                   value={this.state.value} 
                   indicatorColor="primary"
                   textColor="primary"
                   onChange={this.handleChange}>
                <Tab label="View Task" />
                {
                  data.id != null ?
                  <Tab label="Notes" />
                    :
                  null
                }
              </Tabs>
            </AppBar>
            {
              this.state.value === 0 && (
              <this.TabContainer>
                <Paper className={'view-task-component'}>
                  <div className={'view-container'}>
                    
                    {/*
                    FIXME: @Hisku Remove this comment after checking
                    <div className="buttons">*/}
                      {/*{*/}
                        {/*data.id != null &&*/}
                        {/*data.state != null &&*/}
                        {/*data.state != 'Completed' &&*/}
                        {/*data.state != 'Terminated' &&*/}
                        {/*data.state != 'On going task' &&*/}
                        {/*this.props.userInfo.role == Role.TECHNICAL_SUPERVISOR &&*/}
                        {/*<div>*/}
                          {/*<Button*/}
                            {/*color={'primary'}*/}
                            {/*variant={'outlined'}*/}
                            {/*className={'btn'}*/}
                            {/*onClick={this.approveState}*/}
                          {/*>*/}
                            {/*Approve State*/}
                          {/*</Button>*/}
                          {/*< Button*/}
                          {/*color={'secondary'}*/}
                          {/*variant={'outlined'}*/}
                          {/*className={'btn'}*/}
                          {/*onClick={this.rejectState}>*/}
                          {/*Reject State*/}
                          {/*</Button>*/}
                        {/*</div>*/}
                      {/*}*/}

                      {/*{*/}
                        {/*data.id == null && this.props.userInfo.role == Role.TECHNICAL_SUPERVISOR &&*/}
                        {/*<div>*/}
                          {/*<Button*/}
                          {/*color={'primary'}*/}
                          {/*variant={'outlined'}*/}
                          {/*className={'btn'}*/}
                          {/*>*/}
                            {/*Approve Task*/}
                          {/*</Button>*/}
                          {/*<Button*/}
                            {/*color={'secondary'}*/}
                            {/*variant={'outlined'}*/}
                            {/*className={'btn'}*/}
                          {/*>*/}
                            {/*Reject Task*/}
                          {/*</Button>*/}
                        {/*</div>*/}
                      {/*}*/}
                    {/*</div>*/}



                    <span className={'name'}>
                      {data.name}
                    </span>

                    {/*<i className={'state'}>{data.state == null ? 'Pending Task' : data.state}</i>*/}

                    <div className="description-section">
                      <FontAwesomeIcon icon={faEnvelopeOpenText} size={'2x'} className={'icon'}/>
                      <div className="info">
                        <i className={'label'}>Description:</i>
                        <span className={'task-field'}>{data.description}</span>
                      </div>
                    </div>

                    {/*
                    //FIXME: @Hisk Remove unneccessary Comment
                    <div className="info">*/}
                      {/*<span className={'label'}>*/}
                        {/*Created By:*/}
                      {/*</span>*/}
                      {/*<span className={'value'}>*/}
                        {/*{*/}
                          {/*this.employeeSuggestions.map(( value: {name: string; value: string}) => {*/}
                            {/*if(data.createdBy.id == value.value)*/}
                              {/*return value.name*/}
                            {/*else*/}
                              {/*return null*/}
                          {/*})*/}
                        {/*}*/}
                      {/*</span>*/}
                    {/*</div>*/}

                    <div className="the-container">
                      <div className="description-section">
                        <FontAwesomeIcon icon={faExclamationCircle} size={'2x'} className={'icon'}/>
                        <div className="info">
                          <i className={'label'}>Priority </i>
                          <span className={'task-field'}>{
                            data.priority == 1 ?
                              <i style={{color: '#f33'}}>Very High</i>
                              :
                            data.priority == 2 ?
                              <i style={{color: '#f99'}}>High</i>
                              :
                            data.priority == 3 ?
                              <i style={{color: '#f9f'}}>Medium</i>
                              :
                            data.priority == 4 ?
                              <i style={{color: '#99f'}}>Low</i>
                              :
                            data.priority == 5 ?
                              <i style={{color: '#66f'}}>Very Low</i>
                              :
                            <span>{data.priority}</span>

                          }&nbsp;</span>
                        </div>
                      </div>

                      <div className="description-section">
                        <FontAwesomeIcon icon={faCog} size={'2x'} className={'icon'}/>
                        <div className="info">
                            <i className={'label'}>
                              Maintenance Type
                            </i>
                          <span className={'value'}>
                              {data.maintenanceType == 1 ? <span>Preventive</span> : <span>Corrective</span>}
                            </span>
                        </div>
                      </div>
                    </div>

                    {/*Task Type and Info*/}

                    <div className="description-section">
                      <FontAwesomeIcon icon={faTasks} size={'2x'} className={'icon'}/>
                      <div className="info">
                          <i className={'label'}>
                            Task Type
                          </i>
                        <span className={'value'}>
                            {
                              data.taskType == 1 ?
                                <span>Scheduled Type</span>
                                :
                              data.taskType == 2 ?
                                <span>Unscheduled Type</span>
                                :
                              data.taskType == 3 ?
                                <span>Periodic Type</span>
                                :
                              data.taskType == 4 ?
                                <span>Incident Type</span>
                                :
                              null
                            }
                          </span>
                      </div>
                    </div>


                    {/*<span className={'task-type'}>*/}
                      {/*{*/}
                        {/*data.taskType == 1 ?*/}
                          {/*<i>Scheduled Type</i>*/}
                          {/*:*/}
                        {/*data.taskType == 2 ?*/}
                          {/*<i>Unscheduled Type</i>*/}
                          {/*:*/}
                        {/*data.taskType == 3 ?*/}
                          {/*<i>Periodic Type</i>*/}
                          {/*:*/}
                        {/*data.taskType == 4 ?*/}
                          {/*<i>Incident Type</i>*/}
                          {/*:*/}
                        {/*null*/}
                      {/*}*/}
                    {/*</span>*/}

                    {
                      data.taskType == 1 ?
                        <Paper className="scheduled">
                          <div className="date-section">
                            <FontAwesomeIcon icon={faCalendar} size={'2x'} className={'icon'}/>
                            <div className="start-date info">
                              <i className={'label'}>
                                Start Date Expected
                              </i>
                              <span className={'value'}>
                              {
                                dateConverter(data != null && data.startDateExpected != null ? data.startDateExpected as string : '')
                              }
                              </span>
                            </div>
                            <div className="end-date info">
                              <i className={'label'}>
                                End Date Expected
                              </i>
                              <span className={'value'}>
                              {
                                dateConverter(data != null && data.endDateExpected != null ? data.endDateExpected as string : '')
                              }
                              </span>
                            </div>
                          </div>
                        </Paper>
                        :
                      data.taskType == 3 ?
                        <Paper className="periodic">
                          <div className="date-section">
                            <FontAwesomeIcon icon={faCalendar} size={'2x'} className={'icon'}/>
                            <div className="start-date info">
                              <span className={'label'}>
                                Start Date Expected
                              </span>
                              <span className={'value'}>
                              {
                                dateConverter(data != null && data.startDateExpected != null ? data.startDateExpected as string : '')
                              }
                              </span>
                            </div>
                            <div className="end-date info">
                              <span className={'label'}>
                                End Date Expected
                              </span>
                              <span className={'value'}>
                              {
                                dateConverter(data != null && data.endDateExpected != null ? data.endDateExpected as string : '')
                              }
                              </span>
                            </div>

                            <div className="interval info">
                              <span className={'label'}>
                                Interval
                              </span>
                              <span className={'value'}>
                              {
                                data != null && data.intervalDays != null ? data.intervalDays as number : ''
                              }
                              </span>
                            </div>
                          </div>
                        </Paper>
                        :
                      data.taskType == 4 ?
                        <Paper className="incident">
                          <div className="description-section">
                            {/*<FontAwesomeIcon icon={faTasks} size={'2x'} className={'icon'}/>*/}
                            <div className="info">
                              <i className={'label'}>
                                Incident Name
                              </i>
                                  <span className={'value'}>
                                {
                                  data.incidentInfo.incident
                                }
                              </span>
                            </div>
                          </div>
                          <div className="description-section">
                            {/*<FontAwesomeIcon icon={faTasks} size={'2x'} className={'icon'}/>*/}
                            <div className="info">
                              <i className={'label'}>
                                Incident Description
                              </i>
                                  <span className={'value'}>
                                {
                                  data.incidentInfo.description
                                }
                              </span>
                            </div>
                          </div>
                          <div className="description-section">
                            {/*<FontAwesomeIcon icon={faTasks} size={'2x'} className={'icon'}/>*/}
                            <div className="info">
                              <i className={'label'}>
                                Reported By
                              </i>
                                  <span className={'value'}>
                                {
                                  data.incidentInfo.reportedBy
                                }
                              </span>
                            </div>
                          </div>
                          <div className="description-section">
                            {/*<FontAwesomeIcon icon={faTasks} size={'2x'} className={'icon'}/>*/}
                            <div className="info">
                              <i className={'label'}>
                                Incident Images
                              </i>
                              <span className={'value'}>
                                {
                                  this.state.incidentImages.length > 0 &&
                                  this.imageDisplay(this.state.incidentImages[this.state.imageIndex])
                                }

                              </span>
                            </div>
                          </div>
                        </Paper>
                        :
                      null
                    }

                    {
                      data.taskAssignee.assigneeModel != null &&
                      <div className="description-section">
                        {
                          data.taskAssignee.assigneeModel != null &&
                            data.taskAssignee.assigneeType == 1 ?
                          this.employeeSuggestions.map(( value: {name: string; value: string}) => {
                            //@ts-ignore
                            if(data.taskAssignee.assigneeModel.assigneeId == value.value) {
                              return (
                                <FontAwesomeIcon icon={faUser} size={'2x'} className={'icon'} />
                              )
                            }
                            else
                              return null
                          })
                            :
                          data.taskAssignee.assigneeType == 2 &&
                          <FontAwesomeIcon icon={faUsers} size={'2x'} className={'icon'} />
                        }
                        <div className="info-assignee">
                          <i className={'label'}>
                            Task Assignee
                          </i>
                          <span className={'value'}>
                            {
                              data.taskAssignee.assigneeModel != null &&
                              data.taskAssignee.assigneeType == 1 ?
                              this.employeeSuggestions.map(( value: {name: string; value: string}) => {
                                //@ts-ignore
                                if(data.taskAssignee.assigneeModel.assigneeId == value.value) {
                                  return value.name
                                }
                                else
                                  return null
                              })
                                :
                              this.getTeamInfo()
                            }
                          </span>
                        </div>
                      </div>
                    }

                    {
                    data.warrantyDocument.filename != "" &&
                      <div className="date-section">
                        <FontAwesomeIcon icon={faFile} size={'2x'} className={'icon'}/>
                        <div className="info-warranty">
                          <i className={'label'}>
                            Warranty Document
                          </i>
                          <span className={'value'}>
                            {
                                data.warrantyDocument.id != null &&
                                <div>
                                  {data.warrantyDocument.filename}
                                  <Button
                                    style={{
                                      backgroundColor:Color.HAPPY,
                                      color:Color.PRIMARY_FOREGROUND,
                                      borderRadius:'2px',
                                      float: 'right',
                                    }}
                                    onClick={this.handleWarDownload(data.warrantyDocument.id)}
                                  >
                                    Download
                                  </Button>
                                  <a id={data.warrantyDocument.id} title={data.warrantyDocument.filename} hidden={true}/>
                                </div>
                              }
                          </span>
                        </div>
                      </div>
                    }
                    {
                      data.assetId != null &&
                      <div className="asset-info">
                        <span className={'label title'}>Asset Information</span>
                        {
                         this.props.asset.map((value: AssetResponse) => {
                           if(data.assetId == value.id) {
                             return (
                               <Paper className={'mini-map'}>
                                 <MiniMap asset={value}/>
                               </Paper>
                             )
                           }
                           else
                             return null
                         })
                        }
                        <Button
                          onClick={() => {
                              // this.props.history.push(`/asset-detail/${this.props.asset[0].wfid}/${this.props.asset[0].id}`)
                              this.props.history.push(`/asset-detail/${this.props.asset[0].id}`)
                            }
                          }
                          className={'more-asset'}
                          variant={'contained'}
                          color={'primary'}
                        >
                          More Details
                        </Button>
                      </div>
                    }

                    {
                      data.dependUpon.length != 0 &&
                      <div className="description-info">
                        <div style={{width: '100% !important', maxWidth: '100% !important'}}>
                          <ExpansionPanel>
                            <ExpansionPanelSummary expandIcon={<FontAwesomeIcon icon={faAngleDown} size={'sm'}/>}>
                              <span style={{fontSize: '20px', fontWeight: 'lighter'}}>Task Dependencies</span>
                            </ExpansionPanelSummary>
                            <ExpansionPanelDetails>
                              <List>
                              {
                                this.state.dependencyList.map(this.getDependency)
                              }
                              </List>
                            </ExpansionPanelDetails>
                          </ExpansionPanel>
                        </div>
                      </div>
                    }
                    {/*<NavLink to={''}>*/}
                      {/*<Button variant={'outlined'} fullWidth style={{ margin: '20px' }}>*/}
                        {/*EDIT TASK*/}
                      {/*</Button>*/}
                    {/*</NavLink>*/}
                    {/*TODO get the editTask component and pass data*/}
                  </div>
                </Paper>
              </this.TabContainer>
              )
            }
            {
              this.state.value === 1 && (
                <NoteContainer taskId ={data.id as string}/>
              )
            }
          </div>
        </div>
      </div>
        :
        <Redirect to={'/'}/>
    )
  }
}

const mapStateToProps = (state: any) => ({
  notes: state.notes,
  tasks: state.tasks.tasks,
  employees: state.employee.employees,
  asset: state.asset.assets,
  userInfo: state.login.userInfo,
  team: state.team.teams
})

export default connect(mapStateToProps,
  {
    loadTaskNotes,
    viewAllTasks,
    loadAssets,
    approveTaskState,
    rejectTaskState,
    rejectTaskRegistration,
    approveTask,
    getTeamById
  })(ViewTask)
