import React, { Component } from 'react'
import { connect } from 'react-redux';
import { Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions, TextField, Button } from '@material-ui/core';
import { WFModalState } from '../../../../_infrastructure/state/workflow-note-modal-state';
import { submitNoteFromModal, cancelNoteModal, WFNoteActions } from '../../../../_setup/actions/wfnote-modal-actions';
import Color from '../../_helper/color-container/index';
import { WorkflowTypes, WFRequestModel } from '../../../../_infrastructure/model/workflow-model';
import { rejectTeamRegistration } from '../../../../_setup/actions/teamActions';
import { rejectAssetRegistration } from '../../../../_setup/actions/assetActions';
import { rejectTaskRegistration, rejectTaskState, terminateTask, completeTask, cancelTask } from '../../../../_setup/actions/taskActions';

interface IState{
  note : string
}

interface IProps{

}

interface PropsFromDispatch{
  submitNoteFromModal : (note:string)=>void
  cancelNoteModal : () => void
  
  rejectTeamRegistration : (req:WFRequestModel)=> void
  rejectAssetRegistration : (req:WFRequestModel)=> void
  
  
  rejectTaskRegistration: (request: WFRequestModel) => void
  rejectTaskState:(request:WFRequestModel) => void

  terminateTask: (request: WFRequestModel) => void
  completeTask: (request: WFRequestModel) => void
  cancelTask: (request: WFRequestModel) => void,
}

interface PropsFromState{
  noteModal : WFModalState
}

type AllProps = PropsFromDispatch &
                PropsFromState &
                IProps
class WorkflowNoteModal extends Component<AllProps,IState> {
  constructor(props:AllProps){
    super(props)
    this.state={note:''}

    this.handleNoteChange = this.handleNoteChange.bind(this)
    this.handleNoteCancel = this.handleNoteCancel.bind(this)
    this.handleNoteSubmit = this.handleNoteSubmit.bind(this)
    this.handleTaskOperations = this.handleTaskOperations.bind(this)
  }

  handleNoteChange(event:any){
    let {note} = this.state
    this.setState({note:event.target.value})
  }

  handleNoteCancel(){
    this.props.cancelNoteModal()
  }

  handleNoteSubmit(){
    let {note} = this.state
    let { noteModal } = this.props
    if(noteModal.wfid != undefined){
      let reqModel : WFRequestModel = {
        wfid:noteModal.wfid,
        note:note
      }
      switch( noteModal.WFType as WorkflowTypes)
      {
       case WorkflowTypes.Team:
          this.props.rejectTeamRegistration(reqModel)
          break;
       case WorkflowTypes.Task:
          this.handleTaskOperations(reqModel,noteModal.modalAction)
          break;
       case WorkflowTypes.Asset:
          this.props.rejectAssetRegistration(reqModel);
          break;
      }
    }
    this.props.submitNoteFromModal(note)
  }

  handleTaskOperations(wfReq:WFRequestModel , action : WFNoteActions){
    switch(action){
      case WFNoteActions.REJECTION:
        //FIXME: change wfid to WFRequestModel
        this.props.rejectTaskRegistration(wfReq)
        break;

      case WFNoteActions.REJECT_TASK_STATE:
        //FIXME: change wfid to WFRequestModel
        this.props.rejectTaskState(wfReq)
        break;

      case WFNoteActions.TASK_CANCELLATION:
        //FIXME: change wfid to WFRequestModel
        this.props.cancelTask(wfReq)
        break;
      case WFNoteActions.TASK_COMPLETION:
        //FIXME: change wfid to WFRequestModel
        this.props.completeTask(wfReq)
        break;
      case WFNoteActions.TASK_TERMINATION:
        //FIXME: change wfid to WFRequestModel
        this.props.terminateTask(wfReq)
        break;
    }
  }

  render() {
    let {open} = this.props.noteModal
    let {note} = this.state


    //FIXME: Remove console log from here ...
    console.group('======= FROM NOTE MODAL =========')
    console.log(this.props.noteModal)
    console.groupEnd()

    return (
      <Dialog
          open={open}
          aria-labelledby="form-dialog-title"
          maxWidth={'md'}
          fullWidth={true}
        >
          <DialogTitle id="form-dialog-title">Note</DialogTitle>
          <DialogContent>
            <DialogContentText>
              Please type your note for your action here.
            </DialogContentText>
            <TextField
              autoFocus
              margin="dense"
              multiline={true}
              value={note}
              onChange={this.handleNoteChange}
              rows={3}
              id="name"
              label="Note"
              type={'text'}
              fullWidth
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleNoteCancel} variant={'contained'} color="secondary">
              Cancel
            </Button>
            <Button onClick={this.handleNoteSubmit} variant={'contained'} style={{backgroundColor:Color.HAPPY}}>
              Submit
            </Button>
          </DialogActions>
        </Dialog>
    )
  }
}

function mapStateToProps(state:any){
  return {
    noteModal : state.noteModal
  }
}
function mapDispatchToProps(dispatch:any){
  return {
    submitNoteFromModal : (note:string)=>dispatch(submitNoteFromModal(note)),
    cancelNoteModal : () => dispatch(cancelNoteModal()),

    rejectTeamRegistration : (req:WFRequestModel)=>dispatch(rejectTeamRegistration(req)),
    rejectAssetRegistration : (req:WFRequestModel)=>dispatch(rejectAssetRegistration(req)),
    
    
    rejectTaskRegistration: (req: WFRequestModel) => dispatch(rejectTaskRegistration(req)),
    rejectTaskState:(req:WFRequestModel) => dispatch(rejectTaskState(req)),

    terminateTask: (req: WFRequestModel) => dispatch(terminateTask(req)),
    completeTask: (req: WFRequestModel) => dispatch(completeTask(req)),
    cancelTask: (req: WFRequestModel) => dispatch(cancelTask(req))
    
  }
}
export default connect(mapStateToProps,mapDispatchToProps)(WorkflowNoteModal)