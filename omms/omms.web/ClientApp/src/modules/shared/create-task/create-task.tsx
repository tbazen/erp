import React, { Component } from 'react'
import './create-task.scss'
import TaskForm from './task-form/task-form'
import { connect } from 'react-redux'
import { createTask, updateTask } from '../../../_setup/actions/taskActions'
import { Paper, Snackbar } from '@material-ui/core'
import {
  Assignee,
  TaskModel
} from '../../../_infrastructure/model/taskFromModel'
import { TaskRequest } from '../../../_infrastructure/model/task-model'
import { SnackbarContent } from '@material-ui/core/es';

interface ICreateTask {
  data: TaskRequest,
  taskTypeChecked: number,
  open: boolean
}

class CreateTask extends Component<any, ICreateTask> {
  state: ICreateTask = {
    data: {
      id: null,
      wfid: null,
      assetId: this.props.assetId == undefined ? null : this.props.assetId,
      name: '',
      description: '',
      createdBy: null,
      taskType: 0,
      maintenanceType: 0,
      priority: 0,
      startDateExpected: '',
      endDateExpected: '',
      note:'',
      startDateActual: null,
      endDateActual: null,
      incidentInfo: {
        Incident: '',
        ReportedBy: '',
        Description: '',
        GeoLocation: '',
        Images: []
      },
      intervalDays: 0,
      taskAssignee: {
        assigneeType: 0,
        assigneeModel: {
          taskId: '',
          assigneeId: '',
          dateAssigned: null
        }
      },
      warrantyDocument: {
        Date: 0,
        Mimetype: '',
        Type: null,
        Note: '',
        Ref: '',
        Id: null,
        OverrideFilePath: '',
        Filename: '',
        File: ''
      },
      dependUpon: []
    },
    taskTypeChecked: 0,
    open: false
  }

  constructor(props: any) {
    super(props)

    this.submitHandler = this.submitHandler.bind(this)
    this.updateInputValue = this.updateInputValue.bind(this)
    this.handleUnscheduled = this.handleUnscheduled.bind(this)
    this.closeSnackbar = this.closeSnackbar.bind(this)
  }


  async submitHandler() {
    //TODO do frontend validation here, check this.state.data for null or empty values and display a snackbar
    console.log(this.state.data)
    let data = this.state.data
    if(data.name != '' && data.description != '' && data.maintenanceType != 0 && data.priority != 0)
      this.props.createTask(this.state.data)
    else
      this.setState({
        open: true
      })
  }

  closeSnackbar() {
    console.log('closing snackbar')
    this.setState({
      open: false
    })
  }

  updateInputValue(e: any) {
    let { data } = this.state
    data[e.target.name] = e.target.value
    this.setState({
      data: data
    })
  }

  handleUnscheduled(event: any) {
    this.state.data['taskTypeChecked'] = event.target.value
    this.setState({
      taskTypeChecked: event.target.value
    })
  }

  render() {
    //TODO consider a material mobile stepper for the two pages
    let style = this.props.assetId == null || this.props.assetId ==undefined
    ?
    {
       margin: ' 40px 7%', padding: '40px' 
    }
    :
    {
      maxHeight:"90vh",overflow:"auto", margin: ' 40px 7%', padding: '40px' 
    }
    return (
      <Paper style={style}>
        <TaskForm
          onSubmit={this.submitHandler}
          taskModel={this.state.data}
          updateInputValue={this.updateInputValue}
          handleUnscheduled={this.handleUnscheduled}
        />

        <Snackbar
          open={this.state.open}
          onClose={this.closeSnackbar}
          className={'snack-error'}
          message={'Required * fields not complete! Please fill in the required fields'}
          anchorOrigin={{horizontal: 'center', vertical: 'top'}}
        >
        </Snackbar>
      </Paper>
    )
  }
}

const mapStateToProps = (state: any) => ({
  response: state.tasks
})

export default connect(
  mapStateToProps,
  { createTask, updateTask }
)(CreateTask)
