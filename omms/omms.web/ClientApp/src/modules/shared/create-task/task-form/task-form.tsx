import React, { Component } from 'react'
import './task-form.scss'
import {
  Collapse, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle,
  FormControlLabel,
  FormLabel,
  Grow,
  InputLabel,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  MenuItem,
  Radio,
  RadioGroup,
  Select,
  TextField
} from '@material-ui/core'
import Button from '@material-ui/core/Button/Button'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFileUpload, faPlus, faTrash } from '@fortawesome/free-solid-svg-icons'
import { Link } from 'react-router-dom'
import { getBase64 } from '../../../_helper/file-encode'
import TaskListForm from '../../../shared/components/task-card/task-list-form'
import { connect } from 'react-redux'
import { loadAllTeams } from '../../../../_setup/actions/teamActions'
import { loadAllEmployees } from '../../../../_setup/actions/employeeActions'
import { TaskRequest, TaskStatus } from '../../../../_infrastructure/model/task-model'
import { IUserInformation } from '../../../../_infrastructure/model/userInformationModel'
import { TeamModel } from '../../../../_infrastructure/model/teamModel'
import { Assignee, TaskModel } from '../../../../_infrastructure/model/taskFromModel'
import Grid from '@material-ui/core/Grid/Grid'
import { DocumentRequest } from '../../../../_infrastructure/model/documentModel'
import Color from '../../_helper/color-container'
import { getTaskByStatus } from '../../../../_setup/actions/taskActions'



interface IProps{
  onSubmit:()=>void,
  updateInputValue:(event:any) => void,
  handleUnscheduled:(event:any) => void,
  taskModel: TaskRequest,
}

interface PropsFromDispatch{
  loadAllTeams: ()=> void,
  loadAllEmployees: ()=> void,
  getTaskByStatus: (status: number)=> void,
}

interface PropsFromState {
  teamList: TeamModel[],
  employees: IUserInformation[],
  taskByStatus: TaskModel[]
}

type AllProps = IProps &
  PropsFromDispatch &
  PropsFromState

class TaskForm extends Component<AllProps, any> {
  state = {
    incident: '',
    incidentDescription: '',
    reportedBy: '',
    taskTypeChecked: 0,
    file: {
      Date: 0,
      Ref: '',
      Note: '',
      Mimetype: '',
      Type: null,
      Filename: '',
      File: '',
      Id: null,
      OverrideFilePath: ''
    },
    equipment: '',
    equipmentQuantity: '',
    dialog: {
      opened: false,
      type: ''
    },
    team: '',
    employee: '',
    taskAssignee: '',
    equipmentDialog: false,
    fix: false,
    openDependencyForm: false
  }

  teamSuggestions: { name: string; value: string }[] = []

  employeeSuggestions: { name: string; value: string }[] = []

  constructor(props: any) {
    super(props)

    this.updateInputValue = this.updateInputValue.bind(this)
    this.fileInputChangeHandler = this.fileInputChangeHandler.bind(this)
    this.fileInputHandler = this.fileInputHandler.bind(this)
    this.handleCloseDependencyDialog = this.handleCloseDependencyDialog.bind(this)
    this.handleDependencyDialog = this.handleDependencyDialog.bind(this)
    this.openDependencyForm = this.openDependencyForm.bind(this)
    this.handleClose = this.handleClose.bind(this)
    this.handleDone = this.handleDone.bind(this)
    this.handleClickOpen = this.handleClickOpen.bind(this)
    this.openDependencyForm = this.openDependencyForm.bind(this)
    this.handleFileChange = this.handleFileChange.bind(this)
    this.addFile = this.addFile.bind(this)
    this.removeFile = this.removeFile.bind(this)
    this.fileRow = this.fileRow.bind(this)
  }

  handleClickOpen = (event: any) => {
    if (event.target.value == 2)
      this.setState({ dialog: { opened: true, type: 'Team' } })
    else if (event.target.value == 1)
      this.setState({ dialog: { opened: true, type: 'Employee' } })
  }

  openDependencyForm() {
    this.setState({
      openDependencyForm: true
    })
  }

  handleDone() {
    let temp: {
      assigneeType: number
      assigneeModel: {
        TaskId?: string
        AssigneeId?: string
        DateAssigned?: string
        ExpectedFinishDate?: string
        ActualFinishDate?: null
        CostId?: 0
      }
    } = {
      assigneeType: -1,
      assigneeModel: {
        TaskId: '',
        AssigneeId: '',
        DateAssigned: '',
        ExpectedFinishDate: '',
        ActualFinishDate: null,
        CostId: 0
      }
    }

    if (this.state.taskAssignee == '1')
      temp = {
        assigneeType: parseInt(this.state.taskAssignee),
        assigneeModel: { AssigneeId: this.state.employee }
      }
    else if (this.state.taskAssignee == '2')
      temp = {
        assigneeType: parseInt(this.state.taskAssignee),
        assigneeModel: { AssigneeId: this.state.team }
      }

    temp = {
      assigneeType: temp.assigneeType,
      assigneeModel: {
        TaskId: '',
        AssigneeId: temp.assigneeModel.AssigneeId,
        DateAssigned: '',
        ExpectedFinishDate: '',
        ActualFinishDate: null,
        CostId: 0
      }
    }

    this.props.taskModel.taskAssignee = temp as Assignee
    this.handleClose()
  }

  handleClose = () => {
    this.setState({ dialog: { opened: false, taskAssignee: '' } })
    setTimeout(() => {
      this.setState({ dialog: { opened: false, taskAssignee: '' } })
    }, 100)
  }

  handleCloseDependencyDialog() {
    this.setState({
      openDependencyForm: false
    })
  }

  handleDependencyDialog(dependencies: string[]) {
    this.props.taskModel.dependUpon = dependencies
  }

  fileInputHandler() {
    let file = document.getElementById('messageFileInput')
    if (file != null) file.click()
  }

  async fileInputChangeHandler() {
    let file = (document.getElementById('messageFileInput') as HTMLInputElement)
      .files

    if (file != null) {
      let base64 = await getBase64(file[0] as Blob)
      this.setState({
        file: {
          Date: file[0].lastModified,
          Ref: '',
          Note: '',
          Mimetype: file[0].type,
          Type: null,
          Filename: file[0].name,
          File: base64,
          // only for viewing documents from a custom url (esp. in work items)
          Id: null,
          OverrideFilePath: 'api/documents/'
        }
      })

      this.props.taskModel.warrantyDocument = this.state.file
    }
  }

  updateInputValue(e: any) {
    this.setState({
      [e.target.name]: e.target.value
    })
  }

  taskTypeChecked(value: number) {
    this.setState({
      taskTypeChecked: value
    })
  }

  async componentWillMount() {
    await this.props.loadAllTeams()
    this.props.teamList.map((value: any) => {
      this.teamSuggestions.push({ name: value.name, value: value.id })
    })

    await this.props.loadAllEmployees()
    this.props.employees.map((value: any) => {
      this.employeeSuggestions.push({ name: value.userName, value: value.id })
    })

    await this.props.getTaskByStatus(TaskStatus.ONGOING)

    console.log(this.props.taskByStatus)
  }

  async handleFileChange() {
    let file = (document.getElementById('incidentImageInput') as HTMLInputElement)
      .files
    if (file != null) {
      let base64 = await getBase64(file[0] as Blob)
      let doc: DocumentRequest = {
        Date: file[0].lastModified,
        Ref: '',
        Note: '',
        Mimetype: file[0].type,
        Type: null,
        Filename: file[0].name,
        File: base64,
        Id: null,
        OverrideFilePath: 'api/documents/'
      }

      this.addFile(doc)
      file = null
    }
  }

  addFile(doc: DocumentRequest) {
    this.props.taskModel.incidentInfo.Images.push(doc)
    console.log(this.props.taskModel.incidentInfo)
    // this.setState({ this.props.taskModel })
  }

  removeFile = (index: number) => () => {
    this.props.taskModel.incidentInfo.Images.splice(index, 1)
    // this.setState({ this.props.taskModel  })
  }

  fileRow(value: DocumentRequest, key: number) {
    console.log('value')
    return (
      <Grid
        style={{
          margin: '5px'
        }}
        container
        key={key}
        justify="center"
      >
        <Grid item xs={9}>
          <p>{value.Filename}</p>
        </Grid>
        <Grid item xs={2}>
          <Button
            variant="contained"
            color="secondary"
            onClick={this.removeFile(key)}
          >
            <FontAwesomeIcon icon={faTrash} size={'2x'} />
          </Button>
        </Grid>
      </Grid>
    )
  }

  render() {
    return (
      <div style={{padding: '15px'}}>
        <span
          className={'title block'}
          style={{ fontSize: '25px', textAlign: 'left' }}
        >
          Add New Task
          <hr/>
        </span>
        <div className={'first-page'}>

          {/*Name*/}
          <div className="block-margin">
            <TextField
              name={'name'}
              //value={name || ''}
              onChange={this.props.updateInputValue}
              placeholder="* Task Name"
              className={'text-field'}
              fullWidth
              variant={'outlined'}
            />
          </div>

          {/*Asset Id*/}
          {/*<div className="block-margin">*/}
          {/*<TextField*/}
          {/*name={'assetId'}*/}
          {/*type={'number'}*/}
          {/*value={this.props.taskModel.assetId || 0}*/}
          {/*onChange={this.props.updateInputValue}*/}
          {/*placeholder="Asset Id"*/}
          {/*className={'text-field'}*/}
          {/*fullWidth*/}
          {/*variant={'outlined'}*/}
          {/*/>*/}
          {/*</div>*/}

          {/*Description*/}
          <div className="block-margin">
            <TextField
              name={'description'}
              placeholder={'* Task Description'}
              multiline={true}
              rows={5}
              rowsMax={10}
              // value={description || ''}
              onChange={this.props.updateInputValue}
              className={'modal-content text-field'}
              fullWidth
              variant={'outlined'}
            />
          </div>

          {/*Task Type*/}
          <div className="block-margin">
            <FormLabel className={'block'}>* Task Type</FormLabel>
            <RadioGroup
              aria-label={'Task Type'}
              className={'task-type'}
              // value={taskType || ''}
              onChange={this.props.updateInputValue}
              name={'taskType'}
            >
              <FormControlLabel
                value="1"
                control={<Radio />}
                label="Scheduled"
                labelPlacement="start"
                onChange={(event: any) => {
                  this.props.handleUnscheduled(event)
                  this.taskTypeChecked(1)
                }}
                style={{paddingRight: '25px'}}
              />
              <FormControlLabel
                value="2"
                control={<Radio />}
                label="Unscheduled"
                labelPlacement="start"
                onChange={(event: any) => {
                  this.props.handleUnscheduled(event)
                  this.taskTypeChecked(2)
                }}
                style={{paddingRight: '25px'}}
              />
              <FormControlLabel
                value="3"
                control={<Radio />}
                label="Periodic"
                labelPlacement="start"
                onChange={(event: any) => {
                  this.props.handleUnscheduled(event)
                  this.taskTypeChecked(3)
                }}
                style={{paddingRight: '25px'}}
              />
              <FormControlLabel
                value="4"
                control={<Radio />}
                label="Incident"
                labelPlacement="start"
                onChange={(event: any) => {
                  this.props.handleUnscheduled(event)
                  this.taskTypeChecked(4)
                }}
                style={{paddingRight: '25px'}}
              />
            </RadioGroup>
            <Collapse in={this.state.taskTypeChecked == 1}>
              <Grow in={this.state.taskTypeChecked == 1}>
                <div>
                  <TextField
                    name={'startDateExpected'}
                    type={'date'}
                    label={'Start Date Expected'}
                    InputLabelProps={{
                      shrink: true
                    }}
                    // value={startDateExpected || ''}
                    onChange={this.props.updateInputValue}
                    className={'text-field'}
                  />
                  <TextField
                    name={'endDateExpected'}
                    type={'date'}
                    label={'End Date Expected'}
                    InputLabelProps={{
                      shrink: true
                    }}
                    onChange={this.props.updateInputValue}
                    className={'text-field'}
                  />
                </div>
              </Grow>
            </Collapse>
            <Collapse in={this.state.taskTypeChecked == 3}>
              <Grow in={this.state.taskTypeChecked == 3}>
                <div>
                  <TextField
                    name={'startDateExpected'}
                    type={'date'}
                    label={'Start Date Expected'}
                    InputLabelProps={{
                      shrink: true
                    }}
                    onChange={this.props.updateInputValue}
                    className={'text-field'}
                  />
                  <TextField
                    name={'endDateExpected'}
                    type={'date'}
                    label={'End Date Expected'}
                    InputLabelProps={{
                      shrink: true
                    }}
                    onChange={this.props.updateInputValue}
                    className={'text-field'}
                  />
                  <TextField
                    name={'intervalDays'}
                    type={'number'}
                    label={'Interval (in days)'}
                    onChange={this.props.updateInputValue}
                    className={'text-field'}
                  />
                </div>
              </Grow>
            </Collapse>
            <Collapse in={this.state.taskTypeChecked == 4}>
              <Grow in={this.state.taskTypeChecked == 4}>
                <div>
                  <TextField
                    name={'incident'}
                    type={'string'}
                    label={'Incident'}
                    value={this.state.incident || ''}
                    onChange={(event: any) => {
                      this.updateInputValue(event)
                      this.props.taskModel.incidentInfo.Incident = event.target.value
                    }}
                    className={'text-field'}
                  />
                  <TextField
                    name={'incidentDescription'}
                    type={'string'}
                    label={'Incident Description'}
                    multiline={true}
                    rows={5}
                    rowsMax={10}
                    value={this.state.incidentDescription || ''}
                    onChange={(event: any) => {
                      this.updateInputValue(event)
                      this.props.taskModel.incidentInfo.Description = event.target.value
                    }}
                    className={'text-field'}
                  />
                  <TextField
                    name={'reportedBy'}
                    type={'string'}
                    label={'Incident Reported By'}
                    value={this.state.reportedBy || ''}
                    onChange={(event: any) => {
                      this.updateInputValue(event)
                      this.props.taskModel.incidentInfo.ReportedBy = event.target.value
                    }}
                    className={'text-field'}
                  />
                  <div className={'file-input'}>
                    {/*<input*/}
                    {/*type="file"*/}
                    {/*id="messageFileInput"*/}
                    {/*onChange={this.fileInputChangeHandler}*/}
                    {/*hidden*/}
                    {/*/>*/}
                    {/*<div className="fileInput" onClick={this.fileInputHandler}>*/}
                    {/*<FontAwesomeIcon*/}
                    {/*icon={faFileUpload}*/}
                    {/*className="fileIcon"*/}
                    {/*size="2x"*/}
                    {/*/>*/}

                    {/**/}
                    {/*</div>*/}
                    <FormLabel className={'block'}>Incident Images</FormLabel>
                    {
                      this.props.taskModel.incidentInfo.Images.map(this.fileRow)
                    }
                    <input
                      id="incidentImageInput"
                      type="file"
                      onChange={() => {
                        this.setState({
                          fix: false
                        })
                        this.handleFileChange()
                      }}
                    />
                  </div>
                </div>
              </Grow>
            </Collapse>
          </div>

          {/*Maintenance Type*/}
          <div className="block-margin">
            <FormLabel className={'block'}>* Maintenance Type</FormLabel>
            <RadioGroup
              aria-label={'* Maintenance Type'}
              className={'maintenance-type'}
              // value={maintenanceType || ''}
              onChange={this.props.updateInputValue}
              name={'maintenanceType'}
            >
              <FormControlLabel
                value="1"
                control={<Radio />}
                label="Preventive"
                labelPlacement="start"
                style={{paddingRight: '25px'}}
              />
              <FormControlLabel
                value="2"
                control={<Radio />}
                label="Corrective"
                labelPlacement="start"
                style={{paddingRight: '25px'}}
              />
            </RadioGroup>
          </div>

          {/*Priority*/}
          <div className={'priority-dropdown block-margin'}>
            <InputLabel>* Priority</InputLabel>
            <Select
              value={this.props.taskModel.priority}
              onChange={this.props.updateInputValue}
              inputProps={{
                name: 'priority'
              }}
              onClick={() => {
                this.setState({
                  fix: !this.state.fix
                })
              }}
              fullWidth
            >
              <MenuItem value={0}>
                <em>None</em>
              </MenuItem>
              <MenuItem value={1}>Very High</MenuItem>
              <MenuItem value={2}>High</MenuItem>
              <MenuItem value={3}>Medium</MenuItem>
              <MenuItem value={4}>Low</MenuItem>
              <MenuItem value={5}>Very Low</MenuItem>
            </Select>
          </div>


          {/*Task Assignee*/}
          <div className="block-margin">
            <FormLabel className={'block'}>* Task Assignee</FormLabel>
            <RadioGroup
              className={'task-assignee'}
              value={this.state.taskAssignee}
              onChange={this.updateInputValue}
              onClick={() => {
                this.state.fix = !this.state.fix
                //@ts-ignore
                // this.state.taskAssignee = taskAssignee
              }}
              name={'taskAssignee'}
            >
              <FormControlLabel
                value="2"
                control={<Radio />}
                label="Team"
                labelPlacement="start"
                onChange={this.handleClickOpen}
                style={{paddingRight: '25px'}}
              />
              <FormControlLabel
                value="1"
                control={<Radio />}
                label="Employee"
                labelPlacement="start"
                onChange={this.handleClickOpen}
                style={{paddingRight: '25px'}}
              />
            </RadioGroup>

            {/*Warranty Document*/}
            <FormLabel className={'block'} style={{margin: '20px 0'}}>Warranty Document</FormLabel>
            <div className={'file-input'}>
              <input
                type="file"
                id="messageFileInput"
                onChange={this.fileInputChangeHandler}
              />
              {/*<div className="fileInput" onClick={this.fileInputHandler}>*/}
              {/*<FontAwesomeIcon*/}
              {/*icon={faFileUpload}*/}
              {/*className="fileIcon"*/}
              {/*size="4x"*/}
              {/*/>*/}
              {/*</div>*/}
            </div>
          </div>

          {/*Task Dependency*/}
          <div className="block-margin">
            <FormLabel className={'block'} style={{margin: '20px 0'}}>Task Dependency</FormLabel>
            <Button
              onClick={this.openDependencyForm}
              size={'small'}
              style={{
                backgroundColor:Color.PRIMARY,
                color:Color.PRIMARY_FOREGROUND,
                borderRadius:'2px'
              }}
            >
              Add Task Dependency
            </Button>
          </div>

          <TaskListForm
            openForm={this.state.openDependencyForm}
            setDependency={this.handleDependencyDialog}
            task={this.props.taskModel}
            handleClose={this.handleCloseDependencyDialog}
          />
        </div>

        <Button
          onClick={() => {
            this.props.taskModel.incidentInfo.GeoLocation
            this.props.onSubmit()
          }}
          size={'large'}
          style={{
            backgroundColor:Color.HAPPY,
            color:Color.PRIMARY_FOREGROUND,
            borderRadius:'2px',
            float: 'right',
          }}
        >
          Submit
        </Button>

        {/*Dialog for team and employee selection*/}
        <Dialog open={this.state.dialog.opened} onClose={this.handleClose}>
          <DialogTitle>Select {this.state.dialog.type}</DialogTitle>
          <DialogContent>
            <DialogContentText>
              Search {this.state.dialog.type} below to add to assign to the task
              {this.state.dialog.type == 'Team' ? (
                <Select
                  value={this.state.team}
                  onChange={this.updateInputValue}
                  style={{
                    width: '100%'
                  }}
                  inputProps={{
                    name: 'team'
                  }}
                >
                  {this.teamSuggestions.map(
                    (value: { value: string; name: string }, key: number) => {
                      return (
                        <MenuItem value={value.value} key={key}>
                          {value.name}
                        </MenuItem>
                      )
                    }
                  )}
                </Select>
              ) : (
                <Select
                  value={this.state.employee}
                  onChange={this.updateInputValue}
                  style={{
                    width: '100%'
                  }}
                  inputProps={{
                    name: 'employee'
                  }}
                >
                  {this.employeeSuggestions.map(
                    (value: { value: string; name: string }, key: number) => {
                      return (
                        <MenuItem value={value.value} key={key}>
                          {value.name}
                        </MenuItem>
                      )
                    }
                  )}
                </Select>
              )}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              Cancel
            </Button>
            <Button onClick={this.handleDone} color="primary">
              Done
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    )
  }
}

const mapStateToProps = (state: any) => ({
  teamList: state.team.teams,
  employees: state.employee.employees,
  taskByStatus: state.tasks.tasks
})

export default connect(
  mapStateToProps,
  { loadAllTeams, loadAllEmployees, getTaskByStatus }
)(TaskForm)
