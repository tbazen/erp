import React, { Component } from 'react'
import TaskForm from './task-form/task-form'
import './edit-task.scss'
import { connect } from 'react-redux'
import { createTask, updateTask } from '../../../_setup/actions/taskActions'
import { Dialog, Modal, Paper, Theme } from '@material-ui/core'
import { TaskRequest, TaskResponse } from '../../../_infrastructure/model/task-model'
import { IUserInformation } from '../../../_infrastructure/model/userInformationModel'
import { NoteResponse } from '../../../_infrastructure/model/task-note-model'
import UIContainer from '../_helper/ui-helper/containers-util'

interface IEditState {
  data: TaskResponse,
  edited: TaskRequest,
  taskTypeChecked: number,
  // request: TaskRequest
}


interface IProps {
  data: TaskResponse,
  open: boolean,
  onClose: () => void,
  onOpen: () => void
  container:UIContainer
}

interface PropsFromDispatch {
  createTask: (data: TaskRequest,container:UIContainer) => void
}

interface PropsFromState {

}

// const styles = (theme:Theme)=> ({
//   modalStyle1:{
//     position:'absolute',
//     top:'10%',
//     left:'10%',
//     overflow:'scroll',
//     height:'100%',
//     display:'block'
//   }
// });


type AllProps = IProps & PropsFromDispatch & PropsFromState//TODO fix the all props issue, don't use any #Bazen


class EditTask extends Component<AllProps, IEditState> {
  constructor(props: any) {
    super(props)
    this.submitHandler = this.submitHandler.bind(this)
    this.updateInputValue = this.updateInputValue.bind(this)
    this.handleUnscheduled = this.handleUnscheduled.bind(this)
  }

  async submitHandler(data: any) {
    console.log(data)
    // console.log(this.state.edited)
    this.props.createTask(data,this.props.container)
  }

  updateInputValue(e: any) {
    let { data } = this.state
    data[e.target.name] = e.target.value
    this.setState({
      data: data
    })
  }

  handleUnscheduled(event: any) {
    this.state.data['taskTypeChecked'] = event.target.value
    this.setState({
      taskTypeChecked: event.target.value
    })
  }

  async componentWillMount() {
    this.setState({data: this.props.data})
  }

  render() {
    return (
      <Dialog
        open={this.props.open}
        onClose={this.props.onClose}
        // className={styles.modalStyle1}
        style={{overflow: 'scroll', width: '100%'}}
        className={'edit-dialog'}
      >
        <div style={{ margin: ' 10px auto', padding: '40px', width: '100%' }}>
          <TaskForm
            onSubmit={this.submitHandler}
            taskModel={this.props.data}
            edited={this.state.edited}
            updateInputValue={this.updateInputValue}
            handleUnscheduled={this.handleUnscheduled}
          />
        </div>
      </Dialog>
    )
  }
}

const mapStateToProps = (state: any) => ({
  response: state.tasks,
  tasks: state.tasks.tasks
})

export default connect(
  null,
  { createTask }
)(EditTask)
