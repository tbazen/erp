import React, { Component } from 'react'
import { Card, CardTitle, Form, InputGroup } from 'reactstrap'
import { Button, Input, Snackbar } from '@material-ui/core'
import './login.scss'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faUser } from '@fortawesome/free-solid-svg-icons'
import { faLock } from '@fortawesome/free-solid-svg-icons'
import Main from '../../public/shell/main'
import { loginAction, checkUser } from '../../../_setup/actions/loginActions'

import Radio from '@material-ui/core/Radio'
import RadioGroup from '@material-ui/core/RadioGroup'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import Grid from '@material-ui/core/Grid'
import LoadingPage from '../components/loading/loading-page';
import Color from '../_helper/color-container/index'
import CircularProgress from '@material-ui/core/CircularProgress';
import { Link } from 'react-router-dom';
import Typography from '@material-ui/core/Typography';

export interface IloginViewModel {
  username: string
  password: string
  role: string
}

interface IProps{
  swapComponent : ()=>void
}

type AllProps = IProps 

interface IGalleryState {
  loginViewModel: IloginViewModel
  loggedIn: boolean
  userChecked: boolean,
  snackBar: boolean,
  loading:boolean
}

class Login extends Component<AllProps, IGalleryState> {
  state: IGalleryState = {
    loginViewModel: {
      username: '',
      password: '',
      role: '1'
    },
    loggedIn: false,
    userChecked: false,
    snackBar: false,
    loading:false
  }

  constructor(props: AllProps) {
    super(props)
    this.acceptInput = this.acceptInput.bind(this)
    this.preceedLogIn = this.preceedLogIn.bind(this)
    this.submit = this.submit.bind(this)
    this.promptLogin = this.promptLogin.bind(this)
    this.issue = this.issue.bind(this)
    this.handleClose = this.handleClose.bind(this)
  }

  preceedLogIn() {
    this.setState({
      loggedIn: true,
      userChecked: true,
      loading:false
    })
  }

  issue() {

    this.setState({
      snackBar: true,
      loading:false
    })
  }

  promptLogin() {
    this.setState({
      loggedIn: false,
      userChecked: true
    })
  }

  componentDidMount() {
    checkUser(this.preceedLogIn, this.promptLogin)
  }

  acceptInput(e: any) {
    let { loginViewModel } = this.state
    loginViewModel[e.target.name] = e.target.value
    this.setState({
      loginViewModel
    })
  }

  submit(e: any) {
    if (e != null) e.preventDefault()
    this.setState({loading:true})
    loginAction(this.state.loginViewModel, this.preceedLogIn, this.issue)
  }

  handleClose () {
    this.setState({ snackBar: false });
  }

  login() {
    let {loading} = this.state
    return (
      <div className={'login-container'}>
        <div id={'overlay'} />
          <Card className={'login-card'}>
            <CardTitle className={'login-title'}>
              Operational and Maintenance Management System
            </CardTitle>
            <Form className={'login-form'}>
              <InputGroup className={'input-group'}>
                <FontAwesomeIcon icon={faUser} />
                <Input
                  name="username"
                  value={this.state.loginViewModel.username}
                  onChange={this.acceptInput}
                  placeholder="USERNAME"
                  className={'input username'}
                  id={'username'}
                />
              </InputGroup>
              <InputGroup>
                <FontAwesomeIcon icon={faLock} />
                <Input
                  type={'password'}
                  name="password"
                  value={this.state.loginViewModel.password}
                  onChange={this.acceptInput}
                  placeholder="PASSWORD"
                  className={'input password'}
                  id={'password'}
                />
              </InputGroup>
              <Grid container justify="center">
                <RadioGroup
                  aria-label="Role"
                  name="role"
                  value={this.state.loginViewModel.role}
                  onChange={this.acceptInput}
                  row
                >
                  <FormControlLabel
                    value="1"
                    control={<Radio color="primary" />}
                    label="Employee"
                  />
                  {/* <FormControlLabel
                    value="2"
                    control={<Radio color="primary" />}
                    label="Admin"
                  /> */}
                  <FormControlLabel
                    value="3"
                    control={<Radio color="primary" />}
                    label="Officer"
                  />
                  <FormControlLabel
                    value="4"
                    control={<Radio color="primary" />}
                    label="Supervisor"
                  />
                </RadioGroup>
              </Grid>
              <div style={{width: '100%'}}>
                <Button  
                          style={{
                            width:'80%', 
                            height: '45px',  
                            margin: '20px auto', 
                            display: 'block', 
                            backgroundColor:Color.PRIMARY, 
                            color:Color.PRIMARY_FOREGROUND
                            }} 
                            onClick={this.submit}
                            disabled={loading}
                            >
                          {
                            !loading &&
                            <span>LOGIN</span>
                          }
                          {
                            loading &&
                            <CircularProgress style={{
                                                  color:'#eef3fd',
                                                  animationDuration: '550ms'
                                                }} 
                                              size={30}
                                              thickness={5}
                                              variant="indeterminate"
                                              disableShrink />
                          }
                  </Button>
                  <Button 
                       style={{
                        width:'80%', 
                        height: '45px',  
                        margin: '20px auto', 
                        display: 'block'
                        }} 
                        variant={'outlined'}
                        onClick={this.props.swapComponent}
                  >
                     Open incident map
                   </Button>
              </div>
            </Form>
          </Card>


        <Snackbar
          open={this.state.snackBar}
          onClose={this.handleClose}
          className={'snack-error'}
          // autoHideDuration={3000}
          // TransitionComponent={this.state.Transition}
           ContentProps={{
             'aria-describedby': 'message-id',
           }}
           message={
            <span id="message-id" style={{fontWeight: 'lighter'}}>
              {/*<span onClick={this.handleClose} style={{float: 'right'}}><FontAwesomeIcon icon={faWindowClose}/></span><br/>*/}
              Incorrect Credentials. Re-enter your login information
            </span>
         }/>
      </div>
    )
  }

  public render() {
    if (this.state.userChecked && this.state.loggedIn != undefined) {
      return this.state.loggedIn ? (
        <div>
          <Main/>
        </div>
      ) : (
        <div>{this.login()}</div>
      )
    } else {
      return <LoadingPage/>
    }
  }
}

export default Login
