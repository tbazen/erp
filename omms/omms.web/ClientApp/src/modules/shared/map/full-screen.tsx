import React, { Component } from 'react'
import * as ol from 'openlayers'
import './map.scss'
import './ol.css'
import { AssetResponse } from '../../../_infrastructure/model/assetModel'
import { connect } from 'react-redux'
import { getAssetByFid, loadAssets } from '../../../_setup/actions/assetActions'
import Role from '../_helper/role-util/roles';
import { EmployeeItems } from '../_helper/slideout-index/employee-items';
import { OfficerItems } from '../_helper/slideout-index/officer-items';
import { SupervisorItems } from '../_helper/slideout-index/supervisor-items';
import { changeActiveIndex } from '../../../_setup/actions/drawer-actions';
import { viewAssetTasks } from '../../../_setup/actions/taskActions'
import { TaskResponse } from '../../../_infrastructure/model/task-model'
import { geoserverUrl } from '../../_helper/base-url'
import { Button } from '@material-ui/core'
import { easing } from 'openlayers'
import easeIn = easing.easeIn
import proj4 from 'proj4'

class IncidentMap extends Component <any, any> {
  state = {
    open: false,
    openAssetForm: false,
    pixelX: 200,
    pixelY: 200,
    selectedAsset: '',
    markCount: 0,
    asset: {
      id: null,
      wfid: null,
      name: '',
      images: [],
      description: '',
      geoLocation: '',
      assetType: {
        id: 0,
        name: '',
        attributes: []
      }
    },
    incidentCoordinates: [],
    incidentIndex: 0,
    blink: false
  }

  constructor(props: any) {
    super(props)
    this.getArray = this.getArray.bind(this)
    this.handleClose = this.handleClose.bind(this)
    this.handleAssetFormClose = this.handleAssetFormClose.bind(this)
    this.handleOpen = this.handleOpen.bind(this)
    this.setMapToFullScreen = this.setMapToFullScreen.bind(this)
  }

  componentWillMount(){
    let {user} = this.props

    if(user.role == Role.EMPLOYEE)
      this.props.changeActiveIndex(EmployeeItems.MAP)
    else if(user.role == Role.TECHNICAL_OFFICER)
      this.props.changeActiveIndex(OfficerItems.MAP)
    else if(user.role == Role.TECHNICAL_SUPERVISOR)
      this.props.changeActiveIndex(SupervisorItems.MAP)

    //Projection fixes for base layer
    ol.proj.setProj4(proj4)
    proj4.defs("EPSG:32637","+proj=utm +zone=37 +datum=WGS84 +units=m +no_defs")

  }

  async getArray(obj: Object) {
    return await Object.values(obj)
  }

  handleClose = () => {
    this.setState({
      open: false
    })
  }

  handleOpen = () => {
    this.setState({ open: true });
  }

  handleAssetFormClose = () => {
    this.setState({ openAssetForm: false })
  }

  //Full screen map
  setMapToFullScreen(){
    //if your map element id is other than 'map' change it here
    let elem: HTMLElement = document.getElementById('map') as HTMLElement;
    if (elem.requestFullscreen) {
      elem.requestFullscreen().then((res:any) => {
        console.log(res)
      });
    }
  }

  async componentDidMount() {
    await this.props.loadAssets()
    await this.props.viewAssetTasks()

    //Incident Icon Vector
    let styles = {
      'incident': new ol.style.Style({
        image: new ol.style.RegularShape({
          fill: new ol.style.Fill({color: 'red'}),
          stroke: new ol.style.Stroke({color: 'black', width: 2}),
          points: 4,
          radius: 10,
          angle: Math.PI / 4
        })
      })
    }


    //Style functions
    let pipeFunction = (feature: any) => {
      let color: ol.style.Stroke = new ol.style.Stroke ({
        color: 'rgba(110, 110, 110, 1.0)',
        width: 4
      })
      this.props.asset.map((value: AssetResponse) => {
        this.props.tasks.map((task: TaskResponse) => {
          if(value.id == task.assetId){
            if (feature.getId() == value.geoLocation) {
              if(!this.state.blink)
                color = new ol.style.Stroke ({
                  color: 'rgba(255, 0, 0, 0.8)',
                  width: 4
                })
              else
                color = new ol.style.Stroke ({
                  color: 'rgba(110, 110, 110, 1.0)',
                  width: 4
                })
            }
          }
        })
      })
      return new ol.style.Style({stroke: color})
    }

    let valveFunction = (feature: any) => {
      let color: ol.style.Circle = new ol.style.Circle({
        radius: 6,
        fill: new ol.style.Fill({
          color: 'rgba(135, 0, 0, 0.8)'
        })
      })

      this.props.asset.map((value: AssetResponse) => {
        this.props.tasks.map((task: TaskResponse) => {
          if(value.id == task.assetId){
            if (feature.getId() == value.geoLocation) {
              if(!this.state.blink)
                color = new ol.style.Circle({
                  radius: 6,
                  fill: new ol.style.Fill({
                    color: 'rgba(255, 0, 0, 1.0)'
                  })
                })
              else {
                color = new ol.style.Circle({
                  radius: 6,
                  fill: new ol.style.Fill({
                    color: 'rgba(110, 110, 110, 1.0)'
                  })
                })
              }
            }
          }
        })
      })
      return new ol.style.Style({image: color})
    }

    let reservoirFunction = (feature: any) => {
      let color: ol.style.Circle = new ol.style.Circle({
        radius: 10,
        fill: new ol.style.Fill({
          color: 'rgba(135, 0, 0, 0.8)'
        })
      })

      this.props.asset.map((value: AssetResponse) => {
        this.props.tasks.map((task: TaskResponse) => {
          if(value.id == task.assetId){
            if (feature.getId() == value.geoLocation) {
              if(!this.state.blink)
                color = new ol.style.Circle({
                  radius: 10,
                  fill: new ol.style.Fill({
                    color: 'rgba(255, 0, 0, 1.0)'
                  })
                })
              else
                color = new ol.style.Circle({
                  radius: 10,
                  fill: new ol.style.Fill({
                    color: 'rgba(135, 0, 0, 0.8)'
                  })
                })
            }
          }
        })
      })
      return new ol.style.Style({image: color})
    }

    let reduceFunction = (feature: any) => {
      let color: ol.style.Circle = new ol.style.Circle({
        radius: 8,
        fill: new ol.style.Fill({
          color: 'rgba(150, 185, 150, 0.6)'
        })
      })

      this.props.asset.map((value: AssetResponse) => {
        this.props.tasks.map((task: TaskResponse) => {
          if(value.id == task.assetId){
            if (feature.getId() == value.geoLocation) {
              if(!this.state.blink)
                color = new ol.style.Circle({
                  radius: 8,
                  fill: new ol.style.Fill({
                    color: 'rgba(255, 0, 0, 1.0)'
                  })
                })
              else
                color = new ol.style.Circle({
                  radius: 8,
                  fill: new ol.style.Fill({
                    color: 'rgba(150, 185, 150, 0.6)'
                  })
                })
            }
          }
        })
      })
      return new ol.style.Style({image: color})
    }

    let boreholeFunction = (feature: any) => {
      let color: ol.style.Circle = new ol.style.Circle({
        radius: 8,
        stroke: new ol.style.Stroke({
          color: 'rgba(0, 0, 0, 0.6)'
        })
      })

      this.props.asset.map((value: AssetResponse) => {
        this.props.tasks.map((task: TaskResponse) => {
          if(value.id == task.assetId){
            if (feature.getId() == value.geoLocation) {
              if(!this.state.blink)
                color = new ol.style.Circle({
                  radius: 8,
                  stroke: new ol.style.Stroke({
                    color: 'rgba(255, 0, 0, 1.0)'
                  })
                })
              else
                color = new ol.style.Circle({
                  radius: 8,
                  stroke: new ol.style.Stroke({
                    color: 'rgba(0, 0, 0, 0.6)'
                  })
                })
            }
          }
        })
      })
      return new ol.style.Style({image: color})
    }

    let houseFunction = (feature: any) => {
      let color: ol.style.Circle = new ol.style.Circle({
        radius: 12,
        fill: new ol.style.Fill({
          color: 'rgba(165, 165, 20, 0.6)'
        })
      })

      this.props.asset.map((value: AssetResponse) => {
        this.props.tasks.map((task: TaskResponse) => {
          if(value.id == task.assetId){
            if (feature.getId() == value.geoLocation) {
              if(!this.state.blink)
                color = new ol.style.Circle({
                  radius: 12,
                  fill: new ol.style.Fill({
                    color: 'rgba(255, 0, 0, 1.0)'
                  })
                })
              else
                color = new ol.style.Circle({
                  radius: 6,
                  fill: new ol.style.Fill({
                    color: 'rgba(165, 165, 20, 0.6)'
                  })
                })
            }
          }
        })
      })
      return new ol.style.Style({image: color})
    }

    let hydrantFunction = (feature: any) => {
      let color: ol.style.Circle = new ol.style.Circle({
        radius: 6,
        fill: new ol.style.Fill({
          color: 'rgba(165, 20, 165, 0.6)'
        })
      })

      this.props.asset.map((value: AssetResponse) => {
        this.props.tasks.map((task: TaskResponse) => {
          if(value.id == task.assetId){
            if (feature.getId() == value.geoLocation) {
              if(!this.state.blink)
                color = new ol.style.Circle({
                  radius: 6,
                  fill: new ol.style.Fill({
                    color: 'rgba(255, 0, 0, 1.0)'
                  })
                })
              else
                color = new ol.style.Circle({
                  radius: 6,
                  fill: new ol.style.Fill({
                    color: 'rgba(165, 20, 165, 0.6)'
                  })
                })
            }
          }
        })
      })
      return new ol.style.Style({image: color})
    }

    let endpointFunction = (feature: any) => {
      let color: ol.style.Circle = new ol.style.Circle({
        radius: 8,
        stroke: new ol.style.Stroke({
          color: 'rgba(20, 20, 165, 0.6)',
          width: 3
        })
      })

      this.props.asset.map((value: AssetResponse) => {
        this.props.tasks.map((task: TaskResponse) => {
          if(value.id == task.assetId){
            if (feature.getId() == value.geoLocation) {
              if(!this.state.blink)
                color = new ol.style.Circle({
                  radius: 8,
                  stroke: new ol.style.Stroke({
                    color: 'rgba(255, 0, 0, 1.0)',
                    width: 3
                  })
                })
              else
                color = new ol.style.Circle({
                  radius: 8,
                  stroke: new ol.style.Stroke({
                    color: 'rgba(20, 20, 165, 0.6)',
                    width: 3
                  })
                })
            }
          }
        })
      })
      return new ol.style.Style({image: color})
    }

    let watermeterFunction = (feature: any) => {
      let color: ol.style.Circle = new ol.style.Circle({
        radius: 8,
        stroke: new ol.style.Stroke({
          color: 'rgba(20, 20, 165, 0.6)',
          width: 3
        })
      })

      this.props.asset.map((value: AssetResponse) => {
        this.props.tasks.map((task: TaskResponse) => {
          if(value.id == task.assetId){
            if (feature.getId() == value.geoLocation) {
              if(!this.state.blink)
                color = new ol.style.Circle({
                  radius: 8,
                  stroke: new ol.style.Stroke({
                    color: 'rgba(255, 0, 0, 1.0)',
                    width: 3
                  })
                })
              else
                color = new ol.style.Circle({
                  radius: 8,
                  stroke: new ol.style.Stroke({
                    color: 'rgba(20, 20, 165, 0.6)',
                    width: 3
                  })
                })
            }
          }
        })
      })
      return new ol.style.Style({image: color})
    }

    let waypointFunction = (feature: any) => {
      let color: ol.style.Circle = new ol.style.Circle({
        radius: 6,
        fill: new ol.style.Fill({
          color: 'rgba(20, 20, 20, 0.3)'
        })
      })

      this.props.asset.map((value: AssetResponse) => {
        this.props.tasks.map((task: TaskResponse) => {
          if(value.id == task.assetId){
            if (feature.getId() == value.geoLocation) {
              if(!this.state.blink)
                color = new ol.style.Circle({
                  radius: 6,
                  fill: new ol.style.Fill({
                    color: 'rgba(255, 0, 0, 1.0)'
                  })
                })
              else
                color = new ol.style.Circle({
                  radius: 6,
                  fill: new ol.style.Fill({
                    color: 'rgba(20, 20, 20, 0.3)'
                  })
                })
            }
          }
        })
      })
      return new ol.style.Style({image: color})
    }

    //Layers
    let tileBase = new ol.layer.Tile({
      source: new ol.source.OSM({
        crossOrigin: 'anonymous'
      }),
      opacity: 0.6
    })

    // let geoTiff = await new ol.layer.Tile({
    //   source: new ol.source.TileWMS({
    //     // url: 'http://localhost:8080/geoserver/omms/wms?service=WMS&version=1.1.0&request=GetMap&layers=omms%3Aomms_aerial&bbox=491247.89999999997%2C963714.0%2C504393.89999999997%2C974560.5&width=768&height=633&srs=EPSG%3A404000&format=image%2Fjpeg',
    //     url: 'http://localhost:8080/geoserver/omms/wms',
    //     params: {
    //       LAYERS: 'omms:omms_aerial',
    //       REQUEST: 'GetMap',
    //       VERSION: '1.1.1',
    //       // STYLES: '',
    //       FORMAT: 'image/png',
    //       CRS: 'EPSG:3857',
    //       SRS: 'EPSG:3857',
    //       TILED: true
    //       // BBOX: '491247.89999999997,963714.0,504393.89999999997,974560.5'
    //     },
    //     projection: 'EPSG:32637',
    //     serverType: 'geoserver',
    //     crossOrigin: ''
    //   })
    // })

    let pipesVector = new ol.layer.Vector({
      source: new ol.source.Vector({
        format: new ol.format.GeoJSON(),
        url: geoserverUrl('pipes')
      }),
      style: pipeFunction
    })

    let smallerpipesVector = new ol.layer.Vector({
      source: new ol.source.Vector({
        format: new ol.format.GeoJSON(),
        url: geoserverUrl('smaller%20pipes')
      }),
      style: pipeFunction
    })

    let valvesVector = new ol.layer.Vector({
      source: new ol.source.Vector({
        format: new ol.format.GeoJSON(),
        url: geoserverUrl('valves')
      }),
      style: valveFunction
    })

    let reservoirVector = new ol.layer.Vector({
      source: new ol.source.Vector({
        format: new ol.format.GeoJSON(),
        url: geoserverUrl('reservoirs')
      }),
      style: reservoirFunction
    })

    let reduceVector = new ol.layer.Vector({
      source: new ol.source.Vector({
        format: new ol.format.GeoJSON(),
        url: geoserverUrl('reduce')
      }),
      style: reduceFunction
    })

    let boreholeVector = new ol.layer.Vector({
      source: new ol.source.Vector({
        format: new ol.format.GeoJSON(),
        url: geoserverUrl('boreholes')
      }),
      style: boreholeFunction
    })

    let houseConnectionVector = new ol.layer.Vector({
      source: new ol.source.Vector({
        format: new ol.format.GeoJSON(),
        url: geoserverUrl('house%20connections')
      }),
      style: houseFunction
    })

    let hydrantsVector = new ol.layer.Vector({
      source: new ol.source.Vector({
        format: new ol.format.GeoJSON(),
        url: geoserverUrl('hydrants')
      }),
      style: hydrantFunction
    })

    let endpointVector = new ol.layer.Vector({
      source: new ol.source.Vector({
        format: new ol.format.GeoJSON(),
        url: geoserverUrl('endpoint')
      }),
      style: endpointFunction
    })


    //Interaction
    let hoverInteraction = new ol.interaction.Select({
      condition: ol.events.condition.pointerMove,
      layers:[pipesVector, smallerpipesVector, valvesVector]
    })

    //Map Configuration
    let map = new ol.Map({
      target: document.getElementsByClassName('map')[0],
      layers: [
        tileBase,
        // geoTiff,
        pipesVector,
        smallerpipesVector,
        valvesVector,
        reservoirVector,
        reduceVector,
        boreholeVector,
        houseConnectionVector,
        hydrantsVector,
        endpointVector
      ],
      view: new ol.View({
        center: [4337736,981135],
        zoom: 13.5,
        extent: [
          4328439.547742118,
          976486.7738710592,
          4347032.452257882,
          985783.2261289408
        ],
        // projection: 'EPSG:3857',
        // minZoom: 12,
        // maxZoom: 19
      }),
      pixelRatio: 1,
      controls: [new ol.control.ScaleLine, new ol.control.Zoom(), new ol.control.FullScreen()],
      interactions: ol.interaction.defaults(),
    })

    console.log(map.getView().calculateExtent(map.getSize()))

    map.addInteraction(hoverInteraction)

    console.log(pipesVector.getSource())

    //Map events
    map.on('click', (e: any) => {
      let features = map.getFeaturesAtPixel(e.pixel)
      if (features != null) {
        this.setState({
          asset: {
            //@ts-ignore
            geoLocation: features[0].getId()
          }
        })
        //@ts-ignore
        // this.retrieveAsset(features[0].getId())
      }
    })

    //Map Iteration
    await map.getLayers().getArray().map((res: any) => {
      res.getSource().on('change', () => {
        if(res.getSource().getState() == 'ready'){
          res.getSource().getFeatures().map((feature: any) => {
            this.props.asset.map((value: AssetResponse) => {
              this.props.tasks.map((task: TaskResponse) => {
                if(value.id == task.assetId){
                  if (feature.getId() == value.geoLocation) {
                    if(!Array.isArray(feature.getGeometry().getCoordinates()[0])){
                      //@ts-ignore
                      this.state.incidentCoordinates.push(feature.getGeometry().getCoordinates() as any)
                    }
                    else {
                      //@ts-ignore
                      this.state.incidentCoordinates.push(feature.getGeometry().getCoordinates()[0] as any)
                    }
                  }
                }
              })
            })
          })
        }
      })
    })

    //@ts-ignore
    document.getElementById('nextBtn').addEventListener('click', () => {
      if(this.state.incidentCoordinates.length != 0) {
        if(this.state.incidentIndex == this.state.incidentCoordinates.length-1) {
          this.setState({
            incidentIndex: 0
          })
        }
        else {
          this.setState({
            incidentIndex: ++this.state.incidentIndex
          })
        }

        let x:number = 0, y: number = 0
        console.log(this.state.incidentCoordinates)
        Object.keys(this.state.incidentCoordinates[this.state.incidentIndex]).map((key: any) => {
          if(key == 1) {
            y = this.state.incidentCoordinates[this.state.incidentIndex][key]
          }
          else {
            x = this.state.incidentCoordinates[this.state.incidentIndex][key]
          }
        })

        map.getView().animate({
          center: [x, y],
          easing: easeIn,
          zoom: 16
        })
      }
    })

    //@ts-ignore
    document.getElementById('prevBtn').addEventListener('click', () => {
      if(this.state.incidentCoordinates.length != 0) { 
        if(this.state.incidentIndex == 0) {
          this.setState({
            incidentIndex: this.state.incidentCoordinates.length-1
          })
        }
        else {
          this.setState({
            incidentIndex: --this.state.incidentIndex
          })
        }
        let x:number = 0, y: number = 0
        Object.keys(this.state.incidentCoordinates[this.state.incidentIndex]).map((key: any) => {
          if(key == 1) {
            y = this.state.incidentCoordinates[this.state.incidentIndex][key]
          }
          else {
            x = this.state.incidentCoordinates[this.state.incidentIndex][key]
          }
        })

        map.getView().animate({
          center: [x, y],
          easing: easeIn,
          zoom: 16
        })
      }
    })

    setInterval(() => {
      pipesVector.setStyle(pipeFunction)
      smallerpipesVector.setStyle(pipeFunction)
      valvesVector.setStyle(valveFunction)
      reservoirVector.setStyle(reservoirFunction)
      reduceVector.setStyle(reduceFunction)
      boreholeVector.setStyle(boreholeFunction)
      houseConnectionVector.setStyle(houseFunction)
      hydrantsVector.setStyle(hydrantFunction)
      endpointVector.setStyle(endpointFunction)
      this.setState({
        blink: !this.state.blink
      })
    }, 500)
  }

  render() {
    return (
      <div>
        <div className="buttons">
          <Button variant={'contained'} id={'prevBtn'} style={{
            margin: '10px 20px'
          }}>
            Previous Incident
          </Button>
          <Button variant={'contained'} id={'nextBtn'} style={{
            margin: '10px 20px'
          }}>
            Next Incident
          </Button>
        </div>
        <div ref={'map'} className={'map'} id={'map'} style={{ height: '100%!important' }} />
        <div className="map-legend">
          <h1 className="title">
            Map Legend
          </h1>
          <div className="legends">
            <div className={'a-valve l-container'}>
              <div className={'assigned-valve icon'}>&nbsp;</div>
              <div className={'label'}>Incident Task</div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state: any) => ({
  asset: state.asset.assets,
  user: state.login.userInfo,
  tasks: state.tasks.tasks
})

function mapDispatchToProps(dispatch:any){
  return {
    getAssetByFid:(fid:string)=>dispatch(getAssetByFid(fid)),
    loadAssets:()=>dispatch(loadAssets()),
    changeActiveIndex:(index:number)=>dispatch(changeActiveIndex(index)),
    viewAssetTasks: () => dispatch(viewAssetTasks())
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(IncidentMap)