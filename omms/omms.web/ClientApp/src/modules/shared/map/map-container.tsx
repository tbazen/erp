import React, { Component } from 'react'
import { Fab, Paper, Theme, withStyles } from '@material-ui/core'
import { connect } from 'react-redux'
import AppBar from '@material-ui/core/AppBar'
import Tabs from '@material-ui/core/Tabs'
import Tab from '@material-ui/core/Tab'
import IncidentMap from './full-screen'
import Map from './map'

interface ITaskState {
  value: number
}

const styles = (theme: Theme) => ({
  fab: {
    margin: theme.spacing.unit
  },
  extendedIcon: {
    marginRight: theme.spacing.unit
  }
})

class MapContainer extends Component<any, ITaskState> {
  state: ITaskState = {
    value: 0
  }

  constructor(props: any) {
    super(props)

    this.handleChange = this.handleChange.bind(this)
  }

  TabContainer(props: any) {
    return <div>{props.children}</div>
  }

  //@ts-ignore
  handleChange(e: any, value: number) {
    this.setState({ value })
  }


  public render() {
    return (
      <div className={'map-container'}>
        <Paper>
          <AppBar position="sticky" color={'default'}>
            <Tabs 
            value={this.state.value} 
            indicatorColor="primary"
            textColor="primary"
            onChange={this.handleChange}
            >
              <Tab label="Asset Map" />
              <Tab label="Incident Map" />
            </Tabs>
          </AppBar>
          {this.state.value === 0 && (
            <this.TabContainer>
              <Map role={this.props.user.role} {...this.props}/>
            </this.TabContainer>
          )}
          {this.state.value === 1 && (
            <this.TabContainer>
              <IncidentMap role={this.props.user.role} {...this.props}/>
            </this.TabContainer>
          )}
        </Paper>
      </div>
    )
  }
}


const mapStateToProps = (state: any) => ({
  user: state.login.userInfo
})

export default withStyles(styles)(
  connect(
    mapStateToProps,
    null
  )(MapContainer)
)
