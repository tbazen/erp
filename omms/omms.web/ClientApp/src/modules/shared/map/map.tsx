import React, { Component } from 'react'
import * as ol from 'openlayers'
import './map.scss'
import './ol.css'
import AssetFormDialog from '../../admin/asset/asset-form'
import { AssetResponse } from '../../../_infrastructure/model/assetModel'
import { connect } from 'react-redux'
import { getAssetByFid, loadAssets } from '../../../_setup/actions/assetActions'
import Role from '../_helper/role-util/roles'
import { EmployeeItems } from '../_helper/slideout-index/employee-items'
import { OfficerItems } from '../_helper/slideout-index/officer-items'
import { SupervisorItems } from '../_helper/slideout-index/supervisor-items'
import { changeActiveIndex } from '../../../_setup/actions/drawer-actions'
import UIContainer from '../_helper/ui-helper/containers-util'
import { Grid } from '@material-ui/core'
import { geoserverUrl } from '../../_helper/base-url'
import { AssetSearhParams, AssetTypes } from '../../../_infrastructure/model/asset-search-params'
import proj4 from 'proj4'

interface IState {
  open: boolean,
  openAssetForm: boolean,
  pixelX: number,
  pixelY: number,
  selectedAsset: AssetSearhParams,
  markCount: number,
  asset: AssetResponse
}

class Map extends Component <any, any> {
  state: IState = {
    open: false,
    openAssetForm: false,
    pixelX: 200,
    pixelY: 200,
    selectedAsset: {
      assetType: AssetTypes.PIPELINE,
      pkuId: 0
    },
    markCount: 0,
    asset: {
      id: null,
      wfid: null,
      note:'',
      name: '',
      pkuId: 0,
      images: [],
      description: '',
      geoLocation: '',
      assetType: {
        id: 0,
        name: '',
        attributes: []
      }
    }
  }

  constructor(props: any) {
    super(props)
    this.getArray = this.getArray.bind(this)
    this.handleClose = this.handleClose.bind(this)
    this.handleAssetFormClose = this.handleAssetFormClose.bind(this)
    this.handleOpen = this.handleOpen.bind(this)
    this.retrieveAsset = this.retrieveAsset.bind(this)
  }

  componentWillMount(){
    let {user} = this.props
    
    if(user.role == Role.EMPLOYEE)
        this.props.changeActiveIndex(EmployeeItems.MAP)
    else if(user.role == Role.TECHNICAL_OFFICER)
        this.props.changeActiveIndex(OfficerItems.MAP)
    else if(user.role == Role.TECHNICAL_SUPERVISOR)
        this.props.changeActiveIndex(SupervisorItems.MAP)
    
  }
  
  async getArray(obj: Object) {
    return await Object.values(obj)
  }

  handleClose = () => {
    this.setState({
      open: false
    })
  }

  handleOpen = () => {
    this.setState({ open: true });
  }

  handleAssetFormClose = () => {
    this.setState({ openAssetForm: false })
  }

  async retrieveAsset(id: string, pkuid: number) {
    await this.props.getAssetByFid(id)

    console.log('this.props.asset')
    console.log(this.props.asset)

    let assetType: AssetTypes = AssetTypes.PIPELINE

    if(id.startsWith('pipe')) {
      assetType = AssetTypes.PIPELINE
    }
    else if(id.startsWith('valve')) {
      assetType = AssetTypes.VALVE
    }
    else if(id.startsWith('endpoint')) {
      assetType = AssetTypes.ENDPOINTS
    }
    else if(id.startsWith('reservoir')) {
      assetType = AssetTypes.RESERVIOR
    }
    else if(id.startsWith('smaller')) {
      assetType = AssetTypes.PIPELINE
    }
    else if(id.startsWith('hydrant')) {
      assetType = AssetTypes.HYDRANTS
    }
    else if(id.startsWith('watermeter')) {
      assetType = AssetTypes.WATER_METER
    }
    else if(id.startsWith('borehole')) {
      assetType = AssetTypes.BOREHOLE
    }

    console.log(id + ':' + pkuid )

    if(this.props.asset[0] != "")
      // return (<Redirect to={`/asset-detail/${this.props.asset[0].wfid}/${this.props.asset[0].id}`}/>)
      this.props.history.push(`/asset-detail/${this.props.asset[0].id}`)
    else {
      // this.handleOpen()
      if(this.props.role == Role.TECHNICAL_OFFICER)
        this.setState({
          selectedAsset: {pkuId: pkuid, assetType: assetType},
          openAssetForm: true
        })
        console.group('Selected state')
        console.log(this.state)
        console.groupEnd()
    }
    return null
  }
  async componentDidMount() {
    await this.props.loadAssets()
    proj4.defs('EPSG:32637')
    //Style functions
    let pipeFunction = (feature: any) => {
      let color: ol.style.Stroke = new ol.style.Stroke ({
        color: 'rgba(110, 110, 110, 1.0)',
        width: 4
      })
      this.props.asset.map((value: AssetResponse) => {
        if (feature.getId() == value.geoLocation) {
          color = new ol.style.Stroke ({
            color: 'rgba(0, 0, 255, 0.7)',
            width: 4
          })
        }
      })
      return new ol.style.Style({stroke: color})
    }

    let reservoirFunction = (feature: any) => {
      let color: ol.style.Circle = new ol.style.Circle({
        radius: 10,
        fill: new ol.style.Fill({
          color: 'rgba(0, 100, 0, 0.6)'
        })
      })

      this.props.asset.map((value: AssetResponse) => {
        if (feature.getId() == value.geoLocation) {
          color = new ol.style.Circle({
            radius: 10,
            fill: new ol.style.Fill({
              color: 'rgba(0, 245, 0, 1.0)'
            })
          })
        }
      })
      return new ol.style.Style({image: color})
    }

    let reduceFunction = (feature: any) => {
      let color: ol.style.Circle = new ol.style.Circle({
        radius: 8,
        fill: new ol.style.Fill({
          color: 'rgba(150, 185, 150, 0.6)'
        })
      })

      this.props.asset.map((value: AssetResponse) => {
        if (feature.getId() == value.geoLocation) {
          color = new ol.style.Circle({
            radius: 8,
            fill: new ol.style.Fill({
              color: 'rgba(150, 245, 150, 1.0)'
            })
          })
        }
      })
      return new ol.style.Style({image: color})
    }

    let boreholeFunction = (feature: any) => {
      let color: ol.style.Circle = new ol.style.Circle({
        radius: 8,
        stroke: new ol.style.Stroke({
          color: 'rgba(0, 0, 0, 0.6)'
        })
      })

      this.props.asset.map((value: AssetResponse) => {
        if (feature.getId() == value.geoLocation) {
          color = new ol.style.Circle({
            radius: 8,
            stroke: new ol.style.Stroke({
              color: 'rgba(0, 0, 0, 1.0)'
            })
          })
        }
      })
      return new ol.style.Style({image: color})
    }

    let valveFunction = (feature: any) => {
      let color: ol.style.Circle = new ol.style.Circle({
          radius: 6,
          fill: new ol.style.Fill({
            color: 'rgba(135, 0, 0, 0.6)'
          })
        })

      this.props.asset.map((value: AssetResponse) => {
        if (feature.getId() == value.geoLocation) {
          console.log(feature.getId())
          color = new ol.style.Circle({
            radius: 6,
            fill: new ol.style.Fill({
              color: 'rgba(255, 0, 0, 1.0)'
            })
          })
        }
      })
      return new ol.style.Style({image: color})
    }

    let houseFunction = (feature: any) => {
      let color: ol.style.Circle = new ol.style.Circle({
          radius: 12,
          fill: new ol.style.Fill({
            color: 'rgba(165, 165, 20, 0.6)'
          })
        })

      this.props.asset.map((value: AssetResponse) => {
        if (feature.getId() == value.geoLocation) {
          color = new ol.style.Circle({
            radius: 12,
            fill: new ol.style.Fill({
              color: 'rgba(235, 235, 20, 1.0)'
            })
          })
        }
      })
      return new ol.style.Style({image: color})
    }

    let hydrantFunction = (feature: any) => {
      let color: ol.style.Circle = new ol.style.Circle({
          radius: 6,
          fill: new ol.style.Fill({
            color: 'rgba(165, 20, 165, 0.6)'
          })
        })

      this.props.asset.map((value: AssetResponse) => {
        if (feature.getId() == value.geoLocation) {
          color = new ol.style.Circle({
            radius: 6,
            fill: new ol.style.Fill({
              color: 'rgba(235, 20, 235, 1.0)'
            })
          })
        }
      })
      return new ol.style.Style({image: color})
    }

    let endpointFunction = (feature: any) => {
      let color: ol.style.Circle = new ol.style.Circle({
        radius: 8,
        stroke: new ol.style.Stroke({
          color: 'rgba(20, 20, 165, 0.6)',
          width: 3
        })
      })

      this.props.asset.map((value: AssetResponse) => {
        if (  feature.getId() == value.geoLocation) {
          color = new ol.style.Circle({
            radius: 8,
            stroke: new ol.style.Stroke({
              color: 'rgba(20, 20, 235, 1.0)',
              width: 3
            })
          })
        }
      })
      return new ol.style.Style({image: color})
    }

    let watermeterFunction = (feature: any) => {
      let color: ol.style.Circle = new ol.style.Circle({
        radius: 8,
        stroke: new ol.style.Stroke({
          color: 'rgba(20, 20, 165, 0.6)',
          width: 3
        })
      })

      this.props.asset.map((value: AssetResponse) => {
        if (feature.getId() == value.geoLocation) {
          color = new ol.style.Circle({
            radius: 8,
            stroke: new ol.style.Stroke({
              color: 'rgba(20, 20, 235, 1.0)',
              width: 3
            })
          })
        }
      })
      return new ol.style.Style({image: color})
    }

    let waypointFunction = (feature: any) => {
      let color: ol.style.Circle = new ol.style.Circle({
        radius: 6,
        fill: new ol.style.Fill({
          color: 'rgba(20, 20, 20, 0.3)'
        })
      })

      this.props.asset.map((value: AssetResponse) => {
        if (feature.getId() == value.geoLocation) {
          color = new ol.style.Circle({
            radius: 6,
            fill: new ol.style.Fill({
              color: 'rgba(20, 20, 20, 1)'
            })
          })
        }
      })
      return new ol.style.Style({image: color})
    }

    //Projection fixes for base layer
    ol.proj.setProj4(proj4); 
    proj4.defs("EPSG:32637","+proj=utm +zone=37 +datum=WGS84 +units=m +no_defs")
    let projection = ol.proj.get('EPSG:32637')

    //Layers
    let tileBase = new ol.layer.Tile({
      source: new ol.source.OSM({
        crossOrigin: 'anonymous'
      }),
      opacity: 0.6
    })

    // let googleMaps = new ol.layer.Tile({
    //   visible: true,
    //   source: new ol.source.TileImage({
    //     projection: 'EPSG:32637',
    //     url: 'http://maps.googleapis.com/maps/api/js/StaticMapService.GetMapImage?1m2&1i10131692&2i7857840&2e2&3u16&4m2&1u508&2u400&5m5&1e2&5sen-GB&6sus&10b1&12b1&key=AIzaSyCJ8DpO8IL8xNeld9uHEgT0rCNMhUp81sk'
    //   })
    // })

    // let geoTiff = await new ol.layer.Tile({
    //   source: new ol.source.TileWMS({
    //     // url: 'http://localhost:8080/geoserver/omms/wms?service=WMS&version=1.1.0&request=GetMap&layers=omms%3Aomms_aerial&bbox=491247.89999999997%2C963714.0%2C504393.89999999997%2C974560.5&width=768&height=633&srs=EPSG%3A404000&format=image%2Fjpeg',
    //     url: 'http://localhost:8080/geoserver/omms/wms',
    //     params: {
    //       LAYERS: 'omms:omms_aerial',
    //       REQUEST: 'GetMap',
    //       VERSION: '1.1.1',
    //       // STYLES: '',
    //       FORMAT: 'image/jpeg',
    //       CRS: 'EPSG:3857',
    //       SRS: 'EPSG:3857',
    //       TILED: true
    //       // BBOX: '491247.89999999997,963714.0,504393.89999999997,974560.5'
    //     },
    //     projection: 'EPSG:3857',
    //     serverType: 'geoserver',
    //     crossOrigin: ''
    //   })
    // })

    console.log('test')
    console.log(geoTiff.getExtent())

    let pipesVector = new ol.layer.Vector({
      source: new ol.source.Vector({
        format: new ol.format.GeoJSON(),
        url: geoserverUrl('pipes')
      }),
      style: pipeFunction
    })

    let smallerpipesVector = new ol.layer.Vector({
      source: new ol.source.Vector({
        format: new ol.format.GeoJSON(),
        url: geoserverUrl('smaller%20pipes')
      }),
      style: pipeFunction
    })

    let valvesVector = new ol.layer.Vector({
      source: new ol.source.Vector({
        format: new ol.format.GeoJSON(),
        url: geoserverUrl('valves')
      }),
      style: valveFunction
    })

    let reservoirVector = new ol.layer.Vector({
      source: new ol.source.Vector({
        format: new ol.format.GeoJSON(),
        url: geoserverUrl('reservoirs')
      }),
      style: reservoirFunction
    })

    let reduceVector = new ol.layer.Vector({
      source: new ol.source.Vector({
        format: new ol.format.GeoJSON(),
        url: geoserverUrl('reduce')
      }),
      style: reduceFunction
    })

    let boreholeVector = new ol.layer.Vector({
      source: new ol.source.Vector({
        format: new ol.format.GeoJSON(),
        url: geoserverUrl('boreholes')
      }),
      style: boreholeFunction
    })

    let houseConnectionVector = new ol.layer.Vector({
      source: new ol.source.Vector({
        format: new ol.format.GeoJSON(),
        url: geoserverUrl('house%20connections'),
      }),
      style: houseFunction
    })

    let hydrantsVector = new ol.layer.Vector({
      source: new ol.source.Vector({
        format: new ol.format.GeoJSON(),
        url: geoserverUrl('hydrants')
      }),
      style: hydrantFunction
    })

    let endpointVector = new ol.layer.Vector({
      source: new ol.source.Vector({
        format: new ol.format.GeoJSON(),
        url: geoserverUrl('endpoint')
      }),
      style: endpointFunction
    })

    let watermeterVector = new ol.layer.Vector({
      source: new ol.source.Vector({
        format: new ol.format.GeoJSON(),
        url: geoserverUrl('watermeters')
      }),
      style: watermeterFunction
    }) //TODO configure the water meter

    //Interaction
    let hoverInteraction = new ol.interaction.Select({
      condition: ol.events.condition.pointerMove,
      layers:[
        pipesVector,
        smallerpipesVector,
        valvesVector,
        reduceVector,
        reservoirVector,
        boreholeVector,
        houseConnectionVector,
        hydrantsVector,
        endpointVector
      ]
    })

    //Map Configuration
    let map = new ol.Map({
      target: document.getElementsByClassName('map')[0],
      layers: [
        //TODO get the base map from qgis
        tileBase,
        // geoTiff,
        // googleMaps,
        pipesVector,
        smallerpipesVector,
        valvesVector,
        reservoirVector,
        reduceVector,
        boreholeVector,
        houseConnectionVector,
        hydrantsVector,
        endpointVector
      ],
      view: new ol.View({
        center: [4337736,981135],
        // center: [0, 0],
        zoom: 13,
        projection: projection
        // minZoom: 12,
        // maxZoom: 19
      }),
      controls: [new ol.control.ScaleLine, new ol.control.Zoom()],
      interactions: ol.interaction.defaults(),
    })

    map.addInteraction(hoverInteraction)

    //Map events
    map.on('click', (e: any) => {
      let features = map.getFeaturesAtPixel(e.pixel)
      if (features != null) {
        this.setState({
          asset: {
            //@ts-ignore
            geoLocation: features[0].getId()
          }
        })
        //@ts-ignore
        this.retrieveAsset(features[0].getId(), features[0].getProperties().pkuid)
      }
    })

    let displayPopup = (
      coordinate: [number, number],
      id: string,
      pixel: [number, number]
    ) => {
      let content = document.getElementById(
        'pop-up-content'
      ) as HTMLInputElement
      if (content != null) {
        content.innerHTML = 'Feature Id: ' + id
        if (id != '') console.log(id)
        else console.log('Empty space at: ' + pixel)
      }
      console.log(coordinate)
    }

    // GoogleMapsLoader.KEY = apiKey

    // GoogleMapsLoader.load(() => {
    //   new google.maps.Map(document.getElementById('google-maps'), {center: {lat: -34.397, lng: 150.644}})
    // })
  }

  render() {
    return (
      <div>
        {/*<div className="google-maps" id="google-maps"/>*/}

        <div ref={'map'} className={'map'} style={{ height: '100%!important'}}>
          <AssetFormDialog
            container={UIContainer.REGISTERED_CONTAINER}
            openForm={this.state.openAssetForm}
            handleClose={this.handleAssetFormClose}
            asset={this.state.asset}
            searchParams={this.state.selectedAsset}
          />
        </div>
        <div className="map-legend">
          <h1 className="title">
          Map Legend
          </h1>
          <Grid container className="legends">
            <Grid item xs={2}>
              <div className={'a-valve l-container'}>
                <div className={'assigned-valve icon'}>&nbsp;</div>
                <div className={'label'}>Assigned Valve</div>
              </div>
              <div className={'u-valve l-container'}>
                <div className={'unassigned-valve icon'}>&nbsp;</div>
                <div className={'label'}>Unassigned Valve</div>
              </div>
            </Grid>
            <Grid item xs={2}>
              <div className={'a-pipe l-container'}>
                <div className={'assigned-pipe icon'}>&nbsp;</div>
                <div className={'label'}>Assigned Pipe</div>
              </div>
              <div className={'u-pipe l-container'}>
                <div className={'unassigned-pipe icon'}>&nbsp;</div>
                <div className={'label'}>Unassigned Pipe</div>
              </div>
            </Grid>
            <Grid item xs={2}>
              <div className={'u-pipe l-container'}>
                <div className={'assigned-reduce icon'}>&nbsp;</div>
                <div className={'label'}>Assigned Reduce</div>
              </div>
              <div className={'u-pipe l-container'}>
                <div className={'unassigned-reduce icon'}>&nbsp;</div>
                <div className={'label'}>Unassigned Reduce</div>
              </div>
            </Grid>
            <Grid item xs={2}>
              <div className={'u-pipe l-container'}>
                <div className={'assigned-reservoir icon'}>&nbsp;</div>
                <div className={'label'}>Assigned Reservoir</div>
              </div>
              <div className={'u-pipe l-container'}>
                <div className={'unassigned-reservoir icon'}>&nbsp;</div>
                <div className={'label'}>Unassigned Reservoir</div>
              </div>
            </Grid>
            <Grid item xs={2}>
              <div className={'u-pipe l-container'}>
                <div className={'assigned-borehole icon'}>&nbsp;</div>
                <div className={'label'}>Assigned Borehole</div>
              </div>
              <div className={'u-pipe l-container'}>
                <div className={'unassigned-borehole icon'}>&nbsp;</div>
                <div className={'label'}>Unassigned Borehole</div>
              </div>
            </Grid>
            <Grid item xs={2}>
              <div className={'u-pipe l-container'}>
                <div className={'assigned-house icon'}>&nbsp;</div>
                <div className={'label'}>Assigned House Connection</div>
              </div>
              <div className={'u-pipe l-container'}>
                <div className={'unassigned-house icon'}>&nbsp;</div>
                <div className={'label'}>Unassigned House Connection</div>
              </div>
            </Grid>
            <Grid item xs={2}>
              <div className={'u-pipe l-container'}>
                <div className={'assigned-hydrants icon'}>&nbsp;</div>
                <div className={'label'}>Assigned Hydrants</div>
              </div>
              <div className={'u-pipe l-container'}>
                <div className={'unassigned-hydrants icon'}>&nbsp;</div>
                <div className={'label'}>Unassigned Hydrants</div>
              </div>
            </Grid>
            <Grid item xs={2}>
              <div className={'u-pipe l-container'}>
                <div className={'assigned-endpoint icon'}>&nbsp;</div>
                <div className={'label'}>Assigned Endpoint</div>
              </div>
              <div className={'u-pipe l-container'}>
                <div className={'unassigned-endpoint icon'}>&nbsp;</div>
                <div className={'label'}>Unassigned Endpoint</div>
              </div>
            </Grid>
            <Grid item xs={2}>
              <div className={'u-pipe l-container'}>
                <div className={'assigned-waypoint icon'}>&nbsp;</div>
                <div className={'label'}>Assigned Waypoint</div>
              </div>
              <div className={'u-pipe l-container'}>
                <div className={'unassigned-waypoint icon'}>&nbsp;</div>
                <div className={'label'}>Unassigned Waypoint</div>
              </div>
            </Grid>
          </Grid>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state: any) => ({
  asset: state.asset.assets,
  user: state.login.userInfo
})

function mapDispatchToProps(dispatch:any){
  return {
    getAssetByFid:(fid:string)=>dispatch(getAssetByFid(fid)),
    loadAssets:()=>dispatch(loadAssets()),
    changeActiveIndex:(index:number)=>dispatch(changeActiveIndex(index))
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(Map)