import React, { Component } from 'react'
import * as ol from 'openlayers'
import '../ol.css'
import '../map.scss'
import { AssetResponse } from '../../../../_infrastructure/model/assetModel'
import { geoserverUrl } from '../../../_helper/base-url'
import { easing, proj } from 'openlayers'
import fromLonLat = proj.fromLonLat
import easeIn = easing.easeIn
import { TaskResponse } from '../../../../_infrastructure/model/task-model'

export default class MiniMap extends Component <any, any> {
  state = {
    marked: false,
    markCount: 0,
    incidentCoordinates: {x: 0, y: 0}
  }

  constructor(props: any) {
    super(props)
  }

  async componentDidMount() {
    //Style functions
    let pipeFunction = (feature: any) => {
      let color: ol.style.Stroke = new ol.style.Stroke ({
        color: 'rgba(110, 110, 110, 1.0)',
        width: 4
      })
      if (feature.getId() == this.props.asset.geoLocation) {
        color = new ol.style.Stroke ({
          color: 'rgba(0, 255, 255, 1.0)',
          width: 4
        })
      }
      return new ol.style.Style({stroke: color})
    }

    let valveFunction = (feature: any) => {
      let color: ol.style.Circle = new ol.style.Circle({
        radius: 6,
        fill: new ol.style.Fill({
          color: 'rgba(135, 0, 0, 0.6)'
        })
      })
      if (feature.getId() == this.props.asset.geoLocation) {
        console.log(feature.getId())
        color = new ol.style.Circle({
          radius: 6,
          fill: new ol.style.Fill({
            color: 'rgba(0, 255, 255, 1.0)'
          })
        })
      }
      return new ol.style.Style({ image: color })
    }

    let reservoirFunction = (feature: any) => {
      let color: ol.style.Circle = new ol.style.Circle({
        radius: 10,
        fill: new ol.style.Fill({
          color: 'rgba(0, 100, 0, 0.6)'
        })
      })

      if (feature.getId() == this.props.asset.geoLocation) {
        color = new ol.style.Circle({
          radius: 10,
          fill: new ol.style.Fill({
            color: 'rgba(0, 245, 0, 1.0)'
          })
        })
      }
      return new ol.style.Style({image: color})
    }

    let reduceFunction = (feature: any) => {
      let color: ol.style.Circle = new ol.style.Circle({
        radius: 8,
        fill: new ol.style.Fill({
          color: 'rgba(150, 185, 150, 0.6)'
        })
      })

      if (feature.getId() == this.props.asset.geoLocation) {
        color = new ol.style.Circle({
          radius: 8,
          fill: new ol.style.Fill({
            color: 'rgba(150, 245, 150, 1.0)'
          })
        })
      }
      return new ol.style.Style({image: color})
    }

    let boreholeFunction = (feature: any) => {
      let color: ol.style.Circle = new ol.style.Circle({
        radius: 8,
        stroke: new ol.style.Stroke({
          color: 'rgba(0, 0, 0, 0.6)'
        })
      })

      if (feature.getId() == this.props.asset.geoLocation) {
        color = new ol.style.Circle({
          radius: 8,
          stroke: new ol.style.Stroke({
            color: 'rgba(0, 0, 0, 1.0)'
          })
        })
      }
      return new ol.style.Style({image: color})
    }

    let houseFunction = (feature: any) => {
      let color: ol.style.Circle = new ol.style.Circle({
        radius: 12,
        fill: new ol.style.Fill({
          color: 'rgba(165, 165, 20, 0.6)'
        })
      })

      if (feature.getId() == this.props.asset.geoLocation) {
        color = new ol.style.Circle({
          radius: 12,
          fill: new ol.style.Fill({
            color: 'rgba(235, 235, 20, 1.0)'
          })
        })
      }
      return new ol.style.Style({image: color})
    }

    let hydrantFunction = (feature: any) => {
      let color: ol.style.Circle = new ol.style.Circle({
        radius: 6,
        fill: new ol.style.Fill({
          color: 'rgba(165, 20, 165, 0.6)'
        })
      })

      if (feature.getId() == this.props.asset.geoLocation) {
        color = new ol.style.Circle({
          radius: 6,
          fill: new ol.style.Fill({
            color: 'rgba(235, 20, 235, 1.0)'
          })
        })
      }
      return new ol.style.Style({image: color})
    }

    let endpointFunction = (feature: any) => {
      let color: ol.style.Circle = new ol.style.Circle({
        radius: 8,
        stroke: new ol.style.Stroke({
          color: 'rgba(20, 20, 165, 0.6)',
          width: 3
        })
      })

      if (feature.getId() == this.props.asset.geoLocation) {
        color = new ol.style.Circle({
          radius: 8,
          stroke: new ol.style.Stroke({
            color: 'rgba(20, 20, 235, 1.0)',
            width: 3
          })
        })
      }
      return new ol.style.Style({image: color})
    }

    let watermeterFunction = (feature: any) => {
      let color: ol.style.Circle = new ol.style.Circle({
        radius: 8,
        stroke: new ol.style.Stroke({
          color: 'rgba(20, 20, 165, 0.6)',
          width: 3
        })
      })

      if (feature.getId() == this.props.asset.geoLocation) {
        color = new ol.style.Circle({
          radius: 8,
          stroke: new ol.style.Stroke({
            color: 'rgba(20, 20, 235, 1.0)',
            width: 3
          })
        })
      }
      return new ol.style.Style({image: color})
    }

    //Layers
    let tileBase = new ol.layer.Tile({
      source: new ol.source.OSM(),
      opacity: 0.6
    })

    let pipesVector = new ol.layer.Vector({
      source: new ol.source.Vector({
        format: new ol.format.GeoJSON(),
        url: geoserverUrl('pipes')
      }),
      style: pipeFunction
    })

    let smallerpipesVector = new ol.layer.Vector({
      source: new ol.source.Vector({
        format: new ol.format.GeoJSON(),
        url: geoserverUrl('smaller%20pipes')
      }),
      style: pipeFunction
    })

    let valvesVector = new ol.layer.Vector({
      source: new ol.source.Vector({
        format: new ol.format.GeoJSON(),
        url: geoserverUrl('valves')
      }),
      style: valveFunction
    })

    let reservoirVector = new ol.layer.Vector({
      source: new ol.source.Vector({
        format: new ol.format.GeoJSON(),
        url: geoserverUrl('reservoirs')
      }),
      style: reservoirFunction
    })

    let reduceVector = new ol.layer.Vector({
      source: new ol.source.Vector({
        format: new ol.format.GeoJSON(),
        url: geoserverUrl('reduce')
      }),
      style: reduceFunction
    })

    let boreholeVector = new ol.layer.Vector({
      source: new ol.source.Vector({
        format: new ol.format.GeoJSON(),
        url: geoserverUrl('boreholes')
      }),
      style: boreholeFunction
    })

    let houseConnectionVector = new ol.layer.Vector({
      source: new ol.source.Vector({
        format: new ol.format.GeoJSON(),
        url: geoserverUrl('house%20connections')
      }),
      style: houseFunction
    })

    let hydrantsVector = new ol.layer.Vector({
      source: new ol.source.Vector({
        format: new ol.format.GeoJSON(),
        url: geoserverUrl('hydrants')
      }),
      style: hydrantFunction
    })

    let endpointVector = new ol.layer.Vector({
      source: new ol.source.Vector({
        format: new ol.format.GeoJSON(),
        url: geoserverUrl('endpoint')
      }),
      style: endpointFunction
    })

    let map = new ol.Map({
      target: document.getElementsByClassName('map')[0],
      layers: [
        tileBase,
        pipesVector,
        smallerpipesVector,
        valvesVector,
        reservoirVector,
        reduceVector,
        boreholeVector,
        houseConnectionVector,
        hydrantsVector,
        endpointVector
      ],
      view: new ol.View({
        center: [4337736,981135],
        zoom: 12.5,
        maxZoom: 19,
        minZoom: 10
      }),
      controls: [new ol.control.Zoom]
    })

    console.log(map.getLayers())

    //Map Iteration
    await map.getLayers().getArray().map((res: any) => {
      res.getSource().on('change', () => {
        if(res.getSource().getState() == 'ready'){
          res.getSource().getFeatures().map((feature: any) => {
            if (feature.getId() == this.props.asset.geoLocation) {
              if(!Array.isArray(feature.getGeometry().getCoordinates()[0])){
                this.setState({
                  incidentCoordinates: {
                    x: feature.getGeometry().getCoordinates()[0],
                    y: feature.getGeometry().getCoordinates()[1],
                  }
                })
                //@ts-ignore
                // this.state.incidentCoordinates.push(feature.getGeometry().getCoordinates() as any)
              }
              else {
                //@ts-ignore
                // this.state.incidentCoordinates.push(feature.getGeometry().getCoordinates()[0] as any)
                this.setState({
                  incidentCoordinates: {
                    x: feature.getGeometry().getCoordinates()[0][0],
                    y: feature.getGeometry().getCoordinates()[0][1],
                  }
                })
              }
              console.log(feature.getGeometry().getCoordinates())
              console.log(this.state.incidentCoordinates)

              if(this.state.incidentCoordinates.x != 0 && this.state.incidentCoordinates.y != 0)
                setTimeout(() => {
                  map.getView().animate({
                    center: [this.state.incidentCoordinates.x, this.state.incidentCoordinates.y],
                    easing: easeIn,
                    zoom: 15
                  })
                }, 200 )
            }
          })
        }
      })
    })

    console.log(this.state.incidentCoordinates)

    //Map events
    map.on('click', (e: any) => {
      let features = map.getFeaturesAtPixel(e.pixel)
      if (features != null) {
        features.map((feature: any) => {
          //TODO check if feature is a registered asset on the database and display asset creation or asset information accordingly
          console.log(feature.getId())
        })
      }
    })
  }

  render() {
    return (
      <div>
        <div ref={'map'} className={'map'} >

        </div>

        <div className="map-legend">
          <h1 className="title">
            Map Legend
          </h1>
          <div className="legends">
            <div className={'a-valve l-container'}>
              <div className={'selected-asset icon'}>&nbsp;</div>
              <div className={'label'}>Asset</div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}