import React, { Component } from 'react'
import '../report.scss'
import { Button, Table, TableBody, TableCell, TableHead, TableRow, Grid, Typography, Divider, List, ListSubheader, ListItem, Checkbox, ListItemText } from '@material-ui/core';
import { connect } from 'react-redux'
import { AssetReportModel } from '../../../../_infrastructure/model/asset-report-model'
import { baseUrl } from '../../../../_setup/services/url.config';
import { AssetReportState } from '../../../../_infrastructure/state/asset-report-state';
import { getAssetReports, AssetReportFilter, filterAssetReport, cancelAssetFilter } from '../../../../_setup/actions/asset-report-actions';
import LoadingCircle from '../../components/loading/loading-circle';
import ErrorPage from '../../components/error/error-page';
import { PRIMARY } from '../../_helper/color-container/color-container';
import printReport from '../../../_helper/print';


const PRINT_Id = 'printAssetReport'
interface PropsFromState {
  assetReportState: AssetReportState
}

interface PropsFromDispatch {
  getAssetReports: () =>  void,
  cancelARFiltering : () => void
  filterAssetReport : (filter : AssetReportFilter[]) => void
}

interface IProps {

}

type AllProps = IProps & PropsFromDispatch & PropsFromState

interface IState{
  selectedFilters : AssetReportFilter[]
}
class AssetReport extends Component <AllProps, IState>{

  constructor(props: any) {
    super(props)
    this.state={
      selectedFilters:[]
    }
    this.assetTable = this.assetTable.bind(this)
  }

  componentDidMount() {
    this.props.getAssetReports()
  }

  getData(report : AssetReportModel[]) {
    if(report != undefined && report != [])
      return(
        <>
          {
              report.map((assetReport: AssetReportModel, key: number) => {
              return (
                <TableRow hover key={key}>
                  <TableCell align="center">{key+1}</TableCell>
                  <TableCell align="center">{assetReport.name}</TableCell>
                  <TableCell align="center">{assetReport.assetType}</TableCell>
                  <TableCell align="center">{assetReport.onGoingTasks}</TableCell>
                  <TableCell align="center">{assetReport.completedTasks}</TableCell>
                  <TableCell align="center">{assetReport.cancelledTasks}</TableCell>
                  <TableCell align="center">{assetReport.terminatedTasks}</TableCell>
                  <TableCell align="center">{assetReport.totalTasks}</TableCell>
                </TableRow>
              )
            })}
        </>
      )
    else
      return (
        <>
          <TableRow>
          </TableRow>
        </>
      )
  }

  assetTable(report : AssetReportModel[]) {
    return (
      <Table>
        <TableHead>
          <TableRow>
            <TableCell align="center">No. </TableCell>
            <TableCell align="center">Name</TableCell>
            <TableCell align="center">Asset Type</TableCell>
            <TableCell align="center">Ongoing Tasks</TableCell>
            <TableCell align="center">Completed Tasks</TableCell>
            <TableCell align="center">Cancelled Tasks</TableCell>
            <TableCell align="center">Terminated Tasks</TableCell>
            <TableCell align="center">Total Tasks</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {this.getData(report)}
        </TableBody>
      </Table>
    )
  }


  //@ts-ignore
  filterHandler = (value:AssetReportFilter) => (event:any)=>{
    let {selectedFilters} =this.state
    let index = selectedFilters.findIndex(elm => elm==value)
    if(index == -1)
      selectedFilters.push(value)
    else
      selectedFilters.splice(index,1)
    this.setState({selectedFilters})
    if(selectedFilters.length == 0)
      this.props.cancelARFiltering()
    else
      this.props.filterAssetReport(selectedFilters)  
  }

  handlePrinting = ()=>{
    printReport(PRINT_Id)
  }

  public render() {
    let {loading,error,errorMessage,report,filteredReport,filtering} = this.props.assetReportState
    let {selectedFilters} = this.state
    return (
      <div style={{maxWidth:'98%',margin:'auto',marginTop:'15px'}}>  
      <Grid container spacing={8}>
          <Grid item xs={9}>
            <Grid container spacing={8}>
              <div style={{
                width:"100%",
                height:'91vh',
                maxWidth:"100%",
                marginRight:'5px',
                overflowY:'auto',
                overflowX:'auto',
                border:'1px solid lightgray',
                backgroundColor:'#fff'}}
                id={PRINT_Id}  
                >
                {
                  loading &&
                  <div style={{width:'20px',margin:'auto',marginTop:'40vh'}}>
                    <LoadingCircle/>
                  </div>
                }
                {
                  !loading &&
                  error &&
                  <ErrorPage message={errorMessage}/>
                }
                {
                  !loading &&
                  !error &&
                  !filtering &&
                  this.assetTable(report)
                }
                {
                  !loading &&
                  !error &&
                  filtering &&
                  this.assetTable(filteredReport)
                }
              </div>
            </Grid>
          </Grid>
        <Grid item xs={3}>
           <Grid container spacing={8}>
           <div style={{width:"100%",height:'85vh',maxHeight:'85vh',overflowY:'auto',marginBottom:'10px',border:'1px solid lightgray',backgroundColor:'#fff'}}>
                    <Typography component="h2" style={{padding:'5px 15px 5px 15px'}} variant="headline" gutterBottom>
                      Filters
                    </Typography>
                    <Divider />
                    <List
                        component="nav"
                        dense={true}
                        style={{backgroundColor:'inherit'}}
                        subheader={<ListSubheader component="div">Asset Types : </ListSubheader>}
                    >
                        <ListItem button dense>
                          <Checkbox
                            tabIndex={-1}
                            onChange={this.filterHandler(AssetReportFilter.PIPELINE)}
                            disableRipple
                            style={{color:PRIMARY}}
                          />
                          <ListItemText primary={`Pipeline`}/>
                        </ListItem>
                        <ListItem button dense>
                          <Checkbox
                            tabIndex={-1}
                            style={{color:PRIMARY}}
                            onChange={this.filterHandler(AssetReportFilter.VALVE)}
                            />
                          <ListItemText primary={`Valve`}/>
                        </ListItem>
                        <ListItem button dense>
                          <Checkbox
                            tabIndex={-1}
                            style={{color:PRIMARY}}
                            onChange={this.filterHandler(AssetReportFilter.BOREHOLE)}
                          />
                          <ListItemText primary={`Borehole`}/>
                        </ListItem>
                        <ListItem button dense>
                          <Checkbox
                            tabIndex={-1}
                            style={{color:PRIMARY}}
                            onChange={this.filterHandler(AssetReportFilter.ENDPOINTS)}
                          />
                          <ListItemText primary={`End Pionts`}/>
                        </ListItem>
                        <ListItem button dense>
                          <Checkbox
                            tabIndex={-1}
                            style={{color:PRIMARY}}
                            onChange={this.filterHandler(AssetReportFilter.RESERVIOR)}
                          />
                          <ListItemText primary={`Reservior`}/>
                        </ListItem>
                        <ListItem button dense>
                          <Checkbox
                            tabIndex={-1}
                            style={{color:PRIMARY}}
                            onChange={this.filterHandler(AssetReportFilter.HYDRANTS)}
                          />
                          <ListItemText primary={`Hydrants`}/>
                        </ListItem>
                        <ListItem button dense>
                          <Checkbox
                            tabIndex={-1}
                            style={{color:PRIMARY}}
                            onChange={this.filterHandler(AssetReportFilter.REDUCE)}
                          />
                          <ListItemText primary={`Reduce`}/>
                        </ListItem>
                        <ListItem button dense>
                          <Checkbox
                            tabIndex={-1}
                            style={{color:PRIMARY}}
                            onChange={this.filterHandler(AssetReportFilter.HOUSE_CONNECTIONS)}
                          />
                          <ListItemText primary={`House Connections`}/>
                        </ListItem>
                        <ListItem button dense>
                          <Checkbox
                            tabIndex={-1}
                            style={{color:PRIMARY}}
                            onChange={this.filterHandler(AssetReportFilter.WATER_METER)}
                          />
                          <ListItemText primary={`Water Meter`}/>
                        </ListItem>
                    </List>
                    <Divider />
                    <List
                        component="nav"
                        dense={false}
                        subheader={<ListSubheader component="div">Assets With : </ListSubheader>}
                    >
                        <ListItem button dense>
                          <Checkbox
                            tabIndex={-1}
                            style={{color:PRIMARY}}
                            onChange={this.filterHandler(AssetReportFilter.WithOngoingTask)}
                            disableRipple
                          />
                          <ListItemText primary={`Ongoing tasks `}/>
                        </ListItem>
                        <ListItem button dense>
                          <Checkbox
                            style={{color:PRIMARY}}
                            tabIndex={-1}
                            onChange={this.filterHandler(AssetReportFilter.WithCompletedTask)}
                          />
                          <ListItemText primary={`Completed Tasks`}/>
                        </ListItem>
                        <ListItem button dense>
                          <Checkbox
                            tabIndex={-1}
                            style={{color:PRIMARY}}
                            onChange={this.filterHandler(AssetReportFilter.WithCancelledTasks)}
                          />
                          <ListItemText primary={`Cancelled Tasks`}/>
                        </ListItem>
                        <ListItem button dense>
                          <Checkbox
                            tabIndex={-1}
                            style={{color:PRIMARY}}
                            onChange={this.filterHandler(AssetReportFilter.WithTerminatedTasks)}
                          />
                          <ListItemText primary={`Terminated Tasks`}/>
                        </ListItem>
                    </List>
           </div>
           </Grid>
           <Grid container justify={'center'} spacing={8}>
              <Grid xs={12} > 
                  <Button
                          onClick={this.handlePrinting}
                          variant={'contained'}
                          color={'secondary'}
                          style={{borderRadius:'1px',marginBottom:'5px' }}
                          fullWidth
                  >
                    Print Report
                  </Button>
                {/* <a href={`${baseUrl}asset/asset-report-file/Pdf/${selectedFilters.join(';')}`} target="blank">
                  <Button
                    variant={'contained'}
                    color={'secondary'}
                    style={{borderRadius:'1px',marginBottom:'5px' }}
                    fullWidth
                  >
                    Download PDF
                  </Button>
                </a>
              </Grid>
              <Grid xs={12}> 
                <a href={`${baseUrl}asset/asset-report-file/Excel/${selectedFilters.join(';')}`} target="blank">
                  <Button
                    variant={'contained'}
                    color={'primary'}
                    style={{borderRadius:'1px', marginBottom:'5px'}}
                    fullWidth
                  >
                    Download Excel
                  </Button>
                </a> */}
              </Grid>
           </Grid>
        </Grid>
      </Grid>
    </div>
    )
  }
}

const mapStateToProps = (state:any) => ({
  assetReportState: state.assetReport
})


function mapDispatchToProps(dispatch:any){
  return {
    cancelARFiltering : () => dispatch(cancelAssetFilter()),
    getAssetReports : ()=>dispatch(getAssetReports()),
    filterAssetReport : (filters : AssetReportFilter[])=>dispatch(filterAssetReport(filters))
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(AssetReport)
