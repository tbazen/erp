import React, { Component, Fragment } from 'react'
import '../report.scss'
import { Button, Paper, Table, TableBody, TableCell, TableHead, TableRow, Grid, Typography, List, ListSubheader, ListItem, Checkbox, ListItemText, Divider } from '@material-ui/core'
import { connect } from 'react-redux'
import { EmployeeReportModel } from '../../../../_infrastructure/model/employee-report-model'
import { baseUrl } from '../../../../_setup/services/url.config';
import { EmployeeReportState } from '../../../../_infrastructure/state/employee-report-state';
import { getEmployeeReports, EmployeeFilter, cancelEmployeeFilter, filterEmployeeReport } from '../../../../_setup/actions/employee-report-actions';
import LoadingCircle from '../../components/loading/loading-circle';
import ErrorPage from '../../components/error/error-page';
import { PRIMARY } from '../../_helper/color-container/color-container';
import printReport from '../../../_helper/print';


const PRINT_Id = 'printEmployeeReport'
interface PropsFromState {
  employeeReportState: EmployeeReportState
}

interface PropsFromDispatch {
  getEmployeeReports: () => void
  cancelFiltering : () => void
  filterEmployeeReport : (filter : EmployeeFilter[]) => void
}
interface IState{
  selectedFilters : EmployeeFilter[]
}

type AllProps = PropsFromDispatch & PropsFromState

class EmployeeReport extends Component <AllProps, IState> {
  constructor(props: any) {
    super(props)
    this.state={
      selectedFilters:[]
    }
    this.employeeTable = this.employeeTable.bind(this)
    this.getData = this.getData.bind(this)
  }

  getData(report:EmployeeReportModel[]) {
    if(report != undefined && report != [])
      return(
      <>
        {
            report.map((report: EmployeeReportModel, key: number) => {
            return (
              <TableRow hover key={key}>
                <TableCell align="center" >{key+1}</TableCell>
                <TableCell align="center" >{report.employeeId}</TableCell>
                <TableCell align="center" >{report.name}</TableCell>
                <TableCell align="center" >{report.onGoingTasks}</TableCell>
                <TableCell align="center" >{report.completedTasks}</TableCell>
                <TableCell align="center" >{report.cancelledTasks}</TableCell>
                <TableCell align="center" >{report.terminatedTasks}</TableCell>
                <TableCell align="center" >
                                      {`${report.completedTasks+
                                          report.cancelledTasks+
                                          report.terminatedTasks  
                                      } / ${report.totalTasks}`}
                </TableCell>
                <TableCell align="center" >{report.totalTasks}</TableCell>
              </TableRow>
            )
          })}
      </>
      )
    else
      return (
        <>
          <TableRow >
          </TableRow>
        </>
      )
  }

  //@ts-ignore
  filterHandler = (value:EmployeeFilter) => (event:any)=>{
      let {selectedFilters} =this.state
      let index = selectedFilters.findIndex(elm => elm==value)
      
      if(index == -1)
        selectedFilters.push(value)
      else
        selectedFilters.splice(index,1)
      this.setState({selectedFilters})
      if(selectedFilters.length == 0)
        this.props.cancelFiltering()
      else
        this.props.filterEmployeeReport(selectedFilters)  
  }

  async componentDidMount() {
    await this.props.getEmployeeReports()
  }

  employeeTable(report : EmployeeReportModel[]) {
    return (
      <Table>
        <TableHead>
          <TableRow>
            <TableCell align="center">No. </TableCell>
            <TableCell align="center">Employee Id</TableCell>
            <TableCell align="center" >Employee Name</TableCell>
            <TableCell align="center">Ongoing Tasks</TableCell>
            <TableCell align="center">Completed Tasks</TableCell>
            <TableCell align="center">Cancelled Tasks</TableCell>
            <TableCell align="center">Terminated Tasks</TableCell>
            <TableCell align="center"> Employee Efficiency</TableCell>
            <TableCell align="center">Total Tasks</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {this.getData(report)}
        </TableBody>
      </Table>
    )
  }


  handlePrinting = ()=>{
    printReport(PRINT_Id)
  }
  
  public render() {
    let {loading,error,errorMessage,report,filteredReport,filtering} = this.props.employeeReportState
    let {selectedFilters} = this.state
    return (
      <div style={{maxWidth:'98%',margin:'auto',marginTop:'15px'}}>  
        <Grid container spacing={8}>
            <Grid item xs={9}>
              <Grid container spacing={8}>
                <div style={{
                  width:"100%",
                  height:'91vh',
                  maxWidth:"100%",
                  marginRight:'5px',
                  overflowY:'auto',
                  overflowX:'auto',
                  border:'1px solid lightgray',
                  backgroundColor:'#fff'}}
                  id={PRINT_Id}  
                  >
                  {
                    loading &&
                    <div style={{width:'20px',margin:'auto',marginTop:'40vh'}}>
                      <LoadingCircle/>
                    </div>
                  }
                  {
                    !loading &&
                    error &&
                    <ErrorPage message={errorMessage}/>
                  }
                  {
                    !loading &&
                    !error &&
                    !filtering &&
                    this.employeeTable(report)
                  }
                  {
                    !loading &&
                    !error &&
                    filtering &&
                    this.employeeTable(filteredReport)
                  }
                </div>
              </Grid>
            </Grid>
          <Grid item xs={3}>
            <Grid container spacing={8}>
            <div style={{width:"100%",height:'85vh',marginBottom:'10px',border:'1px solid lightgray',backgroundColor:'#fff'}}>
              <Typography component="h2" style={{padding:'5px 15px 5px 15px'}} variant="headline" gutterBottom>
                Filters
              </Typography>
              <Divider />
              <List
                  component="nav"
                  dense={true}
                  subheader={<ListSubheader component="div">Employee Assignment : </ListSubheader>}
              >
                  <ListItem button dense>
                    <Checkbox
                      onChange={this.filterHandler(EmployeeFilter.UnAssignedEmployees)}
                      tabIndex={-1}
                      value={0}
                      style={{color:PRIMARY}}
                      disableRipple
                    />
                    <ListItemText primary={`Unassigned Employees `}/>
                  </ListItem>
                  <ListItem button dense>
                    <Checkbox
                      onChange={this.filterHandler(EmployeeFilter.AssignedEmplyees)}
                      tabIndex={-1}
                      value={1}
                      style={{color:PRIMARY}}
                    />
                    <ListItemText primary={`Assigned Employees`}/>
                  </ListItem>
              </List>
              <Divider />
              <List
                  component="nav"
                  dense={false}
                  subheader={<ListSubheader component="div">Employees With : </ListSubheader>}
              >
                  <ListItem button dense>
                    <Checkbox
                      tabIndex={-1}
                      onChange={this.filterHandler(EmployeeFilter.WithOngoingTask)}
                      value={2}
                      style={{color:PRIMARY}}
                      disableRipple
                    />
                    <ListItemText primary={`Ongoing tasks `}/>
                  </ListItem>
                  <ListItem key={4} button dense>
                    <Checkbox
                      value={3}
                      onChange={this.filterHandler(EmployeeFilter.WithCompletedTask)}
                      tabIndex={-1}
                      style={{color:PRIMARY}}
                    />
                    <ListItemText primary={`Completed Tasks`}/>
                  </ListItem>
                  <ListItem key={5} button dense>
                    <Checkbox
                      value={4}
                      onChange={this.filterHandler(EmployeeFilter.WithCancelledTasks)}
                      tabIndex={-1}
                      style={{color:PRIMARY}}
                    />
                    <ListItemText primary={`Cancelled Tasks`}/>
                  </ListItem>
                  <ListItem key={6} button dense>
                    <Checkbox
                      value={5}
                      onChange={this.filterHandler(EmployeeFilter.WithTerminatedTasks)}
                      tabIndex={-1}
                      style={{color:PRIMARY}}
                    />
                    <ListItemText primary={`Terminated Tasks`}/>
                  </ListItem>
              </List>
            </div>
            </Grid>
            <Grid container justify={'center'} spacing={8}>
                <Grid xs={12} > 
                  
                <Button
                          onClick={this.handlePrinting}
                          variant={'contained'}
                          color={'secondary'}
                          style={{borderRadius:'1px',marginBottom:'5px' }}
                          fullWidth
                  >
                    Print Report
                  </Button>
{/*                   

                  <a href={`${baseUrl}task/employee-report-file/Pdf/${selectedFilters.join(';')}`} target="blank">
                    <Button
                      variant={'contained'}
                      color={'secondary'}
                      style={{borderRadius:'1px',marginBottom:'5px' }}
                      fullWidth
                    >
                      Download PDF
                    </Button>
                  </a>
                </Grid>
                <Grid xs={12}> 
                  <a href={`${baseUrl}task/employee-report-file/Excel/${selectedFilters.join(';')}`} target="blank">
                    <Button
                      variant={'contained'}
                      color={'primary'}
                      style={{borderRadius:'1px', marginBottom:'5px'}}
                      fullWidth
                    >
                      Download Excel
                    </Button>
                  </a> */}
                </Grid>
            </Grid>
            </Grid>
        </Grid>
      </div>
    )
  }
}

const mapStateToProps = (state:any) => ({
  employeeReportState: state.employeeReport
})
function mapDispatchToProps(dispatch:any){
  return {
    cancelFiltering : () => dispatch(cancelEmployeeFilter()),
    getEmployeeReports : ()=>dispatch(getEmployeeReports()),
    filterEmployeeReport : (filters : EmployeeFilter[])=>dispatch(filterEmployeeReport(filters))
  }
}
export default connect(mapStateToProps,mapDispatchToProps)(EmployeeReport)
