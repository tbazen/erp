import React, { Component } from 'react'
import './report.scss'
import { Paper } from '@material-ui/core'
import { PieChart, Pie, Tooltip } from 'recharts'
import AppBar from '@material-ui/core/AppBar/AppBar'
import Tabs from '@material-ui/core/Tabs/Tabs'
import Tab from '@material-ui/core/Tab/Tab'
import TaskReport from './task-report/task-report'
import EmployeeReport from './employee-report/employee-report'
import TeamReport from './team-report/team-report'
import AssetReport from './asset-report/assets-report'
import { connect } from 'react-redux';
import { changeActiveIndex } from '../../../_setup/actions/drawer-actions';
import { IUserInformation } from '../../../_infrastructure/model/userInformationModel';
import Role from '../_helper/role-util/roles';
import { OfficerItems } from '../_helper/slideout-index/officer-items';
import { SupervisorItems } from '../_helper/slideout-index/supervisor-items';



interface PropsFromDispatch{
  changeActiveIndex:(index:number)=>void
}

interface PropsFromState{
  user : IUserInformation
}

type AllProps = PropsFromDispatch & 
                PropsFromState

class Report extends Component<AllProps> {

  state = {
    value: 0
  }

  constructor(props: any) {
    super(props)

    this.handleChange = this.handleChange.bind(this)
  }

  componentWillMount(){
        let {user} = this.props
        if(user.role == Role.TECHNICAL_OFFICER)
            this.props.changeActiveIndex(OfficerItems.REPORT)
        else if(user.role == Role.TECHNICAL_SUPERVISOR)
            this.props.changeActiveIndex(SupervisorItems.REPORT) 
      }

  //@ts-ignore
  handleChange(e: any, value: number) {
    this.setState({ value })
  }

  TabContainer(props: any) {
    return <div>{props.children}</div>
  }


  public render() {
    return (
      <div>
        <AppBar 
                position="sticky" 
                color={'default'}>
          <Tabs
            value={this.state.value}
            indicatorColor="primary"
            textColor="primary"
            onChange={this.handleChange}
          >
            <Tab label="Task Report" />
            <Tab label="Asset Report" />
            <Tab label="Employee Report" />
            <Tab label="Team Report" />
          </Tabs>
        </AppBar>
        {/* <div className={'tab-container'}> */}
          {this.state.value === 0 && <TaskReport />}
          {this.state.value === 1 && (
            <this.TabContainer>
              <AssetReport />
            </this.TabContainer>
          )}
          {this.state.value === 2 && (
            <this.TabContainer>
              <EmployeeReport />
            </this.TabContainer>
          )}
          {this.state.value === 3 && (
            <this.TabContainer>
              <TeamReport />
            </this.TabContainer>
          )}
        {/* </div> */}
      </div>
    )
  }
}


function mapStateToProps(state:any){
  return {
    user: state.login.userInfo
  }
}

function mapDispatchToProps(dispatch:any){
  return {
          changeActiveIndex:(index:number)=>dispatch(changeActiveIndex(index))
  }
}


export default connect(mapStateToProps,mapDispatchToProps)(Report)