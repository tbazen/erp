import React, { Component, Fragment} from 'react'
import '../report.scss'
import { Button, Table, TableBody, TableCell, TableHead, TableRow, Grid, Typography, Divider, List, ListSubheader, ListItem, Checkbox, ListItemText } from '@material-ui/core'
import { connect } from 'react-redux'
import { TaskReportModel, TRMetadata } from '../../../../_infrastructure/model/task-report-model'
import { baseUrl } from '../../../../_setup/services/url.config';
import { TaskReportState } from '../../../../_infrastructure/state/task-report-state';
import LoadingCircle from '../../components/loading/loading-circle';
import ErrorPage from '../../components/error/error-page';
import { getTaskReports, TaskReportFilter, cancelTaskFilter, filterTaskReport } from '../../../../_setup/actions/task-report-actions';
import ReportCard from '../../components/report-card/report-card';
import { IReportCard, IEntry } from '../../components/report-card/report-card';
import printReport from './../../../_helper/print';
import { PRIMARY } from '../../_helper/color-container/color-container';
import './style.scss'

const PRINT_Id = 'printTaskReport'
interface PropsFromState {
  taskReportState: TaskReportState
}

interface PropsFromDispatch {
  getTaskReports: () => void
  cancelTRFiltering : () => void
  filterTaskReport : (filter : TaskReportFilter[]) => void
}

interface IProps {

}

type AllProps = IProps & PropsFromDispatch & PropsFromState

interface IState{
  selectedFilters : TaskReportFilter[]
}

class TaskReport extends Component <AllProps, IState> {
  constructor(props: any) {
    super(props)
    this.state={
      selectedFilters:[]
    }
    this.taskTable = this.taskTable.bind(this)
    this.getData = this.getData.bind(this)
  }

  async componentDidMount() {
    await this.props.getTaskReports()
  }


  //@ts-ignore
  filterHandler = (value:TaskReportFilter) => (event:any)=>{
    let {selectedFilters} =this.state
    let index = selectedFilters.findIndex(elm => elm==value)
    if(index == -1)
      selectedFilters.push(value)
    else
      selectedFilters.splice(index,1)
    this.setState({selectedFilters})
    if(selectedFilters.length == 0)
      this.props.cancelTRFiltering()
    else
      this.props.filterTaskReport(selectedFilters)  
  }

  getData(report : TaskReportModel[]) {
    if(report != undefined && report != [])
      return(
        <>
        {
          report.map((report: TaskReportModel, key: number) => {
            return (
              <TableRow hover key={key}>
                <TableCell align="left">{key+1}</TableCell>
                <TableCell align="left">{report.name}</TableCell>
                <TableCell align="left">{report.type}</TableCell>
                <TableCell align="left">{report.status}</TableCell>
                <TableCell align="left">{report.assigneeType}</TableCell>
                <TableCell align="left">{report.assigneeName}</TableCell>
                <TableCell align="left">{report.startDate}</TableCell>
                <TableCell align="left">{report.endDate}</TableCell>
                {/* <TableCell align="left">{report.delayDays}</TableCell> */}
              </TableRow>
            )
        })}
        </>
      )
    else
      return (
        <>
          <TableRow>
          </TableRow>
        </>
      )
  }

  generateCompatibleType = (input : TRMetadata) :IEntry[]=>{
    const total = input.ongoing+
                  input.cancelled+
                  input.completed+
                  input.terminated

    return [
      {
        name:'Ongoing',
        value:input.ongoing+'',
        percentage: total==0?0 : (input.ongoing*100) / total
      },
      {
        name:'Completed',
        value:input.completed+'',
        percentage: total==0? 0 : (input.completed*100) / total
      },
      {
        name:'Terminated',
        value:input.terminated+'',
        percentage: total==0? 0 : (input.terminated*100) / total
      },
      {
        name:'Cancelled',
        value:input.cancelled+'',
        percentage: total==0? 0 : (input.cancelled*100) / total
      }
    ]
  }


  taskTable(report : TaskReportModel[]) {
    const {periodicTasks,incidentTasks,scheduledTasks,unscheduledTasks} = this.props.taskReportState.report
    
    const totalPrTasks = periodicTasks.ongoing+
                         periodicTasks.completed+
                         periodicTasks.cancelled+
                         periodicTasks.terminated
    
    const totalIncTasks = incidentTasks.ongoing+
                         incidentTasks.completed+
                         incidentTasks.cancelled+
                         incidentTasks.terminated       
    
    const totalScheTasks = scheduledTasks.ongoing+
                         scheduledTasks.completed+
                         scheduledTasks.cancelled+
                         scheduledTasks.terminated
   
    const totalUnscheTasks = unscheduledTasks.ongoing+
                         unscheduledTasks.completed+
                         unscheduledTasks.cancelled+
                         unscheduledTasks.terminated

    return (
        <Fragment>
        <Grid item xs ={12}>
          <div style={{
            display:'flex',
          }}>
            <div className={'report-card'}>
              <ReportCard 
                        title={'Periodic Tasks'} 
                        content={this.generateCompatibleType(periodicTasks)}
                        footer={{name:'Total',value:totalPrTasks}}
                        />
            </div>
            <div className={'report-card'}>
              <ReportCard 
                        title={'Incident Tasks'} 
                        content={this.generateCompatibleType(incidentTasks)}
                        footer={{name:'Total',value:totalIncTasks}}/>
            </div>
            
            <div className={'report-card'}>
              <ReportCard 
                          title={'Scheduled Tasks'} 
                          content={this.generateCompatibleType(scheduledTasks)}
                          footer={{name:'Total',value:totalScheTasks}}/>
            </div>

            <div className={'report-card'}>
              <ReportCard 
                          title={'Unscheduled Tasks'} 
                          content={this.generateCompatibleType(unscheduledTasks)}
                          footer={{name:'Total',value:totalUnscheTasks}}/>
            </div>

          </div>
        </Grid>
          <div style={{
                border:'1px solid lightgray',
                marginTop:'5px',
                width:'100%',
                overflowX:'auto'
                }}
                >
              <Table>
                <TableHead>
                  <TableRow>
                    <TableCell align="center">No. </TableCell>
                    <TableCell align="center">Name</TableCell>
                    <TableCell align="center">Type</TableCell>
                    <TableCell align="center">Task Status</TableCell>
                    <TableCell align="center">Assignee Type</TableCell>
                    <TableCell align="center">Assignee Name</TableCell>
                    <TableCell align="center">Start Date</TableCell>
                    <TableCell align="center">End Date</TableCell>
                    {/* <TableCell align="center">Delay Date</TableCell> */}
                  </TableRow>
                </TableHead>
                <TableBody>
                    {
                      this.getData(report)
                    }
                </TableBody>
              </Table>
          </div>
      </Fragment>
    )
  }

  handlePrinting = ()=>{
    printReport(PRINT_Id)
  }

  public render() {
    let {loading,error,errorMessage,report,filteredReport , filtering} = this.props.taskReportState
    let {selectedFilters} = this.state    
    return (
    <div style={{maxWidth:'98%',margin:'auto',marginTop:'15px'}}>  
      <Grid container spacing={8}>
          <Grid item xs={9}>
            <Grid container spacing={8}>
              <div
                style={{
                width:"100%",
                height:'91vh',
                maxWidth:"100%",
                marginRight:'5px',
                overflowY:'auto',
                overflowX:'auto',
                backgroundColor:'#fff'
                }}
                id={PRINT_Id}  
                >              
                  {
                    loading &&
                    <div style={{width:'20px',margin:'auto',marginTop:'40vh'}}>
                      <LoadingCircle/>
                    </div>
                  }
                  {
                    !loading &&
                    error &&
                    <ErrorPage message={errorMessage}/>
                  }
                  {
                    !loading &&
                    !error &&
                    !filtering &&
                    this.taskTable(report.reports)
                  }
                  {
                    !loading &&
                    !error &&
                    filtering &&
                    this.taskTable(filteredReport)
                  }
              </div>
            </Grid>
          </Grid>
        <Grid item xs={3}>
           <Grid container spacing={8}>
           <div style={{width:"100%",height:'85vh',maxHeight:'85vh',overflowY:'auto',marginBottom:'10px',border:'1px solid lightgray',backgroundColor:'#fff'}}>

              <Typography component="h2" style={{padding:'5px 15px 5px 15px'}} variant="headline" gutterBottom>
                Filters
              </Typography>
              <Divider />
              <List
                  component="nav"
                  style={{backgroundColor:'inherit'}}
                  dense={true}
                  subheader={<ListSubheader component="div">Assignee Type : </ListSubheader>}
              >
                  <ListItem button dense>
                    <Checkbox
                      tabIndex={-1}
                      style={{color:PRIMARY}}
                      onChange ={this.filterHandler(TaskReportFilter.EMPLOYEE)}
                      disableRipple
                    />
                    <ListItemText primary={`Employee`}/>
                  </ListItem>
                  <ListItem button dense>
                    <Checkbox
                    style={{color:PRIMARY}}
                      tabIndex={-1}
                      onChange ={this.filterHandler(TaskReportFilter.TEAM)}
                    />
                    <ListItemText primary={`Team`}/>
                  </ListItem>
              </List>
              <Divider />
              <List
                  component="nav"
                  dense={false}
                  style={{backgroundColor:'inherit'}}
                  subheader={<ListSubheader component="div">Task State : </ListSubheader>}
              >
                  <ListItem button dense>
                    <Checkbox
                      tabIndex={-1}
                      style={{color:PRIMARY}}
                      onChange={this.filterHandler(TaskReportFilter.ONGOING_TASKS)}
                      disableRipple
                    />
                    <ListItemText primary={`Ongoing tasks `}/>
                  </ListItem>
                  <ListItem button dense>
                    <Checkbox
                      tabIndex={-1}
                      style={{color:PRIMARY}}
                      onChange ={this.filterHandler(TaskReportFilter.COMPLETED_TASTKS)}
                    />
                    <ListItemText primary={`Completed Tasks`}/>
                  </ListItem>
                  <ListItem button dense>
                    <Checkbox
                      tabIndex={-1}
                      style={{color:PRIMARY}}
                      onChange ={this.filterHandler(TaskReportFilter.CANCELLED_TASKS)}
                    />
                    <ListItemText primary={`Cancelled Tasks`}/>
                  </ListItem>
                  <ListItem button dense>
                    <Checkbox
                      tabIndex={-1}
                      style={{color:PRIMARY}}
                      onChange ={this.filterHandler(TaskReportFilter.TERMINATED_TASKS)}
                    />
                    <ListItemText primary={`Terminated Tasks`}/>
                  </ListItem>
              </List>
              <Divider/>
              <List
                  component="nav"
                  dense={false}
                  style={{backgroundColor:'inherit'}}
                  subheader={<ListSubheader component="div">Task Type : </ListSubheader>}
              >
                  <ListItem button dense>
                    <Checkbox
                      tabIndex={-1}
                      disableRipple
                      style={{color:PRIMARY}}
                      onChange ={this.filterHandler(TaskReportFilter.UNSCHEDULED_TASKS)}
                    />
                    <ListItemText primary={`Unscheduled Tasks`}/>
                  </ListItem>
                  <ListItem button dense>
                    <Checkbox
                      tabIndex={-1}
                      style={{color:PRIMARY}}
                      onChange ={this.filterHandler(TaskReportFilter.SCHEDULED_TASKS)}
                    />
                    <ListItemText primary={`Scheduled Tasks`}/>
                  </ListItem>
                  <ListItem button dense>
                    <Checkbox
                      tabIndex={-1}
                      style={{color:PRIMARY}}
                      onChange ={this.filterHandler(TaskReportFilter.PERIODIC_TASKS)}
                    />
                    <ListItemText primary={`Periodic Tasks`}/>
                  </ListItem>
                  <ListItem button dense>
                    <Checkbox
                      tabIndex={-1}
                      style={{color:PRIMARY}}
                      onChange ={this.filterHandler(TaskReportFilter.INCIDENT_TASKS)}
                    />
                    <ListItemText primary={`Incident Tasks`}/>
                  </ListItem>
              </List>
           </div>
           </Grid>
           <Grid container justify={'center'} spacing={8}>
              <Grid xs={12} > 
                {/* <a href={`${baseUrl}task/task-report-file/Pdf/${selectedFilters.join(';')}`} target="blank"> */}
                  <Button
                          onClick={this.handlePrinting}
                          variant={'contained'}
                          color={'secondary'}
                          style={{borderRadius:'1px',marginBottom:'5px' }}
                          fullWidth
                  >
                    Print Report
                  </Button>
                {/* </a> */}
              </Grid>
              <Grid xs={12}> 
                {/* <a href={`${baseUrl}task/task-report-file/Excel/${selectedFilters.join(';')}`} target="blank">
                  <Button
                    variant={'contained'}
                    color={'primary'}
                    style={{borderRadius:'1px', marginBottom:'5px'}}
                    fullWidth
                  >
                    Download Excel
                  </Button>
                </a> */}
                {/* <Button
                    variant={'contained'}
                    color={'primary'}
                    style={{borderRadius:'1px', marginBottom:'5px'}}
                    fullWidth
                  >
                    Print Report
                </Button> */}
              </Grid>
           </Grid>
        </Grid>
      </Grid>
    </div>  
    )
  }
}

const mapStateToProps = (state:any) => ({
  taskReportState: state.taskReport
})

function mapDispatchToProps(dispatch:any){
  return {
    cancelTRFiltering : () => dispatch(cancelTaskFilter()),
    getTaskReports : ()=>dispatch(getTaskReports()),
    filterTaskReport : (filters : TaskReportFilter[])=>dispatch(filterTaskReport(filters))
  }
}
export default connect(mapStateToProps,mapDispatchToProps)(TaskReport)