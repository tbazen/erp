import React, { Component } from 'react'
import '../report.scss'
import { Button, Paper, Table, TableBody, TableCell, TableHead, TableRow, Grid, Typography, Divider, List, ListSubheader, ListItem, Checkbox, ListItemText } from '@material-ui/core'
import { connect } from 'react-redux'
import { TeamReportModel } from '../../../../_infrastructure/model/team-report-model'
import { baseUrl } from '../../../../_setup/services/url.config';
import { TeamReportState } from 'src/_infrastructure/state/team-report-state';
import { getTeamReports, TeamReportFilter, cancelTeamFilter, filterTeamReport } from '../../../../_setup/actions/team-report-actions';
import LoadingCircle from '../../components/loading/loading-circle';
import ErrorPage from '../../components/error/error-page';
import { PRIMARY } from '../../_helper/color-container/color-container';
import printReport from '../../../_helper/print';


const PRINT_Id = 'printTeamReport'
interface PropsFromState {
  teamReportState: TeamReportState
}

interface PropsFromDispatch {
  getTeamReports: () =>  void,
  cancelTRFiltering : () => void,
  filterTeamReport : (filters : TeamReportFilter[]) => void
}

interface IProps {

}

interface IState{
  selectedFilters : TeamReportFilter[]
}

type AllProps = IProps & PropsFromDispatch & PropsFromState

class TeamReport extends Component <AllProps, IState> {
  constructor(props: any) {
    super(props)
    this.state= {
      selectedFilters:[]
    }
    this.teamTable = this.teamTable.bind(this)
    this.getData = this.getData.bind(this)
  }

  getData(report : TeamReportModel[]) {
    if(report != undefined && report != [])
      return(
      <>
        {
          report.map((report: TeamReportModel, key: number) => {
            return (
              <TableRow hover key={key}>
                {/* <TableCell align="center" >{report.id}</TableCell> */}
                <TableCell align="center" >{key+1}</TableCell>
                <TableCell align="center" >{report.name}</TableCell>
                <TableCell align="center" >{report.dateCreated}</TableCell>
                <TableCell align="center" >{report.noOfMembers}</TableCell>
                <TableCell align="center" >{report.onGoingTasks}</TableCell>
                <TableCell align="center" >{report.completedTasks}</TableCell>
                <TableCell align="center" >{report.cancelledTasks}</TableCell>
                <TableCell align="center" >{report.terminatedTasks}</TableCell>

                <TableCell align="center" >
                                      {`${report.completedTasks+
                                          report.cancelledTasks+
                                          report.terminatedTasks  
                                      } / ${report.totalTasks}`}
                </TableCell>

                <TableCell align="center" >{report.totalTasks}</TableCell>
              </TableRow>
            )
          })}
      </>
      )
    else
      return (
        <>
          <TableRow>
            {/*<i style={{width: 'fit-content', margin: 'auto'}}>NO DATA</i>*/}
          </TableRow>
        </>
      )
  }

  async componentDidMount() {
    await this.props.getTeamReports()
  }


  teamTable(report : TeamReportModel[]) {
    return (
      <Table>
        <TableHead>
          <TableRow>
            <TableCell align="center">No. </TableCell>
            <TableCell align="center">Name</TableCell>
            <TableCell align="center">Date Created</TableCell>
            <TableCell align="center">Team Members</TableCell>
            <TableCell align="center">On Going Tasks</TableCell>
            <TableCell align="center">Completed Tasks</TableCell>
            <TableCell align="center">Cancelled Tasks</TableCell>
            <TableCell align="center">Terminated Tasks</TableCell>

            <TableCell align="center" >Team Efficiency</TableCell>
            
            <TableCell align="center">Total Tasks</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {this.getData(report)}
        </TableBody>
      </Table>
    )
  }


  //@ts-ignore
  filterHandler = (value:TeamReportFilter) => (event:any)=>{
    let {selectedFilters} =this.state
    let index = selectedFilters.findIndex(elm => elm==value)
    if(index == -1)
      selectedFilters.push(value)
    else
      selectedFilters.splice(index,1)
    this.setState({selectedFilters})
    if(selectedFilters.length == 0)
      this.props.cancelTRFiltering ()
    else
      this.props.filterTeamReport(selectedFilters)  
  }


  handlePrinting = ()=>{
    printReport(PRINT_Id)
  }


  public render() {
    let {loading,error,errorMessage, filtering ,report,filteredReport} = this.props.teamReportState
    let {selectedFilters} = this.state
    return (
      <div style={{maxWidth:'98%',margin:'auto',marginTop:'15px'}}>  
      <Grid container spacing={8}>
          <Grid item xs={9}>
            <Grid container spacing={8}>
              <div style={{
                width:"100%",
                height:'91vh',
                maxWidth:"100%",
                marginRight:'5px',
                overflowY:'auto',
                overflowX:'auto',
                border:'1px solid lightgray',
                backgroundColor:'#fff'}}
                id={PRINT_Id}  
                >
                {
                    loading &&
                    <div style={{width:'20px',margin:'auto',marginTop:'40vh'}}>
                      <LoadingCircle/>
                    </div>
                }
                {
                    !loading &&
                    error &&
                    <ErrorPage message={errorMessage}/>
                }
                {
                  !loading &&
                  !error &&
                  !filtering &&
                  this.teamTable(report)
                }
                {
                  !loading &&
                  !error &&
                  filtering &&
                  this.teamTable(filteredReport)
                }
              </div>
            </Grid>
          </Grid>
        <Grid item xs={3}>
           <Grid container spacing={8}>
           <div style={{width:"100%",height:'85vh',marginBottom:'10px',border:'1px solid lightgray',backgroundColor:'#fff'}}>           
                  <Typography component="h2" style={{padding:'5px 15px 5px 15px'}} variant="headline" gutterBottom>
                    Filters
                  </Typography>
                  <Divider />
                  <List
                      component="nav"
                      dense={true}
                      subheader={<ListSubheader component="div">Team Assignment : </ListSubheader>}
                  >
                      <ListItem button dense>
                        <Checkbox
                          tabIndex={-1}
                          onChange={this.filterHandler(TeamReportFilter.UnAssignedTeams)}
                          style={{color:PRIMARY}}
                          disableRipple
                        />
                        <ListItemText primary={`Unassigned Teams `}/>
                      </ListItem>
                      <ListItem button dense>
                        <Checkbox
                          tabIndex={-1}
                          style={{color:PRIMARY}}
                          onChange={this.filterHandler(TeamReportFilter.AssignedTeams)}
                        />
                        <ListItemText primary={`Assigned Teams`}/>
                      </ListItem>
                  </List>
                  <Divider />
                  <List
                      component="nav"
                      dense={false}
                      subheader={<ListSubheader component="div">Teams With : </ListSubheader>}
                  >
                      <ListItem button dense>
                        <Checkbox
                          tabIndex={-1}
                          onChange={this.filterHandler(TeamReportFilter.WithOngoingTask)}
                          disableRipple
                          style={{color:PRIMARY}}
                        />
                        <ListItemText primary={`Ongoing tasks `}/>
                      </ListItem>
                      <ListItem button dense>
                        <Checkbox
                          onChange={this.filterHandler(TeamReportFilter.WithCompletedTask)}
                          tabIndex={-1}
                          style={{color:PRIMARY}}
                        />
                        <ListItemText primary={`Completed Tasks`}/>
                      </ListItem>
                      <ListItem button dense>
                        <Checkbox
                          tabIndex={-1}
                          onChange={this.filterHandler(TeamReportFilter.WithCancelledTasks)}
                          style={{color:PRIMARY}}
                        />
                        <ListItemText primary={`Cancelled Tasks`}/>
                      </ListItem>
                      <ListItem button dense>
                        <Checkbox
                          tabIndex={-1}
                          style={{color:PRIMARY}}
                          onChange={this.filterHandler(TeamReportFilter.WithTerminatedTasks)}
                        />
                        <ListItemText primary={`Terminated Tasks`}/>
                      </ListItem>
                  </List>
           </div>
           </Grid>
           <Grid container justify={'center'} spacing={8}>
              <Grid xs={12} >

                <Button
                            onClick={this.handlePrinting}
                            variant={'contained'}
                            color={'secondary'}
                            style={{borderRadius:'1px',marginBottom:'5px' }}
                            fullWidth
                    >
                      Print Report
                    </Button>
{/* 
                <a href={`${baseUrl}team/team-report-file/Pdf/${selectedFilters.join(';')}`} target="blank">
                  <Button
                    variant={'contained'}
                    color={'secondary'}
                    style={{borderRadius:'1px',marginBottom:'5px' }}
                    fullWidth
                  >
                    Download PDF
                  </Button>
                </a>
              </Grid>
              <Grid xs={12}> 
                <a href={`${baseUrl}team/team-report-file/Excel/${selectedFilters.join(';')}`} target="blank">
                  <Button
                    variant={'contained'}
                    color={'primary'}
                    style={{borderRadius:'1px', marginBottom:'5px'}}
                    fullWidth
                  >
                    Download Excel
                  </Button>
                </a> */}
              </Grid>
           </Grid>

        </Grid>
      </Grid>
    </div>
    )
  }
}

const mapStateToProps = (state:any) => ({
  teamReportState: state.teamReport
})
function mapDispatchToProps(dispatch:any){
  return {
    cancelTRFiltering : () => dispatch(cancelTeamFilter()),
    getTeamReports : ()=>dispatch(getTeamReports()),
    filterTeamReport : (filters : TeamReportFilter[])=>dispatch(filterTeamReport(filters))
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(TeamReport)