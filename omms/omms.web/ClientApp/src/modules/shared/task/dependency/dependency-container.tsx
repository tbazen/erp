import React,{Component} from 'react'
import TaskTree from './dependency-tree';
import { getTaskDepndecy } from '../../../../_setup/actions/taskActions';

interface IState{
    loading : boolean
    error : boolean
    errorMessage : string | undefined
}

interface IProps{
    taskId:string
}

type AllProps = IProps

class DependencyContainer extends Component<AllProps,IState>{
    constructor(props:AllProps){
        super(props)
        this.state={
            loading:true,
            error:false,
            errorMessage:undefined
        }
    }
    componentWillMount(){
        let {taskId} = this.props
        let dependencyRequest = getTaskDepndecy(taskId)
        dependencyRequest
        .then(node => {
            let tree = new TaskTree(node)
            console.group('DEPENDENCY TREE LOADED')
            console.log(node)
            console.log('Tree : ')
            console.log(tree)
           // tree.traverseTree()
        })
        .catch(error => {
            this.setState({loading:false,error:true,errorMessage:error})
        })    
    }
    render(){
        return (
            <div id="task-dependecy-container"></div>
        )
    }
}

export default DependencyContainer