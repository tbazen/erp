import TaskNode from './task-node';
import { CreateDomElement } from './dom-element-creator';

export default class TaskTree {
  root: TaskNode | null;
  parent: string;
  constructor(node: TaskNode | null = null) {
    this.root = node;
    this.parent=''
    this.getParent = this.getParent.bind(this)
    this.getParentHelper = this.getParentHelper.bind(this)

    this.traverseHelper = this.traverseHelper.bind(this)
    this.traverseTree = this.traverseTree.bind(this)
  }
  
  traverseTree() {
    if (this.root != null) 
      this.traverseHelper(this.root);
  }

  getParent(child: TaskNode) {
    this.parent = '';
    this.getParentHelper(child.data.id == null ? child.data.wfid as string : child.data.id, this.root as TaskNode);
    return this.parent;
  }

  getParentHelper(child: string, node: TaskNode) {
    node.children.forEach(nd => {
      if(nd.data.id != null){
          if(nd.data.id == child)
             this.parent = node.data.id as string
          else
             this.getParentHelper(child,nd)   
      }  
      else{
          if (nd.data.wfid == child) 
             this.parent = node.data.wfid as string
          else
             this.getParentHelper(child,nd)  
      }
    });
  }

  private traverseHelper(node: TaskNode) {
    if (this.root != null) {
      if (this.root.data.id === node.data.id && this.root.data.wfid === node.data.wfid) {
        CreateDomElement(node.data, 'empty', true , true);
      } else {
        this.getParent(node);
        CreateDomElement(node.data, this.parent, node.children.length > 0);
      }
    }
    node.children.forEach(n => this.traverseHelper(n));
  }
}
