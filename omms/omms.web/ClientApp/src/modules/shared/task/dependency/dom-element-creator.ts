import { TaskResponse } from '../../../../_infrastructure/model/task-model';
export function CreateDomElement(
    task: TaskResponse,
    parentId: string,
    isParent:  boolean,
    isRoot: boolean = false,
    recursiveTreeContiner: string = 'recursive-tree-container'
    ) {
  
      let childContainer: HTMLElement
      let textNode = document.createElement('p')
      textNode.innerText = task.description;
      
      if (isRoot) {
        let treeContainer = document.getElementById(recursiveTreeContiner);
        if (treeContainer != null) {
          let rootElement = document.createElement('LI');
          childContainer = document.createElement('UL');
          childContainer.setAttribute('id', task.id == null ? task.wfid as string : task.id );
          rootElement.appendChild(textNode);
          treeContainer.appendChild(rootElement);
          treeContainer.appendChild(childContainer);
        }
      } else {
        let parent = document.getElementById(parentId) as HTMLElement;
        let newElement = document.createElement('LI');
        newElement.appendChild(textNode);
        if (isParent) {
          childContainer = document.createElement('UL');
          childContainer.setAttribute('id', task.id == null ? task.wfid as string : task.id);
          parent.appendChild(newElement);
          parent.appendChild(childContainer);
        } else {
          parent.appendChild(newElement);
        }
      }
}