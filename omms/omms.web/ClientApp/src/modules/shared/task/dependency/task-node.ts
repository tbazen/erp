import { TaskResponse } from '../../../../_infrastructure/model/task-model';
export default class TaskNode {
    data: TaskResponse;
    children: TaskNode[];
    constructor(data: TaskResponse, children: TaskNode[] = []) {
      this.data = data;
      this.children = children;
    }
  }
  