import React, { Component } from 'react'
import { Paper } from '@material-ui/core'
import { NoteResponse } from '../../../../_infrastructure/model/task-note-model'
import './../../components/note-card/note-card.scss'
import DocumentService from '../../../../_setup/services/document.service';
import './note.scss'
import Grid from '@material-ui/core/Grid'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faClipboard } from '@fortawesome/free-solid-svg-icons'
import Typography from '@material-ui/core/Typography';

interface IProps {
   note: NoteResponse
}

interface IState{
    noteImages : Blob[]
    imageIndex : number
}
type AllProps = IProps

let document_service = new DocumentService()

class NoteCard extends Component<AllProps,IState> {
    constructor(props:AllProps){
        super(props)
        this.state = {
            noteImages : [],
            imageIndex : 0
        }
        this.loadImages = this.loadImages.bind(this)
        this.imageDisplay = this.imageDisplay.bind(this)
        this.prevImage = this.prevImage.bind(this)
        this.nextImage = this.nextImage.bind(this)
    }

    componentDidMount(){
        this.loadImages()
    }
    async loadImages() {
        let {images} = this.props.note
        for (let i = 0; i < images.length; i++) {
            let image = await document_service.loadImage(images[i].id)
            let { noteImages } = this.state
            noteImages = [...noteImages, image]
            this.setState({ noteImages })
        }
    }

    prevImage(){
        let {imageIndex} = this.state
        if(imageIndex > 0)
          this.setState({imageIndex:imageIndex-1})
    }
    nextImage(){
        let {imageIndex} = this.state
        console.log(this.state)
        let {images} = this.props.note
        if(imageIndex < images.length-1)
          this.setState({imageIndex:imageIndex+1})
          console.log(this.state)
    }
    imageDisplay(blobData: Blob) {
        let urlObject = URL.createObjectURL(blobData);
        return (
            <div className="container">
                <a href={urlObject} target="_blank">
                  <img style={{width:"100%"}} src={urlObject}></img>
                </a>
                <button onClick={this.prevImage} className="left-btn">Prev</button>
                <button onClick={this.nextImage} className="right-btn">Next</button>
            </div>
        )
    }


    render() {
        let {note} = this.props
        let ndate = note.date
        let date= new Date(ndate.toString())
        let {noteImages} = this.state
        return (
        <Grid item lg={4} md={6} xs={12}>
            <Paper style={{padding:"10px"}}>
                <table>
                    <tr>
                        <td>
                            <FontAwesomeIcon size={'2x'} title="Loaction" icon={faClipboard}/>
                        </td>
                        <td>
                            <Typography component='h3' variant="headline" style={{fontWeight:"bolder",paddingLeft:"10px",paddingTop:"10px"}}>
                            {note.header}
                            </Typography>
                        </td>
                    </tr>
                </table>
                
                <Typography component='h6' variant="headline">
                    Date : { `${date.getFullYear()} - ${date.getMonth()} - ${date.getDate()}` }
                </Typography>
                <div>
                    {   
                        noteImages.length > 0 &&
                        this.imageDisplay(noteImages[this.state.imageIndex])
                    }
                </div>
                <Typography variant="body1" style={{fontSize:"1.25em"}}>
                {note.note}
                </Typography>
                <Typography component="caption" align="left">
                 Note by : {note.noteBy.userName}
                </Typography>
            </Paper>
        </Grid>
        )
    }
}

export default NoteCard