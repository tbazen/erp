import React,{Component} from 'react'
import NoteCard from './note-card';
import { Fab } from '@material-ui/core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import NoteFormModal from './note-form-modal';
import { connect } from 'react-redux';
import { NoteResponse } from '../../../../_infrastructure/model/task-note-model';
import './../../components/view-task/view-task.scss'
import { IUserInformation } from 'src/_infrastructure/model/userInformationModel';
import Role from '../../_helper/role-util/roles'
import Grid from '@material-ui/core/Grid';
import { NoteState } from '../../../../_infrastructure/state/note-state';
import LoadingPage from '../../components/loading/loading-page';
import EmptyContent from '../../components/empty/empty-list-container';
import ErrorPage from '../../components/error/error-page';
import UIContainer from '../../_helper/ui-helper/containers-util';

interface IProps{
    taskId : string
}

interface PropsFromState{
    note:NoteState,
    userInfo: IUserInformation
}

type AllProps = IProps &
                PropsFromState

interface IState{
    openNoteForm : boolean
}

class NoteContainer extends Component<AllProps,IState>{
    constructor(props:AllProps){
        super(props)
        this.state = {
            openNoteForm : false
        }
        this.handleNoteModal = this.handleNoteModal.bind(this)
    }
    handleNoteModal(){
        this.setState({openNoteForm : !this.state.openNoteForm})
    }

    render(){
        let {note} = this.props
        return(
            <div className={'notes-container'}>
                {
                    note.loading &&
                    <LoadingPage/>
                }
                {
                    !note.loading &&
                    note.error &&
                    <ErrorPage message={note.errorMessage}/>
                }
                <Grid style={{width:"98%",margin:"auto",marginTop:"5px"}} container justify="flex-start" spacing={8}>
                {
                    !note.loading &&
                    !note.error &&
                    note.notes.length > 0 &&
                    note.notes.map((value:NoteResponse,key:number)=><NoteCard key={key} note={value}/>)
                }
                {
                    !note.loading &&
                    !note.error &&
                    !(note.notes.length > 0) &&
                    <div style={{margin:'auto'}}>
                      <EmptyContent message="Notes are not added for this task"/>
                    </div>
                }
                </Grid>

                <NoteFormModal
                               taskId ={this.props.taskId}
                               openForm={this.state.openNoteForm}
                               closeHandler ={this.handleNoteModal}
                               container = {UIContainer.NOTE_LIST_CONTAINER}/>
                {
                    this.props.userInfo.role == Role.EMPLOYEE &&
                    <Fab className={'create-note-btn'} onClick={this.handleNoteModal}>
                    <FontAwesomeIcon icon={faPlus} size={'1x'} />
                    </Fab>
                }
            </div>
        )
    }
}

function mapStateToProps(state : any){
    return {
        note : state.note,
        userInfo: state.login.userInfo
    }
}
export default connect(mapStateToProps)(NoteContainer)