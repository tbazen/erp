import React,{Component} from 'react'
import { Modal, Input } from '@material-ui/core';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import { NoteRequest } from '../../../../_infrastructure/model/task-note-model';
import Grid from '@material-ui/core/Grid';
import { DocumentRequest } from '../../../../_infrastructure/model/documentModel';
import DeleteIcon from '@material-ui/icons/Delete';
import IconButton from '@material-ui/core/IconButton';
import { getBase64 } from '../../../_helper/file-encode';
import { createNote } from './../../../../_setup/actions/noteActions';
import { connect } from 'react-redux';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField/TextField';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrash } from '@fortawesome/free-solid-svg-icons'
import UIContainer from '../../_helper/ui-helper/containers-util';

interface IProps{
    openForm : boolean,
    closeHandler : ()=>void,
    taskId : string,
    done?: () => void,
    container : UIContainer
}


interface PropsFromDispatch{
  createNote : (note : NoteRequest,container:UIContainer) => void
}

type AllProps = IProps & 
                PropsFromDispatch

interface IState{
    note : NoteRequest
}

class NoteFormModal extends Component<AllProps,IState>{
    constructor(props:AllProps){
        super(props)
        this.state = {
            note:{
                id:null,
                taskId: this.props.taskId,
                header: '',
                note: '',
                date:  null,
                noteBy:  null,
                images: []
            }
        }
        this.closeModal = this.closeModal.bind(this)
        this.submitNote = this.submitNote.bind(this)
        this.fileRow = this.fileRow.bind(this)
        this.handleFileChange = this.handleFileChange.bind(this)
        this.addFile = this.addFile.bind(this)
        this.removeFile = this.removeFile.bind(this)
        this.handleInput = this.handleInput.bind(this)
    }

    handleInput=(name:string)=>(event:any)=>{
        let {note} = this.state
        note[name] = event.target.value
        this.setState({note})
    }

    fileRow(value:DocumentRequest , key:number){
      return (
        <Grid style={{
                  padding:"5px",
                  marginBottom:"5px",
                  border:"1px solid lightgray"  
              }}
              container 
              key={key} 
              justify="center">
           <Grid item xs={11} >
             <p style={{paddingTop:"10px"}}>{value.Filename}</p> 
           </Grid>
           <Grid item xs={1}>
              <IconButton onClick = {this.removeFile(key)} >
                 <DeleteIcon/>
              </IconButton>
           </Grid>
        </Grid>
      )
    }

    async handleFileChange(){
      let file = (document.getElementById('noteImageInput') as HTMLInputElement).files;
        if(file!=null)
        {
            let base64 = await getBase64(file[0] as Blob); 
            let doc: DocumentRequest =  {
                Date:file[0].lastModified,
                Ref: '',
                Note: '',
                Mimetype: file[0].type,
                Type:  null,
                Filename: file[0].name,
                File: base64,
                Id:  null,
                OverrideFilePath: 'api/documents/',
            }

            this.addFile(doc)
            file = null
        }
    }

    addFile(doc:DocumentRequest){
      let {note} = this.state
      note.images.push(doc)
      this.setState({note})
    }

    removeFile = (index:number)=>()=>{
      let {note} = this.state
      note.images.splice(index,1)
      this.setState({note})
    }

    closeModal(){
        this.props.closeHandler()
    }


    submitNote(){
      let {note} = this.state
      this.props.createNote(note,this.props.container)

      if(this.props.done != undefined)
        this.props.done()
      this.props.closeHandler()
    }

    render(){
        let {note} = this.state
        return (
            <Modal
                    aria-labelledby="create-note"
                    aria-describedby="allows-creation-of-notes"
                    open={this.props.openForm}
                    onClose={this.closeModal}
                    // onEscapeKeyDown={this.escapeClicked}
                  >
                    <Paper style={{margin:"auto",maxHeight:"80vh",overflowY:'auto',width:"60%",padding:"20px",height:"80vh",marginTop:"10vh"}}>
                      <Typography component="h2" variant="display1" >
                        Add Note
                      </Typography>
                      <div  style={{
                        marginBottom:"10px"
                      }} className="header">
                        <TextField
                          placeholder={'Note Header'}                          
                          variant="outlined"
                          onChange={this.handleInput('header')}
                          value={note.header}
                          className={'input'}
                          fullWidth
                        />
                      </div>
                      <div style={{
                        marginBottom:"10px"
                      }} className="note-content">
                        <TextField
                          name={'note'}
                          placeholder={'write note here...'}
                          onChange={this.handleInput('note')}
                          value={note.note}
                          variant="outlined"
                          className={'input'}
                          multiline={true}
                          rows={8}
                          rowsMax={15}
                          fullWidth
                        />
                      </div>
                      <div style={{
                        marginBottom:"10px"
                      }}>
                      {this.state.note.images.map(this.fileRow)}
                      </div>
                      <input id='noteImageInput' type="file" onChange={this.handleFileChange}/>
                      <div style={{
                        marginTop:"10px"
                      }}>
                        <Button
                          variant={'contained'}
                          color={'primary'}
                          className={'done-btn'}
                          type={'submit'}
                          size={'medium'}
                          onClick={this.submitNote}
                        >
                          Done
                        </Button>
                      </div>
                    </Paper>
                  </Modal>
        )
    }
}

function mapStateToProps(){
  return {

  }
}
export default connect(mapStateToProps,{createNote})(NoteFormModal)