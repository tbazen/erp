import React, { Component } from 'react'
import TaskContainer from './../../components/pending/pending-task-container'
import { connect } from 'react-redux'
import { viewAllTasks, filteringTasks, cancelTaskFiltering } from '../../../../_setup/actions/taskActions';
import AppBar from '@material-ui/core/AppBar/AppBar';
import Grid from '@material-ui/core/Grid/Grid';
import SearchField from '../../components/search-bar/search-field';
import Tabs from '@material-ui/core/Tabs/Tabs';
import Tab from '@material-ui/core/Tab/Tab';
import { getTaskByStatus } from './../../../../_setup/actions/taskActions';
import { TaskStatus, TaskResponse } from '../../../../_infrastructure/model/task-model';
import { filterTask } from '../../../../_setup/actions/filter-actions';
import { TaskState } from '../../../../_setup/reducer/taskReducer';
import UIContainer from '../../_helper/ui-helper/containers-util';
import { loadAllTeams } from '../../../../_setup/actions/teamActions';
import { loadAllEmployees } from '../../../../_setup/actions/employeeActions';

interface IProps{

}

interface PropsFromState{
  task :TaskState
}
interface PropsFromDispatch{
    viewAllTasks:()=>void,
    getTaskByStatus:(state:number) => void 

    filteringTasks: ()=>void,
    cancelTaskFiltering : ()=>void,
    filterTask:(name:string,tasks : TaskResponse[])=>void

    loadAllTeams:()=>void,
    loadAllEmployees:()=>void
}

type AllProps = IProps & 
                PropsFromState &
                PropsFromDispatch


interface IState {
  value: number
}

class RegisteredTaskContainer extends Component<AllProps,IState> {
    constructor(props:AllProps) {
        super(props)
        this.state = {
          value: 0
      }
      this.handleFiltering = this.handleFiltering.bind(this)
      this.handleChange = this.handleChange.bind(this)
      this.startApiCall = this.startApiCall.bind(this)
    }

    componentDidMount(){
      this.props.getTaskByStatus(TaskStatus.ONGOING)
      /**
       * loading list of employee and list of regestered teams 
       * for later use in the edit assignee page...
       */
      this.props.loadAllEmployees()
      this.props.loadAllTeams()
    }
    //@ts-ignore
    handleFiltering(event:any){
      let {task} = this.props
        let name:string  = event.target.value
        if(name.length == 0 ){
            this.props.cancelTaskFiltering()
        }
        else{
            this.props.filteringTasks()
            this.props.filterTask(name,task.tasks)
        }
    }
    //@ts-ignore
    handleChange(event: any, value: number) {
      this.setState({ value })
      this.startApiCall(value)
    }
    startApiCall(state:number){
      switch(state){
        case 0:
          this.props.getTaskByStatus(TaskStatus.ONGOING)
        break

        case 1:
          this.props.getTaskByStatus(TaskStatus.COMPLETED)
        break
        
        case 2:
          this.props.getTaskByStatus(TaskStatus.CANCELLED)
        break
        
        case 3:
          this.props.getTaskByStatus(TaskStatus.TERMINATED)
        break
      }
    } 

    render() {
      const { value } = this.state
      return (
        <div>
          <AppBar position="sticky" color="default">
            <Grid container spacing={8}> 
                <Grid item lg={7} md={12} xs={12}>                    
                    <Tabs 
                          value={value} 
                          indicatorColor="primary"
                          textColor="primary"
                          onChange={this.handleChange}>
                        <Tab label="On going"/>
                        <Tab label="Completed" />
                        <Tab label="Cancelled" />
                        <Tab label="Terminated" />
                    </Tabs>
                </Grid>
                <Grid item lg={5} md={12} xs={12}>
                    <SearchField handler={this.handleFiltering}/>
                </Grid>
            </Grid>
          </AppBar>
                  {value === 0 && <TaskContainer  container={UIContainer.REGISTARED_TASK_CONTAINER}  withSearchBar={false}/>}
                  {value === 1 && <TaskContainer  container={UIContainer.REGISTARED_TASK_CONTAINER}  withSearchBar={false}/>}
                  {value === 2 && <TaskContainer  container={UIContainer.REGISTARED_TASK_CONTAINER}  withSearchBar={false}/>}
                  {value === 3 && <TaskContainer  container={UIContainer.REGISTARED_TASK_CONTAINER}  withSearchBar={false}/>}
       </div>
    )
  }
}
function mapStateToProps(state:any){
    return {
      task : state.tasks,
    }
}

function mapDispatchToProps(dispatch:any){
  return {
    viewAllTasks:() => dispatch(viewAllTasks()),
    getTaskByStatus:(state:number) => dispatch(getTaskByStatus(state)),

    //Dispatchers for task filtering
    filteringTasks : ()=>dispatch(filteringTasks()),
    cancelTaskFiltering : ()=>dispatch(cancelTaskFiltering()),
    filterTask : (name:string,tasks : TaskResponse[])=>dispatch(filterTask(name,tasks)), 
    
    //Dispatcher for getting employee and team list
    loadAllTeams:()=>dispatch(loadAllTeams()),
    loadAllEmployees:()=>dispatch(loadAllEmployees())
  }
}
export default connect(mapStateToProps,mapDispatchToProps)(RegisteredTaskContainer)