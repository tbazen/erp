import React,{Component} from 'react'
import Modal from '@material-ui/core/Modal/Modal'
import CreateTask from './../create-task/create-task'

interface IProps{
    openForm:boolean,
    handleClose:()=>void,
    assetId : number | null
}

type AllProps = IProps

export default class TaskFromModal extends Component<AllProps>{
   constructor(props:AllProps) {
        super(props)
    }

    render(){
        return (
            <Modal
                open={this.props.openForm}
                onClose={this.props.handleClose}
            >
                <CreateTask assetId={this.props.assetId}/>
            </Modal>
        )
    }
}