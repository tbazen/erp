import React , {Component} from 'react'
import { connect } from 'react-redux';
import TaskContainer from './../components/pending/pending-task-container';
import { getTeamTasks, filteringTasks, cancelTaskFiltering, markTeamTasksAsSeen } from '../../../_setup/actions/taskActions';
import { changeActiveIndex } from '../../../_setup/actions/drawer-actions';
import UIContainer from '../_helper/ui-helper/containers-util';
import { TaskState } from '../../../_setup/reducer/taskReducer';
import { TaskResponse } from 'src/_infrastructure/model/task-model';
import { AppBar, Grid, Tabs, Tab, Badge } from '@material-ui/core';
import SearchField from '../components/search-bar/search-field';
import { filterTask } from 'src/_setup/actions/filter-actions';
import { MetaData } from '../../../_infrastructure/model/metadata-model';

interface PropsFromState{
    task :TaskState
    metadata : MetaData
}
interface PropsFromDispatch{
    getTeamTasks : ()=>void
    changeActiveIndex:(index:number)=>void

    filteringTasks: ()=>void,
    cancelTaskFiltering : ()=>void,
    filterTask:(name:string,tasks : TaskResponse[])=>void

    markTeamTasksAsSeen : () => void
}

type AllProps = PropsFromState &
                PropsFromDispatch

interface IState {
    value: number
}                

class TeamTaskContainer extends Component<AllProps,IState>{
    
    constructor(props:AllProps) {
        super(props)
        this.state = {
            value: 0
        }
        this.handleFiltering = this.handleFiltering.bind(this)
        this.handleChange = this.handleChange.bind(this)
    }

    //@ts-ignore
    handleFiltering(event:any){
        let {task} = this.props
        let name:string  = event.target.value
        if(name.length == 0 ){
            this.props.cancelTaskFiltering()
        }
        else{
            this.props.filteringTasks()
            this.props.filterTask(name,task.tasks)
        }
    }
    //@ts-ignore
    handleChange(event: any, value: number) {
        this.setState({ value })

        value==1 && 
        this.props.markTeamTasksAsSeen()

        value == 0 &&
        this.props.getTeamTasks()
    }

    componentWillMount(){
        this.props.changeActiveIndex(1)
    }
    componentDidMount(){
        this.props.getTeamTasks()
    }
    render(){
        let {value} = this.state
        let {metadata} = this.props
        return (
            <div>
                <AppBar position="sticky" color="default">
                    <Grid container spacing={8}> 
                        <Grid item lg={5} md={12} xs={12}>                    
                            <Tabs 
                                value={value} 
                                indicatorColor="primary"
                                textColor="primary"
                                onChange={this.handleChange}>
                                <Tab label="Active Tasks"/>
                                <Tab label={ <Badge color="secondary" badgeContent={metadata.newTeamTasks} style={{padding:'5px'}}>New Tasks</Badge>}/>
                            </Tabs>
                        </Grid>
                        <Grid item lg={7} md={12} xs={12}>
                            <SearchField handler={this.handleFiltering}/>
                        </Grid>
                    </Grid>
                </AppBar>
                        {value === 0 && <TaskContainer  container={UIContainer.REGISTARED_TASK_CONTAINER}  withSearchBar={false}/>}
                        {value === 1 && <TaskContainer  container={UIContainer.REGISTARED_TASK_CONTAINER}  withSearchBar={false}/>}
            </div>
        )
    }
}

function mapStateToProps(state:any){
    return {
        task : state.tasks,
        metadata : state.metadata
    }
}

function mapDispatchToProps(dispatch:any){
    return {
        getTeamTasks:()=>dispatch(getTeamTasks()),
        changeActiveIndex:(index:number)=>dispatch(changeActiveIndex(index)),

         //Dispatchers for task filtering
        filteringTasks : ()=>dispatch(filteringTasks()),
        cancelTaskFiltering : ()=>dispatch(cancelTaskFiltering()),
        filterTask : (name:string,tasks : TaskResponse[])=>dispatch(filterTask(name,tasks)),

        markTeamTasksAsSeen : ()=>dispatch(markTeamTasksAsSeen())
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(TeamTaskContainer)