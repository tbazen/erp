import React, { Component,Fragment } from 'react'
import Card from '@material-ui/core/Card'
import CardActionArea from '@material-ui/core/CardActionArea'
import CardActions from '@material-ui/core/CardActions'
import CardContent from '@material-ui/core/CardContent'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'
import { connect } from 'react-redux'
import { TeamModel } from '../../../_infrastructure/model/teamModel'
import TeamFormModal from './team-form-modal'
import { IUserInformation } from '../../../_infrastructure/model/userInformationModel';
import Role from '../_helper/role-util/roles';
import TeamMembersModal from './team-members-modal';
import { approveTeamRegistration, rejectTeamRegistration } from '../../../_setup/actions/teamActions';
import { cancelRegistrationRequest } from '../../../_setup/actions/workflow-actions';
import { Link } from 'react-router-dom';
import Grid from '@material-ui/core/Grid';
import UIContainer from './../_helper/ui-helper/containers-util'
import Color from '../_helper/color-container/index';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faStickyNote, faUsers, faInfo } from '@fortawesome/free-solid-svg-icons';
import { openNoteModal } from '../../../_setup/actions/wfnote-modal-actions';
import { WFModalState } from 'src/_infrastructure/state/workflow-note-modal-state';
import { WorkflowTypes } from '../../../_infrastructure/model/workflow-model';
import { Divider } from '@material-ui/core';

interface IProps {
  team: TeamModel
  container : UIContainer
}
interface PropsFromState{
  activeUser:IUserInformation
  noteState:WFModalState
}

interface PropsFromDispatch{
  rejectTeamRegistration:(wfid:string)=>void
  approveTeamRegistration:(wfid:string)=>void
  cancelRegistrationRequest:(wfid:string,container:UIContainer)=>void

  openNoteModal : (wfid:string,WFType : WorkflowTypes) => void
}

type AllProps = IProps &
                PropsFromState &
                PropsFromDispatch

interface IState{
  openMembersDialog : boolean,
  openFormModal:boolean
}

class TeamCard extends Component<AllProps,IState> {
  state = {
    openMembersDialog: false,
    openFormModal : false
  }
  constructor(props: any) {
    super(props)
    //card constructors
    this.getRegisteredTeamActions = this.getRegisteredTeamActions.bind(this)
    this.getPendingTeamActions = this.getPendingTeamActions.bind(this)

    //dialog controllers
    this.handleMembersDialog = this.handleMembersDialog.bind(this)
    this.handleFormDialog = this.handleFormDialog.bind(this)

    //actions handlers
    this.handleApprove = this.handleApprove.bind(this)
    this.handleReject = this.handleReject.bind(this)
    this.handleCancel = this.handleCancel.bind(this)
    
    this.handleOpenNoteModal = this.handleOpenNoteModal.bind(this)
  }

  handleFormDialog(){
    this.setState({openFormModal:!this.state.openFormModal})
  }
  
  handleMembersDialog() {
    this.setState({ openMembersDialog: !this.state.openMembersDialog })
  }

  handleApprove(){
    if(this.props.team.wfid!= null)
      this.props.approveTeamRegistration(this.props.team.wfid)
  }

  handleReject(){
    if(this.props.team.wfid!= null)
        this.props.openNoteModal(this.props.team.wfid,WorkflowTypes.Team)
  }
  
  handleOpenNoteModal():boolean{
    return true
  }


  handleCancel(){
    let {container} = this.props
    if(this.props.team.wfid!=null)
      this.props.cancelRegistrationRequest(this.props.team.wfid,container)    
  }



  getRegisteredTeamActions(){
    let { role } = this.props.activeUser
    return role == Role.TECHNICAL_OFFICER && 
           <CardActions>
             <Button onClick={this.handleFormDialog} size="small" variant={'outlined'} style={{backgroundColor:Color.SECONDARY,color:Color.PRIMARY_FOREGROUND}}>
                Edit Team
             </Button>
           </CardActions>
  }
  
  getPendingTeamActions() {
    let { role } = this.props.activeUser
    if (role == Role.TECHNICAL_SUPERVISOR)
        return (
            <CardActions>
                <Button onClick={this.handleApprove} size='small' style={{backgroundColor:Color.HAPPY,color:Color.PRIMARY_FOREGROUND}} variant={'outlined'}>
                    Approve
                </Button>
                <Button onClick={this.handleReject} size='small' style={{backgroundColor:Color.SECONDARY,color:Color.PRIMARY_FOREGROUND}} variant={'outlined'} color="secondary">
                    Reject
                </Button>
            </CardActions>
        )
    if (role == Role.TECHNICAL_OFFICER)
        return (
            <CardActions>
              <Button 
                      style={{borderRadius:'2px',color:Color.PRIMARY_FOREGROUND,backgroundColor:Color.HAPPY}}  
                      onClick={this.handleFormDialog} 
                      size="small" 
                      variant={'outlined'}>
                  Edit Team
              </Button>
              <Button 
                      style={{borderRadius:'2px',color:Color.PRIMARY_FOREGROUND,backgroundColor:Color.SECONDARY}}
                      onClick={this.handleCancel}  
                      size="small" 
                      variant={'outlined'}>
                  Cancel Request
              </Button>
            </CardActions>
        )
    return <React.Fragment />
}

  render() {
    let { team,container } = this.props
    return (
      <Grid item lg={4} md={6} xs={12}>
        <Card>
          <CardActionArea>
            <CardContent>
              <table style={{width:'100%'}}>
                <tr>
                    <td>
                        <FontAwesomeIcon size={'lg'} title="Name" icon={faUsers}/>
                    </td>
                    <td>
                        <Typography gutterBottom component="h2" variant={'headline'}>
                                {team.name}
                        </Typography>
                    </td>
                </tr>
                <tr>
                  <td colSpan={2}>
                    <Divider/>
                  </td>
                </tr>
                <tr>
                    <td>
                        <FontAwesomeIcon size={'lg'} title="Note" icon={faInfo}/>
                    </td>
                    <td>
                        <Typography component="p" color="textPrimary" >
                                {team.description}
                        </Typography>
                    </td>
                </tr>
              {
                team.note != '' &&
                team.note != null &&
                <tr>
                    <td>
                        <FontAwesomeIcon size={'lg'} title="Note" icon={faStickyNote}/>
                    </td>
                    <td>
                        <Typography component='h6' variant="subtitle1" color="textSecondary" >
                                {team.note}
                        </Typography>
                    </td>
                </tr>
              }
              </table>
            </CardContent>
          </CardActionArea>
          <CardActions>
            <Button size="small" variant={'outlined'} onClick={this.handleMembersDialog} style={{borderRadius:'2px',backgroundColor:Color.PRIMARY,color:Color.PRIMARY_FOREGROUND}}>
              Members
            </Button>
            {
              this.props.team.id != null &&
              <Link to={`/team-detail/${team.wfid}/${team.id}`}>
                  <Button size="small" style={{backgroundColor:Color.HAPPY,color:Color.PRIMARY_FOREGROUND,borderRadius:'2px'}}>
                    More...
                  </Button>
              </Link>
            }
            {
              container == UIContainer.PENDING_CONTAINER || 
              container == UIContainer.REJECTED_CONTAINER
              ?
              this.getPendingTeamActions()
              :
              this.getRegisteredTeamActions()
            }
          </CardActions>
        </Card>
        <TeamMembersModal
          openForm={this.state.openMembersDialog}
          team={team}
          handleClose={this.handleMembersDialog}
        />
        <TeamFormModal
                      container = {container}
                      openForm={this.state.openFormModal}
                      team={team}
                      handleClose={this.handleFormDialog} />
      </Grid>
    )
  }
}
function mapStateToProps(state:any) {
  return {
    activeUser : state.login.userInfo,
    noteState :state.noteModal
  }
}

function mapDispatchToProps(dispatch:any){
  return {
    rejectTeamRegistration : (wfid:string)=>dispatch(rejectTeamRegistration(wfid)),
    approveTeamRegistration:(wfid:string)=>dispatch(approveTeamRegistration(wfid)),
    cancelRegistrationRequest:(wfid:string,container:UIContainer)=>dispatch(cancelRegistrationRequest(wfid,container)),
    openNoteModal: (wfid:string,WFType : WorkflowTypes)=>dispatch(openNoteModal(wfid,WFType))
  }
}
export default connect(mapStateToProps,mapDispatchToProps)(TeamCard)
