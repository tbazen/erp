import React, { Component } from 'react'
import { connect } from 'react-redux'
import TeamCard from './team-card'
import { TeamModel } from '../../../_infrastructure/model/teamModel'
import { TeamState } from '../../../_infrastructure/state/teamState';
import EmptyContent from '../components/empty/empty-list-container';
import LoadingPage from '../components/loading/loading-page';
import ErrorPage from '../components/error/error-page';
import SearchBar from '../components/search-bar/search-bar';
import { filteringTeams, cancelTeamFiltering } from '../../../_setup/actions/teamActions';
import { filterTeam } from '../../../_setup/actions/filter-actions';
import Grid from '@material-ui/core/Grid';
import UIContainer from '../_helper/ui-helper/containers-util';

interface IProps {
  withSearchBar:boolean
  container : UIContainer
}

interface PropsFromState {
  team: TeamState,
  filteredTeams : TeamModel[]
}

interface PropsFromDispatch{
  filteringTeams : ()=>void,
  cancelTeamFiltering : ()=>void,
  filterTeam: (name:string,teams : TeamModel[])=>void
}

type AllProps = IProps & 
                PropsFromState &
                PropsFromDispatch

class TeamContainer extends Component<AllProps> {
  constructor(props:AllProps) {
    super(props)
    this.handleFiltering = this.handleFiltering.bind(this)
  }

  handleFiltering(event:any){
    let {team} = this.props
    let name:string  = event.target.value
    if(name.length == 0 )
      this.props.cancelTeamFiltering()
    else{
        this.props.filteringTeams()
        this.props.filterTeam(name,team.teams)
    }
  }
  
  render() {
    let {team,filteredTeams,withSearchBar,container} =this.props
    return (
      <div>
        {
          withSearchBar &&
          <SearchBar handler = {this.handleFiltering}/>
        }
        {
          team.loading &&
          <LoadingPage/>
        }
        {
          !team.loading &&
          team.error &&
          <ErrorPage message={team.errorMessage}/>  
        }
        {
          !team.loading &&
          !team.error &&
          team.filtering &&
          filteredTeams.length > 0 &&          
          <Grid style={{width:"98%",margin:'auto',marginTop:"5px"}} container spacing={8} >
            {filteredTeams.map((value:TeamModel,key:number)=><TeamCard key={key} container={container} team={value}/>)}
          </Grid>
        }
        {
          !team.loading &&
          !team.error &&
          team.filtering &&
          !(filteredTeams.length > 0) &&
          <EmptyContent message ="No match found!"/>
        }
        {
          !team.loading &&
          !team.error &&
          !team.filtering &&
          team.teams.length > 0 &&
          <Grid style={{width:"98%",margin:'auto',marginTop:"5px"}} container spacing={8} >
            {team.teams.map(( value:TeamModel,key:number)=><TeamCard container={container} key={key} team={value}/>)}
          </Grid>
        }
        {
          !team.loading &&
          !team.error &&
          !team.filtering &&
          !(team.teams.length > 0) &&
          <EmptyContent message ="No Team on this section!"/>
        }
      </div>
    )
  }
}

function mapStateToProps(state: any) {
  return {
    team: state.team,
    filteredTeams : state.filter.filteredTeam
  }
}
function mapDisptchToProps(dispatch:any){
  return {
    filteringTeams : ()=>dispatch(filteringTeams()),
    cancelTeamFiltering : ()=>dispatch(cancelTeamFiltering()),
    filterTeam : (name:string,teams : TeamModel[])=>dispatch(filterTeam(name,teams))
  }
}
export default connect(mapStateToProps,mapDisptchToProps)(TeamContainer)
