import React, {Component} from 'react'
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import AppBar from '@material-ui/core/AppBar';
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import Typography from '@material-ui/core/Typography';
import { getTeamById } from '../../../_setup/actions/teamActions';
import { connect } from 'react-redux';
import { TeamState } from '../../../_infrastructure/state/teamState';
import EmptyContent from '../components/empty/empty-list-container';
import LoadingPage from '../components/loading/loading-page';
import ErrorPage from '../components/error/error-page';
import { IUserInformation } from './../../../_infrastructure/model/userInformationModel';
import { getTasksByTeamId } from '../../../_setup/actions/taskActions';
import { TaskState } from '../../../_setup/reducer/taskReducer';
import { Link } from 'react-router-dom';
import ListItem from '@material-ui/core/ListItem';
import { TaskResponse } from '../../../_infrastructure/model/task-model';
import ListItemText from '@material-ui/core/ListItemText';
import List from '@material-ui/core/List';
import Avatar from '@material-ui/core/Avatar';
import WorkIcon from '@material-ui/icons/Work';
import { ListItemIcon } from '@material-ui/core';

export enum TaskType {
    Scheduled = 1,
    Unscheduled = 2,
    Periodic = 3,
    Incident = 4
}

interface IProps{
}

interface PropsFromRoute{
    match : any
}
interface PropsFromState{
    team: TeamState,
    task : TaskState
}
interface PropsFromDispatch{
    getTeamById : (teamID:string) =>void,
    getTasksByTeamId :(teamId:string)=>void
}

type AllProps = IProps &
                PropsFromDispatch &
                PropsFromRoute &
                PropsFromState

class TeamDetailPage extends Component<AllProps> {
    constructor(props:AllProps){
        super(props)
        this.teamDetailINfo = this.teamDetailINfo.bind(this)
        this.taskItem = this.taskItem.bind(this)
    }

    componentDidMount(){
        let {teamId} = this.props.match.params
        this.props.getTeamById(teamId)  
        this.props.getTasksByTeamId(teamId)      
    }

    taskItem(value: TaskResponse , key: number){
        return (
            <Link
            to={`/view-task/${value.id}`}
            key={key}
            style={{textDecoration: 'none'}}
            >
            <ListItem>
                <ListItemIcon>
                    <Avatar>
                    <WorkIcon />
                    </Avatar>
                </ListItemIcon>
                <ListItemText primary={value.name} secondary={value.state} />
            </ListItem>
           </Link>
        )
    }
    teamDetailINfo(){
        let team = this.props.team.teams[0]
        let {tasks} = this.props.task
        let scheduled = tasks.filter(t=>t.taskType == TaskType.Scheduled)
        let unscheduled = tasks.filter(t=>t.taskType == TaskType.Unscheduled)
        let periodic = tasks.filter(t=>t.taskType == TaskType.Periodic)
        let incident = tasks.filter(t=>t.taskType == TaskType.Incident)
        return (
            <Grid  style={{maxWidth:"98%",margin:"auto",marginTop:"10px"}} container spacing={8} justify="center">
                <Grid item md={6}>
                    <Paper style={{height :"45vh",padding:"15px"}}>
                        <Typography variant="h4">
                            {team.name} 
                        </Typography>
                        <Typography variant="subtitle1">
                            Description 
                        </Typography>  
                        <Typography variant="body1">
                            {team.description}
                        </Typography>            
                    </Paper>
                </Grid>
                <Grid item md={6}>
                    <Paper style={{height :"45vh",maxHeight :"45vh",overflowY:"auto",overflowX:"hidden",padding:"15px"}}>
                        <Typography variant="h4">
                            Members 
                        </Typography> 
                        {
                            team.members.map((value:IUserInformation,key:number)=>
                            <Typography key={key} variant="subtitle1">
                                {value.userName} 
                            </Typography> )
                        }
                    </Paper>
                </Grid>
                <Grid item md={3}>
                    <Paper  style={{height :"50vh",padding:"15px"}}>
                        <Typography style={{textDecoration:"underline"}} variant="h6">
                                Unscheduled Tasks 
                        </Typography>
                        <List>
                            {unscheduled.map(this.taskItem)}
                        </List> 
                    </Paper>
                </Grid>
                <Grid item md={3}>
                    <Paper style={{height :"50vh",padding:"15px"}}>
                        <Typography style={{textDecoration:"underline"}} variant="h6">
                                Scheduled Tasks 
                        </Typography> 
                        <List>
                            {scheduled.map(this.taskItem)}
                        </List>
                    </Paper>
                </Grid>
                <Grid item md={3}>
                    <Paper style={{height :"50vh",padding:"15px"}}>
                        <Typography style={{textDecoration:"underline"}} variant="h6">
                                Periodic Tasks 
                        </Typography> 
                        <List>
                            {periodic.map(this.taskItem)}
                        </List>
                    </Paper>
                </Grid>
                <Grid item md={3}>
                    <Paper style={{height :"50vh",padding:"15px"}}>
                        <Typography style={{textDecoration:"underline"}} variant="h6">
                                Incident Tasks 
                        </Typography>
                        <List>
                            {incident.map(this.taskItem)}
                        </List> 
                    </Paper>
                </Grid>
            </Grid>   
        )
    }

    render(){
        let {team} =this.props
        return(
            <div>
                {
                    team.loading &&
                    <LoadingPage/>
                }
                {
                    !team.loading &&
                    team.error &&
                    <ErrorPage message={team.errorMessage}/>  
                }
                {
                    !team.loading &&
                    !team.error &&
                    !team.filtering &&
                    team.teams.length > 0 &&
                    this.teamDetailINfo()
                }
                {
                    !team.loading &&
                    !team.error &&
                    !team.filtering &&
                    !(team.teams.length > 0) &&
                    <EmptyContent message ="Team Not Found"/>
                }
            </div>
        )
    }
}

function mapStateToProps(state:any){
    return {
        team: state.team,
        task : state.tasks
    }
}

export default connect(mapStateToProps,{getTeamById,getTasksByTeamId})(TeamDetailPage)