import React, { Component } from 'react'
import Modal from '@material-ui/core/Modal/Modal'
import { connect } from 'react-redux'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import Checkbox from '@material-ui/core/Checkbox'
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction'
import { TeamModel } from '../../../_infrastructure/model/teamModel'
import { IUserInformation } from '../../../_infrastructure/model/userInformationModel'
import { createTeam } from '../../../_setup/actions/teamActions'
import UIContainer from '../_helper/ui-helper/containers-util'
import TeamFormContainer from './../../officer/team-form-container'

interface IState {
  team: TeamModel
}

interface IProps {
  team: TeamModel
  handleClose: () => void
  openForm: boolean
  container : UIContainer
}

interface PropsFromDispatch {
  createTeam :(team:TeamModel,container:UIContainer)=>void
}

interface PropsFromState {
  employees: IUserInformation[]
}

type AllProps = IProps & PropsFromDispatch & PropsFromState

class TeamFormModal extends Component<AllProps, IState> {
  constructor(props: any) {
    super(props)
    this.submitTeam = this.submitTeam.bind(this)
    this.employeeListComponent = this.employeeListComponent.bind(this)
    this.selectHandler = this.selectHandler.bind(this)
    let { team } = this.props
    this.state = {
      team
    }
  }

  selectHandler=(employee:IUserInformation)=>(event: any)=> {
    if (event.target.checked) this.addTeamMember(employee)
    else this.removeTeamMember(employee)
  }

  addTeamMember(employee: IUserInformation) {
    let { team } = this.state
    team.members.push(employee)
    this.setState({
      team
    })
  }

  removeTeamMember(employee: IUserInformation) {
    let { team } = this.state
    team.members = team.members.filter(emp => emp.id != employee.id)
    this.setState({
      team
    })
  }

  employeeListComponent(value: IUserInformation, key: number): JSX.Element {
    let isMember = this.state.team.members.find(m => m.id == value.id) != undefined
    return (
      <ListItem button key={key}>
        <ListItemText primary={value.userName} />
        <ListItemSecondaryAction>
          <Checkbox
            color="primary"
            id={value.id == null ? '' : value.id}
            checked={isMember}
            onChange={this.selectHandler(value)}
          />
        </ListItemSecondaryAction>
      </ListItem>
    )
  }

  submitTeam(e: any) {
    e.preventDefault()
    this.props.createTeam(this.state.team,this.props.container)
    this.props.handleClose()
  }

  handleChange = (name: string) => (event: any) => {
    let { team } = this.state
    team[name] = event.target.value
    this.setState({
      team
    })
  }

  render() {
    //let { team } = this.state
    let {team,container} = this.props
    return (
      <React.Fragment>
        <Modal
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
          open={this.props.openForm}
          onClose={this.props.handleClose}>
             <TeamFormContainer  team={team} container={container}/> 
        </Modal>
      </React.Fragment>
    )
  }
}
function mapStateToProps(state: any) {
  return {
    employees: state.employee.employees
  }
}

export default connect(
  mapStateToProps,
  { createTeam }
)(TeamFormModal)
