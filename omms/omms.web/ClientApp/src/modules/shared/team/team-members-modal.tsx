import React, { Component } from 'react'
import Button from '@material-ui/core/Button'
import Paper from '@material-ui/core/Paper'
import Typography from '@material-ui/core/Typography'
import Modal from '@material-ui/core/Modal/Modal'
import { connect } from 'react-redux'
import Grid from '@material-ui/core/Grid'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import List from '@material-ui/core/List'
import Checkbox from '@material-ui/core/Checkbox'
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction'
import { TeamModel } from '../../../_infrastructure/model/teamModel'
import { IUserInformation } from '../../../_infrastructure/model/userInformationModel'



interface IState {
  team: TeamModel
  btnText: string
}

interface IProps {
  team: TeamModel
  handleClose: () => void
  openForm: boolean
}

class TeamMembersModal extends Component<IProps, IState> {
  constructor(props: any) {
    super(props)
    this.submitTeam = this.submitTeam.bind(this)
    this.employeeListComponent = this.employeeListComponent.bind(this)
    let { team } = this.props

    this.state = {
      team: team,
      btnText:'submit'
    }
  }


  employeeListComponent(value: IUserInformation, key: number): JSX.Element {
    return (
      <ListItem button key={key}>
        <ListItemText primary={value.userName} />
      </ListItem>
    )
  }

  submitTeam(e: any) {
    e.preventDefault()
    
    //replace this with request regustration
    this.props.handleClose()
  }

  handleChange = (name: string) => (event: any) => {
    let { team } = this.state
    team[name] = event.target.value
    this.setState({
      team
    })
  }

  render() {
    return (
        <Modal
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
          open={this.props.openForm}
          onClose={this.props.handleClose}
        >
          <Paper style={{
              width:"60%",
              margin:"auto",
              marginTop:"10vh",
              maxHeight:"70vh",
              overflowY:"auto",
              padding:"20px"
          }}>
            <Grid container justify="center">
            <Grid item md={5}>
                <Typography component="h4" variant="h5">
                  Team
                </Typography>
              </Grid>
              <Grid item md={5}>
                <Typography component="h4" variant="h5">
                  Members
                </Typography>
              </Grid>
              <Grid item md={5}>
                <Typography variant="h5">
                    {this.props.team.name}
                </Typography>
                <Typography variant="body2">
                {this.props.team.description}
                </Typography>
              </Grid>
              <Grid item md={5}>
                <List dense>
                  {this.props.team.members.map(this.employeeListComponent)}
                </List>
              </Grid>
              
                <Grid
                  item 
                  md={10}>
                   <Button
                    type="submit"
                    variant="contained"
                    color="primary"
                    onClick={this.props.handleClose}
                    >
                    close
                  </Button>
                </Grid>
            
            </Grid>
          </Paper>
        </Modal>
    )
  }
}


function mapStateToProps(){
    return {

    }
}
export default connect(mapStateToProps,{

})(TeamMembersModal)
